﻿Imports System.ComponentModel

''' <summary>
''' 出力設定の表示用クラス
''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
''' </summary>
''' <remarks></remarks>
<TypeConverter(GetType(CustomPropertyConverter))> _
Public Class SettingDisplayWrapper
    ''' <summary>
    ''' Boolean型の属性値がTrueの時の表示
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TRUE_STRING As String = "ON"

    ''' <summary>
    ''' Boolean型の属性値がFALSEの時の表示
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const FALSE_STRING As String = "OFF"

    ''' <summary>
    ''' 備考欄の表示（設定値と配列の添字をそろえる）
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared ROW_COUNT_STRING() As String = New String() {"なし", "1列", "2列"}

    ''' <summary>
    ''' 用紙サイズの表示（設定値と配列の添字をそろえる）
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared OUTPUT_SIZE_STRING() As String = New String() {"A4", "A3"}

    ''' <summary>
    ''' 表示カテゴリ
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Const CATEGORY_SETTING As String = ControlChars.Tab & ControlChars.Tab & ControlChars.Tab & "設定"

    ''' <summary>
    ''' 表示カテゴリ
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Const CATEGORY_OUTPUT As String = ControlChars.Tab & ControlChars.Tab & "比較表上に表示"

    ''' <summary>
    ''' 表示カテゴリ
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Const CATEGORY_NEWCOLUMN As String = ControlChars.Tab & "新列の表示"

    ''' <summary>
    ''' 表示カテゴリ
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Const CATEGORY_OLDCOLUMN As String = "旧列の表示"

    ''' <summary>
    ''' 差分のみ表示
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_sabun_only As String

    ''' <summary>
    ''' （追加）/(削除）表示
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_add_and_delete As String

    ''' <summary>
    ''' 新旧を逆に
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_reverse As String

    ''' <summary>
    ''' 備考欄
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_remark As String

    ''' <summary>
    ''' 用紙サイズ
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_size As String

    ''' <summary>
    ''' 更新日自動チェック
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_autoUpdatedDateCheck As String

    ''' <summary>
    ''' 新列の表示色
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_newColColor As Color

    ''' <summary>
    ''' 新列の下線表示
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_newUnderline As String

    ''' <summary>
    ''' 旧列の表示色
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_oldColColor As Color

    ''' <summary>
    ''' 旧列の下線表示
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_oldUnderline As String

    ''' <summary>
    ''' 旧列の取消線表示
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_oldStrike As String

    ''' <summary>
    ''' フォルダ出力
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_outputFolder As String

    ''' <summary>
    ''' ファイル出力
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_outputFile As String

    ''' <summary>
    ''' 更新日付出力
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_outputUpdatedDate As String

    ''' <summary>
    ''' コンストラクタ
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="value">出力設定情報</param>
    ''' <remarks></remarks>
    Public Sub New(ByRef value As Setting)
        m_sabun_only = getBooleanString(value.sabun_only)
        m_add_and_delete = getBooleanString(value.add_and_delete)
        m_reverse = getBooleanString(value.reverse)
        m_remark = ROW_COUNT_STRING(value.remark)
        m_size = OUTPUT_SIZE_STRING(value.size)
        m_newColColor = value.newColColor
        m_newUnderline = getBooleanString(value.newUnderline)
        m_oldColColor = value.oldColColor
        m_oldUnderline = getBooleanString(value.oldUnderline)
        m_oldStrike = getBooleanString(value.oldColStrike)
        m_outputFolder = getBooleanString(value.outputFolder)
        m_outputFile = getBooleanString(value.outputFile)
        m_outputUpdatedDate = getBooleanString(value.outputUpdatedDate)
        m_autoUpdatedDateCheck = getBooleanString(value.autoCheckUpdatedDate)
    End Sub

    ''' <summary>
    ''' Booleanの設定項目の表示値を返します。
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="value">Boolean</param>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Private Function getBooleanString(ByVal value As Boolean) As String
        Dim ret As String = FALSE_STRING
        If value Then
            ret = TRUE_STRING
        End If
        Return ret
    End Function

    ''' <summary>
    ''' 差分のみかどうか
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    <Category(CATEGORY_SETTING)> _
    <PropertyDisplayName("差分のみ表示")> _
    Public ReadOnly Property sabun_only As String
        Get
            Return m_sabun_only
        End Get
    End Property

    ''' <summary>
    ''' (追加)/(削除)を表示するかどうか
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    <Category(CATEGORY_SETTING)> _
    <PropertyDisplayName("(追加)/(削除)を表示")> _
    Public ReadOnly Property add_and_delete As String
        Get
            Return m_add_and_delete
        End Get
    End Property

    ''' <summary>
    ''' 新旧を逆表示するかどうか
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    <Category(CATEGORY_SETTING)> _
    <PropertyDisplayName("新旧を逆にする")> _
    Public ReadOnly Property reverse As String
        Get
            Return m_reverse
        End Get
    End Property

    ''' <summary>
    ''' 備考欄の数
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    <Category(CATEGORY_SETTING)> _
    <PropertyDisplayName("備考欄の表示")> _
    Public ReadOnly Property remark As String
        Get
            Return m_remark
        End Get
    End Property

    ''' <summary>
    ''' 出力サイズ
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    <Category(CATEGORY_SETTING)> _
    <PropertyDisplayName("出力サイズ")> _
    Public ReadOnly Property size As String
        Get
            Return m_size
        End Get
    End Property

    ''' <summary>
    ''' 新旧の自動チェック
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    <Category(CATEGORY_SETTING)> _
    <PropertyDisplayName("新旧ファイルを自動で判断")> _
    Public ReadOnly Property autoUpdatedDateCheck As String
        Get
            Return m_autoUpdatedDateCheck
        End Get
    End Property

    ''' <summary>
    ''' 追加した箇所の色
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>Color</returns>
    ''' <remarks></remarks>
    <Category(CATEGORY_NEWCOLUMN)> _
    <PropertyDisplayName("変更箇所の色")> _
    Public ReadOnly Property newColColor As Color
        Get
            Return m_newColColor
        End Get
    End Property

    ''' <summary>
    ''' 追加した箇所の下線
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    <Category(CATEGORY_NEWCOLUMN)> _
    <PropertyDisplayName("変更箇所に下線")> _
    Public ReadOnly Property newUnderline As String
        Get
            Return m_newUnderline
        End Get
    End Property

    ''' <summary>
    ''' 削除した箇所の色
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>Color</returns>
    ''' <remarks></remarks>
    <Category(CATEGORY_OLDCOLUMN)> _
    <PropertyDisplayName("変更箇所の色")> _
    Public ReadOnly Property oldColColor As Color
        Get
            Return m_oldColColor
        End Get
    End Property

    ''' <summary>
    ''' 削除した箇所の下線
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    <Category(CATEGORY_OLDCOLUMN)> _
    <PropertyDisplayName("変更箇所に下線")> _
    Public ReadOnly Property oldUnderline As String
        Get
            Return m_oldUnderline
        End Get
    End Property

    ''' <summary>
    ''' 削除した箇所の下線
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    <Category(CATEGORY_OLDCOLUMN)> _
    <PropertyDisplayName("削除箇所に取消線")> _
    Public ReadOnly Property oldStrike As String
        Get
            Return m_oldStrike
        End Get
    End Property

    ''' <summary>
    ''' フォルダ名出力
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    <Category(CATEGORY_OUTPUT)> _
    <PropertyDisplayName("フォルダ名")> _
    Public ReadOnly Property outputFolder As String
        Get
            Return m_outputFolder
        End Get
    End Property

    ''' <summary>
    ''' ファイル名出力
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    <Category(CATEGORY_OUTPUT)> _
    <PropertyDisplayName("ファイル名")> _
    Public ReadOnly Property outputFile As String
        Get
            Return m_outputFile
        End Get
    End Property

    ''' <summary>
    ''' 更新日時出力
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    <Category(CATEGORY_OUTPUT)> _
    <PropertyDisplayName("ファイル更新日時")> _
    Public ReadOnly Property outputUpdatedDate As String
        Get
            Return m_outputUpdatedDate
        End Get
    End Property


End Class

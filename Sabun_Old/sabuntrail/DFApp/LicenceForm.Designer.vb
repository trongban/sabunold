﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LicenceForm
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CloseBtn = New System.Windows.Forms.PictureBox()
        Me.HidenBtn = New System.Windows.Forms.PictureBox()
        Me.HelpBtn = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.TrialLabel = New System.Windows.Forms.Label()
        Me.AuthenticateLicence = New System.Windows.Forms.PictureBox()
        Me.LicenceBG4 = New System.Windows.Forms.PictureBox()
        Me.LicenceBG3 = New System.Windows.Forms.PictureBox()
        Me.LicenceBG2 = New System.Windows.Forms.PictureBox()
        Me.LicenceBG1 = New System.Windows.Forms.PictureBox()
        Me.LicenceTextBox4 = New sabuntrail.TextBoxEx()
        Me.LicenceTextBox3 = New sabuntrail.TextBoxEx()
        Me.LicenceTextBox2 = New sabuntrail.TextBoxEx()
        Me.LicenceTextBox1 = New sabuntrail.TextBoxEx()
        Me.Panel1.SuspendLayout()
        CType(Me.CloseBtn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HidenBtn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HelpBtn, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.AuthenticateLicence, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LicenceBG4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LicenceBG3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LicenceBG2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LicenceBG1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sabuntrail.My.Resources.Resources.licence_title
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel1.Controls.Add(Me.CloseBtn)
        Me.Panel1.Controls.Add(Me.HidenBtn)
        Me.Panel1.Controls.Add(Me.HelpBtn)
        Me.Panel1.Location = New System.Drawing.Point(30, 28)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(742, 63)
        Me.Panel1.TabIndex = 0
        '
        'CloseBtn
        '
        Me.CloseBtn.BackColor = System.Drawing.Color.Transparent
        Me.CloseBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CloseBtn.Image = Global.sabuntrail.My.Resources.Resources.licence_exit_btn
        Me.CloseBtn.Location = New System.Drawing.Point(665, 14)
        Me.CloseBtn.Name = "CloseBtn"
        Me.CloseBtn.Size = New System.Drawing.Size(41, 40)
        Me.CloseBtn.TabIndex = 2
        Me.CloseBtn.TabStop = False
        '
        'HidenBtn
        '
        Me.HidenBtn.BackColor = System.Drawing.Color.Transparent
        Me.HidenBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.HidenBtn.Image = Global.sabuntrail.My.Resources.Resources.licence_small_btn
        Me.HidenBtn.Location = New System.Drawing.Point(618, 14)
        Me.HidenBtn.Name = "HidenBtn"
        Me.HidenBtn.Size = New System.Drawing.Size(41, 40)
        Me.HidenBtn.TabIndex = 1
        Me.HidenBtn.TabStop = False
        '
        'HelpBtn
        '
        Me.HelpBtn.BackColor = System.Drawing.Color.Transparent
        Me.HelpBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.HelpBtn.Image = Global.sabuntrail.My.Resources.Resources.licence_help_btn
        Me.HelpBtn.Location = New System.Drawing.Point(571, 14)
        Me.HelpBtn.Name = "HelpBtn"
        Me.HelpBtn.Size = New System.Drawing.Size(41, 40)
        Me.HelpBtn.TabIndex = 0
        Me.HelpBtn.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.BackgroundImage = Global.sabuntrail.My.Resources.Resources.licence_bg_2
        Me.Panel2.Controls.Add(Me.LicenceTextBox4)
        Me.Panel2.Controls.Add(Me.LicenceTextBox3)
        Me.Panel2.Controls.Add(Me.LicenceTextBox2)
        Me.Panel2.Controls.Add(Me.LicenceTextBox1)
        Me.Panel2.Controls.Add(Me.TrialLabel)
        Me.Panel2.Controls.Add(Me.AuthenticateLicence)
        Me.Panel2.Controls.Add(Me.LicenceBG4)
        Me.Panel2.Controls.Add(Me.LicenceBG3)
        Me.Panel2.Controls.Add(Me.LicenceBG2)
        Me.Panel2.Controls.Add(Me.LicenceBG1)
        Me.Panel2.Location = New System.Drawing.Point(30, 95)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(742, 426)
        Me.Panel2.TabIndex = 1
        '
        'TrialLabel
        '
        Me.TrialLabel.BackColor = System.Drawing.Color.Transparent
        Me.TrialLabel.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TrialLabel.ForeColor = System.Drawing.Color.LightSeaGreen
        Me.TrialLabel.Location = New System.Drawing.Point(215, 28)
        Me.TrialLabel.Name = "TrialLabel"
        Me.TrialLabel.Size = New System.Drawing.Size(43, 28)
        Me.TrialLabel.TabIndex = 11
        Me.TrialLabel.Text = "TrialLabel"
        Me.TrialLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'AuthenticateLicence
        '
        Me.AuthenticateLicence.BackColor = System.Drawing.Color.Transparent
        Me.AuthenticateLicence.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AuthenticateLicence.Image = Global.sabuntrail.My.Resources.Resources.licence_auth_btn
        Me.AuthenticateLicence.Location = New System.Drawing.Point(226, 311)
        Me.AuthenticateLicence.Name = "AuthenticateLicence"
        Me.AuthenticateLicence.Size = New System.Drawing.Size(291, 79)
        Me.AuthenticateLicence.TabIndex = 8
        Me.AuthenticateLicence.TabStop = False
        '
        'LicenceBG4
        '
        Me.LicenceBG4.BackColor = System.Drawing.Color.Transparent
        Me.LicenceBG4.BackgroundImage = Global.sabuntrail.My.Resources.Resources.licence_tf_active
        Me.LicenceBG4.Location = New System.Drawing.Point(544, 169)
        Me.LicenceBG4.Name = "LicenceBG4"
        Me.LicenceBG4.Size = New System.Drawing.Size(162, 71)
        Me.LicenceBG4.TabIndex = 3
        Me.LicenceBG4.TabStop = False
        '
        'LicenceBG3
        '
        Me.LicenceBG3.BackColor = System.Drawing.Color.Transparent
        Me.LicenceBG3.BackgroundImage = Global.sabuntrail.My.Resources.Resources.licence_tf_active
        Me.LicenceBG3.Location = New System.Drawing.Point(376, 169)
        Me.LicenceBG3.Name = "LicenceBG3"
        Me.LicenceBG3.Size = New System.Drawing.Size(162, 71)
        Me.LicenceBG3.TabIndex = 2
        Me.LicenceBG3.TabStop = False
        '
        'LicenceBG2
        '
        Me.LicenceBG2.BackColor = System.Drawing.Color.Transparent
        Me.LicenceBG2.BackgroundImage = Global.sabuntrail.My.Resources.Resources.licence_tf_active
        Me.LicenceBG2.Location = New System.Drawing.Point(208, 169)
        Me.LicenceBG2.Name = "LicenceBG2"
        Me.LicenceBG2.Size = New System.Drawing.Size(162, 71)
        Me.LicenceBG2.TabIndex = 1
        Me.LicenceBG2.TabStop = False
        '
        'LicenceBG1
        '
        Me.LicenceBG1.BackColor = System.Drawing.Color.Transparent
        Me.LicenceBG1.BackgroundImage = Global.sabuntrail.My.Resources.Resources.licence_tf_active
        Me.LicenceBG1.Location = New System.Drawing.Point(40, 169)
        Me.LicenceBG1.Name = "LicenceBG1"
        Me.LicenceBG1.Size = New System.Drawing.Size(162, 71)
        Me.LicenceBG1.TabIndex = 0
        Me.LicenceBG1.TabStop = False
        '
        'LicenceTextBox4
        '
        Me.LicenceTextBox4.BackColor = System.Drawing.SystemColors.Window
        Me.LicenceTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.LicenceTextBox4.Font = New System.Drawing.Font("MS UI Gothic", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LicenceTextBox4.ForeColor = System.Drawing.Color.DarkGray
        Me.LicenceTextBox4.Location = New System.Drawing.Point(585, 190)
        Me.LicenceTextBox4.MaxLength = 5
        Me.LicenceTextBox4.Name = "LicenceTextBox4"
        Me.LicenceTextBox4.Placeholder = "FGHIJ"
        Me.LicenceTextBox4.Size = New System.Drawing.Size(85, 24)
        Me.LicenceTextBox4.TabIndex = 14
        '
        'LicenceTextBox3
        '
        Me.LicenceTextBox3.BackColor = System.Drawing.SystemColors.Window
        Me.LicenceTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.LicenceTextBox3.Font = New System.Drawing.Font("MS UI Gothic", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LicenceTextBox3.ForeColor = System.Drawing.Color.DarkGray
        Me.LicenceTextBox3.Location = New System.Drawing.Point(415, 190)
        Me.LicenceTextBox3.MaxLength = 5
        Me.LicenceTextBox3.Name = "LicenceTextBox3"
        Me.LicenceTextBox3.Placeholder = "67890"
        Me.LicenceTextBox3.Size = New System.Drawing.Size(85, 24)
        Me.LicenceTextBox3.TabIndex = 13
        '
        'LicenceTextBox2
        '
        Me.LicenceTextBox2.BackColor = System.Drawing.SystemColors.Window
        Me.LicenceTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.LicenceTextBox2.Font = New System.Drawing.Font("MS UI Gothic", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LicenceTextBox2.ForeColor = System.Drawing.Color.DarkGray
        Me.LicenceTextBox2.Location = New System.Drawing.Point(245, 190)
        Me.LicenceTextBox2.MaxLength = 5
        Me.LicenceTextBox2.Name = "LicenceTextBox2"
        Me.LicenceTextBox2.Placeholder = "ABCDE"
        Me.LicenceTextBox2.Size = New System.Drawing.Size(85, 24)
        Me.LicenceTextBox2.TabIndex = 12
        '
        'LicenceTextBox1
        '
        Me.LicenceTextBox1.BackColor = System.Drawing.SystemColors.Window
        Me.LicenceTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.LicenceTextBox1.Font = New System.Drawing.Font("MS UI Gothic", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LicenceTextBox1.ForeColor = System.Drawing.Color.DarkGray
        Me.LicenceTextBox1.Location = New System.Drawing.Point(75, 190)
        Me.LicenceTextBox1.MaxLength = 5
        Me.LicenceTextBox1.Name = "LicenceTextBox1"
        Me.LicenceTextBox1.Placeholder = "12345"
        Me.LicenceTextBox1.Size = New System.Drawing.Size(85, 24)
        Me.LicenceTextBox1.TabIndex = 4
        '
        'LicenceForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.BackgroundImage = Global.sabuntrail.My.Resources.Resources.licence_bg_1
        Me.ClientSize = New System.Drawing.Size(800, 550)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "LicenceForm"
        Me.Text = "ライセンス認証"
        Me.TransparencyKey = System.Drawing.Color.Snow
        Me.Panel1.ResumeLayout(False)
        CType(Me.CloseBtn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HidenBtn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HelpBtn, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.AuthenticateLicence, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LicenceBG4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LicenceBG3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LicenceBG2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LicenceBG1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents HelpBtn As PictureBox
    Friend WithEvents LicenceBG4 As PictureBox
    Friend WithEvents LicenceBG3 As PictureBox
    Friend WithEvents LicenceBG2 As PictureBox
    Friend WithEvents LicenceBG1 As PictureBox
    Friend WithEvents AuthenticateLicence As PictureBox
    Friend WithEvents CloseBtn As PictureBox
    Friend WithEvents HidenBtn As PictureBox
    Friend WithEvents TrialLabel As Label
    Friend WithEvents LicenceTextBox1 As TextBoxEx
    Friend WithEvents LicenceTextBox4 As TextBoxEx
    Friend WithEvents LicenceTextBox3 As TextBoxEx
    Friend WithEvents LicenceTextBox2 As TextBoxEx
End Class

﻿''' <summary>
''' PropertyGridに表示するプロパティ名称
''' （従来はプロパティ名がそのまま表示される。）
''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
''' </summary>
''' <remarks></remarks>
<AttributeUsage(AttributeTargets.Property)> _
Class PropertyDisplayNameAttribute
    Inherits Attribute

    ''' <summary>
    ''' プロパティ表示名
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private myPropertyDisplayName As String

    ''' <summary>
    ''' コンストラクタ
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="name">表示名</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal name As String)
        myPropertyDisplayName = name
    End Sub

    ''' <summary>
    ''' プロパティ表示名
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>プロパティ表示名</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property PropertyDisplayName() As String
        Get
            Return myPropertyDisplayName
        End Get
    End Property
End Class

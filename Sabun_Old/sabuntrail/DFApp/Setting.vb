﻿Imports System.Xml

''' <summary>
''' 設定情報クラス
''' スレッドセーフではありません。
''' </summary>
''' <remarks></remarks>
Public Class Setting

    ''' <summary>
    ''' 設定情報XML
    ''' </summary>
    ''' <remarks></remarks>
    Private m_xml As New XmlDocument

    ''' <summary>
    ''' 設定情報XMLのパス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_path As String

    ''' <summary>
    ''' 名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CONF_NAMESPACE = "http://www.toppan-f.co.jp/sabuntrail/setting"

    ''' <summary>
    ''' 名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CONF_NAMESPACE_HEADER = "sabun"

    ''' <summary>
    ''' 名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_SABUN_ONLY = "/sabun:root/sabun:option/sabun:sabun_only"

    ''' <summary>
    ''' 名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_ADD_AND_DELETE = "/sabun:root/sabun:option/sabun:add_and_delete"

    ''' <summary>
    ''' 名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_REVERSE = "/sabun:root/sabun:option/sabun:reverse"

    ''' <summary>
    ''' 名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_REMARK = "/sabun:root/sabun:remark"

    ''' <summary>
    ''' 名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_SIZE = "/sabun:root/sabun:size"

    ''' <summary>
    ''' 名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_NEWCOL_COLOR = "/sabun:root/sabun:newcol/sabun:color"

    ''' <summary>
    ''' 名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_NEWCOL_UNDERLINE = "/sabun:root/sabun:newcol/sabun:underline"

    ''' <summary>
    ''' 名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_OLDCOL_COLOR = "/sabun:root/sabun:oldcol/sabun:color"

    ''' <summary>
    ''' 名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_OLDCOL_UNDERLINE = "/sabun:root/sabun:oldcol/sabun:underline"

    ''' <summary>
    ''' 名前空間
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_OLDCOL_STRIKE = "/sabun:root/sabun:oldcol/sabun:strike"

    ''' <summary>
    ''' 名前空間
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_AUTO_UPDATEDDATE_CHECK = "/sabun:root/sabun:autocheck/sabun:updateddate_check"

    ''' <summary>
    ''' 名前空間
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_IGNORE_UPDATEDDATE_WARNING = "/sabun:root/sabun:autocheck/sabun:ignore_updateddate_warning"

    ''' <summary>
    ''' 名前空間
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_OUTPUT_FOLDER = "/sabun:root/sabun:output/sabun:folder"

    ''' <summary>
    ''' 名前空間
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_OUTPUT_FILE = "/sabun:root/sabun:output/sabun:file"

    ''' <summary>
    ''' 名前空間
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_OUTPUT_UPDATEDDATE = "/sabun:root/sabun:output/sabun:updateddate"

    ''' <summary>
    ''' 名前空間
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_OUTPUT_UPDATEDDATE_FORMAT = "/sabun:root/sabun:output/sabun:updateddate_format"

    ''' <summary>
    ''' 名前空間
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_VERSION = "/sabun:root/sabun:version"

    ''' <summary>
    ''' コンストラクタ
    ''' </summary>
    ''' <param name="path">設定情報ファイルのパス</param>
    ''' <remarks></remarks>
    Public Sub New(path As String)

        m_path = path

        m_xml.Load(m_path)

    End Sub

    ''' <summary>
    ''' ノードの情報を取得する。
    ''' </summary>
    ''' <param name="xpath">XPATH</param>
    ''' <returns>ノード</returns>
    ''' <remarks></remarks>
    Private Function GetNode(ByRef xpath As String) As XmlNode
        ' 名前空間テーブル
        Dim nTbl As New NameTable
        Dim nsManager As New XmlNamespaceManager(nTbl)
        nsManager.AddNamespace(CONF_NAMESPACE_HEADER, CONF_NAMESPACE)

        Dim xmlNode As System.Xml.XmlNode = m_xml.SelectSingleNode(xpath, nsManager)

        Return xmlNode
    End Function

    ''' <summary>
    ''' ノードの値を取得します。
    ''' ノードのない場合は空文字を返します。
    ''' </summary>
    ''' <param name="xpath">XPATH</param>
    ''' <returns>文字列</returns>
    ''' <remarks></remarks>
    Private Function GetValue(xpath As String) As String
        Dim value As String = ""

        Dim xmlNode = GetNode(xpath)

        If xmlNode IsNot Nothing Then
            value = xmlNode.InnerText

            ' 値がない場合は空文字を入れます。
            If value Is Nothing OrElse value.Trim.Length = 0 Then
                value = ""
            End If
        Else
            ' 未定義エラー
            value = ""
        End If

        Return value
    End Function

    ''' <summary>
    ''' 差分のみかどうか
    ''' True・・・差分のみ/False・・・差分以外も表示
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Property sabun_only As Boolean
        Get
            Return Boolean.Parse(GetValue(XPATH_SABUN_ONLY))
        End Get
        Set(value As Boolean)
            Dim xmlNode = GetNode(XPATH_SABUN_ONLY)
            xmlNode.InnerText = value.ToString
            ' 2015/03/02 DELETE START [sabuntrail Version 1.1対応] Kitajima
            'm_xml.Save(m_path)
            ' 2015/03/02 DELETE END [sabuntrail Version 1.1対応] Kitajima
        End Set
    End Property

    ''' <summary>
    ''' (追加)/(削除)を表示するかどうか
    ''' True・・・表示する/False・・・表示しない
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Property add_and_delete As Boolean
        Get
            Return Boolean.Parse(GetValue(XPATH_ADD_AND_DELETE))
        End Get
        Set(value As Boolean)
            Dim xmlNode = GetNode(XPATH_ADD_AND_DELETE)
            xmlNode.InnerText = value.ToString
            ' 2015/03/02 DELETE START [sabuntrail Version 1.1対応] Kitajima
            'm_xml.Save(m_path)
            ' 2015/03/02 DELETE END [sabuntrail Version 1.1対応] Kitajima
        End Set
    End Property

    ''' <summary>
    ''' 新旧を逆表示するかどうか
    ''' True・・・逆表示する/False・・・逆表示しない
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Property reverse As Boolean
        Get
            Return Boolean.Parse(GetValue(XPATH_REVERSE))
        End Get
        Set(value As Boolean)
            Dim xmlNode = GetNode(XPATH_REVERSE)
            xmlNode.InnerText = value.ToString
            ' 2015/03/02 DELETE START [sabuntrail Version 1.1対応] Kitajima
            'm_xml.Save(m_path)
            ' 2015/03/02 DELETE END [sabuntrail Version 1.1対応] Kitajima
        End Set
    End Property

    ''' <summary>
    ''' 備考欄の数(0～2)
    ''' </summary>
    ''' <value>Integer</value>
    ''' <returns>Integer</returns>
    ''' <remarks></remarks>
    Public Property remark As Integer
        Get
            Return Integer.Parse(GetValue(XPATH_REMARK))
        End Get
        Set(value As Integer)
            Dim xmlNode = GetNode(XPATH_REMARK)
            xmlNode.InnerText = value.ToString
            ' 2015/03/02 DELETE START [sabuntrail Version 1.1対応] Kitajima
            'm_xml.Save(m_path)
            ' 2015/03/02 DELETE END [sabuntrail Version 1.1対応] Kitajima
        End Set
    End Property

    ''' <summary>
    ''' ページサイズ
    ''' A4=0/A5=1
    ''' </summary>
    ''' <value>Integer</value>
    ''' <returns>Integer</returns>
    ''' <remarks></remarks>
    Public Property size As Integer
        Get
            Return Integer.Parse(GetValue(XPATH_SIZE))
        End Get
        Set(value As Integer)
            Dim xmlNode = GetNode(XPATH_SIZE)
            xmlNode.InnerText = value.ToString
            ' 2015/03/02 DELETE START [sabuntrail Version 1.1対応] Kitajima
            'm_xml.Save(m_path)
            ' 2015/03/02 DELETE END [sabuntrail Version 1.1対応] Kitajima
        End Set
    End Property

    ''' <summary>
    ''' 追加した箇所の色
    ''' </summary>
    ''' <value>Color</value>
    ''' <returns>Color</returns>
    ''' <remarks></remarks>
    Public Property newColColor As Color
        Get
            Return Color.FromArgb(Integer.Parse(GetValue(XPATH_NEWCOL_COLOR)))
        End Get
        Set(value As Color)
            Dim xmlNode = GetNode(XPATH_NEWCOL_COLOR)
            xmlNode.InnerText = value.ToArgb
            ' 2015/03/02 DELETE START [sabuntrail Version 1.1対応] Kitajima
            'm_xml.Save(m_path)
            ' 2015/03/02 DELETE END [sabuntrail Version 1.1対応] Kitajima
        End Set
    End Property

    ''' <summary>
    ''' 追加した箇所の下線
    ''' True・・・表示する/False・・・表示しない
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Property newUnderline As Boolean
        Get
            Return Boolean.Parse(GetValue(XPATH_NEWCOL_UNDERLINE))
        End Get
        Set(value As Boolean)
            Dim xmlNode = GetNode(XPATH_NEWCOL_UNDERLINE)
            xmlNode.InnerText = value.ToString
            ' 2015/03/02 DELETE START [sabuntrail Version 1.1対応] Kitajima
            'm_xml.Save(m_path)
            ' 2015/03/02 DELETE END [sabuntrail Version 1.1対応] Kitajima
        End Set
    End Property

    ''' <summary>
    ''' 削除した箇所の色
    ''' </summary>
    ''' <value>Color</value>
    ''' <returns>Color</returns>
    ''' <remarks></remarks>
    Public Property oldColColor As Color
        Get
            Return Color.FromArgb(Integer.Parse(GetValue(XPATH_OLDCOL_COLOR)))
        End Get
        Set(value As Color)
            Dim xmlNode = GetNode(XPATH_OLDCOL_COLOR)
            xmlNode.InnerText = value.ToArgb
            ' 2015/03/02 DELETE START [sabuntrail Version 1.1対応] Kitajima
            'm_xml.Save(m_path)
            ' 2015/03/02 DELETE END [sabuntrail Version 1.1対応] Kitajima
        End Set
    End Property

    ''' <summary>
    ''' 削除した箇所の下線
    ''' True・・・表示する/False・・・表示しない
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Property oldUnderline As Boolean
        Get
            Return Boolean.Parse(GetValue(XPATH_OLDCOL_UNDERLINE))
        End Get
        Set(value As Boolean)
            Dim xmlNode = GetNode(XPATH_OLDCOL_UNDERLINE)
            xmlNode.InnerText = value.ToString
            ' 2015/03/02 DELETE START [sabuntrail Version 1.1対応] Kitajima
            'm_xml.Save(m_path)
            ' 2015/03/02 DELETE END [sabuntrail Version 1.1対応] Kitajima
        End Set
    End Property

    ''' <summary>
    ''' 削除した箇所の取消線
    ''' True・・・表示する/False・・・表示しない
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Property oldColStrike As Boolean
        Get
            Return Boolean.Parse(GetValue(XPATH_OLDCOL_STRIKE))
        End Get
        Set(value As Boolean)
            Dim xmlNode = GetNode(XPATH_OLDCOL_STRIKE)
            xmlNode.InnerText = value.ToString
        End Set
    End Property

    ''' <summary>
    ''' 更新日付の自動チェック
    ''' True・・・チェックする/False・・・チェックしない
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Property autoCheckUpdatedDate As Boolean
        Get
            Return Boolean.Parse(GetValue(XPATH_AUTO_UPDATEDDATE_CHECK))
        End Get
        Set(value As Boolean)
            Dim xmlNode = GetNode(XPATH_AUTO_UPDATEDDATE_CHECK)
            xmlNode.InnerText = value.ToString
        End Set
    End Property

    ''' <summary>
    ''' 更新日付の自動チェック警告無視
    ''' True・・・無視する/False・・・無視しない
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Property autoCheckIgnoreUpdatedDateWarning As Boolean
        Get
            Return Boolean.Parse(GetValue(XPATH_IGNORE_UPDATEDDATE_WARNING))
        End Get
        Set(value As Boolean)
            Dim xmlNode = GetNode(XPATH_IGNORE_UPDATEDDATE_WARNING)
            xmlNode.InnerText = value.ToString
        End Set
    End Property

    ''' <summary>
    ''' フォルダ名を出力するかどうか
    ''' True・・・出力する/False・・・出力しない
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Property outputFolder As Boolean
        Get
            Return Boolean.Parse(GetValue(XPATH_OUTPUT_FOLDER))
        End Get
        Set(value As Boolean)
            Dim xmlNode = GetNode(XPATH_OUTPUT_FOLDER)
            xmlNode.InnerText = value.ToString
        End Set
    End Property

    ''' <summary>
    ''' ファイル名を出力するかどうか
    ''' True・・・出力する/False・・・出力しない
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Property outputFile As Boolean
        Get
            Return Boolean.Parse(GetValue(XPATH_OUTPUT_FILE))
        End Get
        Set(value As Boolean)
            Dim xmlNode = GetNode(XPATH_OUTPUT_FILE)
            xmlNode.InnerText = value.ToString
        End Set
    End Property

    ''' <summary>
    ''' 更新日を出力するかどうか
    ''' True・・・出力する/False・・・出力しない
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Property outputUpdatedDate As Boolean
        Get
            Return Boolean.Parse(GetValue(XPATH_OUTPUT_UPDATEDDATE))
        End Get
        Set(value As Boolean)
            Dim xmlNode = GetNode(XPATH_OUTPUT_UPDATEDDATE)
            xmlNode.InnerText = value.ToString
        End Set
    End Property

    ''' <summary>
    ''' 更新日の出力フォーマット
    ''' フォーマットが正しいかどうかは使用する先でチェックしてください。
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property outputUpdatedDateFormat As String
        Get
            Return (GetValue(XPATH_OUTPUT_UPDATEDDATE_FORMAT))
        End Get
    End Property

    ''' <summary>
    ''' 出力設定情報を保存する。
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub save()
        m_xml.Save(m_path)
    End Sub

    ''' <summary>
    ''' バージョン情報
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Version As String
        Get
            Return (GetValue(XPATH_VERSION))
        End Get
    End Property

End Class

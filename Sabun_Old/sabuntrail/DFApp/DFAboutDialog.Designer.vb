﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DFAboutDialog
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.logoPictureBox = New System.Windows.Forms.PictureBox()
        Me.ProductLabel = New System.Windows.Forms.Label()
        Me.VersionLabel = New System.Windows.Forms.Label()
        Me.CopyLightLabel = New System.Windows.Forms.Label()
        Me.TrialLabel = New System.Windows.Forms.Label()
        Me.LicTextBox = New System.Windows.Forms.TextBox()
        Me.LicLabel = New System.Windows.Forms.Label()
        Me.LicButton = New System.Windows.Forms.Button()
        CType(Me.logoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(327, 93)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(142, 21)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'logoPictureBox
        '
        Me.logoPictureBox.Image = Global.sabuntrail.My.Resources.Resources.icon
        Me.logoPictureBox.Location = New System.Drawing.Point(420, 16)
        Me.logoPictureBox.Name = "logoPictureBox"
        Me.logoPictureBox.Size = New System.Drawing.Size(49, 38)
        Me.logoPictureBox.TabIndex = 2
        Me.logoPictureBox.TabStop = False
        '
        'ProductLabel
        '
        Me.ProductLabel.AutoSize = True
        Me.ProductLabel.Font = New System.Drawing.Font("MS UI Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.ProductLabel.Location = New System.Drawing.Point(12, 9)
        Me.ProductLabel.Name = "ProductLabel"
        Me.ProductLabel.Size = New System.Drawing.Size(103, 19)
        Me.ProductLabel.TabIndex = 3
        Me.ProductLabel.Text = "SabunTrail"
        '
        'VersionLabel
        '
        Me.VersionLabel.AutoSize = True
        Me.VersionLabel.Location = New System.Drawing.Point(121, 16)
        Me.VersionLabel.Name = "VersionLabel"
        Me.VersionLabel.Size = New System.Drawing.Size(56, 12)
        Me.VersionLabel.TabIndex = 4
        Me.VersionLabel.Text = "バージョン："
        '
        'CopyLightLabel
        '
        Me.CopyLightLabel.AutoSize = True
        Me.CopyLightLabel.Location = New System.Drawing.Point(36, 38)
        Me.CopyLightLabel.Name = "CopyLightLabel"
        Me.CopyLightLabel.Size = New System.Drawing.Size(308, 12)
        Me.CopyLightLabel.TabIndex = 5
        Me.CopyLightLabel.Text = "Copyright © TOPPAN FORMS CO., LTD. All rights reserved"
        '
        'TrialLabel
        '
        Me.TrialLabel.AutoSize = True
        Me.TrialLabel.ForeColor = System.Drawing.Color.Red
        Me.TrialLabel.Location = New System.Drawing.Point(238, 15)
        Me.TrialLabel.Name = "TrialLabel"
        Me.TrialLabel.Size = New System.Drawing.Size(0, 12)
        Me.TrialLabel.TabIndex = 6
        '
        'LicTextBox
        '
        Me.LicTextBox.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.LicTextBox.Location = New System.Drawing.Point(92, 68)
        Me.LicTextBox.MaxLength = 10
        Me.LicTextBox.Name = "LicTextBox"
        Me.LicTextBox.Size = New System.Drawing.Size(218, 19)
        Me.LicTextBox.TabIndex = 7
        '
        'LicLabel
        '
        Me.LicLabel.AutoSize = True
        Me.LicLabel.Location = New System.Drawing.Point(14, 71)
        Me.LicLabel.Name = "LicLabel"
        Me.LicLabel.Size = New System.Drawing.Size(70, 12)
        Me.LicLabel.TabIndex = 8
        Me.LicLabel.Text = "ライセンスキー"
        '
        'LicButton
        '
        Me.LicButton.Location = New System.Drawing.Point(327, 64)
        Me.LicButton.Name = "LicButton"
        Me.LicButton.Size = New System.Drawing.Size(142, 23)
        Me.LicButton.TabIndex = 9
        Me.LicButton.Text = "ライセンス認証"
        Me.LicButton.UseVisualStyleBackColor = True
        '
        'DFAboutDialog
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(494, 125)
        Me.Controls.Add(Me.LicButton)
        Me.Controls.Add(Me.LicLabel)
        Me.Controls.Add(Me.LicTextBox)
        Me.Controls.Add(Me.TrialLabel)
        Me.Controls.Add(Me.CopyLightLabel)
        Me.Controls.Add(Me.VersionLabel)
        Me.Controls.Add(Me.ProductLabel)
        Me.Controls.Add(Me.logoPictureBox)
        Me.Controls.Add(Me.OK_Button)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "DFAboutDialog"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "SabunTrailについて"
        CType(Me.logoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents logoPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents ProductLabel As System.Windows.Forms.Label
    Friend WithEvents VersionLabel As System.Windows.Forms.Label
    Friend WithEvents CopyLightLabel As System.Windows.Forms.Label
    Friend WithEvents TrialLabel As System.Windows.Forms.Label
    Friend WithEvents LicTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LicLabel As System.Windows.Forms.Label
    Friend WithEvents LicButton As System.Windows.Forms.Button

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OutputSettingForm
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(OutputSettingForm))
        Me.OptionGroupBox = New System.Windows.Forms.GroupBox()
        Me.ReverseColCheckBox = New System.Windows.Forms.CheckBox()
        Me.AddAndDeleteCheckBox = New System.Windows.Forms.CheckBox()
        Me.DiffOnlyCheckBox = New System.Windows.Forms.CheckBox()
        Me.NewColDisplayGroupBox = New System.Windows.Forms.GroupBox()
        Me.NewColUnderlineCheckBox = New System.Windows.Forms.CheckBox()
        Me.SelectNewColColorButton = New System.Windows.Forms.Button()
        Me.NewColGreenRadioButton = New System.Windows.Forms.RadioButton()
        Me.NewColBlueRadioButton = New System.Windows.Forms.RadioButton()
        Me.NewColRedRadioButton = New System.Windows.Forms.RadioButton()
        Me.NewColColorLabel = New System.Windows.Forms.Label()
        Me.NewColColorImageLabel = New System.Windows.Forms.Label()
        Me.SizeGroupBox = New System.Windows.Forms.GroupBox()
        Me.A3SizeRadioButton = New System.Windows.Forms.RadioButton()
        Me.A4SizeRadioButton = New System.Windows.Forms.RadioButton()
        Me.RemarkDisplayGroupBox = New System.Windows.Forms.GroupBox()
        Me.Remark2RadioButton = New System.Windows.Forms.RadioButton()
        Me.Remark1RadioButton = New System.Windows.Forms.RadioButton()
        Me.NoRemarkRadioButton = New System.Windows.Forms.RadioButton()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.OldColDisplayGroupBox = New System.Windows.Forms.GroupBox()
        Me.OldColStrikeCheckBox = New System.Windows.Forms.CheckBox()
        Me.OldColUnderlineCheckBox = New System.Windows.Forms.CheckBox()
        Me.SelectOldColColorButton = New System.Windows.Forms.Button()
        Me.OldColGreenRadioButton = New System.Windows.Forms.RadioButton()
        Me.OldColBlueRadioButton = New System.Windows.Forms.RadioButton()
        Me.OldColRedRadioButton = New System.Windows.Forms.RadioButton()
        Me.OldColColorImageLabel = New System.Windows.Forms.Label()
        Me.OldColColorLabel = New System.Windows.Forms.Label()
        Me.ExitButton = New System.Windows.Forms.Button()
        Me.OutputGroupBox = New System.Windows.Forms.GroupBox()
        Me.UpdatedDateCheckBox = New System.Windows.Forms.CheckBox()
        Me.FileCheckBox = New System.Windows.Forms.CheckBox()
        Me.FolderCheckBox = New System.Windows.Forms.CheckBox()
        Me.AutoCheckGroupBox = New System.Windows.Forms.GroupBox()
        Me.CheckUpdatedDateCheckBox = New System.Windows.Forms.CheckBox()
        Me.OKButton = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CloseBtn = New System.Windows.Forms.PictureBox()
        Me.HidenBtn = New System.Windows.Forms.PictureBox()
        Me.HelpBtn = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.SettingSaveBtn = New System.Windows.Forms.PictureBox()
        Me.SettingCancleBtn = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.NewColUnderlineCheckBoxPic = New System.Windows.Forms.PictureBox()
        Me.SelectNewColColorButtonPic = New System.Windows.Forms.PictureBox()
        Me.NewColGreenRadioButtonPic = New System.Windows.Forms.PictureBox()
        Me.NewColBlueRadioButtonPic = New System.Windows.Forms.PictureBox()
        Me.NewColRedRadioButtonPic = New System.Windows.Forms.PictureBox()
        Me.OldColStrikeCheckBoxPic = New System.Windows.Forms.PictureBox()
        Me.OldColUnderlineCheckBoxPic = New System.Windows.Forms.PictureBox()
        Me.SelectOldColColorButtonPic = New System.Windows.Forms.PictureBox()
        Me.OldColGreenRadioButtonPic = New System.Windows.Forms.PictureBox()
        Me.OldColBlueRadioButtonPic = New System.Windows.Forms.PictureBox()
        Me.OldColRedRadioButtonPic = New System.Windows.Forms.PictureBox()
        Me.UpdatedDateCheckBoxPic = New System.Windows.Forms.PictureBox()
        Me.FileCheckBoxPic = New System.Windows.Forms.PictureBox()
        Me.FolderCheckBoxPic = New System.Windows.Forms.PictureBox()
        Me.CheckUpdatedDateCheckBoxPic = New System.Windows.Forms.PictureBox()
        Me.A3SizeRadioButtonPic = New System.Windows.Forms.PictureBox()
        Me.A4SizeRadioButtonPic = New System.Windows.Forms.PictureBox()
        Me.Remark2RadioButtonPic = New System.Windows.Forms.PictureBox()
        Me.Remark1RadioButtonPic = New System.Windows.Forms.PictureBox()
        Me.NoRemarkRadioButtonPic = New System.Windows.Forms.PictureBox()
        Me.ReverseColCheckBoxPic = New System.Windows.Forms.PictureBox()
        Me.AddAndDeleteCheckBoxPic = New System.Windows.Forms.PictureBox()
        Me.DiffOnlyCheckBoxPic = New System.Windows.Forms.PictureBox()
        Me.OptionGroupBox.SuspendLayout()
        Me.NewColDisplayGroupBox.SuspendLayout()
        Me.SizeGroupBox.SuspendLayout()
        Me.RemarkDisplayGroupBox.SuspendLayout()
        Me.OldColDisplayGroupBox.SuspendLayout()
        Me.OutputGroupBox.SuspendLayout()
        Me.AutoCheckGroupBox.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.CloseBtn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HidenBtn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HelpBtn, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.SettingSaveBtn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SettingCancleBtn, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.NewColUnderlineCheckBoxPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SelectNewColColorButtonPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewColGreenRadioButtonPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewColBlueRadioButtonPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewColRedRadioButtonPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OldColStrikeCheckBoxPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OldColUnderlineCheckBoxPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SelectOldColColorButtonPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OldColGreenRadioButtonPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OldColBlueRadioButtonPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OldColRedRadioButtonPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UpdatedDateCheckBoxPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FileCheckBoxPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FolderCheckBoxPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckUpdatedDateCheckBoxPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A3SizeRadioButtonPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A4SizeRadioButtonPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Remark2RadioButtonPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Remark1RadioButtonPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NoRemarkRadioButtonPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReverseColCheckBoxPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AddAndDeleteCheckBoxPic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DiffOnlyCheckBoxPic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OptionGroupBox
        '
        Me.OptionGroupBox.BackColor = System.Drawing.SystemColors.Control
        Me.OptionGroupBox.Controls.Add(Me.ReverseColCheckBox)
        Me.OptionGroupBox.Controls.Add(Me.AddAndDeleteCheckBox)
        Me.OptionGroupBox.Controls.Add(Me.DiffOnlyCheckBox)
        Me.OptionGroupBox.Location = New System.Drawing.Point(665, 3)
        Me.OptionGroupBox.Name = "OptionGroupBox"
        Me.OptionGroupBox.Size = New System.Drawing.Size(460, 48)
        Me.OptionGroupBox.TabIndex = 0
        Me.OptionGroupBox.TabStop = False
        Me.OptionGroupBox.Text = "オプション"
        Me.OptionGroupBox.Visible = False
        '
        'ReverseColCheckBox
        '
        Me.ReverseColCheckBox.AutoSize = True
        Me.ReverseColCheckBox.Location = New System.Drawing.Point(344, 18)
        Me.ReverseColCheckBox.Name = "ReverseColCheckBox"
        Me.ReverseColCheckBox.Size = New System.Drawing.Size(97, 16)
        Me.ReverseColCheckBox.TabIndex = 2
        Me.ReverseColCheckBox.Text = "新旧を逆にする"
        Me.ReverseColCheckBox.UseVisualStyleBackColor = True
        '
        'AddAndDeleteCheckBox
        '
        Me.AddAndDeleteCheckBox.AutoSize = True
        Me.AddAndDeleteCheckBox.Location = New System.Drawing.Point(159, 18)
        Me.AddAndDeleteCheckBox.Name = "AddAndDeleteCheckBox"
        Me.AddAndDeleteCheckBox.Size = New System.Drawing.Size(135, 16)
        Me.AddAndDeleteCheckBox.TabIndex = 1
        Me.AddAndDeleteCheckBox.Text = "（追加）/（削除）を表示"
        Me.AddAndDeleteCheckBox.UseVisualStyleBackColor = True
        '
        'DiffOnlyCheckBox
        '
        Me.DiffOnlyCheckBox.AutoSize = True
        Me.DiffOnlyCheckBox.Location = New System.Drawing.Point(19, 18)
        Me.DiffOnlyCheckBox.Name = "DiffOnlyCheckBox"
        Me.DiffOnlyCheckBox.Size = New System.Drawing.Size(93, 16)
        Me.DiffOnlyCheckBox.TabIndex = 0
        Me.DiffOnlyCheckBox.Text = "差分のみ表示"
        Me.DiffOnlyCheckBox.UseVisualStyleBackColor = True
        '
        'NewColDisplayGroupBox
        '
        Me.NewColDisplayGroupBox.BackColor = System.Drawing.SystemColors.Control
        Me.NewColDisplayGroupBox.Controls.Add(Me.NewColUnderlineCheckBox)
        Me.NewColDisplayGroupBox.Controls.Add(Me.SelectNewColColorButton)
        Me.NewColDisplayGroupBox.Controls.Add(Me.NewColGreenRadioButton)
        Me.NewColDisplayGroupBox.Controls.Add(Me.NewColBlueRadioButton)
        Me.NewColDisplayGroupBox.Controls.Add(Me.NewColRedRadioButton)
        Me.NewColDisplayGroupBox.Location = New System.Drawing.Point(0, 38)
        Me.NewColDisplayGroupBox.Name = "NewColDisplayGroupBox"
        Me.NewColDisplayGroupBox.Size = New System.Drawing.Size(460, 77)
        Me.NewColDisplayGroupBox.TabIndex = 5
        Me.NewColDisplayGroupBox.TabStop = False
        Me.NewColDisplayGroupBox.Text = "新列の表示"
        Me.NewColDisplayGroupBox.Visible = False
        '
        'NewColUnderlineCheckBox
        '
        Me.NewColUnderlineCheckBox.AutoSize = True
        Me.NewColUnderlineCheckBox.Location = New System.Drawing.Point(19, 51)
        Me.NewColUnderlineCheckBox.Name = "NewColUnderlineCheckBox"
        Me.NewColUnderlineCheckBox.Size = New System.Drawing.Size(105, 16)
        Me.NewColUnderlineCheckBox.TabIndex = 4
        Me.NewColUnderlineCheckBox.Text = "変更箇所に下線"
        Me.NewColUnderlineCheckBox.UseVisualStyleBackColor = True
        '
        'SelectNewColColorButton
        '
        Me.SelectNewColColorButton.Location = New System.Drawing.Point(249, 17)
        Me.SelectNewColColorButton.Name = "SelectNewColColorButton"
        Me.SelectNewColColorButton.Size = New System.Drawing.Size(75, 23)
        Me.SelectNewColColorButton.TabIndex = 3
        Me.SelectNewColColorButton.Text = "色選択"
        Me.SelectNewColColorButton.UseVisualStyleBackColor = True
        '
        'NewColGreenRadioButton
        '
        Me.NewColGreenRadioButton.AutoSize = True
        Me.NewColGreenRadioButton.Location = New System.Drawing.Point(208, 21)
        Me.NewColGreenRadioButton.Name = "NewColGreenRadioButton"
        Me.NewColGreenRadioButton.Size = New System.Drawing.Size(35, 16)
        Me.NewColGreenRadioButton.TabIndex = 2
        Me.NewColGreenRadioButton.TabStop = True
        Me.NewColGreenRadioButton.Text = "緑"
        Me.NewColGreenRadioButton.UseVisualStyleBackColor = True
        '
        'NewColBlueRadioButton
        '
        Me.NewColBlueRadioButton.AutoSize = True
        Me.NewColBlueRadioButton.Location = New System.Drawing.Point(167, 21)
        Me.NewColBlueRadioButton.Name = "NewColBlueRadioButton"
        Me.NewColBlueRadioButton.Size = New System.Drawing.Size(35, 16)
        Me.NewColBlueRadioButton.TabIndex = 1
        Me.NewColBlueRadioButton.TabStop = True
        Me.NewColBlueRadioButton.Text = "青"
        Me.NewColBlueRadioButton.UseVisualStyleBackColor = True
        '
        'NewColRedRadioButton
        '
        Me.NewColRedRadioButton.AutoSize = True
        Me.NewColRedRadioButton.Location = New System.Drawing.Point(126, 21)
        Me.NewColRedRadioButton.Name = "NewColRedRadioButton"
        Me.NewColRedRadioButton.Size = New System.Drawing.Size(35, 16)
        Me.NewColRedRadioButton.TabIndex = 0
        Me.NewColRedRadioButton.TabStop = True
        Me.NewColRedRadioButton.Text = "赤"
        Me.NewColRedRadioButton.UseVisualStyleBackColor = True
        '
        'NewColColorLabel
        '
        Me.NewColColorLabel.AutoSize = True
        Me.NewColColorLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.NewColColorLabel.Location = New System.Drawing.Point(321, 246)
        Me.NewColColorLabel.Name = "NewColColorLabel"
        Me.NewColColorLabel.Size = New System.Drawing.Size(75, 12)
        Me.NewColColorLabel.TabIndex = 3
        Me.NewColColorLabel.Text = "変更箇所の色"
        '
        'NewColColorImageLabel
        '
        Me.NewColColorImageLabel.AutoSize = True
        Me.NewColColorImageLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NewColColorImageLabel.Location = New System.Drawing.Point(400, 245)
        Me.NewColColorImageLabel.Name = "NewColColorImageLabel"
        Me.NewColColorImageLabel.Size = New System.Drawing.Size(23, 14)
        Me.NewColColorImageLabel.TabIndex = 4
        Me.NewColColorImageLabel.Text = "　　"
        '
        'SizeGroupBox
        '
        Me.SizeGroupBox.BackColor = System.Drawing.SystemColors.Control
        Me.SizeGroupBox.Controls.Add(Me.A3SizeRadioButton)
        Me.SizeGroupBox.Controls.Add(Me.A4SizeRadioButton)
        Me.SizeGroupBox.Location = New System.Drawing.Point(0, 43)
        Me.SizeGroupBox.Name = "SizeGroupBox"
        Me.SizeGroupBox.Size = New System.Drawing.Size(180, 53)
        Me.SizeGroupBox.TabIndex = 2
        Me.SizeGroupBox.TabStop = False
        Me.SizeGroupBox.Text = "出力サイズ"
        Me.SizeGroupBox.Visible = False
        '
        'A3SizeRadioButton
        '
        Me.A3SizeRadioButton.AutoSize = True
        Me.A3SizeRadioButton.Location = New System.Drawing.Point(108, 21)
        Me.A3SizeRadioButton.Name = "A3SizeRadioButton"
        Me.A3SizeRadioButton.Size = New System.Drawing.Size(37, 16)
        Me.A3SizeRadioButton.TabIndex = 1
        Me.A3SizeRadioButton.TabStop = True
        Me.A3SizeRadioButton.Text = "A3"
        Me.A3SizeRadioButton.UseVisualStyleBackColor = True
        '
        'A4SizeRadioButton
        '
        Me.A4SizeRadioButton.AutoSize = True
        Me.A4SizeRadioButton.Location = New System.Drawing.Point(28, 21)
        Me.A4SizeRadioButton.Name = "A4SizeRadioButton"
        Me.A4SizeRadioButton.Size = New System.Drawing.Size(37, 16)
        Me.A4SizeRadioButton.TabIndex = 0
        Me.A4SizeRadioButton.TabStop = True
        Me.A4SizeRadioButton.Text = "A4"
        Me.A4SizeRadioButton.UseVisualStyleBackColor = True
        '
        'RemarkDisplayGroupBox
        '
        Me.RemarkDisplayGroupBox.BackColor = System.Drawing.SystemColors.Control
        Me.RemarkDisplayGroupBox.Controls.Add(Me.SizeGroupBox)
        Me.RemarkDisplayGroupBox.Controls.Add(Me.Remark2RadioButton)
        Me.RemarkDisplayGroupBox.Controls.Add(Me.Remark1RadioButton)
        Me.RemarkDisplayGroupBox.Controls.Add(Me.NoRemarkRadioButton)
        Me.RemarkDisplayGroupBox.Location = New System.Drawing.Point(629, 20)
        Me.RemarkDisplayGroupBox.Name = "RemarkDisplayGroupBox"
        Me.RemarkDisplayGroupBox.Size = New System.Drawing.Size(274, 53)
        Me.RemarkDisplayGroupBox.TabIndex = 1
        Me.RemarkDisplayGroupBox.TabStop = False
        Me.RemarkDisplayGroupBox.Text = "備考欄の表示"
        Me.RemarkDisplayGroupBox.Visible = False
        '
        'Remark2RadioButton
        '
        Me.Remark2RadioButton.AutoSize = True
        Me.Remark2RadioButton.Location = New System.Drawing.Point(193, 21)
        Me.Remark2RadioButton.Name = "Remark2RadioButton"
        Me.Remark2RadioButton.Size = New System.Drawing.Size(41, 16)
        Me.Remark2RadioButton.TabIndex = 2
        Me.Remark2RadioButton.TabStop = True
        Me.Remark2RadioButton.Text = "2列"
        Me.Remark2RadioButton.UseVisualStyleBackColor = True
        '
        'Remark1RadioButton
        '
        Me.Remark1RadioButton.AutoSize = True
        Me.Remark1RadioButton.Location = New System.Drawing.Point(108, 21)
        Me.Remark1RadioButton.Name = "Remark1RadioButton"
        Me.Remark1RadioButton.Size = New System.Drawing.Size(43, 16)
        Me.Remark1RadioButton.TabIndex = 1
        Me.Remark1RadioButton.TabStop = True
        Me.Remark1RadioButton.Text = "１列"
        Me.Remark1RadioButton.UseVisualStyleBackColor = True
        '
        'NoRemarkRadioButton
        '
        Me.NoRemarkRadioButton.AutoSize = True
        Me.NoRemarkRadioButton.Location = New System.Drawing.Point(28, 21)
        Me.NoRemarkRadioButton.Name = "NoRemarkRadioButton"
        Me.NoRemarkRadioButton.Size = New System.Drawing.Size(42, 16)
        Me.NoRemarkRadioButton.TabIndex = 0
        Me.NoRemarkRadioButton.TabStop = True
        Me.NoRemarkRadioButton.Text = "なし"
        Me.NoRemarkRadioButton.UseVisualStyleBackColor = True
        '
        'SaveButton
        '
        Me.SaveButton.Location = New System.Drawing.Point(177, 1)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(105, 31)
        Me.SaveButton.TabIndex = 8
        Me.SaveButton.Text = "適用"
        Me.SaveButton.UseVisualStyleBackColor = True
        Me.SaveButton.Visible = False
        '
        'OldColDisplayGroupBox
        '
        Me.OldColDisplayGroupBox.BackColor = System.Drawing.SystemColors.Control
        Me.OldColDisplayGroupBox.Controls.Add(Me.OldColStrikeCheckBox)
        Me.OldColDisplayGroupBox.Controls.Add(Me.OldColUnderlineCheckBox)
        Me.OldColDisplayGroupBox.Controls.Add(Me.SelectOldColColorButton)
        Me.OldColDisplayGroupBox.Controls.Add(Me.NewColDisplayGroupBox)
        Me.OldColDisplayGroupBox.Controls.Add(Me.OldColGreenRadioButton)
        Me.OldColDisplayGroupBox.Controls.Add(Me.OldColBlueRadioButton)
        Me.OldColDisplayGroupBox.Controls.Add(Me.OldColRedRadioButton)
        Me.OldColDisplayGroupBox.Location = New System.Drawing.Point(524, 329)
        Me.OldColDisplayGroupBox.Name = "OldColDisplayGroupBox"
        Me.OldColDisplayGroupBox.Size = New System.Drawing.Size(460, 81)
        Me.OldColDisplayGroupBox.TabIndex = 6
        Me.OldColDisplayGroupBox.TabStop = False
        Me.OldColDisplayGroupBox.Text = "旧列の表示"
        Me.OldColDisplayGroupBox.Visible = False
        '
        'OldColStrikeCheckBox
        '
        Me.OldColStrikeCheckBox.AutoSize = True
        Me.OldColStrikeCheckBox.Location = New System.Drawing.Point(219, 52)
        Me.OldColStrikeCheckBox.Name = "OldColStrikeCheckBox"
        Me.OldColStrikeCheckBox.Size = New System.Drawing.Size(117, 16)
        Me.OldColStrikeCheckBox.TabIndex = 5
        Me.OldColStrikeCheckBox.Text = "削除箇所に取消線"
        Me.OldColStrikeCheckBox.UseVisualStyleBackColor = True
        '
        'OldColUnderlineCheckBox
        '
        Me.OldColUnderlineCheckBox.AutoSize = True
        Me.OldColUnderlineCheckBox.Location = New System.Drawing.Point(19, 52)
        Me.OldColUnderlineCheckBox.Name = "OldColUnderlineCheckBox"
        Me.OldColUnderlineCheckBox.Size = New System.Drawing.Size(105, 16)
        Me.OldColUnderlineCheckBox.TabIndex = 4
        Me.OldColUnderlineCheckBox.Text = "変更箇所に下線"
        Me.OldColUnderlineCheckBox.UseVisualStyleBackColor = True
        '
        'SelectOldColColorButton
        '
        Me.SelectOldColColorButton.Location = New System.Drawing.Point(249, 18)
        Me.SelectOldColColorButton.Name = "SelectOldColColorButton"
        Me.SelectOldColColorButton.Size = New System.Drawing.Size(75, 23)
        Me.SelectOldColColorButton.TabIndex = 3
        Me.SelectOldColColorButton.Text = "色選択"
        Me.SelectOldColColorButton.UseVisualStyleBackColor = True
        '
        'OldColGreenRadioButton
        '
        Me.OldColGreenRadioButton.AutoSize = True
        Me.OldColGreenRadioButton.Location = New System.Drawing.Point(208, 21)
        Me.OldColGreenRadioButton.Name = "OldColGreenRadioButton"
        Me.OldColGreenRadioButton.Size = New System.Drawing.Size(35, 16)
        Me.OldColGreenRadioButton.TabIndex = 2
        Me.OldColGreenRadioButton.TabStop = True
        Me.OldColGreenRadioButton.Text = "緑"
        Me.OldColGreenRadioButton.UseVisualStyleBackColor = True
        '
        'OldColBlueRadioButton
        '
        Me.OldColBlueRadioButton.AutoSize = True
        Me.OldColBlueRadioButton.Location = New System.Drawing.Point(167, 21)
        Me.OldColBlueRadioButton.Name = "OldColBlueRadioButton"
        Me.OldColBlueRadioButton.Size = New System.Drawing.Size(35, 16)
        Me.OldColBlueRadioButton.TabIndex = 1
        Me.OldColBlueRadioButton.TabStop = True
        Me.OldColBlueRadioButton.Text = "青"
        Me.OldColBlueRadioButton.UseVisualStyleBackColor = True
        '
        'OldColRedRadioButton
        '
        Me.OldColRedRadioButton.AutoSize = True
        Me.OldColRedRadioButton.Location = New System.Drawing.Point(126, 21)
        Me.OldColRedRadioButton.Name = "OldColRedRadioButton"
        Me.OldColRedRadioButton.Size = New System.Drawing.Size(35, 16)
        Me.OldColRedRadioButton.TabIndex = 0
        Me.OldColRedRadioButton.TabStop = True
        Me.OldColRedRadioButton.Text = "赤"
        Me.OldColRedRadioButton.UseVisualStyleBackColor = True
        '
        'OldColColorImageLabel
        '
        Me.OldColColorImageLabel.AutoSize = True
        Me.OldColColorImageLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.OldColColorImageLabel.Location = New System.Drawing.Point(399, 183)
        Me.OldColColorImageLabel.Name = "OldColColorImageLabel"
        Me.OldColColorImageLabel.Size = New System.Drawing.Size(23, 14)
        Me.OldColColorImageLabel.TabIndex = 4
        Me.OldColColorImageLabel.Text = "　　"
        '
        'OldColColorLabel
        '
        Me.OldColColorLabel.AutoSize = True
        Me.OldColColorLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.OldColColorLabel.Location = New System.Drawing.Point(320, 184)
        Me.OldColColorLabel.Name = "OldColColorLabel"
        Me.OldColColorLabel.Size = New System.Drawing.Size(75, 12)
        Me.OldColColorLabel.TabIndex = 3
        Me.OldColColorLabel.Text = "変更箇所の色"
        '
        'ExitButton
        '
        Me.ExitButton.Location = New System.Drawing.Point(292, 1)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(105, 31)
        Me.ExitButton.TabIndex = 9
        Me.ExitButton.Text = "キャンセル"
        Me.ExitButton.UseVisualStyleBackColor = True
        Me.ExitButton.Visible = False
        '
        'OutputGroupBox
        '
        Me.OutputGroupBox.BackColor = System.Drawing.SystemColors.Control
        Me.OutputGroupBox.Controls.Add(Me.UpdatedDateCheckBox)
        Me.OutputGroupBox.Controls.Add(Me.FileCheckBox)
        Me.OutputGroupBox.Controls.Add(Me.FolderCheckBox)
        Me.OutputGroupBox.Location = New System.Drawing.Point(629, 138)
        Me.OutputGroupBox.Name = "OutputGroupBox"
        Me.OutputGroupBox.Size = New System.Drawing.Size(460, 48)
        Me.OutputGroupBox.TabIndex = 4
        Me.OutputGroupBox.TabStop = False
        Me.OutputGroupBox.Text = "比較表上に表示"
        Me.OutputGroupBox.Visible = False
        '
        'UpdatedDateCheckBox
        '
        Me.UpdatedDateCheckBox.AutoSize = True
        Me.UpdatedDateCheckBox.Location = New System.Drawing.Point(308, 18)
        Me.UpdatedDateCheckBox.Name = "UpdatedDateCheckBox"
        Me.UpdatedDateCheckBox.Size = New System.Drawing.Size(106, 16)
        Me.UpdatedDateCheckBox.TabIndex = 2
        Me.UpdatedDateCheckBox.Text = "ファイル更新日時"
        Me.UpdatedDateCheckBox.UseVisualStyleBackColor = True
        '
        'FileCheckBox
        '
        Me.FileCheckBox.AutoSize = True
        Me.FileCheckBox.Location = New System.Drawing.Point(159, 18)
        Me.FileCheckBox.Name = "FileCheckBox"
        Me.FileCheckBox.Size = New System.Drawing.Size(70, 16)
        Me.FileCheckBox.TabIndex = 1
        Me.FileCheckBox.Text = "ファイル名"
        Me.FileCheckBox.UseVisualStyleBackColor = True
        '
        'FolderCheckBox
        '
        Me.FolderCheckBox.AutoSize = True
        Me.FolderCheckBox.Location = New System.Drawing.Point(19, 18)
        Me.FolderCheckBox.Name = "FolderCheckBox"
        Me.FolderCheckBox.Size = New System.Drawing.Size(71, 16)
        Me.FolderCheckBox.TabIndex = 0
        Me.FolderCheckBox.Text = "フォルダ名"
        Me.FolderCheckBox.UseVisualStyleBackColor = True
        '
        'AutoCheckGroupBox
        '
        Me.AutoCheckGroupBox.BackColor = System.Drawing.SystemColors.Control
        Me.AutoCheckGroupBox.Controls.Add(Me.CheckUpdatedDateCheckBox)
        Me.AutoCheckGroupBox.Location = New System.Drawing.Point(629, 84)
        Me.AutoCheckGroupBox.Name = "AutoCheckGroupBox"
        Me.AutoCheckGroupBox.Size = New System.Drawing.Size(204, 48)
        Me.AutoCheckGroupBox.TabIndex = 3
        Me.AutoCheckGroupBox.TabStop = False
        Me.AutoCheckGroupBox.Text = "自動チェック"
        Me.AutoCheckGroupBox.Visible = False
        '
        'CheckUpdatedDateCheckBox
        '
        Me.CheckUpdatedDateCheckBox.AutoSize = True
        Me.CheckUpdatedDateCheckBox.Location = New System.Drawing.Point(19, 18)
        Me.CheckUpdatedDateCheckBox.Name = "CheckUpdatedDateCheckBox"
        Me.CheckUpdatedDateCheckBox.Size = New System.Drawing.Size(168, 16)
        Me.CheckUpdatedDateCheckBox.TabIndex = 0
        Me.CheckUpdatedDateCheckBox.Text = "新旧ファイルを自動で判断する"
        Me.CheckUpdatedDateCheckBox.UseVisualStyleBackColor = True
        Me.CheckUpdatedDateCheckBox.Visible = False
        '
        'OKButton
        '
        Me.OKButton.Location = New System.Drawing.Point(61, 1)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(105, 31)
        Me.OKButton.TabIndex = 7
        Me.OKButton.Text = "OK"
        Me.OKButton.UseVisualStyleBackColor = True
        Me.OKButton.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sabuntrail.My.Resources.Resources.setting_title
        Me.Panel1.Controls.Add(Me.CloseBtn)
        Me.Panel1.Controls.Add(Me.HidenBtn)
        Me.Panel1.Controls.Add(Me.HelpBtn)
        Me.Panel1.Location = New System.Drawing.Point(31, 31)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(741, 57)
        Me.Panel1.TabIndex = 10
        '
        'CloseBtn
        '
        Me.CloseBtn.BackColor = System.Drawing.Color.Transparent
        Me.CloseBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CloseBtn.Image = Global.sabuntrail.My.Resources.Resources.setting_exit_btn
        Me.CloseBtn.Location = New System.Drawing.Point(676, 8)
        Me.CloseBtn.Name = "CloseBtn"
        Me.CloseBtn.Size = New System.Drawing.Size(41, 40)
        Me.CloseBtn.TabIndex = 5
        Me.CloseBtn.TabStop = False
        '
        'HidenBtn
        '
        Me.HidenBtn.BackColor = System.Drawing.Color.Transparent
        Me.HidenBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.HidenBtn.Image = Global.sabuntrail.My.Resources.Resources.setting_small_btn
        Me.HidenBtn.Location = New System.Drawing.Point(629, 8)
        Me.HidenBtn.Name = "HidenBtn"
        Me.HidenBtn.Size = New System.Drawing.Size(41, 40)
        Me.HidenBtn.TabIndex = 4
        Me.HidenBtn.TabStop = False
        '
        'HelpBtn
        '
        Me.HelpBtn.BackColor = System.Drawing.Color.Transparent
        Me.HelpBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.HelpBtn.Image = Global.sabuntrail.My.Resources.Resources.setting_help_btn
        Me.HelpBtn.Location = New System.Drawing.Point(582, 8)
        Me.HelpBtn.Name = "HelpBtn"
        Me.HelpBtn.Size = New System.Drawing.Size(41, 40)
        Me.HelpBtn.TabIndex = 3
        Me.HelpBtn.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.SettingSaveBtn)
        Me.Panel2.Controls.Add(Me.SettingCancleBtn)
        Me.Panel2.Controls.Add(Me.OptionGroupBox)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.OldColDisplayGroupBox)
        Me.Panel2.Location = New System.Drawing.Point(31, 94)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(741, 427)
        Me.Panel2.TabIndex = 11
        '
        'SettingSaveBtn
        '
        Me.SettingSaveBtn.BackColor = System.Drawing.Color.Transparent
        Me.SettingSaveBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SettingSaveBtn.Image = Global.sabuntrail.My.Resources.Resources.setting_save_btn
        Me.SettingSaveBtn.Location = New System.Drawing.Point(267, 341)
        Me.SettingSaveBtn.Name = "SettingSaveBtn"
        Me.SettingSaveBtn.Size = New System.Drawing.Size(238, 64)
        Me.SettingSaveBtn.TabIndex = 9
        Me.SettingSaveBtn.TabStop = False
        '
        'SettingCancleBtn
        '
        Me.SettingCancleBtn.BackColor = System.Drawing.Color.Transparent
        Me.SettingCancleBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SettingCancleBtn.Image = Global.sabuntrail.My.Resources.Resources.setting_cancel_btn
        Me.SettingCancleBtn.Location = New System.Drawing.Point(29, 348)
        Me.SettingCancleBtn.Name = "SettingCancleBtn"
        Me.SettingCancleBtn.Size = New System.Drawing.Size(180, 56)
        Me.SettingCancleBtn.TabIndex = 8
        Me.SettingCancleBtn.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.BackgroundImage = Global.sabuntrail.My.Resources.Resources.setting_bg_2
        Me.Panel3.Controls.Add(Me.NewColUnderlineCheckBoxPic)
        Me.Panel3.Controls.Add(Me.SelectNewColColorButtonPic)
        Me.Panel3.Controls.Add(Me.NewColGreenRadioButtonPic)
        Me.Panel3.Controls.Add(Me.NewColBlueRadioButtonPic)
        Me.Panel3.Controls.Add(Me.NewColRedRadioButtonPic)
        Me.Panel3.Controls.Add(Me.OldColStrikeCheckBoxPic)
        Me.Panel3.Controls.Add(Me.OldColUnderlineCheckBoxPic)
        Me.Panel3.Controls.Add(Me.SelectOldColColorButtonPic)
        Me.Panel3.Controls.Add(Me.OldColGreenRadioButtonPic)
        Me.Panel3.Controls.Add(Me.OldColBlueRadioButtonPic)
        Me.Panel3.Controls.Add(Me.NewColColorLabel)
        Me.Panel3.Controls.Add(Me.OldColColorImageLabel)
        Me.Panel3.Controls.Add(Me.NewColColorImageLabel)
        Me.Panel3.Controls.Add(Me.OldColColorLabel)
        Me.Panel3.Controls.Add(Me.OldColRedRadioButtonPic)
        Me.Panel3.Controls.Add(Me.UpdatedDateCheckBoxPic)
        Me.Panel3.Controls.Add(Me.FileCheckBoxPic)
        Me.Panel3.Controls.Add(Me.FolderCheckBoxPic)
        Me.Panel3.Controls.Add(Me.CheckUpdatedDateCheckBoxPic)
        Me.Panel3.Controls.Add(Me.OutputGroupBox)
        Me.Panel3.Controls.Add(Me.A3SizeRadioButtonPic)
        Me.Panel3.Controls.Add(Me.A4SizeRadioButtonPic)
        Me.Panel3.Controls.Add(Me.Remark2RadioButtonPic)
        Me.Panel3.Controls.Add(Me.AutoCheckGroupBox)
        Me.Panel3.Controls.Add(Me.Remark1RadioButtonPic)
        Me.Panel3.Controls.Add(Me.RemarkDisplayGroupBox)
        Me.Panel3.Controls.Add(Me.NoRemarkRadioButtonPic)
        Me.Panel3.Controls.Add(Me.ReverseColCheckBoxPic)
        Me.Panel3.Controls.Add(Me.AddAndDeleteCheckBoxPic)
        Me.Panel3.Controls.Add(Me.DiffOnlyCheckBoxPic)
        Me.Panel3.Location = New System.Drawing.Point(32, 31)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(682, 292)
        Me.Panel3.TabIndex = 7
        '
        'NewColUnderlineCheckBoxPic
        '
        Me.NewColUnderlineCheckBoxPic.BackColor = System.Drawing.Color.Transparent
        Me.NewColUnderlineCheckBoxPic.Image = Global.sabuntrail.My.Resources.Resources.setting_6_btn5_off
        Me.NewColUnderlineCheckBoxPic.Location = New System.Drawing.Point(314, 267)
        Me.NewColUnderlineCheckBoxPic.Name = "NewColUnderlineCheckBoxPic"
        Me.NewColUnderlineCheckBoxPic.Size = New System.Drawing.Size(170, 29)
        Me.NewColUnderlineCheckBoxPic.TabIndex = 22
        Me.NewColUnderlineCheckBoxPic.TabStop = False
        '
        'SelectNewColColorButtonPic
        '
        Me.SelectNewColColorButtonPic.BackColor = System.Drawing.Color.Transparent
        Me.SelectNewColColorButtonPic.Image = Global.sabuntrail.My.Resources.Resources.setting_6_btn4
        Me.SelectNewColColorButtonPic.Location = New System.Drawing.Point(601, 237)
        Me.SelectNewColColorButtonPic.Name = "SelectNewColColorButtonPic"
        Me.SelectNewColColorButtonPic.Size = New System.Drawing.Size(80, 29)
        Me.SelectNewColColorButtonPic.TabIndex = 21
        Me.SelectNewColColorButtonPic.TabStop = False
        '
        'NewColGreenRadioButtonPic
        '
        Me.NewColGreenRadioButtonPic.BackColor = System.Drawing.Color.Transparent
        Me.NewColGreenRadioButtonPic.Image = Global.sabuntrail.My.Resources.Resources.setting_6_btngreen_off
        Me.NewColGreenRadioButtonPic.Location = New System.Drawing.Point(546, 237)
        Me.NewColGreenRadioButtonPic.Name = "NewColGreenRadioButtonPic"
        Me.NewColGreenRadioButtonPic.Size = New System.Drawing.Size(46, 29)
        Me.NewColGreenRadioButtonPic.TabIndex = 20
        Me.NewColGreenRadioButtonPic.TabStop = False
        '
        'NewColBlueRadioButtonPic
        '
        Me.NewColBlueRadioButtonPic.BackColor = System.Drawing.Color.Transparent
        Me.NewColBlueRadioButtonPic.Image = Global.sabuntrail.My.Resources.Resources.setting_6_btnblue_off
        Me.NewColBlueRadioButtonPic.Location = New System.Drawing.Point(486, 237)
        Me.NewColBlueRadioButtonPic.Name = "NewColBlueRadioButtonPic"
        Me.NewColBlueRadioButtonPic.Size = New System.Drawing.Size(46, 29)
        Me.NewColBlueRadioButtonPic.TabIndex = 19
        Me.NewColBlueRadioButtonPic.TabStop = False
        '
        'NewColRedRadioButtonPic
        '
        Me.NewColRedRadioButtonPic.BackColor = System.Drawing.Color.Transparent
        Me.NewColRedRadioButtonPic.Image = Global.sabuntrail.My.Resources.Resources.setting_6_btnred_off
        Me.NewColRedRadioButtonPic.Location = New System.Drawing.Point(429, 237)
        Me.NewColRedRadioButtonPic.Name = "NewColRedRadioButtonPic"
        Me.NewColRedRadioButtonPic.Size = New System.Drawing.Size(46, 29)
        Me.NewColRedRadioButtonPic.TabIndex = 18
        Me.NewColRedRadioButtonPic.TabStop = False
        '
        'OldColStrikeCheckBoxPic
        '
        Me.OldColStrikeCheckBoxPic.BackColor = System.Drawing.Color.Transparent
        Me.OldColStrikeCheckBoxPic.Image = Global.sabuntrail.My.Resources.Resources.setting_6_btn6_off
        Me.OldColStrikeCheckBoxPic.Location = New System.Drawing.Point(494, 207)
        Me.OldColStrikeCheckBoxPic.Name = "OldColStrikeCheckBoxPic"
        Me.OldColStrikeCheckBoxPic.Size = New System.Drawing.Size(184, 29)
        Me.OldColStrikeCheckBoxPic.TabIndex = 17
        Me.OldColStrikeCheckBoxPic.TabStop = False
        '
        'OldColUnderlineCheckBoxPic
        '
        Me.OldColUnderlineCheckBoxPic.BackColor = System.Drawing.Color.Transparent
        Me.OldColUnderlineCheckBoxPic.Image = Global.sabuntrail.My.Resources.Resources.setting_6_btn5_off
        Me.OldColUnderlineCheckBoxPic.Location = New System.Drawing.Point(314, 207)
        Me.OldColUnderlineCheckBoxPic.Name = "OldColUnderlineCheckBoxPic"
        Me.OldColUnderlineCheckBoxPic.Size = New System.Drawing.Size(170, 29)
        Me.OldColUnderlineCheckBoxPic.TabIndex = 16
        Me.OldColUnderlineCheckBoxPic.TabStop = False
        '
        'SelectOldColColorButtonPic
        '
        Me.SelectOldColColorButtonPic.BackColor = System.Drawing.Color.Transparent
        Me.SelectOldColColorButtonPic.Image = Global.sabuntrail.My.Resources.Resources.setting_6_btn4
        Me.SelectOldColColorButtonPic.Location = New System.Drawing.Point(600, 177)
        Me.SelectOldColColorButtonPic.Name = "SelectOldColColorButtonPic"
        Me.SelectOldColColorButtonPic.Size = New System.Drawing.Size(80, 29)
        Me.SelectOldColColorButtonPic.TabIndex = 15
        Me.SelectOldColColorButtonPic.TabStop = False
        '
        'OldColGreenRadioButtonPic
        '
        Me.OldColGreenRadioButtonPic.BackColor = System.Drawing.Color.Transparent
        Me.OldColGreenRadioButtonPic.Image = Global.sabuntrail.My.Resources.Resources.setting_6_btngreen_off
        Me.OldColGreenRadioButtonPic.Location = New System.Drawing.Point(545, 177)
        Me.OldColGreenRadioButtonPic.Name = "OldColGreenRadioButtonPic"
        Me.OldColGreenRadioButtonPic.Size = New System.Drawing.Size(46, 29)
        Me.OldColGreenRadioButtonPic.TabIndex = 14
        Me.OldColGreenRadioButtonPic.TabStop = False
        '
        'OldColBlueRadioButtonPic
        '
        Me.OldColBlueRadioButtonPic.BackColor = System.Drawing.Color.Transparent
        Me.OldColBlueRadioButtonPic.Image = Global.sabuntrail.My.Resources.Resources.setting_6_btnblue_off
        Me.OldColBlueRadioButtonPic.Location = New System.Drawing.Point(485, 177)
        Me.OldColBlueRadioButtonPic.Name = "OldColBlueRadioButtonPic"
        Me.OldColBlueRadioButtonPic.Size = New System.Drawing.Size(46, 29)
        Me.OldColBlueRadioButtonPic.TabIndex = 13
        Me.OldColBlueRadioButtonPic.TabStop = False
        '
        'OldColRedRadioButtonPic
        '
        Me.OldColRedRadioButtonPic.BackColor = System.Drawing.Color.Transparent
        Me.OldColRedRadioButtonPic.Image = Global.sabuntrail.My.Resources.Resources.setting_6_btnred_off
        Me.OldColRedRadioButtonPic.Location = New System.Drawing.Point(428, 177)
        Me.OldColRedRadioButtonPic.Name = "OldColRedRadioButtonPic"
        Me.OldColRedRadioButtonPic.Size = New System.Drawing.Size(46, 29)
        Me.OldColRedRadioButtonPic.TabIndex = 12
        Me.OldColRedRadioButtonPic.TabStop = False
        '
        'UpdatedDateCheckBoxPic
        '
        Me.UpdatedDateCheckBoxPic.BackColor = System.Drawing.Color.Transparent
        Me.UpdatedDateCheckBoxPic.Image = Global.sabuntrail.My.Resources.Resources.setting_5_btn3_off
        Me.UpdatedDateCheckBoxPic.Location = New System.Drawing.Point(511, 144)
        Me.UpdatedDateCheckBoxPic.Name = "UpdatedDateCheckBoxPic"
        Me.UpdatedDateCheckBoxPic.Size = New System.Drawing.Size(142, 29)
        Me.UpdatedDateCheckBoxPic.TabIndex = 11
        Me.UpdatedDateCheckBoxPic.TabStop = False
        '
        'FileCheckBoxPic
        '
        Me.FileCheckBoxPic.BackColor = System.Drawing.Color.Transparent
        Me.FileCheckBoxPic.Image = Global.sabuntrail.My.Resources.Resources.setting_5_btn2_off
        Me.FileCheckBoxPic.Location = New System.Drawing.Point(322, 144)
        Me.FileCheckBoxPic.Name = "FileCheckBoxPic"
        Me.FileCheckBoxPic.Size = New System.Drawing.Size(113, 29)
        Me.FileCheckBoxPic.TabIndex = 10
        Me.FileCheckBoxPic.TabStop = False
        '
        'FolderCheckBoxPic
        '
        Me.FolderCheckBoxPic.BackColor = System.Drawing.Color.Transparent
        Me.FolderCheckBoxPic.Image = Global.sabuntrail.My.Resources.Resources.setting_5_btn1_off
        Me.FolderCheckBoxPic.Location = New System.Drawing.Point(164, 144)
        Me.FolderCheckBoxPic.Name = "FolderCheckBoxPic"
        Me.FolderCheckBoxPic.Size = New System.Drawing.Size(113, 29)
        Me.FolderCheckBoxPic.TabIndex = 9
        Me.FolderCheckBoxPic.TabStop = False
        '
        'CheckUpdatedDateCheckBoxPic
        '
        Me.CheckUpdatedDateCheckBoxPic.BackColor = System.Drawing.Color.Transparent
        Me.CheckUpdatedDateCheckBoxPic.Image = Global.sabuntrail.My.Resources.Resources.setting_4_btn1_off
        Me.CheckUpdatedDateCheckBoxPic.Location = New System.Drawing.Point(164, 109)
        Me.CheckUpdatedDateCheckBoxPic.Name = "CheckUpdatedDateCheckBoxPic"
        Me.CheckUpdatedDateCheckBoxPic.Size = New System.Drawing.Size(271, 29)
        Me.CheckUpdatedDateCheckBoxPic.TabIndex = 8
        Me.CheckUpdatedDateCheckBoxPic.TabStop = False
        '
        'A3SizeRadioButtonPic
        '
        Me.A3SizeRadioButtonPic.BackColor = System.Drawing.Color.Transparent
        Me.A3SizeRadioButtonPic.Image = Global.sabuntrail.My.Resources.Resources.setting_3_btn2_off
        Me.A3SizeRadioButtonPic.Location = New System.Drawing.Point(322, 74)
        Me.A3SizeRadioButtonPic.Name = "A3SizeRadioButtonPic"
        Me.A3SizeRadioButtonPic.Size = New System.Drawing.Size(113, 29)
        Me.A3SizeRadioButtonPic.TabIndex = 7
        Me.A3SizeRadioButtonPic.TabStop = False
        '
        'A4SizeRadioButtonPic
        '
        Me.A4SizeRadioButtonPic.BackColor = System.Drawing.Color.Transparent
        Me.A4SizeRadioButtonPic.Image = Global.sabuntrail.My.Resources.Resources.setting_3_btn1_off
        Me.A4SizeRadioButtonPic.Location = New System.Drawing.Point(164, 74)
        Me.A4SizeRadioButtonPic.Name = "A4SizeRadioButtonPic"
        Me.A4SizeRadioButtonPic.Size = New System.Drawing.Size(113, 29)
        Me.A4SizeRadioButtonPic.TabIndex = 6
        Me.A4SizeRadioButtonPic.TabStop = False
        '
        'Remark2RadioButtonPic
        '
        Me.Remark2RadioButtonPic.BackColor = System.Drawing.Color.Transparent
        Me.Remark2RadioButtonPic.Image = Global.sabuntrail.My.Resources.Resources.setting_2_btn3_off
        Me.Remark2RadioButtonPic.Location = New System.Drawing.Point(511, 39)
        Me.Remark2RadioButtonPic.Name = "Remark2RadioButtonPic"
        Me.Remark2RadioButtonPic.Size = New System.Drawing.Size(113, 29)
        Me.Remark2RadioButtonPic.TabIndex = 5
        Me.Remark2RadioButtonPic.TabStop = False
        '
        'Remark1RadioButtonPic
        '
        Me.Remark1RadioButtonPic.BackColor = System.Drawing.Color.Transparent
        Me.Remark1RadioButtonPic.Image = Global.sabuntrail.My.Resources.Resources.setting_2_btn2_off
        Me.Remark1RadioButtonPic.Location = New System.Drawing.Point(322, 39)
        Me.Remark1RadioButtonPic.Name = "Remark1RadioButtonPic"
        Me.Remark1RadioButtonPic.Size = New System.Drawing.Size(113, 29)
        Me.Remark1RadioButtonPic.TabIndex = 4
        Me.Remark1RadioButtonPic.TabStop = False
        '
        'NoRemarkRadioButtonPic
        '
        Me.NoRemarkRadioButtonPic.BackColor = System.Drawing.Color.Transparent
        Me.NoRemarkRadioButtonPic.Image = Global.sabuntrail.My.Resources.Resources.setting_2_btn1_off
        Me.NoRemarkRadioButtonPic.Location = New System.Drawing.Point(164, 39)
        Me.NoRemarkRadioButtonPic.Name = "NoRemarkRadioButtonPic"
        Me.NoRemarkRadioButtonPic.Size = New System.Drawing.Size(113, 29)
        Me.NoRemarkRadioButtonPic.TabIndex = 3
        Me.NoRemarkRadioButtonPic.TabStop = False
        '
        'ReverseColCheckBoxPic
        '
        Me.ReverseColCheckBoxPic.BackColor = System.Drawing.Color.Transparent
        Me.ReverseColCheckBoxPic.Image = Global.sabuntrail.My.Resources.Resources.setting_1_btn3_off
        Me.ReverseColCheckBoxPic.Location = New System.Drawing.Point(511, 4)
        Me.ReverseColCheckBoxPic.Name = "ReverseColCheckBoxPic"
        Me.ReverseColCheckBoxPic.Size = New System.Drawing.Size(155, 29)
        Me.ReverseColCheckBoxPic.TabIndex = 2
        Me.ReverseColCheckBoxPic.TabStop = False
        '
        'AddAndDeleteCheckBoxPic
        '
        Me.AddAndDeleteCheckBoxPic.BackColor = System.Drawing.Color.Transparent
        Me.AddAndDeleteCheckBoxPic.Image = Global.sabuntrail.My.Resources.Resources.setting_1_btn2_off
        Me.AddAndDeleteCheckBoxPic.Location = New System.Drawing.Point(322, 4)
        Me.AddAndDeleteCheckBoxPic.Name = "AddAndDeleteCheckBoxPic"
        Me.AddAndDeleteCheckBoxPic.Size = New System.Drawing.Size(147, 29)
        Me.AddAndDeleteCheckBoxPic.TabIndex = 1
        Me.AddAndDeleteCheckBoxPic.TabStop = False
        '
        'DiffOnlyCheckBoxPic
        '
        Me.DiffOnlyCheckBoxPic.BackColor = System.Drawing.Color.Transparent
        Me.DiffOnlyCheckBoxPic.Image = Global.sabuntrail.My.Resources.Resources.setting_1_btn1_off
        Me.DiffOnlyCheckBoxPic.Location = New System.Drawing.Point(164, 4)
        Me.DiffOnlyCheckBoxPic.Name = "DiffOnlyCheckBoxPic"
        Me.DiffOnlyCheckBoxPic.Size = New System.Drawing.Size(113, 29)
        Me.DiffOnlyCheckBoxPic.TabIndex = 0
        Me.DiffOnlyCheckBoxPic.TabStop = False
        '
        'OutputSettingForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.BackgroundImage = Global.sabuntrail.My.Resources.Resources.setting_bg_1
        Me.ClientSize = New System.Drawing.Size(800, 550)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.OKButton)
        Me.Controls.Add(Me.SaveButton)
        Me.Controls.Add(Me.ExitButton)
        Me.Controls.Add(Me.Panel2)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "OutputSettingForm"
        Me.Text = "出力設定"
        Me.TransparencyKey = System.Drawing.Color.Snow
        Me.OptionGroupBox.ResumeLayout(False)
        Me.OptionGroupBox.PerformLayout()
        Me.NewColDisplayGroupBox.ResumeLayout(False)
        Me.NewColDisplayGroupBox.PerformLayout()
        Me.SizeGroupBox.ResumeLayout(False)
        Me.SizeGroupBox.PerformLayout()
        Me.RemarkDisplayGroupBox.ResumeLayout(False)
        Me.RemarkDisplayGroupBox.PerformLayout()
        Me.OldColDisplayGroupBox.ResumeLayout(False)
        Me.OldColDisplayGroupBox.PerformLayout()
        Me.OutputGroupBox.ResumeLayout(False)
        Me.OutputGroupBox.PerformLayout()
        Me.AutoCheckGroupBox.ResumeLayout(False)
        Me.AutoCheckGroupBox.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.CloseBtn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HidenBtn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HelpBtn, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.SettingSaveBtn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SettingCancleBtn, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.NewColUnderlineCheckBoxPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SelectNewColColorButtonPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewColGreenRadioButtonPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewColBlueRadioButtonPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewColRedRadioButtonPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OldColStrikeCheckBoxPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OldColUnderlineCheckBoxPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SelectOldColColorButtonPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OldColGreenRadioButtonPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OldColBlueRadioButtonPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OldColRedRadioButtonPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UpdatedDateCheckBoxPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FileCheckBoxPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FolderCheckBoxPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckUpdatedDateCheckBoxPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A3SizeRadioButtonPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A4SizeRadioButtonPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Remark2RadioButtonPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Remark1RadioButtonPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NoRemarkRadioButtonPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReverseColCheckBoxPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AddAndDeleteCheckBoxPic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DiffOnlyCheckBoxPic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents OptionGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents ReverseColCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents AddAndDeleteCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents DiffOnlyCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents NewColDisplayGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents NewColUnderlineCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SelectNewColColorButton As System.Windows.Forms.Button
    Friend WithEvents NewColColorImageLabel As System.Windows.Forms.Label
    Friend WithEvents NewColColorLabel As System.Windows.Forms.Label
    Friend WithEvents NewColGreenRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents NewColBlueRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents NewColRedRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents SizeGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents A3SizeRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents A4SizeRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents RemarkDisplayGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents Remark2RadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents Remark1RadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents NoRemarkRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents SaveButton As System.Windows.Forms.Button
    Friend WithEvents OldColDisplayGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents OldColStrikeCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents OldColUnderlineCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SelectOldColColorButton As System.Windows.Forms.Button
    Friend WithEvents OldColColorImageLabel As System.Windows.Forms.Label
    Friend WithEvents OldColColorLabel As System.Windows.Forms.Label
    Friend WithEvents OldColGreenRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents OldColBlueRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents OldColRedRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents ExitButton As System.Windows.Forms.Button
    Friend WithEvents OutputGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents UpdatedDateCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents FileCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents FolderCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents AutoCheckGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents CheckUpdatedDateCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents OKButton As System.Windows.Forms.Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents CloseBtn As PictureBox
    Friend WithEvents HidenBtn As PictureBox
    Friend WithEvents HelpBtn As PictureBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents SettingSaveBtn As PictureBox
    Friend WithEvents SettingCancleBtn As PictureBox
    Friend WithEvents DiffOnlyCheckBoxPic As PictureBox
    Friend WithEvents AddAndDeleteCheckBoxPic As PictureBox
    Friend WithEvents ReverseColCheckBoxPic As PictureBox
    Friend WithEvents Remark2RadioButtonPic As PictureBox
    Friend WithEvents Remark1RadioButtonPic As PictureBox
    Friend WithEvents NoRemarkRadioButtonPic As PictureBox
    Friend WithEvents A3SizeRadioButtonPic As PictureBox
    Friend WithEvents A4SizeRadioButtonPic As PictureBox
    Friend WithEvents CheckUpdatedDateCheckBoxPic As PictureBox
    Friend WithEvents UpdatedDateCheckBoxPic As PictureBox
    Friend WithEvents FileCheckBoxPic As PictureBox
    Friend WithEvents FolderCheckBoxPic As PictureBox
    Friend WithEvents OldColStrikeCheckBoxPic As PictureBox
    Friend WithEvents OldColUnderlineCheckBoxPic As PictureBox
    Friend WithEvents SelectOldColColorButtonPic As PictureBox
    Friend WithEvents OldColGreenRadioButtonPic As PictureBox
    Friend WithEvents OldColBlueRadioButtonPic As PictureBox
    Friend WithEvents OldColRedRadioButtonPic As PictureBox
    Friend WithEvents NewColUnderlineCheckBoxPic As PictureBox
    Friend WithEvents SelectNewColColorButtonPic As PictureBox
    Friend WithEvents NewColGreenRadioButtonPic As PictureBox
    Friend WithEvents NewColBlueRadioButtonPic As PictureBox
    Friend WithEvents NewColRedRadioButtonPic As PictureBox
End Class

﻿Imports System.Xml
Imports System.IO

''' <summary>
''' サブントレイル本体（Form)
''' </summary>
''' <remarks></remarks>
Public Class DFForm

    ''' <summary>
    ''' 定義ファイルフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CONF_DIRECTORY = "\conf"

    ''' <summary>
    ''' スタイルシートフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const STYLE_DIRECTORY = "\style"

    ' ワードフォルダ
    ''' <summary>
    ''' テンプレートフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const WORD_DIRECTORY As String = "\template"


    ''' <summary>
    ''' 一時作業用フォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TMP_DIRECTORY As String = "\tmp"

    ''' <summary>
    ''' ログフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const LOG_DIRECTORY As String = "\log"

    ''' <summary>
    ''' ライセンスフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const LIC_DIRECTORY As String = "\lic"


    ''' <summary>
    ''' マニュアルフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MAN_DIRECTORY As String = "\man"

    ''' <summary>
    ''' 設定ファイル
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CONFIG_FILE As String = "\setting.xml"

    ''' <summary>
    ''' ライセンスファイル
    ''' </summary>
    ''' <remarks></remarks>
    Private Const LIC_FILE As String = "\lic.dat"

    ''' <summary>
    ''' GUI起動時に変換結果を保存する一時ファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TEMP_OUT_FILE_NAME As String = "\tempout_{0}"

    ''' <summary>
    ''' GUI起動時に変換結果を保存する一時ファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TEMP_NEW_FILE_NAME As String = "\tempnew_{0}"

    ''' <summary>
    ''' GUI起動時に変換結果を保存する一時ファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TEMP_OLD_FILE_NAME As String = "\tempold_{0}"

    ''' <summary>
    ''' GUI起動時に変換結果を保存する一時ファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MANUAL_FILE_NAME As String = "\manual.pdf"

    ''' <summary>
    ''' 定義ファイルフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_conf_dir As String = Nothing

    ''' <summary>
    ''' WORDテンプレートフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_word_dir As String = Nothing

    ''' <summary>
    ''' 一次作業フォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_tmp_dir As String = Nothing

    ''' <summary>
    ''' スタイルシートフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_style_dir As String = Nothing

    ''' <summary>
    ''' ログフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_log_dir As String = Nothing

    ''' <summary>
    ''' ライセンスパス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_lic_dir As String = Nothing

    ''' <summary>
    ''' マニュアルフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_man_dir As String = Nothing

    ''' <summary>
    ''' 定義ファイルのファイルパス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_conf_fpath As String = Nothing

    ''' <summary>
    ''' コマンド起動時の戻り値
    ''' </summary>
    ''' <remarks></remarks>
    Private Enum RETURN_CODE
        SUCCESS = 0
        LIC_ERROR = 1
        ARGUMENT_ERROR = 2
        FILE_NOT_INPUT = 3
        FILE_EXT_INVALID = 4
        FILE_NOT_FOUND = 5
        SYSTEM_ERROR = 99
    End Enum

    ''' <summary>
    ''' ログオブジェクト
    ''' </summary>
    ''' <remarks></remarks>
    Private m_log As LogWriter = Nothing

    ''' <summary>
    ''' 差分作成オブジェクト
    ''' </summary>
    ''' <remarks></remarks>
    Private m_dfCreator As DFCreator = Nothing

    ''' <summary>
    ''' 設定情報オブジェクト
    ''' </summary>
    ''' <remarks></remarks>
    Private m_setting As Setting = Nothing

    ''' <summary>
    ''' カスタムカラー
    ''' 色選択画面で作成した色を保持する。
    ''' </summary>
    ''' <remarks></remarks>
    Private m_cunstomColor As Integer() = Nothing

    ' ''' <summary>
    ' ''' 備考欄のラジオボタン配列
    ' ''' 2015/03/04 DELETE [sabuntrail Version 1.1対応] Kitajima
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Private remarkArray As RadioButton()

    ' ''' <summary>
    ' ''' ページサイズのラジオボタン配列
    ' ''' 2015/03/04 DELETE [sabuntrail Version 1.1対応] Kitajima
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Private sizeArray As RadioButton()

    ''' <summary>
    ''' 出力パス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_outputPath As String = Nothing

    ''' <summary>
    ''' コマンドラインモード
    ''' </summary>
    ''' <remarks></remarks>
    Private m_consoleMode As Boolean = False

    ''' <summary>
    ''' コンストラクタ
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        Try
            ' この呼び出しは、Windows フォーム デザイナで必要です。
            InitializeComponent()

            ' InitializeComponent() 呼び出しの後で初期化を追加します。
            Initialize()

        Catch ex As Exception
            ' ログの初期化に失敗している可能性があるのでログ出力はしない。
            ShowErrorDialog(String.Format(Message.GetInstance.GetMessage(Message.ERR_CODE_9990), ex.Message), Message.ERR_CODE_9990)
            If m_log IsNot Nothing Then
                m_log.WriteErrorLog(ex.ToString)
            End If
            ' システムエラーで終了
            Environment.Exit(RETURN_CODE.SYSTEM_ERROR)
        End Try
    End Sub

    ''' <summary>
    ''' 情報ダイアログ表示メソッド
    ''' </summary>
    ''' <param name="msgId">メッセージ</param>
    ''' <param name="msg">メッセージID</param>
    ''' <remarks></remarks>
    Public Sub ShowInfoDialog(ByVal msg As String, ByVal msgId As String)

        'MessageBox.Show(msg, msgId, MessageBoxButtons.OK, MessageBoxIcon.Information)
        'MessageBox.Show(msg, msgId, MessageBoxButtons.OK)

        Dim dialog As New DialogNoCheckBox(msg, msgId, False, DialogWithCheckBox.MESSAGE_TYPE.INFO)
        dialog.StartPosition = FormStartPosition.CenterParent
        Dim result As DialogResult = dialog.ShowDialog()

        'Dim frm As New MsgBoxEx(msg, msgId)
        'frm.StartPosition = FormStartPosition.CenterParent
        'frm.ShowDialog()

        If m_log IsNot Nothing Then
            m_log.WriteTraceLog(msgId & " " & msg)
        End If
    End Sub

    ''' <summary>
    ''' エラーダイアログ表示メソッド
    ''' </summary>
    ''' <param name="msgId">メッセージ</param>
    ''' <param name="msg">メッセージID</param>
    ''' <remarks></remarks>
    Public Sub ShowErrorDialog(ByVal msg As String, ByVal msgId As String)
        'MessageBox.Show(msg, msgId, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'MessageBox.Show(msg, msgId, MessageBoxButtons.OK)


        Dim dialog As New DialogNoCheckBox(msg, msgId, False, DialogWithCheckBox.MESSAGE_TYPE.ERR)
        dialog.StartPosition = FormStartPosition.CenterParent
        Dim result As DialogResult = dialog.ShowDialog()
        'Dim frm As New MsgBoxEx(msg, msgId)
        'frm.StartPosition = FormStartPosition.CenterParent
        'frm.ShowDialog()

        If m_log IsNot Nothing Then
            m_log.WriteErrorLog(msgId & " " & msg)
        End If
    End Sub

    ''' <summary>
    ''' 質問ダイアログ表示
    ''' </summary>
    ''' <param name="msgId">メッセージ</param>
    ''' <param name="msg">メッセージID</param>
    ''' <returns>ダイアログの結果(YesOrNo)</returns>
    ''' <remarks></remarks>
    Public Function ShowQuestionDialog(ByVal msg As String, ByVal msgId As String) As DialogResult

        'Dim result As DialogResult = MessageBox.Show(msg, msgId, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        'Dim result As DialogResult = MessageBox.Show(msg, msgId, MessageBoxButtons.YesNo)
        Dim dialog As New DialogNoCheckBox(msg, msgId, True, DialogWithCheckBox.MESSAGE_TYPE.QUE)
        dialog.StartPosition = FormStartPosition.CenterParent
        Dim result As DialogResult = dialog.ShowDialog()

        If m_log IsNot Nothing Then
            m_log.WriteTraceLog(msgId & " " & msg & " " & result)
        End If

        Return result

    End Function

    ''' <summary>
    ''' 新旧のファイルパスを入れ替える。
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SwapFilePath()
        Dim tmpStr As String = OldTextBox.Text
        OldTextBox.Text = NewTextBox.Text
        NewTextBox.Text = tmpStr
    End Sub

    ''' <summary>
    ''' 実行ボタン押下時の処理
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ExecuteButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExecuteButton.Click, pic_ExecuteButton.Click

        Dim newPath As String = Nothing
        Dim oldPath As String = Nothing
        Dim outPath As String = Nothing

        Try
            SyncLock (GetType(Application))

                Me.Enabled = False

                If Not CheckBlank() Then
                    Return
                End If

                If Not CheckExtention() Then
                    Return
                End If

                If Not CheckExists() Then
                    Return
                End If

                ' 2015/03/02 ADD START [sabuntrail Version 1.1対応] Kitajima
                ' 自動チェックフラグが有効
                If m_setting.autoCheckUpdatedDate Then
                    ' 旧の日付の方が大きい場合
                    If Date.Compare(File.GetLastWriteTime(OldTextBox.Text), File.GetLastWriteTime(NewTextBox.Text)) > 0 Then
                        ' 警告無視フラグがFalse
                        If Not m_setting.autoCheckIgnoreUpdatedDateWarning Then
                            Dim dialog As New DialogWithCheckBox(Message.GetInstance.GetMessage(Message.QUE_CODE_0002), Message.GetInstance.GetMessage(Message.QUE_CODE_0003), Message.QUE_CODE_0002, DialogWithCheckBox.MESSAGE_TYPE.WARN)
                            Dim result As DialogResult = dialog.ShowDialog()
                            If result = Windows.Forms.DialogResult.Yes Then
                                SwapFilePath()
                                If dialog.isChecked Then
                                    m_setting.autoCheckIgnoreUpdatedDateWarning = True
                                    m_setting.save()
                                End If
                            End If
                        Else
                            SwapFilePath()
                        End If
                    End If
                End If
                ' 2015/03/02 ADD END [sabuntrail Version 1.1対応] Kitajima

                ' 2015/03/02 ADD START [sabuntrail Version 1.1対応] Kitajima
                Dim oItems As New OutputItems

                If m_setting.outputFolder Then
                    oItems.NewFolderPath = Path.GetDirectoryName(NewTextBox.Text)
                    oItems.OldFolderPath = Path.GetDirectoryName(OldTextBox.Text)
                End If

                If m_setting.outputFile Then
                    oItems.NewFileName = Path.GetFileName(NewTextBox.Text)
                    oItems.OldFileName = Path.GetFileName(OldTextBox.Text)
                End If

                If m_setting.outputUpdatedDate Then
                    oItems.NewUpdatedDate = Common.ConvertFormattedDate(File.GetLastWriteTime(NewTextBox.Text), m_setting.outputUpdatedDateFormat)
                    oItems.OldUpdatedDate = Common.ConvertFormattedDate(File.GetLastWriteTime(OldTextBox.Text), m_setting.outputUpdatedDateFormat)
                End If
                ' 2015/03/02 ADD END [sabuntrail Version 1.1対応] Kitajima

                newPath = m_tmp_dir & String.Format(TEMP_NEW_FILE_NAME, DateTime.Now.ToString("yyyyMMddHHmmssfffffff")) & Path.GetExtension(NewTextBox.Text)
                oldPath = m_tmp_dir & String.Format(TEMP_OLD_FILE_NAME, DateTime.Now.ToString("yyyyMMddHHmmssfffffff")) & Path.GetExtension(OldTextBox.Text)

                File.Copy(NewTextBox.Text, newPath)
                File.Copy(OldTextBox.Text, oldPath)

                If m_consoleMode Then
                    outPath = m_outputPath
                Else
                    outPath = m_tmp_dir & String.Format(TEMP_OUT_FILE_NAME, DateTime.Now.ToString("yyyyMMddHHmmssfffffff")) & ".docx"
                End If

                ' 2015/03/03 MOD START [sabuntrail Version 1.1対応] Kitajima
                'Dim pgsForm As New ProgressForm(m_dfCreator, newPath, oldPath, outPath, m_setting)
                Dim pgsForm As New ProgressForm(m_dfCreator, newPath, oldPath, outPath, m_setting, oItems)
                ' 2015/03/03 MOD END [sabuntrail Version 1.1対応] Kitajima
                pgsForm.ShowDialog()

                If pgsForm.status = ProgressForm.PROGRESS_STATUS.CANCELED Then
                    ' ユーザにキャンセルされた場合
                    ShowInfoDialog(Message.GetInstance.GetMessage(Message.INF_CODE_0001), Message.INF_CODE_0001)
                ElseIf pgsForm.status = ProgressForm.PROGRESS_STATUS.EXCEPTION Then
                    ' 例外が発生した場合はスローする
                    Throw pgsForm.Exception
                Else
                    ' 出力パス指定がない場合はGUI操作
                    If Not m_consoleMode Then
                        Dim savePath As String = Nothing
                        savePath = SaveFileDialog(Message.GetInstance.GetMessage(Message.INF_CODE_0002), True, "docファイル|*.doc|docxファイル|*.docx")
                        If savePath IsNot Nothing Then
                            If File.Exists(savePath) Then
                                ' 指定された保存先にファイルが既にある場合は上書きするか確認する。
                                If ShowQuestionDialog(Message.GetInstance.GetMessage(Message.QUE_CODE_0001), Message.QUE_CODE_0001) = Windows.Forms.DialogResult.Yes Then
                                    ' 上書きする。
                                    If Path.GetExtension(savePath).ToLower = ".doc" Then
                                        m_dfCreator.ConvDocxToDoc(outPath, savePath)
                                    Else
                                        IO.File.Copy(outPath, savePath, True)
                                    End If
                                    ShowInfoDialog(Message.GetInstance.GetMessage(Message.INF_CODE_0003), Message.INF_CODE_0003)
                                Else
                                    ShowInfoDialog(Message.GetInstance.GetMessage(Message.INF_CODE_0001), Message.INF_CODE_0001)
                                End If
                            Else
                                ' 指定された保存先に同名ファイルがない場合はそのまま保存する。
                                If Path.GetExtension(savePath).ToLower = ".doc" Then
                                    m_dfCreator.ConvDocxToDoc(outPath, savePath)
                                Else
                                    IO.File.Copy(outPath, savePath, False)
                                End If
                                ShowInfoDialog(Message.GetInstance.GetMessage(Message.INF_CODE_0003), Message.INF_CODE_0003)
                            End If
                        End If
                    End If
                End If
            End SyncLock
        Catch ex As Exception
            ShowErrorDialog(String.Format(Message.GetInstance.GetMessage(Message.ERR_CODE_9991), ex.Message), Message.ERR_CODE_9991)
            If m_log IsNot Nothing Then
                m_log.WriteErrorLog(ex.ToString)
            End If
        Finally
            Common.DeleteFile(newPath)
            Common.DeleteFile(oldPath)
            If Not m_consoleMode Then
                Common.DeleteFile(outPath)
            End If
            Me.Enabled = True
        End Try
    End Sub

    ''' <summary>
    ''' 新ファイルの参照ボタン
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub RefNewButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefNewButton.Click, pic_NewFile.Click
        Dim newPath As String = Nothing
        newPath = OpenFileDialog(Message.GetInstance.GetMessage(Message.INF_CODE_0004), False, "docファイル|*.doc|docxファイル|*.docx")
        If newPath IsNot Nothing Then
            NewTextBox.Text = newPath
        End If
    End Sub

    ''' <summary>
    ''' 旧ファイルの参照ボタン
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub RefOldButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefOldButton.Click, pic_OldFile.Click
        Dim oldPath As String = Nothing
        oldPath = OpenFileDialog(Message.GetInstance.GetMessage(Message.INF_CODE_0005), False, "docファイル|*.doc|docxファイル|*.docx")
        If oldPath IsNot Nothing Then
            OldTextBox.Text = oldPath
        End If
    End Sub

    ''' <summary>
    ''' ファイルダイアログを開く
    ''' </summary>
    ''' <param name="title">ダイアログのキャプション</param>
    ''' <param name="AllowNotFound">存在しないファイルを許可するかどうか</param>
    ''' <param name="filter">表示する拡張子を絞り込むフィルタ</param>
    ''' <returns>選択されたパス</returns>
    ''' <remarks></remarks>
    Private Function OpenFileDialog(ByVal title As String, ByVal AllowNotFound As Boolean, ByVal filter As String) As String

        Dim rtn As String = Nothing

        ' OpenFileDialog の新しいインスタンスを生成する (デザイナから追加している場合は必要ない)
        Dim OpenFileDialog1 As New OpenFileDialog()

        ' ダイアログのタイトルを設定する
        OpenFileDialog1.Title = title

        ' ファイルのフィルタを設定する
        OpenFileDialog1.Filter = filter

        OpenFileDialog1.CheckFileExists = Not (AllowNotFound)

        ' ダイアログを表示し、戻り値が [OK] の場合は、選択したファイルパスを返す
        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            rtn = OpenFileDialog1.FileName
        End If

        ' 不要になった時点で破棄する
        OpenFileDialog1.Dispose()

        Return rtn

    End Function

    ''' <summary>
    ''' 保存ダイアログを開く
    ''' </summary>
    ''' <param name="title">ダイアログのキャプション</param>
    ''' <param name="AllowNotFound">存在しないファイルを許可するかどうか</param>
    ''' <param name="filter">表示する拡張子を絞り込むフィルタ</param>
    ''' <returns>選択されたパス</returns>
    ''' <remarks></remarks>
    Private Function SaveFileDialog(ByVal title As String, ByVal AllowNotFound As Boolean, ByVal filter As String) As String

        Dim rtn As String = Nothing

        ' OpenFileDialog の新しいインスタンスを生成する (デザイナから追加している場合は必要ない)
        Dim SaveFileDialog1 As New SaveFileDialog()

        ' ダイアログのタイトルを設定する
        SaveFileDialog1.Title = title

        ' ファイルのフィルタを設定する
        SaveFileDialog1.Filter = filter

        SaveFileDialog1.CheckFileExists = Not (AllowNotFound)

        SaveFileDialog1.OverwritePrompt = False

        ' ダイアログを表示し、戻り値が [OK] の場合は、選択したファイルパスを返す
        If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
            rtn = SaveFileDialog1.FileName
        End If

        ' 不要になった時点で破棄する
        SaveFileDialog1.Dispose()

        Return rtn

    End Function

    ''' <summary>
    ''' テキストボックスに入力があるかを確認する。
    ''' </summary>
    ''' <returns>正常：true、異常：false</returns>
    ''' <remarks></remarks>
    Private Function CheckBlank() As Boolean
        If NewTextBox.Text.Trim.Length = 0 Then
            ShowErrorDialog(Message.GetInstance.GetMessage(Message.ERR_CODE_0001), Message.ERR_CODE_0001)
            Return False
        End If

        If OldTextBox.Text.Trim.Length = 0 Then
            ShowErrorDialog(Message.GetInstance.GetMessage(Message.ERR_CODE_0002), Message.ERR_CODE_0002)
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' 拡張子が正しいかどうか確認する。
    ''' </summary>
    ''' <returns>正常：true、異常：false</returns>
    ''' <remarks></remarks>
    Private Function CheckExtention() As Boolean
        If Not (NewTextBox.Text.EndsWith(".doc", StringComparison.CurrentCultureIgnoreCase)) AndAlso Not (NewTextBox.Text.EndsWith(".docx", StringComparison.CurrentCultureIgnoreCase)) Then
            ShowErrorDialog(Message.GetInstance.GetMessage(Message.ERR_CODE_0003), Message.ERR_CODE_0003)
            Return False
        End If

        If Not (OldTextBox.Text.EndsWith(".doc", StringComparison.CurrentCultureIgnoreCase)) AndAlso Not (OldTextBox.Text.EndsWith(".docx", StringComparison.CurrentCultureIgnoreCase)) Then
            ShowErrorDialog(Message.GetInstance.GetMessage(Message.ERR_CODE_0004), Message.ERR_CODE_0004)
            Return False
        End If
        Return True
    End Function

    ''' <summary>
    ''' 存在チェック
    ''' </summary>
    ''' <returns>正常：true、異常：false</returns>
    ''' <remarks></remarks>
    Private Function CheckExists() As Boolean

        If Not (IO.File.Exists(NewTextBox.Text)) Then
            ShowErrorDialog(Message.GetInstance.GetMessage(Message.ERR_CODE_0005), Message.ERR_CODE_0005)
            Return False
        End If

        If Not (IO.File.Exists(OldTextBox.Text)) Then
            ShowErrorDialog(Message.GetInstance.GetMessage(Message.ERR_CODE_0006), Message.ERR_CODE_0006)
            Return False
        End If

        Return True

    End Function

    ''' <summary>
    ''' メンバ変数の初期化
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Initialize()
        SyncLock (GetType(Application))

            ' 非表示にしておく。
            Me.Visible = False

            ' 2015/03/02 DELETE START [sabuntrail Version 1.1対応] Kitajima
            'remarkArray = New RadioButton() {NoRemarkRadioButton, Remark1RadioButton, Remark2RadioButton}
            'sizeArray = New RadioButton() {A4SizeRadioButton, A3SizeRadioButton}
            ' 2015/03/02 DELETE END [sabuntrail Version 1.1対応] Kitajima

            ' 定義ファイルフォルダ(\なし)
            m_conf_dir = GetFileSystemPath(Environment.SpecialFolder.LocalApplicationData) & CONF_DIRECTORY

            ' ワードテンプレートフォルダ(\なし)
            m_word_dir = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) & WORD_DIRECTORY

            ' 一時フォルダ(\なし)
            m_tmp_dir = IO.Path.GetTempPath & Application.ProductName

            ' スタイルシートフォルダ(\なし)
            ' スタイルシートのコピーはインストーラーがやる
            'Dim pg_style_dir As String = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) & STYLE_DIRECTORY
            m_style_dir = GetFileSystemPath(Environment.SpecialFolder.CommonApplicationData) & STYLE_DIRECTORY

            ' ログフォルダ(\なし)
            m_log_dir = GetFileSystemPath(Environment.SpecialFolder.LocalApplicationData) & LOG_DIRECTORY

            ' ライセンスパス(\なし)
            m_lic_dir = GetFileSystemPath(Environment.SpecialFolder.CommonApplicationData) & LIC_DIRECTORY

            ' マニュアルフォルダ(\なし)
            m_man_dir = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) & MAN_DIRECTORY

            m_conf_fpath = m_conf_dir & CONFIG_FILE

            m_tmp_dir = m_tmp_dir & "\" & DateTime.Now.ToString("yyyyMMddHHmmssfffffff")

            Common.MakeDirNotFoundDir(m_tmp_dir)

            Common.MakeDirNotFoundDir(m_log_dir)

            Common.MakeDirNotFoundDir(m_lic_dir)

            Common.MakeDirNotFoundDir(m_conf_dir)

            If Not IO.File.Exists(m_conf_fpath) Then
                IO.File.Copy(IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) & CONFIG_FILE, m_conf_fpath)
            End If

            'Common.CopyDirectory(pg_style_dir, m_style_dir)

            m_setting = New Setting(m_conf_fpath)

            ' 2015/03/02 ADD START [sabuntrail Version 1.1対応] Kitajima
            ' 過去のバージョンがインストールされていた場合、使用していた出力設定ファイルが残る。
            ' インストールされた出力設定ファイルのバージョン情報と配置されている出力設定ファイルのバージョン情報が異なる場合、
            ' または出力設定ファイルにバージョン情報が存在しない場合（バージョン1.0.0.0)
            ' インストールされた出力設定ファイルで出力設定ファイルを上書きする。
            Dim installedSetting As Setting = New Setting(IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) & CONFIG_FILE)

            If m_setting.Version.Length = 0 OrElse m_setting.Version <> installedSetting.Version Then
                IO.File.Copy(IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) & CONFIG_FILE, m_conf_fpath, True)
                m_setting = New Setting(m_conf_fpath)
            End If
            ' 2015/03/02 ADD END [sabuntrail Version 1.1対応] Kitajima

            ' 2015/03/02 DELETE START [sabuntrail Version 1.1対応] Kitajima
            'DiffOnlyCheckBox.Checked = m_setting.sabun_only
            'AddAndDeleteCheckBox.Checked = m_setting.add_and_delete
            'ReverseColCheckBox.Checked = m_setting.reverse

            'remarkArray(m_setting.remark).Checked = True
            'sizeArray(m_setting.size).Checked = True

            'NewColColorImageLabel.BackColor = m_setting.newColColor
            'NewColUnderlineCheckBox.Checked = m_setting.newUnderline

            'If NewColColorImageLabel.BackColor.ToArgb = Color.Red.ToArgb Then
            '    NewColRedRadioButton.Checked = True
            'ElseIf NewColColorImageLabel.BackColor.ToArgb = Color.Blue.ToArgb Then
            '    NewColBlueRadioButton.Checked = True
            'ElseIf NewColColorImageLabel.BackColor.ToArgb = Color.Green.ToArgb Then
            '    NewColGreenRadioButton.Checked = True
            'End If

            'OldColColorImageLabel.BackColor = m_setting.oldColColor
            'OldColUnderlineCheckBox.Checked = m_setting.oldUnderline

            'If OldColColorImageLabel.BackColor.ToArgb = Color.Red.ToArgb Then
            '    OldColRedRadioButton.Checked = True
            'ElseIf OldColColorImageLabel.BackColor.ToArgb = Color.Blue.ToArgb Then
            '    OldColBlueRadioButton.Checked = True
            'ElseIf OldColColorImageLabel.BackColor.ToArgb = Color.Green.ToArgb Then
            '    OldColGreenRadioButton.Checked = True
            'End If
            ' 2015/03/02 DELETE END [sabuntrail Version 1.1対応] Kitajima

            ' 2015/03/02 ADD START [sabuntrail Version 1.1対応] Kitajima
            SettingPropertyGrid.SelectedObject = New SettingDisplayWrapper(m_setting)
            ' 2015/03/02 ADD END [sabuntrail Version 1.1対応] Kitajima

            Dim cmds As String() = System.Environment.GetCommandLineArgs()

            ' デバッグモードの対応
            ' 疑似的にコンソールアプリとして起動する。
            If ((cmds.Length = 5 AndAlso cmds(1).ToLower = "/c") OrElse
               (cmds.Length = 6 AndAlso cmds(1).ToLower = "/c" AndAlso cmds(5).ToLower = "/d") OrElse
               (cmds.Length = 6 AndAlso cmds(2).ToLower = "/c" AndAlso cmds(1).ToLower = "/d")) Then
                If cmds.Length = 6 Then
                    m_log = New LogWriter(m_log_dir, True)
                    If cmds(1).ToLower = "/c" Then
                        ConsoleMode(cmds(2), cmds(3), cmds(4))
                    Else
                        ConsoleMode(cmds(3), cmds(4), cmds(5))
                    End If
                Else
                    m_log = New LogWriter(m_log_dir)
                    ConsoleMode(cmds(2), cmds(3), cmds(4))
                End If
            ElseIf cmds.Length = 1 OrElse
                   (cmds.Length = 2 AndAlso cmds(1).ToLower = "/d") Then

                If cmds.Length = 2 Then
                    m_log = New LogWriter(m_log_dir, True)
                Else
                    m_log = New LogWriter(m_log_dir)
                End If

                m_dfCreator = New DFCreator(m_style_dir, m_word_dir, m_tmp_dir, m_log)
                Me.Visible = True
                ' ライセンス認証に失敗した場合
                Dim lic_result = Common.licCheck()
                If lic_result = Common.LIC_ENUM.NG Then
                    'If Common.licCheck(m_lic_dir & LIC_FILE) = Common.LIC_ENUM.NG Then
                    ShowErrorDialog(Message.GetInstance.GetMessage(Message.ERR_CODE_0007), Message.ERR_CODE_0007)
                    'Dim about As New DFAboutDialog(m_lic_dir & LIC_FILE)
                    Dim about As LicenceForm = New LicenceForm(m_lic_dir & LIC_FILE)
                    about.Owner = Me
                    about.ShowDialog()

                    ' キー入力がなかった場合は強制終了
                    If about.isStatus = Common.LIC_ENUM.NG Then
                        Environment.Exit(RETURN_CODE.LIC_ERROR)
                    End If

                ElseIf lic_result = Common.LIC_ENUM.OK Then
                    Me.pic_licence.Visible = False
                End If
            Else
                '引数エラー
                Environment.Exit(RETURN_CODE.ARGUMENT_ERROR)
            End If

        End SyncLock
    End Sub

    ''' <summary>
    ''' コンソール時の処理
    ''' </summary>
    ''' <param name="newfilePath">新ファイルパス</param>
    ''' <param name="oldfilepath">旧ファイルパス</param>
    ''' <param name="outputfilepath">出力ファイルパス</param>
    ''' <remarks></remarks>
    Private Sub ConsoleMode(ByVal newfilePath As String, ByVal oldfilepath As String, outputfilepath As String)

        m_consoleMode = True

        m_dfCreator = New DFCreator(m_style_dir, m_word_dir, m_tmp_dir, m_log)


        NewTextBox.Text = newfilePath
        OldTextBox.Text = oldfilepath
        m_outputPath = outputfilepath

        '        If Common.licCheck(m_lic_dir & LIC_FILE) = Common.LIC_ENUM.NG Then
        If Common.licCheck() = Common.LIC_ENUM.NG Then
            Environment.Exit(RETURN_CODE.LIC_ERROR)
        End If

        ExecuteButton_Click(Nothing, Nothing)

        Environment.Exit(RETURN_CODE.SUCCESS)

    End Sub

    ''' <summary>
    ''' サブントレイルについてダイアログ表示
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pic_licence.Click
        Try
            ' Dim about As DFAboutDialog = New DFAboutDialog(m_lic_dir & LIC_FILE)
            Dim about As LicenceForm = New LicenceForm(m_lic_dir & LIC_FILE)
            about.Owner = Me
            about.ShowDialog()
            about.Dispose()
        Catch ex As Exception
            ShowErrorDialog(String.Format(Message.GetInstance.GetMessage(Message.ERR_CODE_9993), ex.Message), Message.ERR_CODE_9993)
            If m_log IsNot Nothing Then
                m_log.WriteErrorLog(ex.ToString)
            End If
        End Try
    End Sub

    ''' <summary>
    ''' 特殊パス\会社名\製品名のパスを返す。
    ''' </summary>
    ''' <param name="folder">特殊パス（列挙体の値）</param>
    ''' <returns>パス</returns>
    ''' <remarks></remarks>
    Private Function GetFileSystemPath(ByRef folder As Environment.SpecialFolder) As String
        Dim path As String = String.Format("{0}\{1}\{2}", Environment.GetFolderPath(folder), Application.CompanyName, Application.ProductName)
        Return path
    End Function

    ' 2015/03/02 DELETE START [sabuntrail Version 1.1対応] Kitajima
    ' ''' <summary>
    ' ''' 差分のみチェックボックス変更時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub DiffOnlyCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonCheckBox_CheckedChanged(DiffOnlyCheckBox, m_setting.sabun_only)
    'End Sub

    ' ''' <summary>
    ' ''' （追加）/（削除）表示チェックボックス変更時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub AddAndDeleteCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonCheckBox_CheckedChanged(AddAndDeleteCheckBox, m_setting.add_and_delete)
    'End Sub

    ' ''' <summary>
    ' ''' 新旧逆転チェックボックス変更時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub ReverseColCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonCheckBox_CheckedChanged(ReverseColCheckBox, m_setting.reverse)
    'End Sub

    ' ''' <summary>
    ' ''' 備考なしチェック時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub NoRemarkRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonRemarkRaddioButton_CheckedChanged()
    'End Sub

    ' ''' <summary>
    ' ''' 備考欄ラジオボタン選択時の共通処理
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Private Sub CommonRemarkRaddioButton_CheckedChanged()
    '    SyncLock (GetType(Application))
    '        Dim setValue = -1
    '        For i As Integer = 0 To remarkArray.Length - 1
    '            If remarkArray(i).Checked Then
    '                setValue = i
    '            End If
    '        Next
    '        m_setting.remark = setValue
    '    End SyncLock
    'End Sub

    ' ''' <summary>
    ' ''' 備考(1)チェック時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub Remark1RadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonRemarkRaddioButton_CheckedChanged()
    'End Sub

    ' ''' <summary>
    ' ''' 備考(2)チェック時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub Remark2RadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonRemarkRaddioButton_CheckedChanged()
    'End Sub

    ' ''' <summary>
    ' ''' 用紙サイズ(A4)チェック時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub A4SizeRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonSizeRaddioButton_CheckedChanged()
    'End Sub

    ' ''' <summary>
    ' ''' 用紙サイズ（A3）チェック時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub A3SizeRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonSizeRaddioButton_CheckedChanged()
    'End Sub

    ' ''' <summary>
    ' ''' 用紙サイズ変更時の共通処理
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Private Sub CommonSizeRaddioButton_CheckedChanged()
    '    SyncLock (GetType(Application))

    '        Dim setValue = -1
    '        For i As Integer = 0 To sizeArray.Length - 1
    '            If sizeArray(i).Checked Then
    '                setValue = i
    '            End If
    '        Next
    '        m_setting.size = setValue
    '    End SyncLock
    'End Sub

    ' ''' <summary>
    ' ''' 色選択ボタン（新）押下時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub SelectNewColColorButton_Click(sender As System.Object, e As System.EventArgs)
    '    CommonColorButton_Click(NewColColorImageLabel, NewColRedRadioButton, NewColBlueRadioButton, NewColGreenRadioButton, m_setting.newColColor)
    'End Sub

    ' ''' <summary>
    ' ''' 色選択ボタン（旧）押下時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub SelectOldColColorButton_Click(sender As System.Object, e As System.EventArgs)
    '    CommonColorButton_Click(OldColColorImageLabel, OldColRedRadioButton, OldColBlueRadioButton, OldColGreenRadioButton, m_setting.oldColColor)
    'End Sub

    ' ''' <summary>
    ' ''' 色選択ボタン押下時の処理
    ' ''' </summary>
    ' ''' <param name="colImageLabel">色見本ラベル</param>
    ' ''' <param name="rRadioButton">赤ラジオボタン</param>
    ' ''' <param name="bRadioButton">青ラジオボタン</param>
    ' ''' <param name="gRadioButton">緑ラジオボタン</param>
    ' ''' <param name="settingcolor">設定情報</param>
    ' ''' <remarks></remarks>
    'Private Sub CommonColorButton_Click(ByRef colImageLabel As Label, ByRef rRadioButton As RadioButton, ByRef bRadioButton As RadioButton, ByRef gRadioButton As RadioButton, ByRef settingcolor As Color)
    '    SyncLock (GetType(Application))

    '        'ColorDialogクラスのインスタンスを作成
    '        Dim cd As New ColorDialog()

    '        'はじめに選択されている色を設定
    '        cd.Color = colImageLabel.BackColor

    '        Dim beforeColor = colImageLabel.BackColor

    '        If m_cunstomColor IsNot Nothing Then
    '            cd.CustomColors = m_cunstomColor
    '        End If
    '        '[作成した色]に指定した色（RGB値）を表示する
    '        If cd.ShowDialog() = DialogResult.OK Then
    '            '選択された色の取得
    '            colImageLabel.BackColor = cd.Color

    '            If colImageLabel.BackColor.ToArgb = Color.Red.ToArgb Then
    '                rRadioButton.Checked = True
    '            ElseIf colImageLabel.BackColor.ToArgb = Color.Blue.ToArgb Then
    '                bRadioButton.Checked = True
    '            ElseIf colImageLabel.BackColor.ToArgb = Color.Green.ToArgb Then
    '                gRadioButton.Checked = True
    '            Else
    '                If beforeColor.ToArgb = Color.Red.ToArgb Then
    '                    rRadioButton.Checked = False
    '                ElseIf beforeColor.ToArgb = Color.Blue.ToArgb Then
    '                    bRadioButton.Checked = False
    '                ElseIf beforeColor.ToArgb = Color.Green.ToArgb Then
    '                    gRadioButton.Checked = False
    '                End If
    '            End If

    '            settingcolor = cd.Color

    '        End If

    '        m_cunstomColor = cd.CustomColors
    '    End SyncLock

    'End Sub

    ' ''' <summary>
    ' ''' 新差分色（赤）ラジオボタン選択時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub NewColRedRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonColorRadioButton_Click(NewColColorImageLabel, NewColRedRadioButton, NewColBlueRadioButton, NewColGreenRadioButton, m_setting.newColColor)
    'End Sub

    ' ''' <summary>
    ' ''' 新差分色（青）ラジオボタン選択時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub NewColBlueRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonColorRadioButton_Click(NewColColorImageLabel, NewColRedRadioButton, NewColBlueRadioButton, NewColGreenRadioButton, m_setting.newColColor)
    'End Sub

    ' ''' <summary>
    ' ''' 新差分色（緑）ラジオボタン選択時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub NewColGreenRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonColorRadioButton_Click(NewColColorImageLabel, NewColRedRadioButton, NewColBlueRadioButton, NewColGreenRadioButton, m_setting.newColColor)
    'End Sub

    ' ''' <summary>
    ' ''' 色ラジオボタン変更時の共通処理
    ' ''' </summary>
    ' ''' <param name="colImageLabel">色見本ラベル</param>
    ' ''' <param name="rRadioButton">赤ラジオボタン</param>
    ' ''' <param name="bRadioButton">青ラジオボタン</param>
    ' ''' <param name="gRadioButton">緑ラジオボタン</param>
    ' ''' <param name="settingcolor">設定情報</param>
    ' ''' <remarks></remarks>
    'Private Sub CommonColorRadioButton_Click(ByRef colImageLabel As Label, ByRef rRadioButton As RadioButton, ByRef bRadioButton As RadioButton, ByRef gRadioButton As RadioButton, ByRef settingcolor As Color)
    '    SyncLock (GetType(Application))

    '        If rRadioButton.Checked Then
    '            colImageLabel.BackColor = Color.Red
    '            settingcolor = Color.Red
    '        ElseIf bRadioButton.Checked Then
    '            colImageLabel.BackColor = Color.Blue
    '            settingcolor = Color.Blue
    '        ElseIf gRadioButton.Checked Then
    '            colImageLabel.BackColor = Color.Green
    '            settingcolor = Color.Green
    '        End If

    '    End SyncLock
    'End Sub

    ' ''' <summary>
    ' ''' 旧差分色（赤）ラジオボタン選択時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub OldColRedRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonColorRadioButton_Click(OldColColorImageLabel, OldColRedRadioButton, OldColBlueRadioButton, OldColGreenRadioButton, m_setting.oldColColor)
    'End Sub

    ' ''' <summary>
    ' ''' 旧差分色（青）ラジオボタン選択時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub OldColBlueRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonColorRadioButton_Click(OldColColorImageLabel, OldColRedRadioButton, OldColBlueRadioButton, OldColGreenRadioButton, m_setting.oldColColor)
    'End Sub

    ' ''' <summary>
    ' ''' 旧差分色（緑）ラジオボタン選択時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub OldColGreenRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonColorRadioButton_Click(OldColColorImageLabel, OldColRedRadioButton, OldColBlueRadioButton, OldColGreenRadioButton, m_setting.oldColColor)
    'End Sub

    ' ''' <summary>
    ' ''' 新差分の下線チェックボックス変更時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub NewColUnderlineCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonCheckBox_CheckedChanged(NewColUnderlineCheckBox, m_setting.newUnderline)
    'End Sub

    ' ''' <summary>
    ' ''' 旧差分の下線チェックボックス変更時の処理
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub OldColUnderlineCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs)
    '    CommonCheckBox_CheckedChanged(OldColUnderlineCheckBox, m_setting.oldUnderline)
    'End Sub

    ' ''' <summary>
    ' ''' チェックボックス変更時の共通処理
    ' ''' </summary>
    ' ''' <param name="cb">変更されたチェックボックス</param>
    ' ''' <param name="setting">変更する設定情報</param>
    ' ''' <remarks></remarks>
    'Private Sub CommonCheckBox_CheckedChanged(ByRef cb As CheckBox, ByRef setting As Boolean)
    '    SyncLock (GetType(Application))
    '        setting = cb.Checked
    '    End SyncLock
    'End Sub
    ' 2015/03/02 DELETE END [sabuntrail Version 1.1対応] Kitajima

    ''' <summary>
    ''' 終了ボタンクリック時の処理
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ExitButton_Click(sender As System.Object, e As System.EventArgs) Handles ExitButton.Click, pic_Exit.Click
        Me.Dispose()
    End Sub

    ''' <summary>
    ''' 新ファイルのテキストボックスにドラッグ＆ドロップした際の処理
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub NewTextBox_DragDrop(sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles NewTextBox.DragDrop, pnl_New.DragDrop
        If NewTextBox.Text.Length <= 0 Then
            pic_New.Image = My.Resources.Resources.main_dd_new_on
        End If

        Common_DragDrop(NewTextBox, sender, e)
    End Sub

    ''' <summary>
    ''' 旧ファイルのテキストボックスにドラッグ＆ドロップした際の処理
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub OldTextBox_DragDrop(sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles OldTextBox.DragDrop, pnl_Old.DragDrop
        Common_DragDrop(OldTextBox, sender, e)
    End Sub

    ''' <summary>
    ''' ドラッグアンドドロップ時の共通処理
    ''' </summary>
    ''' <param name="tbox">ドラッグ＆ドロップした際の共通処理</param>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Common_DragDrop(ByRef tbox As TextBox, sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs)
        Dim fileName As String() = CType(
            e.Data.GetData(DataFormats.FileDrop, False),
            String())

        Dim ext As String = IO.Path.GetExtension(fileName(0)).ToLower

        If ext = ".docx" OrElse ext = ".doc" Then
            tbox.Text = fileName(0)
        End If
    End Sub

    ''' <summary>
    ''' 新ファイルのテキストボックスにドラッグした際の処理
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub NewTextBox_DragEnter(sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles NewTextBox.DragEnter, pnl_New.DragEnter
        If NewTextBox.Text.Length <= 0 Then
            pic_New.Image = My.Resources.Resources.main_dd_new_on
        End If

        Common_DragEnter(sender, e)
    End Sub

    ''' <summary>
    ''' 旧ファイルのテキストボックスにドラッグした際の処理
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub OldTextBox_DragEnter(sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles OldTextBox.DragEnter, pnl_Old.DragEnter
        If OldTextBox.Text.Length <= 0 Then
            pic_Old.Image = My.Resources.Resources.main_dd_old_on
        End If

        Common_DragEnter(sender, e)
    End Sub

    ''' <summary>
    ''' ドラッグ時の共通処理
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Common_DragEnter(sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs)
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            'ドラッグされたデータ形式を調べ、ファイルのときはコピーとする
            e.Effect = DragDropEffects.Copy
        Else
            'ファイル以外は受け付けない
            e.Effect = DragDropEffects.None
        End If
    End Sub

    ''' <summary>
    ''' 終了処理
    ''' 一時フォルダを削除する。失敗しても無視
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Finalyze() Handles Me.Disposed
        Try
            Directory.Delete(m_tmp_dir)
        Catch e As Exception
        Finally

        End Try
    End Sub

    ''' <summary>
    ''' 出力設定ボタン
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub OutputSettingButton_Click(sender As System.Object, e As System.EventArgs) Handles OutputSettingButton.Click, pic_OutputSettingButton.Click
        Try
            Dim form As New OutputSettingForm(m_conf_fpath)
            form.CustomColor = m_cunstomColor
            form.ShowDialog()
            m_cunstomColor = form.CustomColor
            If form.Changed Then
                m_setting = New Setting(m_conf_fpath)
                SettingPropertyGrid.SelectedObject = New SettingDisplayWrapper(m_setting)
            End If
        Catch ex As Exception
            ShowErrorDialog(String.Format(Message.GetInstance.GetMessage(Message.ERR_CODE_9994), ex.Message), Message.ERR_CODE_9994)
            If m_log IsNot Nothing Then
                m_log.WriteErrorLog(ex.ToString)
            End If
        Finally
        End Try
    End Sub

    Private mouseOff As Point
    Private leftFlag As Boolean
    Private Sub DFForm_MouseMove(sender As Object, e As MouseEventArgs) Handles MyBase.MouseMove
        If leftFlag Then
            Dim mouseSet As Point = Control.MousePosition
            mouseSet.Offset(mouseOff.X, mouseOff.Y)
            Location = mouseSet
        End If
    End Sub

    Private Sub DFForm_MouseDown(sender As Object, e As MouseEventArgs) Handles MyBase.MouseDown
        If e.Button = MouseButtons.Left Then
            mouseOff = New Point(-e.X, -e.Y)
            leftFlag = True
        End If
    End Sub

    Private Sub DFForm_MouseUp(sender As Object, e As MouseEventArgs) Handles MyBase.MouseUp
        If leftFlag Then
            leftFlag = False
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles pic_Hide.Click
        '最小化状態で表示するようにする
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub FileSelectGroupBox_DragDrop(sender As Object, e As DragEventArgs)
        MessageBox.Show("OK", "001", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub UsageToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles pic_Help.Click
        Try
            System.Diagnostics.Process.Start(m_man_dir & MANUAL_FILE_NAME)
        Catch ex As Exception
            ShowErrorDialog(String.Format(Message.GetInstance.GetMessage(Message.ERR_CODE_9992), ex.Message), Message.ERR_CODE_9992)
            If m_log IsNot Nothing Then
                m_log.WriteErrorLog(ex.ToString)
            End If
        Finally
        End Try
    End Sub
    ''' <summary>
    ''' ライセンス認証ボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_licence_MouseHover(sender As Object, e As EventArgs) Handles pic_licence.MouseEnter
        pic_licence.Image = My.Resources.Resources.main_licence_btn_hover
    End Sub
    ''' <summary>
    ''' ライセンス認証ボタンのマウスホバー終了
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_licence_MouseLeave(sender As Object, e As EventArgs) Handles pic_licence.MouseLeave
        pic_licence.Image = My.Resources.Resources.main_licence_btn
    End Sub
    ''' <summary>
    ''' ヘルプボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_Help_MouseHover(sender As Object, e As EventArgs) Handles pic_Help.MouseEnter
        pic_Help.Image = My.Resources.Resources.main_help_btn_hover
    End Sub
    ''' <summary>
    ''' ヘルプボタンのマウスホバー終了
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_Help_MouseLeave(sender As Object, e As EventArgs) Handles pic_Help.MouseLeave
        pic_Help.Image = My.Resources.Resources.main_help_btn
    End Sub
    ''' <summary>
    ''' 最小化ボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_Hide_MouseHover(sender As Object, e As EventArgs) Handles pic_Hide.MouseEnter
        pic_Hide.Image = My.Resources.Resources.main_small_btn_hover
    End Sub
    ''' <summary>
    ''' 最小化ボタンのマウスホバー終了
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_Hide_MouseLeave(sender As Object, e As EventArgs) Handles pic_Hide.MouseLeave
        pic_Hide.Image = My.Resources.Resources.main_small_btn
    End Sub
    ''' <summary>
    ''' クローズボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_Exit_MouseHover(sender As Object, e As EventArgs) Handles pic_Exit.MouseEnter
        pic_Exit.Image = My.Resources.Resources.main_exit_btn_hover
    End Sub
    ''' <summary>
    ''' クローズボタンのマウスホバー終了
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_Exit_MouseLeave(sender As Object, e As EventArgs) Handles pic_Exit.MouseLeave
        pic_Exit.Image = My.Resources.Resources.main_exit_btn
    End Sub

    ''' <summary>
    ''' 旧ファイルを参照ボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_OldFile_MouseHover(sender As Object, e As EventArgs) Handles pic_OldFile.MouseEnter
        pic_OldFile.Image = My.Resources.Resources.main_file_btn_hover
    End Sub
    ''' <summary>
    ''' 旧ファイルを参照ボタンのマウスホバー終了
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_OldFile_MouseLeave(sender As Object, e As EventArgs) Handles pic_OldFile.MouseLeave
        pic_OldFile.Image = My.Resources.Resources.main_file_btn
    End Sub
    ''' <summary>
    ''' 新ファイルを参照ボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_NewFile_MouseHover(sender As Object, e As EventArgs) Handles pic_NewFile.MouseEnter
        pic_NewFile.Image = My.Resources.Resources.main_file_btn_hover
    End Sub
    ''' <summary>
    ''' 新ファイルを参照ボタンのマウスホバー終了
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_NewFile_MouseLeave(sender As Object, e As EventArgs) Handles pic_NewFile.MouseLeave
        pic_NewFile.Image = My.Resources.Resources.main_file_btn
    End Sub
    ''' <summary>
    ''' 出力設定を確認・変更ボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_OutputSettingButton_MouseHover(sender As Object, e As EventArgs) Handles pic_OutputSettingButton.MouseEnter
        pic_OutputSettingButton.Image = My.Resources.Resources.main_setting_btn_hover
    End Sub
    ''' <summary>
    ''' 出力設定を確認・変更のマウスホバー終了
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_OutputSettingButton_MouseLeave(sender As Object, e As EventArgs) Handles pic_OutputSettingButton.MouseLeave
        pic_OutputSettingButton.Image = My.Resources.Resources.main_setting_btn
    End Sub
    ''' <summary>
    ''' 作成するボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_ExecuteButton_MouseHover(sender As Object, e As EventArgs) Handles pic_ExecuteButton.MouseEnter
        pic_ExecuteButton.Image = My.Resources.Resources.main_sabun_btn_hover
    End Sub
    ''' <summary>
    ''' 作成するボタンからマウスホバー終了
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pic_ExecuteButton_MouseLeave(sender As Object, e As EventArgs) Handles pic_ExecuteButton.MouseLeave
        pic_ExecuteButton.Image = My.Resources.Resources.main_sabun_btn
    End Sub
    ''' <summary>
    ''' 旧ファイル領域のドラッグ終了
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pnl_Old_DragLeave(sender As Object, e As EventArgs) Handles pnl_Old.DragLeave
        If OldTextBox.Text.Length <= 0 Then
            pic_Old.Image = My.Resources.Resources.main_dd_old
        End If
    End Sub
    ''' <summary>
    ''' 新ファイル領域のドラッグ終了
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pnl_New_DragLeave(sender As Object, e As EventArgs) Handles pnl_New.DragLeave
        If NewTextBox.Text.Length <= 0 Then
            pic_New.Image = My.Resources.Resources.main_dd_new
        End If
    End Sub

    Private Sub DFForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'pnl_main.BackColor = Color.FromArgb(100, Color.White)
    End Sub
    ''' <summary>
    ''' 旧ファイルのフルパス表示用テキストボックス内容変更時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub OldTextBox_TextChanged(sender As Object, e As EventArgs) Handles OldTextBox.TextChanged
        If OldTextBox.Text.Length > 0 Then
            pic_Old.Image = My.Resources.Resources.main_dd_selected_03

            pic_OldFile.Visible = False

            CleanSelectedOldFile.Visible = True

            OldTextBox.Visible = True

            OldFileNameTextBox.Text = Path.GetFileName(OldTextBox.Text)
            OldFileNameTextBox.Visible = True
        Else
            pic_Old.Image = My.Resources.Resources.main_dd_old

            pic_OldFile.Visible = True

            CleanSelectedOldFile.Visible = False

            OldTextBox.Visible = False

            OldFileNameTextBox.Text = String.Empty
            OldFileNameTextBox.Visible = False
        End If
    End Sub
    ''' <summary>
    ''' 新ファイルのフルパス表示用テキストボックス内容変更時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub NewTextBox_TextChanged(sender As Object, e As EventArgs) Handles NewTextBox.TextChanged
        If NewTextBox.Text.Length > 0 Then
            pic_New.Image = My.Resources.Resources.main_dd_selected_03

            pic_NewFile.Visible = False
            CleanSelectedNewFile.Visible = True
            NewTextBox.Visible = True

            NewFileNameTextBox.Text = Path.GetFileName(NewTextBox.Text)
            NewFileNameTextBox.Visible = True
        Else
            pic_New.Image = My.Resources.Resources.main_dd_new

            pic_NewFile.Visible = True

            CleanSelectedNewFile.Visible = False
            NewTextBox.Visible = False

            NewFileNameTextBox.Text = String.Empty
            NewFileNameTextBox.Visible = False
        End If
    End Sub
    ''' <summary>
    ''' 旧ファイルのクリアボタン押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub CleanSelectedOldFile_Click(sender As Object, e As EventArgs) Handles CleanSelectedOldFile.Click
        If OldTextBox.Text.Length > 0 Then
            OldTextBox.Text = String.Empty
        End If
    End Sub
    ''' <summary>
    ''' 新ファイルのクリアボタン押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub CleanSelectedNewFile_Click(sender As Object, e As EventArgs) Handles CleanSelectedNewFile.Click
        If NewTextBox.Text.Length > 0 Then
            NewTextBox.Text = String.Empty
        End If
    End Sub
End Class

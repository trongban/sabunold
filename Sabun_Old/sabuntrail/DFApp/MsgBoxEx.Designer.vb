﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MsgBoxEx
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pic_close = New System.Windows.Forms.PictureBox()
        Me.MsgText = New System.Windows.Forms.Label()
        Me.IdTxt = New System.Windows.Forms.Label()
        CType(Me.pic_close, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pic_close
        '
        Me.pic_close.BackColor = System.Drawing.Color.Snow
        Me.pic_close.Image = Global.sabuntrail.My.Resources.Resources.dialog_close_btn
        Me.pic_close.Location = New System.Drawing.Point(165, 168)
        Me.pic_close.Name = "pic_close"
        Me.pic_close.Size = New System.Drawing.Size(234, 61)
        Me.pic_close.TabIndex = 0
        Me.pic_close.TabStop = False
        '
        'MsgText
        '
        Me.MsgText.AutoEllipsis = True
        Me.MsgText.BackColor = System.Drawing.Color.Snow
        Me.MsgText.Font = New System.Drawing.Font("MS UI Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.MsgText.Location = New System.Drawing.Point(47, 67)
        Me.MsgText.Name = "MsgText"
        Me.MsgText.Size = New System.Drawing.Size(472, 98)
        Me.MsgText.TabIndex = 1
        Me.MsgText.Text = "text"
        '
        'IdTxt
        '
        Me.IdTxt.AutoEllipsis = True
        Me.IdTxt.AutoSize = True
        Me.IdTxt.BackColor = System.Drawing.Color.Snow
        Me.IdTxt.Font = New System.Drawing.Font("MS UI Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.IdTxt.Location = New System.Drawing.Point(24, 37)
        Me.IdTxt.Name = "IdTxt"
        Me.IdTxt.Size = New System.Drawing.Size(26, 19)
        Me.IdTxt.TabIndex = 1
        Me.IdTxt.Text = "ID"
        '
        'MsgBoxEx
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.sabuntrail.My.Resources.Resources.dialog_bg
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(564, 252)
        Me.ControlBox = False
        Me.Controls.Add(Me.IdTxt)
        Me.Controls.Add(Me.MsgText)
        Me.Controls.Add(Me.pic_close)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.HelpButton = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MsgBoxEx"
        Me.ShowIcon = False
        Me.TransparencyKey = System.Drawing.SystemColors.Control
        CType(Me.pic_close, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pic_close As PictureBox
    Friend WithEvents MsgText As Label
    Friend WithEvents IdTxt As Label
End Class

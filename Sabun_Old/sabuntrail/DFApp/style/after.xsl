<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                version="1.0">

  <xsl:template match="/">
      <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="*">
    <xsl:choose>
      <xsl:when test="local-name()='ins' or local-name()='del'">
        <xsl:apply-templates select ="./*"/>
      </xsl:when>
      <xsl:when test="local-name()='pPrChange' or local-name()='cellDel' or local-name()='cellIns' or local-name()='tcPrChange' or local-name()='cellMerge'">
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:copy-of select="@*"/>
          <xsl:apply-templates/>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
 
  <xsl:template match="text()">
    <xsl:value-of select="."/>
  </xsl:template>
  
</xsl:stylesheet> 
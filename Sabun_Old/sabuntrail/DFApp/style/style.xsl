﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:dgm="http://schemas.openxmlformats.org/drawingml/2006/diagram" xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" xmlns:c="http://schemas.openxmlformats.org/drawingml/2006/chart" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" version="1.0">
  <xsl:include href="variable.xsl" />
  <xsl:template match="w:hyperlink" mode="new">
    <xsl:apply-templates mode="new" />
  </xsl:template>
  <xsl:template match="w:hyperlink" mode="old">
    <xsl:apply-templates mode="old" />
  </xsl:template>
  <xsl:template name="tblTemplate">
    <w:tr>
      <w:tc>
        <w:tcPr>
          <w:tcW w:w="{$newcolwidth}" w:type="pct" />
          <w:tcBorders>
            <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
            <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
          </w:tcBorders>
        </w:tcPr>
            <xsl:choose>
              <xsl:when test="$reverse=1">
                <xsl:if test="not(count(w:tr/w:trPr/w:ins)=count(w:tr))">
                  <w:tbl>
                    <w:tblPr>
                      <xsl:copy-of select="./w:tblPr/*[local-name()!='tblW' and local-name()!='tblPrChange']" />
                      <xsl:if test="./w:tblPr/w:tblW">
                        <xsl:variable name="width" select="./w:tblPr/w:tblW/@w:w" />
                        <w:tblW>
                          <xsl:attribute name="w:w">
                            <xsl:choose>
                              <xsl:when test="$remarks&gt;0 and $pgsize=0">
                                <xsl:value-of select="$width*0.42" />
                              </xsl:when>
                              <xsl:when test="$remarks=0 and $pgsize=1">
                                <xsl:value-of select="$width" />
                              </xsl:when>
                              <xsl:when test="$remarks&gt;0 and $pgsize=1">
                                <xsl:value-of select="$width*0.92" />
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="$width*0.5" />
                              </xsl:otherwise>
                            </xsl:choose>
                          </xsl:attribute>
                          <xsl:attribute name="w:type">
                            <xsl:value-of select="./w:tblPr/w:tblW/@w:type" />
                          </xsl:attribute>
                        </w:tblW>
                      </xsl:if>
                    </w:tblPr>
                    <w:tblGrid>
                      <xsl:copy-of select="./w:tblGrid/*[local-name()!='tblGridChange']" />
                    </w:tblGrid>
                    <xsl:apply-templates select="w:tr" mode="old" />
                  </w:tbl>
                </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <xsl:if test="not(count(w:tr/w:trPr/w:del)=count(w:tr))">
                  <w:tbl>
                    <w:tblPr>
                      <xsl:copy-of select="./w:tblPr/*[local-name()!='tblW' and local-name()!='tblPrChange']" />
                      <xsl:if test="./w:tblPr/w:tblW">
                        <xsl:variable name="width" select="./w:tblPr/w:tblW/@w:w" />
                        <w:tblW>
                          <xsl:attribute name="w:w">
                            <xsl:choose>
                              <xsl:when test="$remarks&gt;0 and $pgsize=0">
                                <xsl:value-of select="$width*0.42" />
                              </xsl:when>
                              <xsl:when test="$remarks=0 and $pgsize=1">
                                <xsl:value-of select="$width" />
                              </xsl:when>
                              <xsl:when test="$remarks&gt;0 and $pgsize=1">
                                <xsl:value-of select="$width*0.92" />
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="$width*0.5" />
                              </xsl:otherwise>
                            </xsl:choose>
                          </xsl:attribute>
                          <xsl:attribute name="w:type">
                            <xsl:value-of select="./w:tblPr/w:tblW/@w:type" />
                          </xsl:attribute>
                        </w:tblW>
                      </xsl:if>
                    </w:tblPr>
                    <w:tblGrid>
                      <xsl:copy-of select="./w:tblGrid/*[local-name()!='tblGridChange']" />
                    </w:tblGrid>
                    <xsl:apply-templates select="w:tr" mode="new" />
                  </w:tbl>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
        <w:p>
          <w:pPr>
            <w:keepNext />
            <w:rPr>
              <w:rFonts w:hint="eastAsia" />
            </w:rPr>
          </w:pPr>
        </w:p>
      </w:tc>
      <w:tc>
        <w:tcPr>
          <w:tcW w:w="{$oldcolwidth}" w:type="pct" />
          <w:tcBorders>
            <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
            <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
          </w:tcBorders>
        </w:tcPr>
            <xsl:choose>
              <xsl:when test="$reverse=1">
                <xsl:if test="not(count(w:tr/w:trPr/w:del)=count(w:tr))">
                  <w:tbl>
                    <w:tblPr>
                      <xsl:copy-of select="./w:tblPr/*[local-name()!='tblW' and local-name()!='tblPrChange']" />
                      <xsl:if test="./w:tblPr/w:tblW">
                        <xsl:variable name="width" select="./w:tblPr/w:tblW/@w:w" />
                        <w:tblW>
                          <xsl:attribute name="w:w">
                            <xsl:choose>
                              <xsl:when test="$remarks&gt;0 and $pgsize=0">
                                <xsl:value-of select="$width*0.42" />
                              </xsl:when>
                              <xsl:when test="$remarks=0 and $pgsize=1">
                                <xsl:value-of select="$width" />
                              </xsl:when>
                              <xsl:when test="$remarks&gt;0 and $pgsize=1">
                                <xsl:value-of select="$width*0.92" />
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="$width*0.5" />
                              </xsl:otherwise>
                            </xsl:choose>
                          </xsl:attribute>
                          <xsl:attribute name="w:type">
                            <xsl:value-of select="./w:tblPr/w:tblW/@w:type" />
                          </xsl:attribute>
                        </w:tblW>
                      </xsl:if>
                    </w:tblPr>
                    <w:tblGrid>
                      <xsl:copy-of select="./w:tblGrid/*[local-name()!='tblGridChange']" />
                    </w:tblGrid>
                    <xsl:apply-templates select="w:tr" mode="new" />
                  </w:tbl>
                </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <xsl:if test="not(count(w:tr/w:trPr/w:ins)=count(w:tr))">
                  <w:tbl>
                    <w:tblPr>
                      <xsl:copy-of select="./w:tblPr/*[local-name()!='tblW' and local-name()!='tblPrChange']" />
                      <xsl:if test="./w:tblPr/w:tblW">
                        <xsl:variable name="width" select="./w:tblPr/w:tblW/@w:w" />
                        <w:tblW>
                          <xsl:attribute name="w:w">
                            <xsl:choose>
                              <xsl:when test="$remarks&gt;0 and $pgsize=0">
                                <xsl:value-of select="$width*0.42" />
                              </xsl:when>
                              <xsl:when test="$remarks=0 and $pgsize=1">
                                <xsl:value-of select="$width" />
                              </xsl:when>
                              <xsl:when test="$remarks&gt;0 and $pgsize=1">
                                <xsl:value-of select="$width*0.92" />
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="$width*0.5" />
                              </xsl:otherwise>
                            </xsl:choose>
                          </xsl:attribute>
                          <xsl:attribute name="w:type">
                            <xsl:value-of select="./w:tblPr/w:tblW/@w:type" />
                          </xsl:attribute>
                        </w:tblW>
                      </xsl:if>
                    </w:tblPr>
                    <w:tblGrid>
                      <xsl:copy-of select="./w:tblGrid/*[local-name()!='tblGridChange']" />
                    </w:tblGrid>
                    <xsl:apply-templates select="w:tr" mode="old" />
                  </w:tbl>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
        <w:p>
          <w:pPr>
            <w:keepNext />
            <w:rPr>
              <w:rFonts w:hint="eastAsia" />
            </w:rPr>
          </w:pPr>
        </w:p>
      </w:tc>
      <xsl:if test="$remarks&gt;0">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="{$remark1colwidth}" w:type="pct" />
            <xsl:if test="$remarks&gt;0">
              <w:tcBorders>
                <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
              </w:tcBorders>
            </xsl:if>
          </w:tcPr>
          <w:p>
            <w:pPr>
              <w:jc w:val="left" />
              <w:keepNext />
              <w:rPr>
                <w:rFonts w:hint="eastAsia" />
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
      </xsl:if>
      <xsl:if test="$remarks&gt;1">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="{$remark2colwidth}" w:type="pct" />
            <xsl:if test="$remarks&gt;1">
              <w:tcBorders>
                <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
              </w:tcBorders>
            </xsl:if>
          </w:tcPr>
          <w:p>
            <w:pPr>
              <w:jc w:val="left" />
              <w:keepNext />
              <w:rPr>
                <w:rFonts w:hint="eastAsia" />
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
      </xsl:if>
    </w:tr>
  </xsl:template>
  <xsl:template match="w:tbl[parent::w:body]">
    <xsl:choose>
      <xsl:when test="$sabunonly=1">
        <xsl:choose>
          <xsl:when test=".//w:ins or .//w:del">
            <xsl:call-template name="tblTemplate" />
          </xsl:when>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="tblTemplate" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="w:sdt|m:oMath|m:oMathPara|*[ancestor::m:oMathPara or ancestor::m:oMath or ancestor::w:sdt]" mode="new" priority="0.8">
    <xsl:copy>
      <xsl:copy-of select="./@*" />
      <xsl:apply-templates mode="new" />
    </xsl:copy>
  </xsl:template>
    <xsl:template match="w:pPr[ancestor::m:oMathPara or ancestor::m:oMath or ancestor::w:sdt]" mode="new" priority="0.9">
    <xsl:copy>
      <xsl:copy-of select="./*[local-name()!='pPrChange' and local-name()!='rPr']" />
      <xsl:apply-templates select="./w:rPr" mode="new" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:rPr[ancestor::m:oMathPara or ancestor::m:oMath or ancestor::w:sdt]" mode="new" priority="0.9">
    <xsl:copy>
      <xsl:copy-of select="./*[local-name()!='rPrChange']" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:pPr[ancestor::m:oMathPara or ancestor::m:oMath or ancestor::w:sdt]" mode="old" priority="0.9">
    <xsl:choose>
      <xsl:when test="./w:pPrChange">
        <xsl:copy>
          <xsl:copy-of select="./w:pPrChange/w:pPr/*[local-name()!='rPr']" />
          <xsl:apply-templates select="./w:pPrChange/w:pPr/w:rPr" mode="old" />
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:copy-of select="./*[local-name()!='rPr']" />
          <xsl:apply-templates select="./w:rPr" mode="old" />
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="w:rPr[ancestor::m:oMathPara or ancestor::m:oMath or ancestor::w:sdt]" mode="old" priority="0.9">
    <xsl:choose>
      <xsl:when test="./w:rPrChange">
        <xsl:copy>
          <xsl:copy-of select="./w:rPrChange/w:rPr/*" />
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:copy-of select="./*" />
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="w:ins[ancestor::m:oMathPara or ancestor::m:oMath or ancestor::w:sdt]" mode="new" priority="0.9">
    <xsl:apply-templates select="./*" mode="new" />
  </xsl:template>
  <xsl:template match="w:del[ancestor::m:oMathPara or ancestor::m:oMath or ancestor::w:sdt]" mode="new" priority="0.9">
  </xsl:template>
  <xsl:template match="m:r[./w:del]" mode="new" priority="0.9">
  </xsl:template>
  <xsl:template match="m:rad[./m:radPr/m:ctrlPr/w:del]|m:limLow[./m:limLowPr/m:ctrlPr/w:del]|m:nary[./m:naryPr/m:ctrlPr/w:del]|m:sSup[./m:sSupPr/m:ctrlPr/w:del]|m:func[./m:funcPr/m:ctrlPr/w:del]|m:sSub[./m:sSubPr/m:ctrlPr/w:del]|m:f[./m:fPr/m:ctrlPr/w:del]|m:d[./m:dPr/m:ctrlPr/w:del]" mode="new" priority="0.9">
  </xsl:template>
  <xsl:template match="w:sdt|m:oMath|m:oMathPara|*[ancestor::m:oMathPara or ancestor::m:oMath or ancestor::w:sdt]" mode="old" priority="0.8">
    <xsl:copy>
      <xsl:copy-of select="./@*" />
      <xsl:apply-templates mode="old" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:ins[ancestor::m:oMathPara or ancestor::m:oMath or ancestor::w:sdt]" mode="old" priority="0.9">
  </xsl:template>
  <xsl:template match="w:del[ancestor::m:oMathPara or ancestor::m:oMath or ancestor::w:sdt]" mode="old" priority="0.9">
    <xsl:apply-templates select="./*" mode="old" />
  </xsl:template>
  <xsl:template match="m:r[./w:ins]" mode="old" priority="0.9">
  </xsl:template>
  <xsl:template match="m:rad[./m:radPr/m:ctrlPr/w:ins]|m:limLow[./m:limLowPr/m:ctrlPr/w:ins]|m:nary[./m:naryPr/m:ctrlPr/w:ins]|m:sSup[./m:sSupPr/m:ctrlPr/w:ins]|m:func[./m:funcPr/m:ctrlPr/w:ins]|m:sSub[./m:sSubPr/m:ctrlPr/w:ins]|m:f[./m:fPr/m:ctrlPr/w:ins]|m:d[./m:dPr/m:ctrlPr/w:ins]" mode="old" priority="0.9">
  </xsl:template>
  <xsl:template match="w:tc" mode="new">
    <w:tc>
      <w:tcPr>
        <xsl:copy-of select="./w:tcPr/*[local-name()!='tcPrChange' and local-name()!='tcW']" />
        <xsl:if test="./w:tcPr/w:tcW">
          <xsl:variable name="width" select="./w:tcPr/w:tcW/@w:w" />
          <w:tcW>
            <xsl:attribute name="w:w">
              <xsl:choose>
                <xsl:when test="$remarks&gt;0 and $pgsize=0">
                  <xsl:value-of select="$width*0.42" />
                </xsl:when>
                <xsl:when test="$remarks=0 and $pgsize=1">
                  <xsl:value-of select="$width" />
                </xsl:when>
                <xsl:when test="$remarks&gt;0 and $pgsize=1">
                  <xsl:value-of select="$width*0.92" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$width*0.5" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="w:type">
              <xsl:value-of select="./w:tcPr/w:tcW/@w:type" />
            </xsl:attribute>
          </w:tcW>
        </xsl:if>
      </w:tcPr>
      <xsl:apply-templates mode="new" />
      <w:p>
        <w:pPr>
          <w:keepNext />
          <w:rPr>
            <w:rFonts w:hint="eastAsia" />
          </w:rPr>
        </w:pPr>
      </w:p>
    </w:tc>
  </xsl:template>
  <xsl:template match="w:tc" mode="old">
    <w:tc>
      <w:tcPr>
        <xsl:copy-of select="./w:tcPr/*[local-name()!='tcPrChange' and local-name()!='tcW']" />
        <xsl:if test="./w:tcPr/w:tcW">
          <xsl:variable name="width" select="./w:tcPr/w:tcW/@w:w" />
          <w:tcW>
            <xsl:attribute name="w:w">
              <xsl:choose>
                <xsl:when test="$remarks&gt;0 and $pgsize=0">
                  <xsl:value-of select="$width*0.42" />
                </xsl:when>
                <xsl:when test="$remarks=0 and $pgsize=1">
                  <xsl:value-of select="$width" />
                </xsl:when>
                <xsl:when test="$remarks&gt;0 and $pgsize=1">
                  <xsl:value-of select="$width*0.92" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$width*0.5" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="w:type">
              <xsl:value-of select="./w:tcPr/w:tcW/@w:type" />
            </xsl:attribute>
          </w:tcW>
        </xsl:if>
      </w:tcPr>
      <xsl:apply-templates mode="old" />
      <w:p>
        <w:pPr>
          <w:keepNext />
          <w:rPr>
            <w:rFonts w:hint="eastAsia" />
          </w:rPr>
        </w:pPr>
      </w:p>
    </w:tc>
  </xsl:template>
  <xsl:template match="w:tr" mode="new">
    <xsl:copy>
      <xsl:apply-templates select="w:tc" mode="new" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:tr" mode="old">
    <xsl:copy>
      <xsl:apply-templates select="w:tc" mode="old" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:tbl" mode="new">
    <xsl:if test="not(count(w:tr/w:trPr/w:del)=count(w:tr))">
      <xsl:copy>
        <w:tblPr>
          <xsl:copy-of select="./w:tblPr/*[local-name()!='tblW' and local-name()!='tblPrChange']" />
          <xsl:if test="./w:tblPr/w:tblW">
            <xsl:variable name="width" select="./w:tblPr/w:tblW/@w:w" />
            <w:tblW>
              <xsl:attribute name="w:w">
                <xsl:choose>
                  <xsl:when test="$remarks&gt;0 and $pgsize=0">
                    <xsl:value-of select="$width*0.42" />
                  </xsl:when>
                  <xsl:when test="$remarks=0 and $pgsize=1">
                    <xsl:value-of select="$width" />
                  </xsl:when>
                  <xsl:when test="$remarks&gt;0 and $pgsize=1">
                    <xsl:value-of select="$width*0.92" />
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$width*0.5" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
              <xsl:attribute name="w:type">
                <xsl:value-of select="./w:tblPr/w:tblW/@w:type" />
              </xsl:attribute>
            </w:tblW>
          </xsl:if>
        </w:tblPr>
        <w:tblGrid>
          <xsl:copy-of select="./w:tblGrid/*[local-name()!='tblGridChange']" />
        </w:tblGrid>
        <xsl:apply-templates select="w:tr" mode="new" />
      </xsl:copy>
    </xsl:if>
  </xsl:template>
  <xsl:template match="w:tbl" mode="old">
    <xsl:if test="not(count(w:tr/w:trPr/w:ins)=count(w:tr))">
      <xsl:copy>
        <w:tblPr>
          <xsl:copy-of select="./w:tblPr/*[local-name()!='tblW' and local-name()!='tblPrChange']" />
          <xsl:if test="./w:tblPr/w:tblW">
            <xsl:variable name="width" select="./w:tblPr/w:tblW/@w:w" />
            <w:tblW>
              <xsl:attribute name="w:w">
                <xsl:choose>
                  <xsl:when test="$remarks&gt;0 and $pgsize=0">
                    <xsl:value-of select="$width*0.42" />
                  </xsl:when>
                  <xsl:when test="$remarks=0 and $pgsize=1">
                    <xsl:value-of select="$width" />
                  </xsl:when>
                  <xsl:when test="$remarks&gt;0 and $pgsize=1">
                    <xsl:value-of select="$width*0.92" />
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$width*0.5" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
              <xsl:attribute name="w:type">
                <xsl:value-of select="./w:tblPr/w:tblW/@w:type" />
              </xsl:attribute>
            </w:tblW>
          </xsl:if>
        </w:tblPr>
        <w:tblGrid>
          <xsl:copy-of select="./w:tblGrid/*[local-name()!='tblGridChange']" />
        </w:tblGrid>
        <xsl:apply-templates select="w:tr" mode="old" />
      </xsl:copy>
    </xsl:if>
  </xsl:template>
  <xsl:template match="w:t" mode="new">
    <w:t>
      <xsl:value-of select="." />
    </w:t>
  </xsl:template>
  <xsl:template match="w:t|w:delText" mode="old">
    <w:t>
      <xsl:value-of select="." />
    </w:t>
  </xsl:template>
  <xsl:template match="w:fldChar" mode="new">
    <xsl:copy-of select="." />
  </xsl:template>
  <xsl:template match="w:instrText" mode="new">
    <xsl:copy>
      <xsl:apply-templates mode="new" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:delInstrText" mode="new">
    <w:instrText>
      <xsl:value-of select="text()" />
    </w:instrText>
  </xsl:template>
  <xsl:template match="w:instrText[ancestor::w:del]" mode="new">
  </xsl:template>
  <xsl:template match="w:delInstrText[ancestor::w:del]" mode="new">
  </xsl:template>
  <xsl:template match="w:fldChar" mode="old">
    <xsl:copy-of select="." />
  </xsl:template>
  <xsl:template match="w:instrText" mode="old">
    <xsl:copy>
      <xsl:apply-templates mode="old" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:delInstrText" mode="old">
    <w:instrText>
      <xsl:value-of select="text()" />
    </w:instrText>
  </xsl:template>
  <xsl:template match="w:delInstrText[ancestor::w:ins]" mode="old">
  </xsl:template>
  <xsl:template match="w:instrText[ancestor::w:ins]" mode="old">
  </xsl:template>
  <xsl:template match="w:ruby" mode="new">
    <xsl:copy>
      <xsl:apply-templates mode="new" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:rubyPr" mode="new">
    <xsl:copy-of select="." />
  </xsl:template>
  <xsl:template match="w:rt" mode="new">
    <xsl:copy>
      <xsl:apply-templates mode="new" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:rubyBase" mode="new">
    <xsl:copy>
      <xsl:apply-templates mode="new" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:ruby" mode="old">
    <xsl:copy>
      <xsl:apply-templates mode="old" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:rubyPr" mode="old">
    <xsl:copy-of select="." />
  </xsl:template>
  <xsl:template match="w:rt" mode="old">
    <xsl:copy>
      <xsl:apply-templates mode="old" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:rubyBase" mode="old">
    <xsl:copy>
      <xsl:apply-templates mode="old" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="*" mode="new">
  </xsl:template>
  <xsl:template match="*" mode="old">
  </xsl:template>
  <xsl:template match="w:ins" mode="new">
    <xsl:apply-templates mode="new" select="./*" />
  </xsl:template>
  <xsl:template match="w:del" mode="new">
  </xsl:template>
  <xsl:template match="w:ins" mode="old">
  </xsl:template>
  <xsl:template match="w:del" mode="old">
    <xsl:apply-templates mode="old" select="./*" />
  </xsl:template>
  <xsl:template match="mc:AlternateContent" mode="new">
    <xsl:apply-templates select="./mc:Fallback/*" mode="new" />
  </xsl:template>
  <xsl:template match="mc:AlternateContent" mode="old">
    <xsl:apply-templates select="./mc:Fallback/*" mode="old" />
  </xsl:template>
  <xsl:template match="w:object" mode="new">
    <w:t>[オブジェクトは表示できません]</w:t>
  </xsl:template>
  <xsl:template match="w:object" mode="old">
    <w:t>[オブジェクトは表示できません]</w:t>
  </xsl:template>
  <xsl:template match="w:pict" mode="new">
    <xsl:choose>
      <xsl:when test=".//o:OLEObject">
        <w:t>[オブジェクトは表示できません]</w:t>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates mode="new" />
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="*[ancestor::w:pict]" mode="new" priority="0.9">
    <xsl:copy>
      <xsl:copy-of select="./@*" />
      <xsl:apply-templates mode="new" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:pPr[ancestor::w:pict]" mode="new" priority="0.9">
    <xsl:copy>
      <xsl:copy-of select="./*[local-name()!='pPrChange' and local-name()!='rPr']" />
      <xsl:apply-templates select="./w:rPr" mode="new" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:rPr[ancestor::w:pict]" mode="new" priority="0.9">
    <xsl:copy>
      <xsl:copy-of select="./*[local-name()!='rPrChange']" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:pict" mode="old">
    <xsl:choose>
      <xsl:when test=".//o:OLEObject">
        <w:t>[オブジェクトは表示できません]</w:t>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates mode="old" />
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="*[ancestor::w:pict]" mode="old" priority="0.8">
    <xsl:copy>
      <xsl:copy-of select="./@*" />
      <xsl:apply-templates mode="old" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:pPr[ancestor::w:pict]" mode="old" priority="0.9">
    <xsl:choose>
      <xsl:when test="./w:pPrChange">
        <xsl:copy>
          <xsl:copy-of select="./w:pPrChange/w:pPr/*[local-name()!='rPr']" />
          <xsl:apply-templates select="./w:pPrChange/w:pPr/w:rPr" mode="old" />
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:copy-of select="./*[local-name()!='rPr']" />
          <xsl:apply-templates select="./w:rPr" mode="old" />
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="w:rPr[ancestor::w:pict]" mode="old" priority="0.9">
    <xsl:choose>
      <xsl:when test="./w:rPrChange">
        <xsl:copy>
          <xsl:copy-of select="./w:rPrChange/w:rPr/*" />
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:copy-of select="./*" />
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="w:t[not(ancestor::w:del) and ancestor::w:pict]" mode="new" priority="0.9">
    <w:t>
      <xsl:value-of select="." />
    </w:t>
  </xsl:template>
  <xsl:template match="w:t[not(ancestor::w:ins) and ancestor::w:pict] | w:delText[ancestor::w:pict]" mode="old" priority="0.9">
    <w:t>
      <xsl:value-of select="." />
    </w:t>
  </xsl:template>
  <xsl:template match="w:t[ancestor::w:del and ancestor::w:pict] | w:delText[ancestor::w:pict]" mode="new" priority="0.9">
  </xsl:template>
  <xsl:template match="w:t[ancestor::w:ins and ancestor::w:pict]" mode="old" priority="0.9">
  </xsl:template>
  <xsl:template match="w:drawing" mode="new">
    <xsl:choose>
      <xsl:when test=".//dgm:relIds">
        <w:t>[SmartArtは表示できません]</w:t>
      </xsl:when>
      <xsl:when test=".//c:chart">
        <w:t>[グラフは表示できません]</w:t>
      </xsl:when>
      <xsl:when test=".//a:hlinkClick">
        <w:t>[ハイパーリンクを含む図、クリップアートは表示できません]</w:t>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="." />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="w:drawing" mode="old">
    <xsl:choose>
      <xsl:when test=".//dgm:relIds">
        <w:t>[SmartArtは表示できません]</w:t>
      </xsl:when>
      <xsl:when test=".//c:chart">
        <w:t>[グラフは表示できません]</w:t>
      </xsl:when>
      <xsl:when test=".//a:hlinkClick">
        <w:t>[ハイパーリンクを含む図、クリップアートは表示できません]</w:t>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="." />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="w:r" mode="new">
    <w:r>
      <xsl:apply-templates mode="new" />
    </w:r>
  </xsl:template>
  <xsl:template match="w:r" mode="old">
    <w:r>
      <xsl:apply-templates mode="old" />
    </w:r>
  </xsl:template>
  <xsl:template match="w:r[parent::w:ins]" mode="new">
    <w:r>
        <w:rPr>
          <xsl:copy-of select="./w:rPr/*[local-name()!='color' and local-name()!='u' and local-name()!='rPrChange']" />
          <w:color w:val="{$newcolor}" />
          <xsl:choose>
            <xsl:when test="$newunderline=1">
              <w:u w:val="single" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="./w:rPr/w:u" />
            </xsl:otherwise>
          </xsl:choose>
        </w:rPr>
      <xsl:apply-templates select="./*[local-name()!='rPr']" mode="new" />
    </w:r>
  </xsl:template>
  <xsl:template match="w:r[preceding-sibling::w:del[1]/w:r[position()=last()]/w:fldChar/@w:fldCharType='separate']" mode="new">
  </xsl:template>
  <xsl:template match="w:r[preceding-sibling::w:ins[1]/w:r[position()=last()]/w:fldChar/@w:fldCharType='separate']" mode="old">
  </xsl:template>
  <xsl:template match="w:r[parent::w:ins]" mode="old">
  </xsl:template>
  <xsl:template match="w:r[parent::w:del]" mode="new">
  </xsl:template>
  <xsl:template match="w:r[parent::w:del]" mode="old">
    <w:r>
        <w:rPr>
          <xsl:choose>
            <xsl:when test="./w:rPr/w:rPrChange">
              <xsl:variable name="style" select="./w:rPr/w:rPrChange/w:rPr/w:rStyle/@w:val" />
              <xsl:copy-of select="./w:rPr/w:rPrChange/w:rPr/*[(local-name()!='color') and (local-name()!='u') and (local-name()!='strike') and (local-name()!='dstrike') and (local-name()!='rStyle')]" />
              <xsl:if test="./w:rPr/w:rPrChange/w:rPr/w:rStyle">
                <w:rStyle>
                  <xsl:attribute name="w:val">
                    <xsl:value-of select="concat('old_',$style)" />
                  </xsl:attribute>
                </w:rStyle>
              </xsl:if>
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="./w:rPr/*[(local-name()!='color') and (local-name()!='u') and (local-name()!='strike') and (local-name()!='dstrike')]" />
            </xsl:otherwise>
          </xsl:choose>
          <w:color w:val="{$oldcolor}" />
          <xsl:choose>
            <xsl:when test="$oldstrike=1">  
            <w:strike />
            </xsl:when>
            <xsl:otherwise>
              <xsl:choose>
                <xsl:when test="./w:rPr/w:rPrChange">
                  <xsl:variable name="style" select="./w:rPr/w:rPrChange/w:rPr/w:rStyle/@w:val" />
                  <xsl:copy-of select="./w:rPr/w:rPrChange/w:rPr/*[(local-name()='strike') or (local-name()='dstrike')]" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:copy-of select="./w:rPr/*[(local-name()='strike') or (local-name()='dstrike')]" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="$oldunderline=1">
              <w:u w:val="single" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="./w:rPr/w:u" />
            </xsl:otherwise>
          </xsl:choose>
        </w:rPr>
      <xsl:apply-templates select="./*[local-name()!='rPr']" mode="old" />
    </w:r>
  </xsl:template>
  <xsl:template match="w:rPr" mode="new">
    <xsl:copy>
      <xsl:copy-of select="./*[local-name()!='rPrChange']" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:rPr" mode="old">
    <xsl:choose>
      <xsl:when test="./w:rPrChange">
        <xsl:variable name="style" select="./w:rPrChange/w:rPr/w:rStyle/@w:val" />
        <xsl:copy>
          <xsl:copy-of select="./w:rPrChange/w:rPr/*[local-name()!='rStyle']" />
          <xsl:if test="./w:rPrChange/w:rPr/w:rStyle">
            <w:rStyle>
              <xsl:attribute name="w:val">
                <xsl:value-of select="concat('old_',$style)" />
              </xsl:attribute>
            </w:rStyle>
          </xsl:if>       
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:copy-of select="./*" />
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="w:pPr" mode="new">
    <xsl:copy>
      <xsl:copy-of select="./*[local-name()!='rPr' and local-name()!='keepNext' and local-name()!='sectPr' and local-name()!='pPrChange' and local-name()!='rPr']" />
      <xsl:apply-templates select="./w:rPr" mode="new" />
      <w:keepNext />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:pPr" mode="old">
    <xsl:copy>
      <xsl:choose>
        <xsl:when test="./w:pPrChange">
          <xsl:variable name="style" select="./w:pPrChange/w:pPr/w:pStyle/@w:val" />
          <xsl:copy-of select="./w:pPrChange/w:pPr/*[local-name()!='pStyle' and local-name()!='keepNext' and local-name()!='sectPr' and local-name()!='rPr']" />
          <xsl:apply-templates select="./w:pPrChange/w:pPr/w:rPr" mode="old" />
          <xsl:if test="./w:pPrChange/w:pPr/w:pStyle">
            <w:pStyle>
              <xsl:attribute name="w:val">
                <xsl:value-of select="concat('old_',$style)" />
              </xsl:attribute>
            </w:pStyle>
          </xsl:if>
        </xsl:when>
        <xsl:otherwise>
          <xsl:variable name="style" select="./w:pStyle/@w:val" />
          <xsl:copy-of select="./*[local-name()!='pStyle' and local-name()!='keepNext' and local-name()!='sectPr' and local-name()!='rPr']" />
          <xsl:apply-templates select="./w:rPr" mode="old" />
          <xsl:choose>
            <xsl:when test="./w:pStyle">
              <w:pStyle>
                <xsl:attribute name="w:val">
                  <xsl:value-of select="concat('old_',$style)" />
                </xsl:attribute>
              </w:pStyle>
            </xsl:when>
            <xsl:otherwise>
              <w:pStyle>
                <xsl:attribute name="w:val">
                  <xsl:value-of select="$oldnormal" />
                </xsl:attribute>
              </w:pStyle>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
      <w:keepNext />
    </xsl:copy>
  </xsl:template>
  <xsl:template name="newTemplate">
    <xsl:choose>
      <xsl:when test="./w:del and not(./w:ins) and (count(./*[local-name()='del'])=count(./*[local-name()!='pPr']))">
        <xsl:choose>
          <xsl:when test="$addanddelete=1">
            <w:pPr>
              <w:jc w:val="center" />
              <w:keepNext />
              <w:rPr>
                <w:rFonts w:hint="eastAsia" />
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="eastAsia" />
              </w:rPr>
              <w:t>(削除)</w:t>
            </w:r>
          </xsl:when>
          <xsl:otherwise>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates mode="new" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template> 
  <xsl:template name="oldTemplate">
    <xsl:choose>
      <xsl:when test="./w:ins and not(./w:del) and (count(./*[local-name()='ins'])=count(./*[local-name()!='pPr']))">
        <xsl:choose>
          <xsl:when test="$addanddelete=1">
            <w:pPr>
              <w:jc w:val="center" />
              <w:keepNext />
              <w:rPr>
                <w:rFonts w:hint="eastAsia" />
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="eastAsia" />
              </w:rPr>
              <w:t>(追加)</w:t>
            </w:r>
          </xsl:when>
          <xsl:otherwise>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates mode="old" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="paraTemplate">
    <w:tr w:rsidR="00AA5F04" w:rsidTr="00AA5F04">
      <w:tc>
        <w:tcPr>
          <w:tcW w:w="{$newcolwidth}" w:type="pct" />
          <w:tcBorders>
            <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
            <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
          </w:tcBorders>
        </w:tcPr>
        <w:p>
          <xsl:choose>
            <xsl:when test="$reverse=1">
              <xsl:call-template name="oldTemplate" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="newTemplate" />
            </xsl:otherwise>
          </xsl:choose>
        </w:p>
      </w:tc>
      <w:tc>
        <w:tcPr>
          <w:tcW w:w="{$oldcolwidth}" w:type="pct" />
          <w:tcBorders>
            <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
            <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
          </w:tcBorders>
        </w:tcPr>
        <w:p>
          <xsl:choose>
            <xsl:when test="$reverse=1">
              <xsl:call-template name="newTemplate" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="oldTemplate" />
            </xsl:otherwise>
          </xsl:choose>
        </w:p>
      </w:tc>
      <xsl:if test="$remarks&gt;0">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="{$remark1colwidth}" w:type="pct" />
            <xsl:if test="$remarks&gt;0">
              <w:tcBorders>
                <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
              </w:tcBorders>
            </xsl:if>
          </w:tcPr>
          <w:p>
            <w:pPr>
              <w:jc w:val="left" />
              <w:keepNext />
              <w:rPr>
                <w:rFonts w:hint="eastAsia" />
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
      </xsl:if>
      <xsl:if test="$remarks&gt;1">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="{$remark2colwidth}" w:type="pct" />
            <xsl:if test="$remarks&gt;0">
              <w:tcBorders>
                <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
              </w:tcBorders>
            </xsl:if>
          </w:tcPr>
          <w:p>
            <w:pPr>
              <w:jc w:val="left" />
              <w:keepNext />
              <w:rPr>
                <w:rFonts w:hint="eastAsia" />
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
      </xsl:if>
    </w:tr>
  </xsl:template>
  <xsl:template match="w:p" mode="new">
    <xsl:copy>
      <xsl:apply-templates mode="new" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:p" mode="old">
    <xsl:copy>
      <xsl:apply-templates mode="old" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="w:p[parent::w:body]">
    <xsl:choose>
      <xsl:when test="$sabunonly=1">
        <xsl:choose>
          <xsl:when test=".//w:ins or .//w:del">
            <xsl:call-template name="paraTemplate" />
          </xsl:when>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="paraTemplate" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="w:document">
    <w:document xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml">
      <w:body>
        <w:tbl>
          <w:tblPr>
            <w:tblStyle w:val="tf_a3" />
            <w:tblW w:w="{$tblwidth}" w:type="pct" />
            <w:tblLayout w:type="fixed" />
            <w:tblLook w:val="04A0" />
            <w:tblBorders>
              <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto" />
              <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto" />
            </w:tblBorders>
          </w:tblPr>
          <w:tblGrid>
            <w:gridCol w:w="{$newcolwidth}" />
            <w:gridCol w:w="{$oldcolwidth}" />
            <xsl:if test="$remarks&gt;0">
              <w:gridCol w:w="{$remark1colwidth}" />
            </xsl:if>
            <xsl:if test="$remarks&gt;1">
              <w:gridCol w:w="{$remark2colwidth}" />
            </xsl:if>
          </w:tblGrid>
          <w:tr>
            <w:trPr>
              <w:tblHeader />
            </w:trPr>
            <w:tc>
              <w:tcPr>
                <w:tcW w:w="{$newcolwidth}" w:type="pct" />
                <w:tcBorders>
                  <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                </w:tcBorders>
              </w:tcPr>
              <w:p>
                <w:pPr>
                  <w:jc w:val="center" />
                  <w:keepNext />
                  <w:rPr>
                    <w:rFonts w:hint="eastAsia" />
                  </w:rPr>
                </w:pPr>
                <w:r>
                  <w:rPr>
                    <w:rFonts w:hint="eastAsia" />
                  </w:rPr>
                  <xsl:choose>
                    <xsl:when test="$reverse=1">
                      <w:t>旧</w:t>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:t>新</w:t>
                    </xsl:otherwise>
                  </xsl:choose>
                </w:r>
              </w:p>
            </w:tc>
            <w:tc>
              <w:tcPr>
                <w:tcW w:w="{$oldcolwidth}" w:type="pct" />
                <w:tcBorders>
                  <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                </w:tcBorders>
              </w:tcPr>
              <w:p>
                <w:pPr>
                  <w:jc w:val="center" />
                  <w:keepNext />
                  <w:rPr>
                    <w:rFonts w:hint="eastAsia" />
                  </w:rPr>
                </w:pPr>
                <w:r>
                  <w:rPr>
                    <w:rFonts w:hint="eastAsia" />
                  </w:rPr>
                  <xsl:choose>
                    <xsl:when test="$reverse=1">
                      <w:t>新</w:t>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:t>旧</w:t>
                    </xsl:otherwise>
                  </xsl:choose>
                </w:r>
              </w:p>
            </w:tc>
            <xsl:if test="$remarks&gt;0">
              <w:tc>
                <w:tcPr>
                  <w:tcW w:w="{$remark1colwidth}" w:type="pct" />
                  <w:tcBorders>
                    <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  </w:tcBorders>
                </w:tcPr>
                <w:p>
                  <w:pPr>
                    <w:jc w:val="center" />
                    <w:keepNext />
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                  </w:pPr>
                  <w:r>
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                    <xsl:choose>
                      <xsl:when test="$remarks&gt;0">
                        <w:t>備考</w:t>
                      </xsl:when>
                      <xsl:when test="$remarks&gt;1">
                        <w:t>備考1</w:t>
                      </xsl:when>
                    </xsl:choose>
                  </w:r>
                </w:p>
              </w:tc>
            </xsl:if>
            <xsl:if test="$remarks&gt;1">
              <w:tc>
                <w:tcPr>
                  <w:tcW w:w="{$remark2colwidth}" w:type="pct" />
                  <w:tcBorders>
                    <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  </w:tcBorders>
                </w:tcPr>
                <w:p>
                  <w:pPr>
                    <w:jc w:val="center" />
                    <w:keepNext />
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                  </w:pPr>
                  <w:r>
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                    <xsl:choose>
                      <xsl:when test="$remarks&gt;1">
                        <w:t>備考2</w:t>
                      </xsl:when>
                      <xsl:otherwise>
                        <w:t />
                      </xsl:otherwise>
                    </xsl:choose>
                  </w:r>
                </w:p>
              </w:tc>
            </xsl:if>
          </w:tr>
          <xsl:if test="$newfolderpath!=''">
          <w:tr>
            <w:tc>
              <w:tcPr>
                <w:tcW w:w="{$newcolwidth}" w:type="pct" />
                <w:tcBorders>
                  <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                </w:tcBorders>
              </w:tcPr>
              <w:p>
                <w:pPr>
                  <w:jc w:val="left" />
                  <w:keepNext />
                  <w:wordWrap w:val="0"/>
                  <w:rPr>
                    <w:rFonts w:hint="eastAsia" />
                  </w:rPr>
                </w:pPr>
                <w:r>
                  <w:rPr>
                    <w:rFonts w:hint="eastAsia" />
                  </w:rPr>
                  <xsl:choose>
                    <xsl:when test="$reverse=1">
                      <w:t>フォルダ名：<xsl:value-of select="$oldfolderpath"/></w:t>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:t>フォルダ名：<xsl:value-of select="$newfolderpath"/></w:t>
                    </xsl:otherwise>
                  </xsl:choose>
                </w:r>
              </w:p>
            </w:tc>
            <w:tc>
              <w:tcPr>
                <w:tcW w:w="{$oldcolwidth}" w:type="pct" />
                <w:tcBorders>
                  <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                </w:tcBorders>
              </w:tcPr>
              <w:p>
                <w:pPr>
                  <w:jc w:val="left" />
                  <w:keepNext />
                  <w:wordWrap w:val="0"/>
                  <w:rPr>
                    <w:rFonts w:hint="eastAsia" />
                  </w:rPr>
                </w:pPr>
                <w:r>
                  <w:rPr>
                    <w:rFonts w:hint="eastAsia" />
                  </w:rPr>
                  <xsl:choose>
                    <xsl:when test="$reverse=1">
                      <w:t>フォルダ名：<xsl:value-of select="$newfolderpath"/></w:t>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:t>フォルダ名：<xsl:value-of select="$oldfolderpath"/></w:t>
                    </xsl:otherwise>
                  </xsl:choose>
                </w:r>
              </w:p>
            </w:tc>
            <xsl:if test="$remarks&gt;0">
              <w:tc>
                <w:tcPr>
                  <w:tcW w:w="{$remark1colwidth}" w:type="pct" />
                  <w:tcBorders>
                    <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  </w:tcBorders>
                </w:tcPr>
                <w:p>
                  <w:pPr>
                    <w:jc w:val="left" />
                    <w:keepNext />
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                  </w:pPr>
                  <w:r>
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                    <xsl:choose>
                      <xsl:when test="$remarks&gt;0">
                        <w:t />
                      </xsl:when>
                      <xsl:when test="$remarks&gt;1">
                        <w:t />
                      </xsl:when>
                    </xsl:choose>
                  </w:r>
                </w:p>
              </w:tc>
            </xsl:if>
            <xsl:if test="$remarks&gt;1">
              <w:tc>
                <w:tcPr>
                  <w:tcW w:w="{$remark2colwidth}" w:type="pct" />
                  <w:tcBorders>
                    <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  </w:tcBorders>
                </w:tcPr>
                <w:p>
                  <w:pPr>
                    <w:jc w:val="left" />
                    <w:keepNext />
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                  </w:pPr>
                  <w:r>
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                    <xsl:choose>
                      <xsl:when test="$remarks&gt;1">
                        <w:t />
                      </xsl:when>
                      <xsl:otherwise>
                        <w:t />
                      </xsl:otherwise>
                    </xsl:choose>
                  </w:r>
                </w:p>
              </w:tc>
            </xsl:if>
          </w:tr>
          </xsl:if>
          <xsl:if test="$newfilename!=''">
            <w:tr>
              <w:tc>
                <w:tcPr>
                  <w:tcW w:w="{$newcolwidth}" w:type="pct" />
                  <w:tcBorders>
                    <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  </w:tcBorders>
                </w:tcPr>
                <w:p>
                  <w:pPr>
                    <w:jc w:val="left" />
                    <w:keepNext />
                    <w:wordWrap w:val="0"/>
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                  </w:pPr>
                  <w:r>
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                    <xsl:choose>
                      <xsl:when test="$reverse=1">
                        <w:t>
                          ファイル名：<xsl:value-of select="$oldfilename"/>
                        </w:t>
                      </xsl:when>
                      <xsl:otherwise>
                        <w:t>
                          ファイル名：<xsl:value-of select="$newfilename"/>
                        </w:t>
                      </xsl:otherwise>
                    </xsl:choose>
                  </w:r>
                </w:p>
              </w:tc>
              <w:tc>
                <w:tcPr>
                  <w:tcW w:w="{$oldcolwidth}" w:type="pct" />
                  <w:tcBorders>
                    <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  </w:tcBorders>
                </w:tcPr>
                <w:p>
                  <w:pPr>
                    <w:jc w:val="left" />
                    <w:keepNext />
                    <w:wordWrap w:val="0"/>
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                  </w:pPr>
                  <w:r>
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                    <xsl:choose>
                      <xsl:when test="$reverse=1">
                        <w:t>
                          ファイル名：<xsl:value-of select="$newfilename"/>
                        </w:t>
                      </xsl:when>
                      <xsl:otherwise>
                        <w:t>
                          ファイル名：<xsl:value-of select="$oldfilename"/>
                        </w:t>
                      </xsl:otherwise>
                    </xsl:choose>
                  </w:r>
                </w:p>
              </w:tc>
              <xsl:if test="$remarks&gt;0">
                <w:tc>
                  <w:tcPr>
                    <w:tcW w:w="{$remark1colwidth}" w:type="pct" />
                    <w:tcBorders>
                      <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    </w:tcBorders>
                  </w:tcPr>
                  <w:p>
                    <w:pPr>
                      <w:jc w:val="left" />
                      <w:keepNext />
                      <w:rPr>
                        <w:rFonts w:hint="eastAsia" />
                      </w:rPr>
                    </w:pPr>
                    <w:r>
                      <w:rPr>
                        <w:rFonts w:hint="eastAsia" />
                      </w:rPr>
                      <xsl:choose>
                        <xsl:when test="$remarks&gt;0">
                          <w:t />
                        </xsl:when>
                        <xsl:when test="$remarks&gt;1">
                          <w:t />
                        </xsl:when>
                      </xsl:choose>
                    </w:r>
                  </w:p>
                </w:tc>
              </xsl:if>
              <xsl:if test="$remarks&gt;1">
                <w:tc>
                  <w:tcPr>
                    <w:tcW w:w="{$remark2colwidth}" w:type="pct" />
                    <w:tcBorders>
                      <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    </w:tcBorders>
                  </w:tcPr>
                  <w:p>
                    <w:pPr>
                      <w:jc w:val="left" />
                      <w:keepNext />
                      <w:rPr>
                        <w:rFonts w:hint="eastAsia" />
                      </w:rPr>
                    </w:pPr>
                    <w:r>
                      <w:rPr>
                        <w:rFonts w:hint="eastAsia" />
                      </w:rPr>
                      <xsl:choose>
                        <xsl:when test="$remarks&gt;1">
                          <w:t />
                        </xsl:when>
                        <xsl:otherwise>
                          <w:t />
                        </xsl:otherwise>
                      </xsl:choose>
                    </w:r>
                  </w:p>
                </w:tc>
              </xsl:if>
            </w:tr>
          </xsl:if>
          <xsl:if test="$newupdateddate!=''">
            <w:tr>
              <w:tc>
                <w:tcPr>
                  <w:tcW w:w="{$newcolwidth}" w:type="pct" />
                  <w:tcBorders>
                    <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  </w:tcBorders>
                </w:tcPr>
                <w:p>
                  <w:pPr>
                    <w:jc w:val="left" />
                    <w:keepNext />
                    <w:wordWrap w:val="0"/>
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                  </w:pPr>
                  <w:r>
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                    <xsl:choose>
                      <xsl:when test="$reverse=1">
                        <w:t>
                          更新日時：<xsl:value-of select="$oldupdateddate"/>
                        </w:t>
                      </xsl:when>
                      <xsl:otherwise>
                        <w:t>
                          更新日時：<xsl:value-of select="$newupdateddate"/>
                        </w:t>
                      </xsl:otherwise>
                    </xsl:choose>
                  </w:r>
                </w:p>
              </w:tc>
              <w:tc>
                <w:tcPr>
                  <w:tcW w:w="{$oldcolwidth}" w:type="pct" />
                  <w:tcBorders>
                    <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  </w:tcBorders>
                </w:tcPr>
                <w:p>
                  <w:pPr>
                    <w:jc w:val="left" />
                    <w:keepNext />
                    <w:wordWrap w:val="0"/>
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                  </w:pPr>
                  <w:r>
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                    <xsl:choose>
                      <xsl:when test="$reverse=1">
                        <w:t>
                          更新日時：<xsl:value-of select="$newupdateddate"/>
                        </w:t>
                      </xsl:when>
                      <xsl:otherwise>
                        <w:t>
                          更新日時：<xsl:value-of select="$oldupdateddate"/>
                        </w:t>
                      </xsl:otherwise>
                    </xsl:choose>
                  </w:r>
                </w:p>
              </w:tc>
              <xsl:if test="$remarks&gt;0">
                <w:tc>
                  <w:tcPr>
                    <w:tcW w:w="{$remark1colwidth}" w:type="pct" />
                    <w:tcBorders>
                      <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    </w:tcBorders>
                  </w:tcPr>
                  <w:p>
                    <w:pPr>
                      <w:jc w:val="left" />
                      <w:keepNext />
                      <w:rPr>
                        <w:rFonts w:hint="eastAsia" />
                      </w:rPr>
                    </w:pPr>
                    <w:r>
                      <w:rPr>
                        <w:rFonts w:hint="eastAsia" />
                      </w:rPr>
                      <xsl:choose>
                        <xsl:when test="$remarks&gt;0">
                          <w:t />
                        </xsl:when>
                        <xsl:when test="$remarks&gt;1">
                          <w:t />
                        </xsl:when>
                      </xsl:choose>
                    </w:r>
                  </w:p>
                </w:tc>
              </xsl:if>
              <xsl:if test="$remarks&gt;1">
                <w:tc>
                  <w:tcPr>
                    <w:tcW w:w="{$remark2colwidth}" w:type="pct" />
                    <w:tcBorders>
                      <w:top w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    </w:tcBorders>
                  </w:tcPr>
                  <w:p>
                    <w:pPr>
                      <w:jc w:val="left" />
                      <w:keepNext />
                      <w:rPr>
                        <w:rFonts w:hint="eastAsia" />
                      </w:rPr>
                    </w:pPr>
                    <w:r>
                      <w:rPr>
                        <w:rFonts w:hint="eastAsia" />
                      </w:rPr>
                      <xsl:choose>
                        <xsl:when test="$remarks&gt;1">
                          <w:t />
                        </xsl:when>
                        <xsl:otherwise>
                          <w:t />
                        </xsl:otherwise>
                      </xsl:choose>
                    </w:r>
                  </w:p>
                </w:tc>
              </xsl:if>
            </w:tr>
          </xsl:if>
          <xsl:apply-templates />
          <w:tr>
            <w:tc>
              <w:tcPr>
                <w:tcW w:w="{$newcolwidth}" w:type="pct" />
                <w:tcBorders>
                  <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                </w:tcBorders>
              </w:tcPr>
              <w:p>
                <w:pPr>
                  <w:jc w:val="center" />
                  <w:keepNext />
                  <w:rPr>
                    <w:rFonts w:hint="eastAsia" />
                  </w:rPr>
                </w:pPr>
              </w:p>
            </w:tc>
            <w:tc>
              <w:tcPr>
                <w:tcW w:w="{$oldcolwidth}" w:type="pct" />
                <w:tcBorders>
                  <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                  <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                </w:tcBorders>
              </w:tcPr>
              <w:p>
                <w:pPr>
                  <w:keepNext />
                  <w:rPr>
                    <w:rFonts w:hint="eastAsia" />
                  </w:rPr>
                </w:pPr>
              </w:p>
            </w:tc>
            <xsl:if test="$remarks&gt;0">
              <w:tc>
                <w:tcPr>
                  <w:tcW w:w="{$remark1colwidth}" w:type="pct" />
                  <xsl:if test="$remarks&gt;0">
                    <w:tcBorders>
                      <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    </w:tcBorders>
                  </xsl:if>
                </w:tcPr>
                <w:p>
                  <w:pPr>
                    <w:jc w:val="center" />
                    <w:keepNext />
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                  </w:pPr>
                </w:p>
              </w:tc>
            </xsl:if>
            <xsl:if test="$remarks&gt;1">
              <w:tc>
                <w:tcPr>
                  <w:tcW w:w="{$remark2colwidth}" w:type="pct" />
                  <xsl:if test="$remarks&gt;1">
                    <w:tcBorders>
                      <w:right w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000" />
                      <w:left w:val="single" w:sz="4" w:space="0" w:color="000000" />
                    </w:tcBorders>
                  </xsl:if>
                </w:tcPr>
                <w:p>
                  <w:pPr>
                    <w:jc w:val="center" />
                    <w:keepNext />
                    <w:rPr>
                      <w:rFonts w:hint="eastAsia" />
                    </w:rPr>
                  </w:pPr>
                </w:p>
              </w:tc>
            </xsl:if>
          </w:tr>
        </w:tbl>
        <w:sectPr>
          <xsl:choose>
            <xsl:when test="$pgsize=1">
              <w:pgSz w:w="{$A3Width}" w:h="{$A3Height}" w:orient="landscape" />
            </xsl:when>
            <xsl:otherwise>
              <w:pgSz w:w="{$A4Width}" w:h="{$A4Height}" w:orient="landscape" />
            </xsl:otherwise>
          </xsl:choose>
          <w:pgMar w:top="1440" w:right="1077" w:bottom="1440" w:left="1077" w:header="851" w:footer="992" w:gutter="0" />
          <w:cols w:space="425" />
          <w:docGrid w:type="lines" w:linePitch="360" />
        </w:sectPr>
      </w:body>
    </w:document>
  </xsl:template>
</xsl:stylesheet>
﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:variable name="newcolor" select="'#008000'" />
  <xsl:variable name="oldcolor" select="'#FF0000'" />
  <xsl:variable name="newunderline" select="1" />
  <xsl:variable name="oldunderline" select="1" />
  <xsl:variable name="sabunonly" select="0" />
  <xsl:variable name="addanddelete" select="1" />
  <xsl:variable name="reverse" select="0" />
  <xsl:variable name="remarks" select="0" />
  <xsl:variable name="tblwidth" select="5000" />
  <xsl:variable name="newcolwidth" select="2500" />
  <xsl:variable name="oldcolwidth" select="2500" />
  <xsl:variable name="remark1colwidth" select="0" />
  <xsl:variable name="remark2colwidth" select="0" />
  <xsl:variable name="pgsize" select="1" />
  <xsl:variable name="A4Width" select="16838" />
  <xsl:variable name="A4Height" select="11906" />
  <xsl:variable name="A3Width" select="23814" />
  <xsl:variable name="A3Height" select="16840" />
  <xsl:variable name="oldnormal" select="old_a" />
  <xsl:variable name="newfolderpath" select="''"/>
  <xsl:variable name="oldfolderpath" select="''"/>
  <xsl:variable name="newfilename" select="''"/>
  <xsl:variable name="oldfilename" select="''"/>
  <xsl:variable name="newupdateddate" select="''"/>
  <xsl:variable name="oldupdateddate" select="''"/>
  <xsl:variable name="oldstrike" select="1" />
</xsl:stylesheet>
﻿''' <summary>
''' 新旧対比表に出力するデータを格納するクラス。
''' Setting.xmlに書き出さないが
''' 対比表生成に必要なデータをメイン画面から引き渡したい場合は
''' このクラスのメンバとして定義して引き渡す。
''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
''' </summary>
''' <remarks></remarks>
Public Class OutputItems

    ''' <summary>
    ''' 新ファイルのフォルダパス
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_newFolderPath As String = ""


    ''' <summary>
    ''' 旧ファイルのフォルダパス
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_oldFolderPath As String = ""

    ''' <summary>
    ''' 新ファイルの名前
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_newFileName As String = ""

    ''' <summary>
    ''' 旧ファイルの名前
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_oldFileName As String = ""

    ''' <summary>
    ''' 新ファイルの更新日時
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_newUpdatedDate As String = ""

    ''' <summary>
    ''' 旧ファイルの更新日時
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_oldUpdatedDate As String = ""

    ''' <summary>
    ''' 新ファイルのフォルダパス
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value>新ファイルのフォルダパス</value>
    ''' <returns>新ファイルのフォルダパス</returns>
    ''' <remarks></remarks>
    Public Property NewFolderPath As String
        Get
            Return m_newFolderPath
        End Get
        Set(value As String)
            m_newFolderPath = value
        End Set
    End Property

    ''' <summary>
    ''' 旧ファイルのフォルダパス
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value>旧ファイルのフォルダパス</value>
    ''' <returns>旧ファイルのフォルダパス</returns>
    ''' <remarks></remarks>
    Public Property OldFolderPath As String
        Get
            Return m_oldFolderPath
        End Get
        Set(value As String)
            m_oldFolderPath = value
        End Set
    End Property

    ''' <summary>
    ''' 新ファイルのファイル名
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value>新ファイルのファイル名</value>
    ''' <returns>新ファイルのファイル名</returns>
    ''' <remarks></remarks>
    Public Property NewFileName As String
        Get
            Return m_newFileName
        End Get
        Set(value As String)
            m_newFileName = value
        End Set
    End Property

    ''' <summary>
    ''' 旧ファイルのフォルダパス
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value>旧ファイルのファイル名</value>
    ''' <returns>旧ファイルのファイル名</returns>
    ''' <remarks></remarks>
    Public Property OldFileName As String
        Get
            Return m_oldFileName
        End Get
        Set(value As String)
            m_oldFileName = value
        End Set
    End Property

    ''' <summary>
    ''' 新ファイルの更新日時
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value>新ファイルの更新日時</value>
    ''' <returns>新ファイルの更新日時</returns>
    ''' <remarks></remarks>
    Public Property NewUpdatedDate As String
        Get
            Return m_newUpdatedDate
        End Get
        Set(value As String)
            m_newUpdatedDate = value
        End Set
    End Property

    ''' <summary>
    ''' 旧ファイルの更新日時
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value>旧ファイルの更新日時</value>
    ''' <returns>旧ファイルの更新日時</returns>
    ''' <remarks></remarks>
    Public Property OldUpdatedDate As String
        Get
            Return m_oldUpdatedDate
        End Get
        Set(value As String)
            m_oldUpdatedDate = value
        End Set
    End Property

End Class

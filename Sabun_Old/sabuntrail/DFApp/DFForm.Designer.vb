﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class DFForm
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DFForm))
        Me.NewTextBox = New System.Windows.Forms.TextBox()
        Me.RefOldButton = New System.Windows.Forms.Button()
        Me.RefNewButton = New System.Windows.Forms.Button()
        Me.ExecuteButton = New System.Windows.Forms.Button()
        Me.ExitButton = New System.Windows.Forms.Button()
        Me.OutputSettingButton = New System.Windows.Forms.Button()
        Me.SettingPropertyGrid = New System.Windows.Forms.PropertyGrid()
        Me.pic_OutputSettingButton = New System.Windows.Forms.PictureBox()
        Me.pic_ExecuteButton = New System.Windows.Forms.PictureBox()
        Me.pic_NewFile = New System.Windows.Forms.PictureBox()
        Me.pic_OldFile = New System.Windows.Forms.PictureBox()
        Me.pic_Exit = New System.Windows.Forms.PictureBox()
        Me.pic_Hide = New System.Windows.Forms.PictureBox()
        Me.pic_Help = New System.Windows.Forms.PictureBox()
        Me.OldTextBox = New System.Windows.Forms.TextBox()
        Me.pnl_Old = New System.Windows.Forms.Panel()
        Me.OldFileNameTextBox = New System.Windows.Forms.TextBox()
        Me.CleanSelectedOldFile = New System.Windows.Forms.Panel()
        Me.pic_Old = New System.Windows.Forms.PictureBox()
        Me.pnl_New = New System.Windows.Forms.Panel()
        Me.NewFileNameTextBox = New System.Windows.Forms.TextBox()
        Me.CleanSelectedNewFile = New System.Windows.Forms.Panel()
        Me.pic_New = New System.Windows.Forms.PictureBox()
        Me.pic_licence = New System.Windows.Forms.PictureBox()
        Me.pnl_main = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        CType(Me.pic_OutputSettingButton, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_ExecuteButton, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_NewFile, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_OldFile, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_Exit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_Hide, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_Help, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl_Old.SuspendLayout()
        CType(Me.pic_Old, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl_New.SuspendLayout()
        CType(Me.pic_New, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_licence, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NewTextBox
        '
        Me.NewTextBox.AllowDrop = True
        Me.NewTextBox.BackColor = System.Drawing.Color.White
        Me.NewTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NewTextBox.CausesValidation = False
        Me.NewTextBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.NewTextBox.Enabled = False
        Me.NewTextBox.Font = New System.Drawing.Font("MS UI Gothic", 9.0!)
        Me.NewTextBox.ForeColor = System.Drawing.Color.DimGray
        Me.NewTextBox.Location = New System.Drawing.Point(50, 181)
        Me.NewTextBox.MaxLength = 260
        Me.NewTextBox.Multiline = True
        Me.NewTextBox.Name = "NewTextBox"
        Me.NewTextBox.ReadOnly = True
        Me.NewTextBox.Size = New System.Drawing.Size(210, 87)
        Me.NewTextBox.TabIndex = 0
        Me.NewTextBox.TabStop = False
        Me.NewTextBox.Visible = False
        '
        'RefOldButton
        '
        Me.RefOldButton.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.RefOldButton.Location = New System.Drawing.Point(294, 521)
        Me.RefOldButton.Name = "RefOldButton"
        Me.RefOldButton.Size = New System.Drawing.Size(60, 20)
        Me.RefOldButton.TabIndex = 3
        Me.RefOldButton.Text = "参照"
        Me.RefOldButton.UseVisualStyleBackColor = True
        Me.RefOldButton.Visible = False
        '
        'RefNewButton
        '
        Me.RefNewButton.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.RefNewButton.Location = New System.Drawing.Point(360, 519)
        Me.RefNewButton.Name = "RefNewButton"
        Me.RefNewButton.Size = New System.Drawing.Size(60, 20)
        Me.RefNewButton.TabIndex = 1
        Me.RefNewButton.Text = "参照"
        Me.RefNewButton.UseVisualStyleBackColor = True
        Me.RefNewButton.Visible = False
        '
        'ExecuteButton
        '
        Me.ExecuteButton.Location = New System.Drawing.Point(96, 520)
        Me.ExecuteButton.Name = "ExecuteButton"
        Me.ExecuteButton.Size = New System.Drawing.Size(60, 20)
        Me.ExecuteButton.TabIndex = 4
        Me.ExecuteButton.Text = "作成する"
        Me.ExecuteButton.UseVisualStyleBackColor = True
        Me.ExecuteButton.Visible = False
        '
        'ExitButton
        '
        Me.ExitButton.Location = New System.Drawing.Point(228, 520)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(60, 20)
        Me.ExitButton.TabIndex = 3
        Me.ExitButton.Text = "終了する"
        Me.ExitButton.UseVisualStyleBackColor = True
        Me.ExitButton.Visible = False
        '
        'OutputSettingButton
        '
        Me.OutputSettingButton.Location = New System.Drawing.Point(162, 521)
        Me.OutputSettingButton.Name = "OutputSettingButton"
        Me.OutputSettingButton.Size = New System.Drawing.Size(60, 20)
        Me.OutputSettingButton.TabIndex = 2
        Me.OutputSettingButton.Text = "出力設定"
        Me.OutputSettingButton.UseVisualStyleBackColor = True
        Me.OutputSettingButton.Visible = False
        '
        'SettingPropertyGrid
        '
        Me.SettingPropertyGrid.CategoryForeColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.SettingPropertyGrid.HelpVisible = False
        Me.SettingPropertyGrid.LineColor = System.Drawing.SystemColors.ControlDark
        Me.SettingPropertyGrid.Location = New System.Drawing.Point(30, 520)
        Me.SettingPropertyGrid.Name = "SettingPropertyGrid"
        Me.SettingPropertyGrid.PropertySort = System.Windows.Forms.PropertySort.Categorized
        Me.SettingPropertyGrid.Size = New System.Drawing.Size(60, 20)
        Me.SettingPropertyGrid.TabIndex = 1
        Me.SettingPropertyGrid.ToolbarVisible = False
        Me.SettingPropertyGrid.Visible = False
        '
        'pic_OutputSettingButton
        '
        Me.pic_OutputSettingButton.BackColor = System.Drawing.Color.White
        Me.pic_OutputSettingButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pic_OutputSettingButton.Image = Global.sabuntrail.My.Resources.Resources.main_setting_btn
        Me.pic_OutputSettingButton.Location = New System.Drawing.Point(70, 449)
        Me.pic_OutputSettingButton.Name = "pic_OutputSettingButton"
        Me.pic_OutputSettingButton.Size = New System.Drawing.Size(176, 44)
        Me.pic_OutputSettingButton.TabIndex = 29
        Me.pic_OutputSettingButton.TabStop = False
        '
        'pic_ExecuteButton
        '
        Me.pic_ExecuteButton.BackColor = System.Drawing.Color.White
        Me.pic_ExecuteButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pic_ExecuteButton.Image = Global.sabuntrail.My.Resources.Resources.main_sabun_btn
        Me.pic_ExecuteButton.Location = New System.Drawing.Point(285, 430)
        Me.pic_ExecuteButton.Name = "pic_ExecuteButton"
        Me.pic_ExecuteButton.Size = New System.Drawing.Size(235, 63)
        Me.pic_ExecuteButton.TabIndex = 28
        Me.pic_ExecuteButton.TabStop = False
        '
        'pic_NewFile
        '
        Me.pic_NewFile.BackColor = System.Drawing.SystemColors.Control
        Me.pic_NewFile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pic_NewFile.Image = Global.sabuntrail.My.Resources.Resources.main_file_btn
        Me.pic_NewFile.Location = New System.Drawing.Point(130, 230)
        Me.pic_NewFile.Name = "pic_NewFile"
        Me.pic_NewFile.Size = New System.Drawing.Size(118, 30)
        Me.pic_NewFile.TabIndex = 27
        Me.pic_NewFile.TabStop = False
        '
        'pic_OldFile
        '
        Me.pic_OldFile.BackColor = System.Drawing.SystemColors.Control
        Me.pic_OldFile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pic_OldFile.Image = Global.sabuntrail.My.Resources.Resources.main_file_btn
        Me.pic_OldFile.Location = New System.Drawing.Point(130, 230)
        Me.pic_OldFile.Name = "pic_OldFile"
        Me.pic_OldFile.Size = New System.Drawing.Size(118, 30)
        Me.pic_OldFile.TabIndex = 26
        Me.pic_OldFile.TabStop = False
        '
        'pic_Exit
        '
        Me.pic_Exit.BackColor = System.Drawing.Color.Transparent
        Me.pic_Exit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pic_Exit.Image = Global.sabuntrail.My.Resources.Resources.main_exit_btn
        Me.pic_Exit.Location = New System.Drawing.Point(665, 12)
        Me.pic_Exit.Name = "pic_Exit"
        Me.pic_Exit.Size = New System.Drawing.Size(38, 40)
        Me.pic_Exit.TabIndex = 23
        Me.pic_Exit.TabStop = False
        '
        'pic_Hide
        '
        Me.pic_Hide.BackColor = System.Drawing.Color.Transparent
        Me.pic_Hide.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pic_Hide.Image = Global.sabuntrail.My.Resources.Resources.main_small_btn
        Me.pic_Hide.Location = New System.Drawing.Point(621, 12)
        Me.pic_Hide.Name = "pic_Hide"
        Me.pic_Hide.Size = New System.Drawing.Size(38, 40)
        Me.pic_Hide.TabIndex = 22
        Me.pic_Hide.TabStop = False
        '
        'pic_Help
        '
        Me.pic_Help.BackColor = System.Drawing.Color.Transparent
        Me.pic_Help.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pic_Help.Image = Global.sabuntrail.My.Resources.Resources.main_help_btn
        Me.pic_Help.Location = New System.Drawing.Point(577, 12)
        Me.pic_Help.Name = "pic_Help"
        Me.pic_Help.Size = New System.Drawing.Size(38, 40)
        Me.pic_Help.TabIndex = 21
        Me.pic_Help.TabStop = False
        '
        'OldTextBox
        '
        Me.OldTextBox.AllowDrop = True
        Me.OldTextBox.BackColor = System.Drawing.Color.White
        Me.OldTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.OldTextBox.CausesValidation = False
        Me.OldTextBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.OldTextBox.Enabled = False
        Me.OldTextBox.Font = New System.Drawing.Font("MS UI Gothic", 9.0!)
        Me.OldTextBox.ForeColor = System.Drawing.Color.DimGray
        Me.OldTextBox.Location = New System.Drawing.Point(50, 181)
        Me.OldTextBox.MaxLength = 260
        Me.OldTextBox.Multiline = True
        Me.OldTextBox.Name = "OldTextBox"
        Me.OldTextBox.ReadOnly = True
        Me.OldTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.OldTextBox.Size = New System.Drawing.Size(210, 87)
        Me.OldTextBox.TabIndex = 0
        Me.OldTextBox.TabStop = False
        Me.OldTextBox.Visible = False
        '
        'pnl_Old
        '
        Me.pnl_Old.AllowDrop = True
        Me.pnl_Old.BackColor = System.Drawing.SystemColors.Control
        Me.pnl_Old.Controls.Add(Me.OldFileNameTextBox)
        Me.pnl_Old.Controls.Add(Me.pic_OldFile)
        Me.pnl_Old.Controls.Add(Me.OldTextBox)
        Me.pnl_Old.Controls.Add(Me.CleanSelectedOldFile)
        Me.pnl_Old.Controls.Add(Me.pic_Old)
        Me.pnl_Old.Location = New System.Drawing.Point(70, 125)
        Me.pnl_Old.Name = "pnl_Old"
        Me.pnl_Old.Size = New System.Drawing.Size(315, 288)
        Me.pnl_Old.TabIndex = 30
        '
        'OldFileNameTextBox
        '
        Me.OldFileNameTextBox.AllowDrop = True
        Me.OldFileNameTextBox.BackColor = System.Drawing.Color.White
        Me.OldFileNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.OldFileNameTextBox.CausesValidation = False
        Me.OldFileNameTextBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.OldFileNameTextBox.Font = New System.Drawing.Font("MS UI Gothic", 14.0!, System.Drawing.FontStyle.Bold)
        Me.OldFileNameTextBox.ForeColor = System.Drawing.Color.SteelBlue
        Me.OldFileNameTextBox.Location = New System.Drawing.Point(117, 94)
        Me.OldFileNameTextBox.MaxLength = 260
        Me.OldFileNameTextBox.Multiline = True
        Me.OldFileNameTextBox.Name = "OldFileNameTextBox"
        Me.OldFileNameTextBox.ReadOnly = True
        Me.OldFileNameTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.OldFileNameTextBox.Size = New System.Drawing.Size(143, 86)
        Me.OldFileNameTextBox.TabIndex = 28
        Me.OldFileNameTextBox.TabStop = False
        Me.OldFileNameTextBox.Text = "old_file_name"
        Me.OldFileNameTextBox.Visible = False
        '
        'CleanSelectedOldFile
        '
        Me.CleanSelectedOldFile.BackColor = System.Drawing.Color.Snow
        Me.CleanSelectedOldFile.BackgroundImage = Global.sabuntrail.My.Resources.Resources.main_close_btn
        Me.CleanSelectedOldFile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CleanSelectedOldFile.Location = New System.Drawing.Point(264, 22)
        Me.CleanSelectedOldFile.Name = "CleanSelectedOldFile"
        Me.CleanSelectedOldFile.Size = New System.Drawing.Size(30, 27)
        Me.CleanSelectedOldFile.TabIndex = 0
        Me.CleanSelectedOldFile.Visible = False
        '
        'pic_Old
        '
        Me.pic_Old.BackgroundImage = Global.sabuntrail.My.Resources.Resources.main_dd_old
        Me.pic_Old.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pic_Old.Location = New System.Drawing.Point(0, 0)
        Me.pic_Old.Name = "pic_Old"
        Me.pic_Old.Size = New System.Drawing.Size(315, 288)
        Me.pic_Old.TabIndex = 27
        Me.pic_Old.TabStop = False
        '
        'pnl_New
        '
        Me.pnl_New.AllowDrop = True
        Me.pnl_New.BackColor = System.Drawing.SystemColors.Control
        Me.pnl_New.Controls.Add(Me.NewFileNameTextBox)
        Me.pnl_New.Controls.Add(Me.CleanSelectedNewFile)
        Me.pnl_New.Controls.Add(Me.pic_NewFile)
        Me.pnl_New.Controls.Add(Me.NewTextBox)
        Me.pnl_New.Controls.Add(Me.pic_New)
        Me.pnl_New.Location = New System.Drawing.Point(418, 125)
        Me.pnl_New.Name = "pnl_New"
        Me.pnl_New.Size = New System.Drawing.Size(315, 288)
        Me.pnl_New.TabIndex = 31
        '
        'NewFileNameTextBox
        '
        Me.NewFileNameTextBox.AllowDrop = True
        Me.NewFileNameTextBox.BackColor = System.Drawing.Color.White
        Me.NewFileNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NewFileNameTextBox.CausesValidation = False
        Me.NewFileNameTextBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.NewFileNameTextBox.Font = New System.Drawing.Font("MS UI Gothic", 14.0!, System.Drawing.FontStyle.Bold)
        Me.NewFileNameTextBox.ForeColor = System.Drawing.Color.SteelBlue
        Me.NewFileNameTextBox.Location = New System.Drawing.Point(117, 94)
        Me.NewFileNameTextBox.MaxLength = 260
        Me.NewFileNameTextBox.Multiline = True
        Me.NewFileNameTextBox.Name = "NewFileNameTextBox"
        Me.NewFileNameTextBox.ReadOnly = True
        Me.NewFileNameTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.NewFileNameTextBox.Size = New System.Drawing.Size(143, 86)
        Me.NewFileNameTextBox.TabIndex = 29
        Me.NewFileNameTextBox.TabStop = False
        Me.NewFileNameTextBox.Text = "new_file_name"
        Me.NewFileNameTextBox.Visible = False
        '
        'CleanSelectedNewFile
        '
        Me.CleanSelectedNewFile.BackColor = System.Drawing.Color.Snow
        Me.CleanSelectedNewFile.BackgroundImage = Global.sabuntrail.My.Resources.Resources.main_close_btn
        Me.CleanSelectedNewFile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CleanSelectedNewFile.Location = New System.Drawing.Point(264, 22)
        Me.CleanSelectedNewFile.Name = "CleanSelectedNewFile"
        Me.CleanSelectedNewFile.Size = New System.Drawing.Size(30, 27)
        Me.CleanSelectedNewFile.TabIndex = 0
        Me.CleanSelectedNewFile.Visible = False
        '
        'pic_New
        '
        Me.pic_New.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pic_New.Image = Global.sabuntrail.My.Resources.Resources.main_dd_new
        Me.pic_New.Location = New System.Drawing.Point(0, 0)
        Me.pic_New.Name = "pic_New"
        Me.pic_New.Size = New System.Drawing.Size(315, 288)
        Me.pic_New.TabIndex = 28
        Me.pic_New.TabStop = False
        '
        'pic_licence
        '
        Me.pic_licence.BackColor = System.Drawing.Color.Transparent
        Me.pic_licence.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pic_licence.Image = Global.sabuntrail.My.Resources.Resources.main_licence_btn
        Me.pic_licence.Location = New System.Drawing.Point(519, 12)
        Me.pic_licence.Name = "pic_licence"
        Me.pic_licence.Size = New System.Drawing.Size(48, 46)
        Me.pic_licence.TabIndex = 32
        Me.pic_licence.TabStop = False
        '
        'pnl_main
        '
        Me.pnl_main.BackColor = System.Drawing.Color.Transparent
        Me.pnl_main.BackgroundImage = Global.sabuntrail.My.Resources.Resources.main_bg_2
        Me.pnl_main.Location = New System.Drawing.Point(30, 95)
        Me.pnl_main.Name = "pnl_main"
        Me.pnl_main.Size = New System.Drawing.Size(740, 426)
        Me.pnl_main.TabIndex = 33
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sabuntrail.My.Resources.Resources.main_title
        Me.Panel1.Controls.Add(Me.pic_Hide)
        Me.Panel1.Controls.Add(Me.pic_licence)
        Me.Panel1.Controls.Add(Me.pic_Help)
        Me.Panel1.Controls.Add(Me.pic_Exit)
        Me.Panel1.Location = New System.Drawing.Point(30, 29)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(740, 60)
        Me.Panel1.TabIndex = 34
        '
        'DFForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.BackgroundImage = Global.sabuntrail.My.Resources.Resources.main_bg_1
        Me.ClientSize = New System.Drawing.Size(800, 550)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.RefNewButton)
        Me.Controls.Add(Me.pnl_New)
        Me.Controls.Add(Me.pnl_Old)
        Me.Controls.Add(Me.pic_OutputSettingButton)
        Me.Controls.Add(Me.pic_ExecuteButton)
        Me.Controls.Add(Me.RefOldButton)
        Me.Controls.Add(Me.SettingPropertyGrid)
        Me.Controls.Add(Me.OutputSettingButton)
        Me.Controls.Add(Me.ExitButton)
        Me.Controls.Add(Me.ExecuteButton)
        Me.Controls.Add(Me.pnl_main)
        Me.DoubleBuffered = True
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.HelpButton = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "DFForm"
        Me.Text = "SabunTrail"
        Me.TransparencyKey = System.Drawing.Color.Snow
        CType(Me.pic_OutputSettingButton, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_ExecuteButton, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_NewFile, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_OldFile, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_Exit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_Hide, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_Help, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl_Old.ResumeLayout(False)
        Me.pnl_Old.PerformLayout()
        CType(Me.pic_Old, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl_New.ResumeLayout(False)
        Me.pnl_New.PerformLayout()
        CType(Me.pic_New, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_licence, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NewTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RefOldButton As System.Windows.Forms.Button
    Friend WithEvents RefNewButton As System.Windows.Forms.Button
    Friend WithEvents ExecuteButton As System.Windows.Forms.Button
    Friend WithEvents ExitButton As System.Windows.Forms.Button
    Friend WithEvents OutputSettingButton As System.Windows.Forms.Button
    Friend WithEvents SettingPropertyGrid As System.Windows.Forms.PropertyGrid
    Friend WithEvents pic_Help As PictureBox
    Friend WithEvents pic_Hide As PictureBox
    Friend WithEvents pic_Exit As PictureBox
    Friend WithEvents pic_OldFile As PictureBox
    Friend WithEvents pic_NewFile As PictureBox
    Friend WithEvents pic_ExecuteButton As PictureBox
    Friend WithEvents pic_OutputSettingButton As PictureBox
    Friend WithEvents OldTextBox As TextBox
    Friend WithEvents pnl_Old As Panel
    Friend WithEvents pnl_New As Panel
    Friend WithEvents pic_licence As PictureBox
    Friend WithEvents pic_Old As PictureBox
    Friend WithEvents pic_New As PictureBox
    Friend WithEvents pnl_main As Panel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents CleanSelectedOldFile As Panel
    Friend WithEvents CleanSelectedNewFile As Panel
    Friend WithEvents OldFileNameTextBox As TextBox
    Friend WithEvents NewFileNameTextBox As TextBox
End Class

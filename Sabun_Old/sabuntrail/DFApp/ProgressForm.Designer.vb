﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProgressForm
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ConvertProgressBar = New System.Windows.Forms.ProgressBar()
        Me.ProgressCancelButton = New System.Windows.Forms.Button()
        Me.BackgroundConvertWorker = New System.ComponentModel.BackgroundWorker()
        Me.SuspendLayout
        '
        'ConvertProgressBar
        '
        Me.ConvertProgressBar.Location = New System.Drawing.Point(12, 12)
        Me.ConvertProgressBar.Name = "ConvertProgressBar"
        Me.ConvertProgressBar.Size = New System.Drawing.Size(218, 23)
        Me.ConvertProgressBar.TabIndex = 0
        '
        'ProgressCancelButton
        '
        Me.ProgressCancelButton.Location = New System.Drawing.Point(155, 41)
        Me.ProgressCancelButton.Name = "ProgressCancelButton"
        Me.ProgressCancelButton.Size = New System.Drawing.Size(75, 23)
        Me.ProgressCancelButton.TabIndex = 1
        Me.ProgressCancelButton.Text = "中断"
        Me.ProgressCancelButton.UseVisualStyleBackColor = true
        '
        'BackgroundConvertWorker
        '
        Me.BackgroundConvertWorker.WorkerReportsProgress = true
        Me.BackgroundConvertWorker.WorkerSupportsCancellation = true
        '
        'ProgressForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 12!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(242, 73)
        Me.Controls.Add(Me.ProgressCancelButton)
        Me.Controls.Add(Me.ConvertProgressBar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "ProgressForm"
        Me.Text = "変換中"
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents ConvertProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents ProgressCancelButton As System.Windows.Forms.Button
    Friend WithEvents BackgroundConvertWorker As System.ComponentModel.BackgroundWorker
End Class

﻿Public Class MsgBoxEx

    Private Sub pic_close_MouseHover(sender As Object, e As EventArgs) Handles pic_close.MouseEnter
        pic_close.Image = My.Resources.Resources.dialog_close_btn_on
    End Sub

    Private Sub pic_close_MouseLeave(sender As Object, e As EventArgs) Handles pic_close.MouseLeave
        pic_close.Image = My.Resources.Resources.dialog_close_btn
    End Sub


    Private Sub pic_close_Click(sender As Object, e As EventArgs) Handles pic_close.Click
        'PictureBox1.Image = My.Resources.Resources.dialog_close_btn_on
        Me.Close()
    End Sub

    Public Sub New(ByVal argMsg As String, ByVal argMsg2 As String)
        ' この呼び出しはデザイナーで必要です。
        InitializeComponent()

        ' InitializeComponent() 呼び出しの後で初期化を追加します。
        MsgText.Text = argMsg
        IdTxt.Text = argMsg2
    End Sub

End Class
﻿Imports Microsoft.Office.Interop.Word
Imports System.Xml
Imports System.IO.Packaging
Imports System.IO
Imports System.Xml.Xsl
Imports System.Threading

''' <summary>
''' 新旧対比表作成クラス
''' </summary>
''' <remarks>非スレッドセーフ</remarks>
Public Class DFCreator

    ''' <summary>
    ''' DOCX加工用名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const DOCUMENT_RELATIONSHIP_TYPE As String = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    ''' <summary>
    ''' DOCX加工用名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const IMG_RELATIONSHIP_TYPE As String = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    ''' <summary>
    ''' DOCX加工用名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const STYLE_RELATIONSHIP_TYPE As String = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"

    ''' <summary>
    ''' DOCX加工用名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const NUMBER_RELATIONSHIP_TYPE As String = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering"

    ''' <summary>
    ''' DOCX加工用名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const WORD_NAMESPACE As String = "http://schemas.openxmlformats.org/wordprocessingml/2006/main"

    ''' <summary>
    ''' DOCX加工用名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const EMBEDDINGS_RELATIONSHIP_TYPE As String = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/oleObject"

    ''' <summary>
    ''' 名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XSL_NAMESPACE = "http://www.w3.org/1999/XSL/Transform"

    ''' <summary>
    ''' 名前空間（省略名）
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XSL_NAMESPACE_HEADER = "xsl"

    ''' <summary>
    ''' XPATH情報（差分のみ)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_SABUN_ONLY = "/xsl:stylesheet/xsl:variable[@name='sabunonly']"

    ''' <summary>
    ''' XPATH情報（追加/削除表示)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_ADD_AND_DELETE = "/xsl:stylesheet/xsl:variable[@name='addanddelete']"

    ''' <summary>
    ''' XPATH情報（新旧逆転)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_REVERSE = "/xsl:stylesheet/xsl:variable[@name='reverse']"

    ''' <summary>
    ''' XPATH情報（備考の数)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_REMARK = "/xsl:stylesheet/xsl:variable[@name='remarks']"

    ''' <summary>
    ''' XPATH情報（用紙サイズ)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_SIZE = "/xsl:stylesheet/xsl:variable[@name='pgsize']"

    ''' <summary>
    ''' XPATH情報（新列の色)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_NEW_COLOR = "/xsl:stylesheet/xsl:variable[@name='newcolor']"

    ''' <summary>
    ''' XPATH情報（旧列の色)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_OLD_COLOR = "/xsl:stylesheet/xsl:variable[@name='oldcolor']"

    ''' <summary>
    ''' XPATH情報（新列の下線)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_NEW_UNDERLINE = "/xsl:stylesheet/xsl:variable[@name='newunderline']"

    ''' <summary>
    ''' XPATH情報（旧列の下線)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_OLD_UNDERLINE = "/xsl:stylesheet/xsl:variable[@name='oldunderline']"

    ''' <summary>
    ''' XPATH情報（新列の列幅)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_NEW_WIDTH = "/xsl:stylesheet/xsl:variable[@name='newcolwidth']"

    ''' <summary>
    ''' XPATH情報（旧列の列幅)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_OLD_WIDTH = "/xsl:stylesheet/xsl:variable[@name='oldcolwidth']"

    ''' <summary>
    ''' XPATH情報（備考1の列幅)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_REMARK1_WIDTH = "/xsl:stylesheet/xsl:variable[@name='remark1colwidth']"

    ''' <summary>
    ''' XPATH情報（備考2の列幅)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_REMARK2_WIDTH = "/xsl:stylesheet/xsl:variable[@name='remark2colwidth']"

    ''' <summary>
    ''' XPATH情報（旧の標準スタイルID)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_NORMALSTYLEID = "/xsl:stylesheet/xsl:variable[@name='oldnormal']"


    ''' <summary>
    ''' XPATH情報（旧の取消線)
    ''' 2015/03/03 ADD START [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_OLD_STRIKE = "/xsl:stylesheet/xsl:variable[@name='oldstrike']"

    ''' <summary>
    ''' XPATH情報（新ファイルのパス情報)
    ''' 2015/03/03 ADD START [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_NEW_FOLDERPATH = "/xsl:stylesheet/xsl:variable[@name='newfolderpath']"

    ''' <summary>
    ''' XPATH情報（旧ファイルのパス情報)
    ''' 2015/03/03 ADD START [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_OLD_FOLDERPATH = "/xsl:stylesheet/xsl:variable[@name='oldfolderpath']"

    ''' <summary>
    ''' XPATH情報（新ファイルのファイル名)
    ''' 2015/03/03 ADD START [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_NEW_FILENAME = "/xsl:stylesheet/xsl:variable[@name='newfilename']"

    ''' <summary>
    ''' XPATH情報（旧ファイルのファイル名)
    ''' 2015/03/03 ADD START [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_OLD_FILENAME = "/xsl:stylesheet/xsl:variable[@name='oldfilename']"

    ''' <summary>
    ''' XPATH情報（新ファイルの更新日時)
    ''' 2015/03/03 ADD START [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_NEW_UPDATEDDATE = "/xsl:stylesheet/xsl:variable[@name='newupdateddate']"

    ''' <summary>
    ''' XPATH情報（旧ファイルの更新日時)
    ''' 2015/03/03 ADD START [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_OLD_UPDATEDDATE = "/xsl:stylesheet/xsl:variable[@name='oldupdateddate']"

    ''' <summary>
    ''' 一時ファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TMP_FILE As String = "tmpdiff.docx"

    ''' <summary>
    ''' 一時ファイル(XML/AFTER前)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TMP_XML_FILE As String = "tmpdiff.xml"

    ''' <summary>
    ''' WORDテンプレートファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CMP_SHEET_FILE As String = "comparesheet.docx"

    ''' <summary>
    ''' メインスタイルシートパス
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MAIN_STYLESHEET As String = "style.xsl"

    ''' <summary>
    ''' 後処理用スタイルシートパス
    ''' </summary>
    ''' <remarks></remarks>
    Private Const AFTER_STYLESHEET As String = "after.xsl"

    ''' <summary>
    ''' 外部パラメータスタイルシートファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const PARAM_STYLE_SHEET As String = "\variable.xsl"

    ''' <summary>
    ''' スタイルシートのパス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_stylePath As String = Nothing

    ''' <summary>
    ''' WORDテンプレートパス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_wordPath As String = Nothing

    ''' <summary>
    ''' 一時ファイルパス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_tmpPath As String = Nothing

    ''' <summary>
    ''' ログ出力クラス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_log As LogWriter = Nothing

    ''' <summary>
    ''' 出力ファイルパス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_outFilePath As String = Nothing

    ''' <summary>
    ''' 設定情報
    ''' </summary>
    ''' <remarks></remarks>
    Private m_setting As Setting = Nothing

    ''' <summary>
    ''' WORDのcloseに失敗した場合のリトライ回数
    ''' </summary>
    ''' <remarks></remarks>
    Private Const WORD_QUIT_RETRY_MAX As Integer = 5

    ''' <summary>
    ''' WORDのcloseに失敗した場合のリトライインターバル
    ''' </summary>
    ''' <remarks></remarks>
    Private Const WORD_QUIT_RETRY_INTERVAL As Integer = 200

    ''' <summary>
    ''' ステータスコードリスト
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum STATUS_CODE
        NEUTRAL = 0
        WORD_DIFF = 70
        XML_TRASLATING = 90
        XML_TRANSLATED = 100
    End Enum

    ''' <summary>
    ''' 内部的な処理ステータス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_status As Integer = STATUS_CODE.NEUTRAL

    ''' <summary>
    ''' 内部ステータスを返却します。
    ''' </summary>
    ''' <value></value>
    ''' <returns>列挙型STATUS_CODEの値を返します。</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property status As Integer
        Get
            Return m_status
        End Get
    End Property

    ''' <summary>
    ''' 旧ファイルのドキュメントオブジェクト
    ''' </summary>
    ''' <remarks></remarks>
    Private m_olddoc As Document = Nothing

    ''' <summary>
    ''' 新ファイルのドキュメントオブジェクト
    ''' </summary>
    ''' <remarks></remarks>
    Private m_newdoc As Document = Nothing

    ''' <summary>
    ''' 一時ファイルのドキュメントオブジェクト
    ''' </summary>
    ''' <remarks></remarks>
    Private m_tmpdoc As Document = Nothing


    ''' <summary>
    ''' コンストラクタ
    ''' </summary>
    ''' <param name="stylePath">スタイルシートの配置パス</param>
    ''' <param name="wordPath">テンプレートになるwordファイルのパス</param>
    ''' <param name="tmpPath">一時ファイルのパス</param>
    ''' <param name="log">ログクラス</param>
    ''' <remarks>例外は上位にスローします。</remarks>
    Public Sub New(ByVal stylePath As String, ByVal wordPath As String, ByVal tmpPath As String, ByVal log As LogWriter)
        m_stylePath = stylePath
        m_wordPath = wordPath
        m_tmpPath = tmpPath
        m_log = log
    End Sub

    ' 2015/03/03 MOD START [sabuntrail Version 1.1対応] Kitajima
    ''' <summary>
    ''' 差分作成
    ''' </summary>
    ''' <param name="newFilePath">新ファイルのフルパス</param>
    ''' <param name="oldFilePath">旧ファイルのフルパス</param>
    ''' <param name="outFilePath">出力ファイルのフルパス</param>
    ''' <param name="conf" >設定情報</param>
    ''' <param name="oItems">出力情報（新旧ファイルのファイル名、更新日時等）</param>
    ''' <remarks>例外は上位にスローします。</remarks>
    Public Sub Create(ByVal newFilePath As String, ByVal oldFilePath As String, ByVal outFilePath As String, ByRef conf As Setting, ByRef oItems As OutputItems)
        'Public Sub Create(ByVal newFilePath As String, ByVal oldFilePath As String, ByVal outFilePath As String, ByRef conf As Setting)
        ' 2015/03/03 MOD END [sabuntrail Version 1.1対応] Kitajima
        Try
            m_outFilePath = outFilePath
            m_status = STATUS_CODE.NEUTRAL
            CreateDiff(newFilePath, oldFilePath)
            m_status = STATUS_CODE.WORD_DIFF
            ' 2015/03/03 MOD START [sabuntrail Version 1.1対応] Kitajima
            'Transrate(conf)
            Transrate(conf, oItems)
            ' 2015/03/03 MOD END [sabuntrail Version 1.1対応] Kitajima
            m_status = STATUS_CODE.XML_TRANSLATED
        Finally
            Common.DeleteFile(m_tmpPath & "\" & TMP_FILE)
            Common.DeleteFile(m_tmpPath & "\" & TMP_XML_FILE)
        End Try
    End Sub

    ''' <summary>
    ''' 差分ファイルを作成します。
    ''' </summary>
    ''' <param name="newFilePath">新ファイルフルパス</param>
    ''' <param name="oldFilePath">旧ファイルフルパス</param>
    ''' <remarks>例外は上位にスローします。</remarks>
    Private Sub CreateDiff(ByVal newFilePath As String, ByVal oldFilePath As String)
        ' Wordを起動します。
        Dim wdApp As Application = Nothing

        Try
            wdApp = New Application()

            ' 不可視に
            wdApp.Visible = False
            wdApp.ScreenUpdating = False
            wdApp.DisplayAlerts = False

            ' 新旧ファイルを開きます。
            m_olddoc = wdApp.Documents.Open(oldFilePath)
            m_newdoc = wdApp.Documents.Open(newFilePath)

            ' 前処理します。
            PreConvert(m_olddoc)
            PreConvert(m_newdoc)

            ' 差分ファイルを作成します。
            Dim diffdoc As Document = wdApp.CompareDocuments(OriginalDocument:=m_olddoc, _
                                   RevisedDocument:=m_newdoc, _
                                   Destination:=WdCompareDestination.wdCompareDestinationNew, _
                                   Granularity:=WdGranularity.wdGranularityCharLevel, _
                                   CompareFormatting:=True, _
                                   CompareCaseChanges:=False, _
                                   CompareWhitespace:=True, _
                                   CompareTables:=True, _
                                   CompareHeaders:=False, _
                                   CompareFootnotes:=True, _
                                   CompareTextboxes:=True, _
                                   CompareFields:=True, _
                                   CompareComments:=False, _
                                   CompareMoves:=False, _
                                   RevisedAuthor:="", _
                                   IgnoreAllComparisonWarnings:=True)

            ' 一時領域に移動
            wdApp.ChangeFileOpenDirectory(m_tmpPath)

            ' 一時領域にtmpDiff.docxを保存します。
            diffdoc.SaveAs(FileName:=TMP_FILE, _
                                        FileFormat:=WdSaveFormat.wdFormatXMLDocument, _
                                        LockComments:=False, _
                                        Password:="", _
                                        AddToRecentFiles:=False, _
                                        WritePassword:="", _
                                        ReadOnlyRecommended:=False, _
                                        EmbedTrueTypeFonts:=False, _
                                        SaveNativePictureFormat:=False, _
                                        SaveFormsData:=False, _
                                        SaveAsAOCELetter:=False)


        Catch ex As Exception
            m_log.WriteErrorLog(ex.ToString)
            Throw ex
        Finally
            'クローズ処理中にキャンセルされた場合を想定して
            ' Wordファイルをクローズしに行く。
            WordQuit(wdApp)
        End Try
    End Sub

    ''' <summary>
    ''' 変換前処理
    ''' </summary>
    ''' <param name="doc">対象となるWORDオブジェクト</param>
    ''' <remarks></remarks>
    Private Sub PreConvert(ByRef doc As Document)
        doc.AcceptAllRevisions()
        For i As Integer = doc.Hyperlinks.Count To 1 Step -1
            Try
                doc.Hyperlinks(i).Delete()
            Catch e As Exception
                m_log.WriteErrorLog(e.ToString)
            End Try
        Next
        'For i As Integer = doc.Fields.Count To 1 Step -1
        '    Try
        '        doc.Fields(i).Unlink()
        '    Catch e As Exception
        '        m_log.WriteErrorLog(e.ToString)
        '    End Try
        'Next
    End Sub

    ''' <summary>
    ''' WORDを終了します。
    ''' エラーが発生した場合{WORD_QUIT_RETRY_INTERVAL}ミリ秒間隔で
    ''' 合計{WORD_QUIT_RETRY_MAX}回、クローズを試行します。
    ''' </summary>
    ''' <param name="wdApp"></param>
    ''' <remarks></remarks>
    Private Sub WordQuit(ByRef wdApp As Application)
        For i As Integer = 0 To WORD_QUIT_RETRY_MAX - 1
            Try
                If wdApp IsNot Nothing Then
                    Try
                        If m_olddoc IsNot Nothing Then
                            m_olddoc.Close(WdSaveOptions.wdDoNotSaveChanges)
                        End If
                        If m_newdoc IsNot Nothing Then
                            m_newdoc.Close(WdSaveOptions.wdDoNotSaveChanges)
                        End If
                        If m_tmpdoc IsNot Nothing Then
                            m_tmpdoc.Close(WdSaveOptions.wdDoNotSaveChanges)
                        End If
                        If wdApp.Documents IsNot Nothing AndAlso wdApp.Documents.Count > 0 Then
                            wdApp.Documents.Close(WdSaveOptions.wdDoNotSaveChanges)
                        End If
                    Catch e As Exception
                        ' ドキュメントを閉じるのに失敗しても無視します。
                        m_log.WriteTraceLog(e.ToString)
                    End Try
                    wdApp.Quit(WdSaveOptions.wdDoNotSaveChanges)
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(wdApp)
                    wdApp = Nothing
                    Exit For
                End If
            Catch e As Exception
                m_log.WriteTraceLog(e.ToString)
                Thread.Sleep(WORD_QUIT_RETRY_INTERVAL)
            End Try
        Next
        m_olddoc = Nothing
        m_newdoc = Nothing
        m_tmpdoc = Nothing
    End Sub

    ' 2015/03/03 MOD START [sabuntrail Version 1.1対応] Kitajima
    ''' <summary>
    ''' 外部パラメータ用スタイルシートを更新します。
    ''' </summary>
    ''' <param name="conf">設定情報</param>
    ''' <param name="NormalStyleId">標準のスタイルID</param>
    ''' <param name="oItems">出力情報（新旧ファイルのファイル名、更新日時等）</param>
    ''' <remarks>例外は上位にスローします。</remarks>
    Private Sub UpdateXSLT(ByRef conf As Setting, ByVal NormalStyleId As String, ByRef oItems As OutputItems)
        'Private Sub UpdateXSLT(ByRef conf As Setting, ByVal NormalStyleId As String)
        ' 2015/03/03 MOD END [sabuntrail Version 1.1対応] Kitajima
        Dim var As New XmlDocument

        Dim nTbl As New NameTable
        Dim nsManager As New XmlNamespaceManager(nTbl)
        nsManager.AddNamespace(XSL_NAMESPACE_HEADER, XSL_NAMESPACE)

        ' 外部パラメータ用のスタイルシートを読み込む。
        var.Load(m_stylePath & PARAM_STYLE_SHEET)

        ' 差分のみの設定反映
        Dim xmlNode As Xml.XmlNode
        xmlNode = var.SelectSingleNode(XPATH_SABUN_ONLY, nsManager)
        If conf.sabun_only Then
            xmlNode.Attributes("select").InnerText = "1"
        Else
            xmlNode.Attributes("select").InnerText = "0"
        End If

        ' 追加/削除表示の設定反映
        xmlNode = var.SelectSingleNode(XPATH_ADD_AND_DELETE, nsManager)
        If conf.add_and_delete Then
            xmlNode.Attributes("select").InnerText = "1"
        Else
            xmlNode.Attributes("select").InnerText = "0"
        End If

        ' 逆表示の設定反映
        xmlNode = var.SelectSingleNode(XPATH_REVERSE, nsManager)
        If conf.reverse Then
            xmlNode.Attributes("select").InnerText = "1"
        Else
            xmlNode.Attributes("select").InnerText = "0"
        End If

        ' 備考列の設定反映
        xmlNode = var.SelectSingleNode(XPATH_REMARK, nsManager)
        xmlNode.Attributes("select").InnerText = conf.remark
        If conf.remark = 0 Then
            xmlNode = var.SelectSingleNode(XPATH_NEW_WIDTH, nsManager)
            xmlNode.Attributes("select").InnerText = "2500"
            xmlNode = var.SelectSingleNode(XPATH_OLD_WIDTH, nsManager)
            xmlNode.Attributes("select").InnerText = "2500"
            xmlNode = var.SelectSingleNode(XPATH_REMARK1_WIDTH, nsManager)
            xmlNode.Attributes("select").InnerText = "0"
            xmlNode = var.SelectSingleNode(XPATH_REMARK2_WIDTH, nsManager)
            xmlNode.Attributes("select").InnerText = "0"
        ElseIf conf.remark = 1 Then
            xmlNode = var.SelectSingleNode(XPATH_NEW_WIDTH, nsManager)
            xmlNode.Attributes("select").InnerText = "2100"
            xmlNode = var.SelectSingleNode(XPATH_OLD_WIDTH, nsManager)
            xmlNode.Attributes("select").InnerText = "2100"
            xmlNode = var.SelectSingleNode(XPATH_REMARK1_WIDTH, nsManager)
            xmlNode.Attributes("select").InnerText = "800"
            xmlNode = var.SelectSingleNode(XPATH_REMARK2_WIDTH, nsManager)
            xmlNode.Attributes("select").InnerText = "0"
        Else
            xmlNode = var.SelectSingleNode(XPATH_NEW_WIDTH, nsManager)
            xmlNode.Attributes("select").InnerText = "2100"
            xmlNode = var.SelectSingleNode(XPATH_OLD_WIDTH, nsManager)
            xmlNode.Attributes("select").InnerText = "2100"
            xmlNode = var.SelectSingleNode(XPATH_REMARK1_WIDTH, nsManager)
            xmlNode.Attributes("select").InnerText = "400"
            xmlNode = var.SelectSingleNode(XPATH_REMARK2_WIDTH, nsManager)
            xmlNode.Attributes("select").InnerText = "400"
        End If

        ' 用紙サイズの設定反映
        xmlNode = var.SelectSingleNode(XPATH_SIZE, nsManager)
        xmlNode.Attributes("select").InnerText = conf.size

        ' 新色
        xmlNode = var.SelectSingleNode(XPATH_NEW_COLOR, nsManager)
        xmlNode.Attributes("select").InnerText = "'" & ColorTranslator.ToHtml(conf.newColColor) & "'"

        ' 旧色
        xmlNode = var.SelectSingleNode(XPATH_OLD_COLOR, nsManager)
        xmlNode.Attributes("select").InnerText = "'" & ColorTranslator.ToHtml(conf.oldColColor) & "'"

        ' 新下線
        xmlNode = var.SelectSingleNode(XPATH_NEW_UNDERLINE, nsManager)
        If conf.newUnderline Then
            xmlNode.Attributes("select").InnerText = "1"
        Else
            xmlNode.Attributes("select").InnerText = "0"
        End If

        ' 旧下線
        xmlNode = var.SelectSingleNode(XPATH_OLD_UNDERLINE, nsManager)
        If conf.oldUnderline Then
            xmlNode.Attributes("select").InnerText = "1"
        Else
            xmlNode.Attributes("select").InnerText = "0"
        End If

        ' 旧標準スタイルID
        xmlNode = var.SelectSingleNode(XPATH_NORMALSTYLEID, nsManager)
        xmlNode.Attributes("select").InnerText = "old_" & NormalStyleId

        ' 2015/03/03 ADD START [sabuntrail Version 1.1対応] Kitajima
        ' 取消線
        xmlNode = var.SelectSingleNode(XPATH_OLD_STRIKE, nsManager)
        If conf.oldColStrike Then
            xmlNode.Attributes("select").InnerText = "1"
        Else
            xmlNode.Attributes("select").InnerText = "0"
        End If

        ' 新ファイルのフォルダパス
        xmlNode = var.SelectSingleNode(XPATH_NEW_FOLDERPATH, nsManager)
        xmlNode.Attributes("select").InnerText = "'" & Common.EscapeXMLInterdictionString(oItems.NewFolderPath) & "'"

        ' 旧ファイルのフォルダパス
        xmlNode = var.SelectSingleNode(XPATH_OLD_FOLDERPATH, nsManager)
        xmlNode.Attributes("select").InnerText = "'" & Common.EscapeXMLInterdictionString(oItems.OldFolderPath) & "'"

        ' 新ファイルのファイル名
        xmlNode = var.SelectSingleNode(XPATH_NEW_FILENAME, nsManager)
        xmlNode.Attributes("select").InnerText = "'" & Common.EscapeXMLInterdictionString(oItems.NewFileName) & "'"

        ' 旧ファイルのファイル名
        xmlNode = var.SelectSingleNode(XPATH_OLD_FILENAME, nsManager)
        xmlNode.Attributes("select").InnerText = "'" & Common.EscapeXMLInterdictionString(oItems.OldFileName) & "'"

        ' 新ファイルの更新日時
        xmlNode = var.SelectSingleNode(XPATH_NEW_UPDATEDDATE, nsManager)
        xmlNode.Attributes("select").InnerText = "'" & oItems.NewUpdatedDate & "'"

        ' 旧ファイルの更新日時
        xmlNode = var.SelectSingleNode(XPATH_OLD_UPDATEDDATE, nsManager)
        xmlNode.Attributes("select").InnerText = "'" & oItems.OldUpdatedDate & "'"

        ' 2015/03/03 MOD END [sabuntrail Version 1.1対応] Kitajima

        m_log.WriteTraceLog(var.OuterXml)

        var.Save(m_stylePath & PARAM_STYLE_SHEET)

    End Sub

    ' 2015/03/03 MOD START [sabuntrail Version 1.1対応] Kitajima
    ''' <summary>
    ''' 変換処理
    ''' </summary>
    ''' <param name="conf">設定情報</param>
    ''' <param name="oItems">出力情報（新旧ファイルのファイル名、更新日時等）</param>
    ''' <remarks>例外は上位にスローします。</remarks>
    Private Sub Transrate(ByRef conf As Setting, oItems As OutputItems)
        'Private Sub Transrate(ByRef conf As Setting)
        ' 2015/03/03 MOD END [sabuntrail Version 1.1対応] Kitajima
        Try
            System.IO.File.Copy(m_wordPath & "\" & CMP_SHEET_FILE, m_outFilePath, True)

            ' 名前空間テーブル
            Dim nTbl As New NameTable
            Dim nsManager As New XmlNamespaceManager(nTbl)
            nsManager.AddNamespace("w", WORD_NAMESPACE)

            ' 以降、番号のついていないインスタンスは差分ファイル
            ' 番号2が最後に付与されているインスタンスは出力ファイルのものです。
            ' 差分ファイルを開きます。
            Using wdPackage As Package = Package.Open(m_tmpPath & "\" & TMP_FILE, FileMode.Open, FileAccess.ReadWrite)

                ' word/document.xmlのリレーションシップを取得します。
                Dim docPackageRelationship As PackageRelationship = wdPackage.GetRelationshipsByType(DOCUMENT_RELATIONSHIP_TYPE).FirstOrDefault()

                ' リレーションシップが見つかった場合のみ処理
                If (docPackageRelationship IsNot Nothing) Then

                    ' word/document.xmlのUriとParckagePartを取得します。
                    Dim documentUri As Uri = PackUriHelper.ResolvePartUri(New Uri("/", UriKind.Relative), docPackageRelationship.TargetUri)
                    Dim documentPart As PackagePart = wdPackage.GetPart(documentUri)

                    ' 出力ファイルを開きます。
                    'Using wdPackage2 As Package = Package.Open(m_tmpPath & "\" & TMP_FILE2, FileMode.Open, FileAccess.ReadWrite)
                    Using wdPackage2 As Package = Package.Open(m_outFilePath, FileMode.Open, FileAccess.ReadWrite)

                        ' word/document.xmlのリレーションシップを取得します。
                        Dim docPackageRelationship2 As PackageRelationship = wdPackage2.GetRelationshipsByType(DOCUMENT_RELATIONSHIP_TYPE).FirstOrDefault()

                        ' リレーションシップが見つかった場合のみ処理
                        If (docPackageRelationship2 IsNot Nothing) Then

                            ' word/document.xmlのUriとParckagePartを取得します。
                            Dim documentUri2 As Uri = PackUriHelper.ResolvePartUri(New Uri("/", UriKind.Relative), docPackageRelationship2.TargetUri)
                            Dim documentPart2 As PackagePart = wdPackage2.GetPart(documentUri2)

                            ' Word差分ファイルのスタイルのリレーションシップを取得します。
                            Dim styleRelationShip3 As PackageRelationship = documentPart.GetRelationshipsByType(STYLE_RELATIONSHIP_TYPE).FirstOrDefault

                            Dim normalStyleId As String = "a"

                            ' スタイルのリレーションシップが存在する場合のみ処理します。
                            If (styleRelationShip3 IsNot Nothing) Then
                                ' Word差分ファイルのUriとPackagePartを取得
                                Dim styleUri As Uri = PackUriHelper.ResolvePartUri(documentUri, styleRelationShip3.TargetUri)
                                Dim stylePart As PackagePart = wdPackage.GetPart(styleUri)

                                ' コピーするスタイルのリスト
                                Dim xml1 As New XmlDocument
                                xml1.Load(stylePart.GetStream)
                                Dim style As Xml.XmlNode = xml1.SelectSingleNode("/w:styles/w:style[./w:name/@w:val['Normal']]", nsManager)
                                If style IsNot Nothing Then
                                    normalStyleId = style.Attributes("w:styleId").Value
                                End If
                            End If

                            ' 外部パラメータ用のXSLT更新
                            ' 2015/03/03 MOD START [sabuntrail Version 1.1対応] Kitajima
                            'UpdateXSLT(conf, normalStyleId)
                            UpdateXSLT(conf, normalStyleId, oItems)
                            ' 2015/03/03 MOD END [sabuntrail Version 1.1対応] Kitajima

                            ' XSLT変換(MAIN)
                            Using reader As XmlReader = XmlReader.Create(documentPart.GetStream)
                                Using writer As XmlWriter = XmlWriter.Create(m_tmpPath & "\" & TMP_XML_FILE)
                                    Dim xslt As XslCompiledTransform = New XslCompiledTransform
                                    xslt.Load(m_stylePath & "\" & MAIN_STYLESHEET)
                                    xslt.Transform(reader, writer)
                                End Using
                            End Using

                            ' XSLT変換(after)
                            Using reader As XmlReader = XmlReader.Create(m_tmpPath & "\" & TMP_XML_FILE)
                                Using writer As XmlWriter = XmlWriter.Create(documentPart2.GetStream(FileMode.Create, FileAccess.Write))
                                    Dim xslt As XslCompiledTransform = New XslCompiledTransform
                                    xslt.Load(m_stylePath & "\" & AFTER_STYLESHEET)
                                    xslt.Transform(reader, writer)
                                End Using
                            End Using

                            ' ログ用に開きます。
                            Dim docDoc As New XmlDocument
                            docDoc.Load(documentPart2.GetStream)
                            m_log.WriteTraceLog("XSLT_XML:" & docDoc.OuterXml)

                            ' イメージがあるか調査
                            Dim imgRelations As PackageRelationshipCollection = documentPart.GetRelationshipsByType(IMG_RELATIONSHIP_TYPE)

                            ' イメージがある場合はコピーします。
                            For Each iRel As PackageRelationship In imgRelations
                                ' 画像のUriとParckagePartを取得します。
                                Dim imgUri As Uri = PackUriHelper.ResolvePartUri(documentUri, iRel.TargetUri)
                                Dim imgPart As PackagePart = wdPackage.GetPart(imgUri)

                                ' 出力先にイメージのリレーションシップを作成してパッケージをコピーします。
                                documentPart2.CreateRelationship(iRel.TargetUri, iRel.TargetMode, iRel.RelationshipType, iRel.Id)
                                Dim imgPart2 As PackagePart = documentPart2.Package.CreatePart(imgUri, imgPart.ContentType)
                                CopyStream(imgPart.GetStream, imgPart2.GetStream)
                            Next

                            ' 組み込みのデータがあるか調査
                            Dim embRelations As PackageRelationshipCollection = documentPart.GetRelationshipsByType(EMBEDDINGS_RELATIONSHIP_TYPE)

                            ' 組み込みがある場合はコピーします。
                            For Each eRel As PackageRelationship In embRelations
                                ' 画像のUriとParckagePartを取得します。
                                Dim embUri As Uri = PackUriHelper.ResolvePartUri(documentUri, eRel.TargetUri)
                                Dim embPart As PackagePart = wdPackage.GetPart(embUri)

                                ' 出力先にイメージのリレーションシップを作成してパッケージをコピーします。
                                documentPart2.CreateRelationship(eRel.TargetUri, eRel.TargetMode, eRel.RelationshipType, eRel.Id)
                                Dim embPart2 As PackagePart = documentPart2.Package.CreatePart(embUri, embPart.ContentType)
                                CopyStream(embPart.GetStream, embPart2.GetStream)
                            Next

                            ' 新旧のナンバリングのマッピング
                            ' style.xmlを書き換えるときに使用します。
                            Dim newOldNumberTable As New Hashtable

                            ' 連番ルールのリレーションシップをコピーします。
                            Dim numberRelationShip As PackageRelationship = documentPart.GetRelationshipsByType(NUMBER_RELATIONSHIP_TYPE).FirstOrDefault

                            ' リレーションシップが存在する場合
                            If (numberRelationShip IsNot Nothing) Then
                                ' word/numberingのUriとParckagePartを取得します。
                                Dim numberUri As Uri = PackUriHelper.ResolvePartUri(documentUri, numberRelationShip.TargetUri)
                                Dim numberPart As PackagePart = wdPackage.GetPart(numberUri)

                                ' 出力ファイルに連番ルールのリレーションシップがあるかどうか調べる。
                                Dim numberRelationShip2 As PackageRelationship = documentPart2.GetRelationshipsByType(NUMBER_RELATIONSHIP_TYPE).FirstOrDefault

                                Dim numberUri2 As Uri = Nothing
                                Dim numberPart2 As PackagePart = Nothing

                                ' 連番ルールが存在しない場合は作る。（存在しないはず。存在する場合は入れ替える。）
                                If (numberRelationShip2 IsNot Nothing) Then
                                    numberUri2 = PackUriHelper.ResolvePartUri(documentUri2, numberRelationShip2.TargetUri)
                                    numberPart2 = wdPackage2.GetPart(numberUri2)
                                    CopyStream(numberPart.GetStream, numberPart2.GetStream)
                                Else
                                    documentPart2.CreateRelationship(numberRelationShip.TargetUri, numberRelationShip.TargetMode, numberRelationShip.RelationshipType, numberRelationShip.Id)
                                    numberPart2 = documentPart2.Package.CreatePart(numberUri, "application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml")
                                    CopyStream(numberPart.GetStream, numberPart2.GetStream)
                                End If

                                ' DOMで扱えるように連番ルールを変換します。
                                Dim numberDoc As New XmlDocument
                                numberDoc.Load(numberPart2.GetStream)

                                ' アブストラクトの要素を抜きます。
                                Dim abdstractNums As XmlNodeList = numberDoc.SelectNodes("/w:numbering/w:abstractNum", nsManager)

                                ' アブストラクト要素のId番号の最大値
                                Dim maxANId As Integer = 0

                                ' アブストラクト要素のId番号の最大値を調べます。
                                For Each abstractNumber As System.Xml.XmlNode In abdstractNums
                                    ' アブストラクト要素のId番号をIntegerに
                                    Dim NumId As Integer = Integer.Parse(abstractNumber.Attributes("w:abstractNumId").InnerText)

                                    ' /w:abstractNumId/w:nsid要素は削除します。
                                    Dim nsid As System.Xml.XmlNode = abstractNumber.SelectSingleNode("w:nsid", nsManager)
                                    abstractNumber.RemoveChild(nsid)

                                    ' これまで出現した値より大きい場合はmaxANIdを入れ替えます。
                                    If maxANId < NumId Then
                                        maxANId = NumId
                                    End If
                                Next

                                ' num要素を抜きます。
                                Dim nums As XmlNodeList = numberDoc.SelectNodes("/w:numbering/w:num", nsManager)

                                ' numIdの最大値
                                Dim maxNId As Integer = 0

                                ' numId要素の最大値を調べます。
                                For Each num As System.Xml.XmlNode In nums
                                    ' numIdをIntegerに
                                    Dim NumId As Integer = Integer.Parse(num.Attributes("w:numId").InnerText)

                                    ' これまで出現した値より大きい場合はmaxNIdを入れ替えます。
                                    If maxNId < NumId Then
                                        maxNId = NumId
                                    End If
                                Next

                                ' aアブストラクト要素がある間繰り返します。
                                For Each abstractNumber As System.Xml.XmlNode In abdstractNums
                                    ' ルート要素
                                    Dim root As System.Xml.XmlNode = numberDoc.SelectSingleNode("w:numbering", nsManager)

                                    ' アブストラクトノードのクローン。
                                    Dim clone As System.Xml.XmlNode = abstractNumber.CloneNode(True)

                                    ' クローンのアブストラクトIDを新しく採番
                                    maxANId += 1

                                    ' 変更前の値を保持しておく。
                                    Dim baseValue As String = clone.Attributes("w:abstractNumId").Value

                                    ' クローンのＩＤを更新
                                    clone.Attributes("w:abstractNumId").Value = maxANId.ToString

                                    ' 変更前のアブストラクトＩＤを参照しているnuｍ要素を取得します。
                                    Dim numList As XmlNodeList = numberDoc.SelectNodes("/w:numbering/w:num[./w:abstractNumId/@w:val=""" & baseValue & """]", nsManager)

                                    ' クローンのアブストラクトＩＤを参照するようにしたnum要素を追加します。
                                    For Each num As System.Xml.XmlNode In numList

                                        ' num要素のクローン
                                        Dim numClone As System.Xml.XmlNode = num.CloneNode(True)

                                        ' num要素のIDを採番
                                        maxNId += 1

                                        ' 新旧でナンバリングの対応テーブルを作る。
                                        ' numIdを渡すとクローンのnumIDを返す。(1:1)
                                        newOldNumberTable.Add(numClone.Attributes("w:numId").Value, maxNId)

                                        ' 値を変更する。
                                        numClone.Attributes("w:numId").Value = maxNId.ToString

                                        ' リンクするアブストラクトIdも変更する。
                                        numClone.SelectSingleNode("w:abstractNumId", nsManager).Attributes("w:val").Value = maxANId

                                        ' クローンノードを出力先のＸＭＬにインポート
                                        numberDoc.ImportNode(numClone, True)

                                        ' クローンをコピー元の後に挿入する。
                                        root.InsertAfter(numClone, num)

                                    Next

                                    ' /w:numbering/w:abstractNum/w:lvl/w:pStyleが存在する場合は旧のスタイルを参照するように
                                    ' クローンを書き換える。
                                    Dim styleList As XmlNodeList = clone.SelectNodes("./w:lvl/w:pStyle", nsManager)

                                    For Each style As System.Xml.XmlNode In styleList
                                        Dim val As XmlAttribute = style.Attributes("w:val")
                                        val.Value = "old_" & val.Value
                                    Next

                                    ' クローンノードを出力先のXMLにインポート
                                    numberDoc.ImportNode(clone, True)

                                    ' クローンをコピー元の後に挿入する。
                                    root.InsertAfter(clone, abstractNumber)
                                Next

                                m_log.WriteTraceLog("NUMBER_XML:" & numberDoc.OuterXml)

                                ' 連番XMLを保存する。
                                numberDoc.Save(numberPart2.GetStream)

                            End If

                            m_status = STATUS_CODE.XML_TRASLATING

                            ' 旧列における段落で属性している箇所を読み取る。
                            Dim NumNodes As Xml.XmlNodeList = docDoc.SelectNodes("/w:document/w:body/w:tbl/w:tr/w:tc[2]//w:p/w:pPr/w:numPr/w:numId/@w:val", nsManager)

                            ' 連番IDを入れ替え。
                            For Each nNode As Xml.XmlNode In NumNodes
                                nNode.InnerText = newOldNumberTable(nNode.InnerText)
                            Next

                            m_log.WriteTraceLog(docDoc.OuterXml)

                            docDoc.Save(documentPart2.GetStream)

                            ' Word差分ファイルのスタイルのリレーションシップを取得します。
                            Dim styleRelationShip As PackageRelationship = documentPart.GetRelationshipsByType(STYLE_RELATIONSHIP_TYPE).FirstOrDefault

                            ' スタイルのリレーションシップが存在する場合のみ処理します。
                            If (styleRelationShip IsNot Nothing) Then
                                ' Word差分ファイルのUriとPackagePartを取得
                                Dim styleUri As Uri = PackUriHelper.ResolvePartUri(documentUri, styleRelationShip.TargetUri)
                                Dim stylePart As PackagePart = wdPackage.GetPart(styleUri)

                                ' 出力ファイルのスタイルのリレーションシップを取得します。
                                Dim styleRelationShip2 As PackageRelationship = documentPart2.GetRelationshipsByType(STYLE_RELATIONSHIP_TYPE).FirstOrDefault

                                ' スタイルのリレーションシップが存在する場合のみ処理します。
                                If (styleRelationShip2 IsNot Nothing) Then
                                    ' 出力ファイルのUriとPackagePartを取得する。
                                    Dim styleUri2 As Uri = PackUriHelper.ResolvePartUri(documentUri2, styleRelationShip2.TargetUri)
                                    Dim stylePart2 As PackagePart = wdPackage2.GetPart(styleUri2)

                                    ' コピーするスタイルのリスト
                                    Dim xml1 As New XmlDocument
                                    xml1.Load(stylePart.GetStream)
                                    Dim styleList As XmlNodeList = xml1.SelectNodes("/w:styles/w:style", nsManager)

                                    ' コピー先のXml
                                    Dim xml2 As New XmlDocument
                                    xml2.Load(stylePart2.GetStream)
                                    Dim node As System.Xml.XmlNode = xml2.SelectSingleNode("/w:styles", nsManager)

                                    For Each style As System.Xml.XmlNode In styleList
                                        ' スタイルをコピー
                                        Dim styleCopy As System.Xml.XmlNode = xml2.ImportNode(style, True)
                                        node.AppendChild(styleCopy)

                                        ' ADD
                                        ' 旧用のスタイルを作る
                                        Dim styleCopy2 As System.Xml.XmlNode = styleCopy.CloneNode(True)
                                        ' IDはold_スタイルIDに変更
                                        styleCopy2.Attributes("w:styleId").Value = "old_" & styleCopy2.Attributes("w:styleId").Value
                                        ' スタイル名もold_スタイル名
                                        styleCopy2.SelectSingleNode("w:name", nsManager).Attributes("w:val").Value = "old_" & styleCopy2.SelectSingleNode("w:name", nsManager).Attributes("w:val").Value

                                        ' 旧スタイルがある場合は入れ替える。
                                        Dim beforeParaPr As System.Xml.XmlNode = styleCopy2.SelectSingleNode("./w:pPr/w:pPrChange/w:pPr", nsManager)

                                        If beforeParaPr IsNot Nothing Then
                                            Dim parentNode As Xml.XmlNode = styleCopy2.SelectSingleNode("./w:pPr", nsManager)
                                            ' 変更後の情報は全て削除する。
                                            Dim deleteNodes As Xml.XmlNodeList = styleCopy2.SelectNodes("./w:pPr/*[local-name()!='pPrChange']", nsManager)
                                            For Each deleteNode As Xml.XmlNode In deleteNodes
                                                parentNode.RemoveChild(deleteNode)
                                            Next
                                            ' 変更前の情報を追加する。
                                            Dim insertNodes As Xml.XmlNodeList = beforeParaPr.SelectNodes("./*", nsManager)
                                            For Each insertNode As Xml.XmlNode In insertNodes
                                                parentNode.AppendChild(insertNode)
                                            Next
                                            parentNode.RemoveChild(beforeParaPr.ParentNode)
                                        End If

                                        ' 旧スタイルがある場合は入れ替える。
                                        Dim beforeRunPr As System.Xml.XmlNode = styleCopy2.SelectSingleNode("./w:rPr/w:rPrChange/w:rPr", nsManager)

                                        If beforeRunPr IsNot Nothing Then
                                            Dim parentNode As Xml.XmlNode = styleCopy2.SelectSingleNode("./w:rPr", nsManager)
                                            ' 変更後の情報は全て削除する。
                                            Dim deleteNodes As Xml.XmlNodeList = styleCopy2.SelectNodes("./w:rPr/*[local-name()!='rPrChange']", nsManager)
                                            For Each deleteNode As Xml.XmlNode In deleteNodes
                                                parentNode.RemoveChild(deleteNode)
                                            Next
                                            ' 変更前の情報を追加する。
                                            Dim insertNodes As Xml.XmlNodeList = beforeRunPr.SelectNodes("./*", nsManager)
                                            For Each insertNode As Xml.XmlNode In insertNodes
                                                parentNode.AppendChild(insertNode)
                                            Next
                                            parentNode.RemoveChild(beforeRunPr.ParentNode)
                                        End If

                                        ' カスタムスタイル属性が付いていない場合は付けます。
                                        If styleCopy2.Attributes("w:customStyle") Is Nothing Then
                                            Dim customStyleAttr As XmlAttribute = xml2.CreateAttribute("customStyle", WORD_NAMESPACE)
                                            customStyleAttr.Value = "1"
                                            styleCopy2.Attributes.Append(customStyleAttr)
                                        End If

                                        ' Default属性が付いている場合は外します。
                                        Dim defaultAttr As XmlAttribute = styleCopy2.Attributes("w:default")
                                        If defaultAttr IsNot Nothing Then
                                            styleCopy2.Attributes.Remove(defaultAttr)
                                        End If

                                        Dim linkNode As System.Xml.XmlNode = styleCopy2.SelectSingleNode("w:link", nsManager)
                                        ' リンクノードがある場合はoldのスタイルにリンクするように書き換えます。
                                        If linkNode IsNot Nothing Then
                                            Dim linkID As String = linkNode.Attributes("w:val").Value.ToString
                                            linkNode.Attributes("w:val").Value = "old_" & linkID
                                        End If

                                        ' 連番ルールを参照している場合
                                        Dim numId As System.Xml.XmlNode = style.SelectSingleNode("./w:pPr/w:numPr/w:numId", nsManager)

                                        If numId IsNot Nothing Then
                                            ' コピーされた連番ルールを参照するように変更する。
                                            Dim numId2 As System.Xml.XmlNode = styleCopy.SelectSingleNode("./w:pPr/w:numPr/w:numId", nsManager)
                                            numId2.Attributes("w:val").Value = newOldNumberTable(numId.Attributes("w:val").Value)

                                        End If

                                        ' 出力先XMLにインポートする。
                                        Dim copy3 As System.Xml.XmlNode = xml2.ImportNode(styleCopy2, True)

                                        ' w:styleの子ノードとして追加する。
                                        node.AppendChild(copy3)

                                    Next

                                    ' 履歴ありの文書を比較した場合に、スタイルの差分が作られてしまうことがあるため、ここで削除する。
                                    Dim delNodes As XmlNodeList = node.SelectNodes(".//w:pPrChange", nsManager)

                                    For i As Integer = delNodes.Count - 1 To 0 Step -1
                                        delNodes(i).RemoveAll()
                                        delNodes(i).ParentNode.RemoveChild(delNodes(i))
                                    Next

                                    delNodes = node.SelectNodes(".//w:rPrChange", nsManager)

                                    For i As Integer = delNodes.Count - 1 To 0 Step -1
                                        delNodes(i).RemoveAll()
                                        delNodes(i).ParentNode.RemoveChild(delNodes(i))
                                    Next

                                    m_log.WriteTraceLog("STYLE_XML:" & xml2.OuterXml)

                                    ' 出力先xmlを保存する。
                                    xml2.Save(stylePart2.GetStream)

                                End If
                            End If

                        End If

                    End Using
                End If
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Docxをdocファイルで保存しなおします。
    ''' </summary>
    ''' <param name="docxPath">docxファイルのフルパス</param>
    ''' <param name="docPath">保存先のフルパス</param>
    ''' <remarks>例外は上位にスローします。</remarks>
    Public Sub ConvDocxToDoc(ByVal docxPath As String, ByVal docPath As String)

        ' Wordを起動します。
        Dim wdApp As Application = Nothing

        Try
            wdApp = New Application

            ' 不可視に
            wdApp.Visible = False
            wdApp.ScreenUpdating = False

            ' 新旧ファイルを開きます。
            ' wdApp.Documents.Open(docxPath)
            m_tmpdoc = wdApp.Documents.Open(docxPath, False, True, False, , , , , , , , False)
            'wdApp.Documents.Open(m_tmpPath & "\" & TMP_FILE2)
            wdApp.ChangeFileOpenDirectory(Path.GetDirectoryName(docPath))
            m_tmpdoc.SaveAs(docPath, _
                                WdSaveFormat.wdFormatDocument, _
                                False, _
                                "", _
                                True, _
                                "", _
                                False, _
                                False, _
                                False, _
                                False, _
                                False)

        Catch ex As Exception
            Throw ex
        Finally
            WordQuit(wdApp)
        End Try
    End Sub

    ''' <summary>
    ''' sourceのストリームの情報をtargetのストリームに書き込みます。
    ''' </summary>
    ''' <param name="source">入力ストリーム</param>
    ''' <param name="target">出力ストリーム</param>
    ''' <remarks>例外は上位にスローします。</remarks>
    Private Sub CopyStream(ByVal source As Stream, ByVal target As Stream)
        Const bufSize As Integer = &H1000
        Dim buf(bufSize - 1) As Byte
        Dim bytesRead As Integer = 0
        bytesRead = source.Read(buf, 0, bufSize)
        Do While bytesRead > 0
            target.Write(buf, 0, bytesRead)
            bytesRead = source.Read(buf, 0, bufSize)
        Loop
    End Sub

End Class

﻿Imports System.Xml

''' <summary>
''' メッセージリソース
''' シングルトンクラス
''' </summary>
''' <remarks></remarks>
Public Class Message

    ''' <summary>
    ''' メッセージXML
    ''' </summary>
    ''' <remarks></remarks>
    Private xml As Xml.XmlDocument = Nothing

    ''' <summary>
    ''' ロック用オブジェクト
    ''' </summary>
    ''' <remarks></remarks>
    Private lock As New Object

    ''' <summary>
    ''' インスタンス
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared m_instance As Message = Nothing

    ''' <summary>
    ''' メッセージファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MES_FILE As String = "\message.xml"

    ''' <summary>
    ''' 名前空間
    ''' </summary>
    ''' <remarks></remarks>
    Private Const RES_NAMESPACE = "http://www.toppan-f.co.jp/sabuntrail/message"

    ''' <summary>
    ''' 名前空間（省略）
    ''' </summary>
    ''' <remarks></remarks>
    Private Const RES_NAMESPACE_HEADER = "msg"

    ''' <summary>
    ''' XPATH
    ''' </summary>
    ''' <remarks></remarks>
    Private Const XPATH_VALUE = "/msg:root/msg:message[@id='{0}']"

    ''' <summary>
    ''' 名前空間管理オブジェクト
    ''' </summary>
    ''' <remarks></remarks>
    Private m_nsManager As XmlNamespaceManager = Nothing

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const INF_CODE_0001 As String = "I-0001"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const INF_CODE_0002 As String = "I-0002"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const INF_CODE_0003 As String = "I-0003"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const INF_CODE_0004 As String = "I-0004"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const INF_CODE_0005 As String = "I-0005"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const INF_CODE_0006 As String = "I-0006"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const INF_CODE_0007 As String = "I-0007"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const INF_CODE_0008 As String = "I-0008"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const INF_CODE_0009 As String = "I-0009"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const INF_CODE_0010 As String = "I-0010"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const QUE_CODE_0001 As String = "Q-0001"

    ''' <summary>
    ''' メッセージコード
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Public Const QUE_CODE_0002 As String = "Q-0002"

    ''' <summary>
    ''' メッセージコード
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Public Const QUE_CODE_0003 As String = "Q-0003"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const QUE_CODE_0004 As String = "Q-0004"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const QUE_CODE_0005 As String = "Q-0005"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const QUE_CODE_0006 As String = "Q-0006"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const QUE_CODE_0007 As String = "Q-0007"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const QUE_CODE_0008 As String = "Q-0008"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const QUE_CODE_0009 As String = "Q-0009"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const QUE_CODE_0010 As String = "Q-0010"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const ERR_CODE_0001 As String = "E-0001"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const ERR_CODE_0002 As String = "E-0002"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const ERR_CODE_0003 As String = "E-0003"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const ERR_CODE_0004 As String = "E-0004"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const ERR_CODE_0005 As String = "E-0005"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const ERR_CODE_0006 As String = "E-0006"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const ERR_CODE_0007 As String = "E-0007"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const ERR_CODE_0008 As String = "E-0008"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const ERR_CODE_0009 As String = "E-0009"

    ' ''' <summary>
    ' ''' メッセージコード
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Const ERR_CODE_0010 As String = "E-0010"


    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const ERR_CODE_9990 As String = "E-9990"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const ERR_CODE_9991 As String = "E-9991"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const ERR_CODE_9992 As String = "E-9992"

    ''' <summary>
    ''' メッセージコード
    ''' </summary>
    ''' <remarks></remarks>
    Public Const ERR_CODE_9993 As String = "E-9993"

    ''' <summary>
    ''' メッセージコード
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Public Const ERR_CODE_9994 As String = "E-9994"

    ''' <summary>
    ''' コンストラクタ
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub New()
        SyncLock lock
            xml = New XmlDocument
            xml.Load(IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) & MES_FILE)

            Dim nTbl As New NameTable
            m_nsManager = New XmlNamespaceManager(nTbl)
            m_nsManager.AddNamespace(RES_NAMESPACE_HEADER, RES_NAMESPACE)

        End SyncLock
    End Sub

    ''' <summary>
    ''' インスタンス取得メソッド
    ''' </summary>
    ''' <returns>メッセージリソース</returns>
    ''' <remarks></remarks>
    Public Shared Function GetInstance() As Message
        If m_instance Is Nothing Then
            m_instance = New Message
        End If
        Return m_instance
    End Function

    ''' <summary>
    ''' メッセージ取得
    ''' </summary>
    ''' <param name="ID">メッセージID</param>
    ''' <returns>メッセージ</returns>
    ''' <remarks></remarks>
    Public Function GetMessage(ByVal ID As String) As String
        Dim node As XmlNode = xml.SelectSingleNode(String.Format(XPATH_VALUE, ID), m_nsManager)
        Return node.InnerText
    End Function

End Class

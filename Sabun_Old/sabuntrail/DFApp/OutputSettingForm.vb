﻿''' <summary>
''' 出力設定画面
''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
''' </summary>
''' <remarks></remarks>
Public Class OutputSettingForm

    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As IntPtr,
                                                                            ByVal wMsg As Integer,
                                                                            ByVal wParam As Integer,
                                                                            ByVal lParam As String) As Integer
    Public Declare Function ReleaseCapture Lib "user32" () As Long
    Private Const WM_NCLBUTTONDOWN = &HA1
    Private Const HTCAPTION = 2

    ''' <summary>
    ''' 定義ファイルフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CONF_DIRECTORY = "\conf"

    ''' <summary>
    ''' スタイルシートフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const STYLE_DIRECTORY = "\style"

    ' ワードフォルダ
    ''' <summary>
    ''' テンプレートフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const WORD_DIRECTORY As String = "\template"


    ''' <summary>
    ''' 一時作業用フォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TMP_DIRECTORY As String = "\tmp"

    ''' <summary>
    ''' ログフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const LOG_DIRECTORY As String = "\log"

    ''' <summary>
    ''' ライセンスフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const LIC_DIRECTORY As String = "\lic"


    ''' <summary>
    ''' マニュアルフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MAN_DIRECTORY As String = "\man"

    ''' <summary>
    ''' 設定ファイル
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CONFIG_FILE As String = "\setting.xml"

    ''' <summary>
    ''' ライセンスファイル
    ''' </summary>
    ''' <remarks></remarks>
    Private Const LIC_FILE As String = "\lic.dat"

    ''' <summary>
    ''' GUI起動時に変換結果を保存する一時ファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TEMP_OUT_FILE_NAME As String = "\tempout_{0}"

    ''' <summary>
    ''' GUI起動時に変換結果を保存する一時ファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TEMP_NEW_FILE_NAME As String = "\tempnew_{0}"

    ''' <summary>
    ''' GUI起動時に変換結果を保存する一時ファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TEMP_OLD_FILE_NAME As String = "\tempold_{0}"

    ''' <summary>
    ''' GUI起動時に変換結果を保存する一時ファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MANUAL_FILE_NAME As String = "\manual.pdf"

    ''' <summary>
    ''' 定義ファイルフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_conf_dir As String = Nothing

    ''' <summary>
    ''' WORDテンプレートフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_word_dir As String = Nothing

    ''' <summary>
    ''' 一次作業フォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_tmp_dir As String = Nothing

    ''' <summary>
    ''' スタイルシートフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_style_dir As String = Nothing

    ''' <summary>
    ''' ログフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_log_dir As String = Nothing

    ''' <summary>
    ''' ライセンスパス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_lic_dir As String = Nothing

    ''' <summary>
    ''' マニュアルフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_man_dir As String = Nothing

    ''' <summary>
    ''' 定義ファイルのファイルパス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_conf_fpath As String = Nothing

    ''' <summary>
    ''' コマンド起動時の戻り値
    ''' </summary>
    ''' <remarks></remarks>
    Private Enum RETURN_CODE
        SUCCESS = 0
        LIC_ERROR = 1
        ARGUMENT_ERROR = 2
        FILE_NOT_INPUT = 3
        FILE_EXT_INVALID = 4
        FILE_NOT_FOUND = 5
        SYSTEM_ERROR = 99
    End Enum

    ''' <summary>
    ''' ログオブジェクト
    ''' </summary>
    ''' <remarks></remarks>
    Private m_log As LogWriter = Nothing

    ''' <summary>
    ''' 差分作成オブジェクト
    ''' </summary>
    ''' <remarks></remarks>
    Private m_dfCreator As DFCreator = Nothing

    ''' <summary>
    ''' 出力パス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_outputPath As String = Nothing

    ''' <summary>
    ''' コマンドラインモード
    ''' </summary>
    ''' <remarks></remarks>
    Private m_consoleMode As Boolean = False

    ''' <summary>
    ''' 設定情報オブジェクト
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_setting As Setting = Nothing

    ''' <summary>
    ''' カスタムカラー
    ''' 色選択画面で作成した色を保持する。
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_cunstomColor As Integer() = Nothing

    ''' <summary>
    ''' 備考欄のラジオボタン配列
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private remarkArray As RadioButton()

    ''' <summary>
    ''' ページサイズのラジオボタン配列
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private sizeArray As RadioButton()

    ''' <summary>
    ''' 変更が保存されたかどうか。True・・・保存された False・・・保存されていない
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_changed As Boolean = False

    ''' <summary>
    ''' 初期化済みかどうか。True・・・初期化済み False・・・未初期化
    ''' コンストラクタが終了するタイミングでTrueにする。
    ''' コンストラクタのコントロールの初期化で行いたくない処理があれば
    ''' このフラグを参照。
    ''' 実際使用しているのはCheckUpdatedDateCheckBox
    ''' 2015/03/09 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_initialized As Boolean = False
    ''' <summary>
    ''' タイトルバーを非表示したため、Formのマウス移動を実現
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub LicenceForm_MouseDown(sender As Object, e As MouseEventArgs) Handles MyBase.MouseDown
        ReleaseCapture()
        SendMessage(Handle, WM_NCLBUTTONDOWN, HTCAPTION, 0)
    End Sub
    ''' <summary>
    ''' ヘルプボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub HelpBtn_MouseHover(sender As Object, e As EventArgs) Handles HelpBtn.MouseHover
        HelpBtn.Image = My.Resources.setting_help_btn_hover
    End Sub
    ''' <summary>
    ''' ヘルプボタンのマウス離れ
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub HelpBtn_MouseLeave(sender As Object, e As EventArgs) Handles HelpBtn.MouseLeave
        HelpBtn.Image = My.Resources.setting_help_btn
    End Sub
    ''' <summary>
    ''' エラーダイアログ表示メソッド
    ''' </summary>
    ''' <param name="msgId">メッセージ</param>
    ''' <param name="msg">メッセージID</param>
    ''' <remarks></remarks>
    Public Sub ShowErrorDialog(ByVal msg As String, ByVal msgId As String)
        'MessageBox.Show(msg, msgId, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'Dim frm As New MsgBoxEx(msg, msgId)
        'frm.StartPosition = FormStartPosition.CenterParent
        'frm.ShowDialog()

        Dim dialog As New DialogNoCheckBox(msg, msgId, False, DialogWithCheckBox.MESSAGE_TYPE.ERR)
        dialog.StartPosition = FormStartPosition.CenterParent
        Dim result As DialogResult = dialog.ShowDialog()
        If m_log IsNot Nothing Then
            m_log.WriteErrorLog(msgId & " " & msg)
        End If
    End Sub
    ''' <summary>
    ''' ヘルプボタンの押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub HelpBtn_Click(sender As Object, e As EventArgs) Handles HelpBtn.Click
        Try
            ' マニュアルフォルダ(\なし)
            m_man_dir = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) & MAN_DIRECTORY
            System.Diagnostics.Process.Start(m_man_dir & MANUAL_FILE_NAME)
        Catch ex As Exception
            ShowErrorDialog(String.Format(Message.GetInstance.GetMessage(Message.ERR_CODE_9992), ex.Message), Message.ERR_CODE_9992)
            If m_log IsNot Nothing Then
                m_log.WriteErrorLog(ex.ToString)
            End If
        Finally
        End Try
    End Sub
    ''' <summary>
    ''' 最小化ボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub HidenBtn_MouseHover(sender As Object, e As EventArgs) Handles HidenBtn.MouseHover
        HidenBtn.Image = My.Resources.setting_small_btn_hover
    End Sub
    ''' <summary>
    ''' 最小化ボタンのマウス離れ
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub HidenBtn_MouseLeave(sender As Object, e As EventArgs) Handles HidenBtn.MouseLeave
        HidenBtn.Image = My.Resources.setting_small_btn
    End Sub
    ''' <summary>
    ''' 最小化ボタン押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub HidenBtn_Click(sender As Object, e As EventArgs) Handles HidenBtn.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub
    ''' <summary>
    ''' 終了ボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub CloseBtn_MouseHover(sender As Object, e As EventArgs) Handles CloseBtn.MouseHover
        CloseBtn.Image = My.Resources.setting_exit_btn_hover
    End Sub
    ''' <summary>
    ''' 終了ボタンのマウス離れ
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub CloseBtn_MouseLeave(sender As Object, e As EventArgs) Handles CloseBtn.MouseLeave
        CloseBtn.Image = My.Resources.setting_exit_btn
    End Sub
    ''' <summary>
    ''' 終了ボタン押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub CloseBtn_Click(sender As Object, e As EventArgs) Handles CloseBtn.Click
        Application.Exit()
    End Sub
    ''' <summary>
    ''' コンストラクタ
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="arg_path">出力設定情報ファイルのフルパス</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal arg_path As String)
        ' この呼び出しは、Windows フォーム デザイナで必要です。
        InitializeComponent()

        ' InitializeComponent() 呼び出しの後で初期化を追加します。
        m_setting = New Setting(arg_path)

        ' 配列にラジオボタンを格納（設定値と配列の添え字が連動）
        remarkArray = New RadioButton() {NoRemarkRadioButton, Remark1RadioButton, Remark2RadioButton}
        sizeArray = New RadioButton() {A4SizeRadioButton, A3SizeRadioButton}

        ' コントロールの初期化
        DiffOnlyCheckBox.Checked = m_setting.sabun_only
        AddAndDeleteCheckBox.Checked = m_setting.add_and_delete
        ReverseColCheckBox.Checked = m_setting.reverse

        remarkArray(m_setting.remark).Checked = True
        sizeArray(m_setting.size).Checked = True

        NewColColorImageLabel.BackColor = m_setting.newColColor
        NewColUnderlineCheckBox.Checked = m_setting.newUnderline

        If NewColColorImageLabel.BackColor.ToArgb = Color.Red.ToArgb Then
            NewColRedRadioButton.Checked = True
        ElseIf NewColColorImageLabel.BackColor.ToArgb = Color.Blue.ToArgb Then
            NewColBlueRadioButton.Checked = True
        ElseIf NewColColorImageLabel.BackColor.ToArgb = Color.Green.ToArgb Then
            NewColGreenRadioButton.Checked = True
        End If

        OldColColorImageLabel.BackColor = m_setting.oldColColor
        OldColUnderlineCheckBox.Checked = m_setting.oldUnderline

        If OldColColorImageLabel.BackColor.ToArgb = Color.Red.ToArgb Then
            OldColRedRadioButton.Checked = True
        ElseIf OldColColorImageLabel.BackColor.ToArgb = Color.Blue.ToArgb Then
            OldColBlueRadioButton.Checked = True
        ElseIf OldColColorImageLabel.BackColor.ToArgb = Color.Green.ToArgb Then
            OldColGreenRadioButton.Checked = True
        End If

        ' Ver1.1の追加設定
        CheckUpdatedDateCheckBox.Checked = m_setting.autoCheckUpdatedDate

        FolderCheckBox.Checked = m_setting.outputFolder
        FileCheckBox.Checked = m_setting.outputFile
        UpdatedDateCheckBox.Checked = m_setting.outputUpdatedDate

        OldColStrikeCheckBox.Checked = m_setting.oldColStrike

        m_initialized = True

    End Sub

    ''' <summary>
    ''' 差分のみチェックボックス変更時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub DiffOnlyCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles DiffOnlyCheckBox.CheckedChanged
        CommonCheckBox_CheckedChanged(DiffOnlyCheckBox, m_setting.sabun_only)
        If DiffOnlyCheckBox.Checked = True Then
            DiffOnlyCheckBoxPic.Image = My.Resources.setting_1_btn1_on
        Else
            DiffOnlyCheckBoxPic.Image = My.Resources.setting_1_btn1_off
        End If
    End Sub

    ''' <summary>
    ''' （追加）/（削除）表示チェックボックス変更時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub AddAndDeleteCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles AddAndDeleteCheckBox.CheckedChanged
        CommonCheckBox_CheckedChanged(AddAndDeleteCheckBox, m_setting.add_and_delete)
        If AddAndDeleteCheckBox.Checked = True Then
            AddAndDeleteCheckBoxPic.Image = My.Resources.setting_1_btn2_on
        Else
            AddAndDeleteCheckBoxPic.Image = My.Resources.setting_1_btn2_off
        End If
    End Sub

    ''' <summary>
    ''' 新旧逆転チェックボックス変更時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ReverseColCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles ReverseColCheckBox.CheckedChanged
        CommonCheckBox_CheckedChanged(ReverseColCheckBox, m_setting.reverse)
        If ReverseColCheckBox.Checked = True Then
            ReverseColCheckBoxPic.Image = My.Resources.setting_1_btn3_on
        Else
            ReverseColCheckBoxPic.Image = My.Resources.setting_1_btn3_off
        End If
    End Sub

    ''' <summary>
    ''' 備考なしチェック時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub NoRemarkRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles NoRemarkRadioButton.CheckedChanged
        CommonRemarkRaddioButton_CheckedChanged()
    End Sub

    ''' <summary>
    ''' 備考欄ラジオボタン選択時の共通処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CommonRemarkRaddioButton_CheckedChanged()
        SyncLock (GetType(Application))
            Dim setValue = -1
            For i As Integer = 0 To remarkArray.Length - 1
                If remarkArray(i).Checked Then
                    setValue = i
                End If
            Next
            m_setting.remark = setValue
        End SyncLock
    End Sub

    ''' <summary>
    ''' 備考(1)チェック時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Remark1RadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Remark1RadioButton.CheckedChanged
        CommonRemarkRaddioButton_CheckedChanged()
    End Sub

    ''' <summary>
    ''' 備考(2)チェック時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Remark2RadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Remark2RadioButton.CheckedChanged
        CommonRemarkRaddioButton_CheckedChanged()
    End Sub

    ''' <summary>
    ''' 用紙サイズ(A4)チェック時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub A4SizeRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles A4SizeRadioButton.CheckedChanged
        CommonSizeRaddioButton_CheckedChanged()
    End Sub

    ''' <summary>
    ''' 用紙サイズ（A3）チェック時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub A3SizeRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles A3SizeRadioButton.CheckedChanged
        CommonSizeRaddioButton_CheckedChanged()
    End Sub

    ''' <summary>
    ''' 用紙サイズ変更時の共通処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CommonSizeRaddioButton_CheckedChanged()
        SyncLock (GetType(Application))

            Dim setValue = -1
            For i As Integer = 0 To sizeArray.Length - 1
                If sizeArray(i).Checked Then
                    setValue = i
                End If
            Next
            m_setting.size = setValue
        End SyncLock
    End Sub

    ''' <summary>
    ''' 色選択ボタン（新）押下時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub SelectNewColColorButton_Click(sender As System.Object, e As System.EventArgs) Handles SelectNewColColorButton.Click
        CommonColorButton_Click(NewColColorImageLabel, NewColRedRadioButton, NewColBlueRadioButton, NewColGreenRadioButton, m_setting.newColColor)
    End Sub

    ''' <summary>
    ''' 色選択ボタン（旧）押下時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub SelectOldColColorButton_Click(sender As System.Object, e As System.EventArgs) Handles SelectOldColColorButton.Click
        CommonColorButton_Click(OldColColorImageLabel, OldColRedRadioButton, OldColBlueRadioButton, OldColGreenRadioButton, m_setting.oldColColor)
    End Sub

    ''' <summary>
    ''' 色選択ボタン押下時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="colImageLabel">色見本ラベル</param>
    ''' <param name="rRadioButton">赤ラジオボタン</param>
    ''' <param name="bRadioButton">青ラジオボタン</param>
    ''' <param name="gRadioButton">緑ラジオボタン</param>
    ''' <param name="settingcolor">設定情報</param>
    ''' <remarks></remarks>
    Private Sub CommonColorButton_Click(ByRef colImageLabel As Label, ByRef rRadioButton As RadioButton, ByRef bRadioButton As RadioButton, ByRef gRadioButton As RadioButton, ByRef settingcolor As Color)
        SyncLock (GetType(Application))

            'ColorDialogクラスのインスタンスを作成
            Dim cd As New ColorDialog()

            'はじめに選択されている色を設定
            cd.Color = colImageLabel.BackColor

            Dim beforeColor = colImageLabel.BackColor

            If m_cunstomColor IsNot Nothing Then
                cd.CustomColors = m_cunstomColor
            End If
            '[作成した色]に指定した色（RGB値）を表示する
            If cd.ShowDialog() = DialogResult.OK Then
                '選択された色の取得
                colImageLabel.BackColor = cd.Color

                If colImageLabel.BackColor.ToArgb = Color.Red.ToArgb Then
                    rRadioButton.Checked = True
                ElseIf colImageLabel.BackColor.ToArgb = Color.Blue.ToArgb Then
                    bRadioButton.Checked = True
                ElseIf colImageLabel.BackColor.ToArgb = Color.Green.ToArgb Then
                    gRadioButton.Checked = True
                Else
                    If beforeColor.ToArgb = Color.Red.ToArgb Then
                        rRadioButton.Checked = False
                    ElseIf beforeColor.ToArgb = Color.Blue.ToArgb Then
                        bRadioButton.Checked = False
                    ElseIf beforeColor.ToArgb = Color.Green.ToArgb Then
                        gRadioButton.Checked = False
                    End If
                End If

                settingcolor = cd.Color

            End If

            m_cunstomColor = cd.CustomColors
        End SyncLock

    End Sub

    ''' <summary>
    ''' 新差分色（赤）ラジオボタン選択時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub NewColRedRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles NewColRedRadioButton.CheckedChanged
        CommonColorRadioButton_Click(NewColColorImageLabel, NewColRedRadioButton, NewColBlueRadioButton, NewColGreenRadioButton, m_setting.newColColor)
    End Sub

    ''' <summary>
    ''' 新差分色（青）ラジオボタン選択時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub NewColBlueRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles NewColBlueRadioButton.CheckedChanged
        CommonColorRadioButton_Click(NewColColorImageLabel, NewColRedRadioButton, NewColBlueRadioButton, NewColGreenRadioButton, m_setting.newColColor)
    End Sub

    ''' <summary>
    ''' 新差分色（緑）ラジオボタン選択時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub NewColGreenRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles NewColGreenRadioButton.CheckedChanged
        CommonColorRadioButton_Click(NewColColorImageLabel, NewColRedRadioButton, NewColBlueRadioButton, NewColGreenRadioButton, m_setting.newColColor)
    End Sub

    ''' <summary>
    ''' 色ラジオボタン変更時の共通処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="colImageLabel">色見本ラベル</param>
    ''' <param name="rRadioButton">赤ラジオボタン</param>
    ''' <param name="bRadioButton">青ラジオボタン</param>
    ''' <param name="gRadioButton">緑ラジオボタン</param>
    ''' <param name="settingcolor">設定情報</param>
    ''' <remarks></remarks>
    Private Sub CommonColorRadioButton_Click(ByRef colImageLabel As Label, ByRef rRadioButton As RadioButton, ByRef bRadioButton As RadioButton, ByRef gRadioButton As RadioButton, ByRef settingcolor As Color)
        SyncLock (GetType(Application))

            If rRadioButton.Checked Then
                colImageLabel.BackColor = Color.Red
                settingcolor = Color.Red
                If rRadioButton.Name.Equals(OldColRedRadioButton.Name) Then
                    OldColRedRadioButtonPic.Image = My.Resources.Resources.setting_6_btnred_on
                    OldColBlueRadioButtonPic.Image = My.Resources.Resources.setting_6_btnblue_off
                    OldColGreenRadioButtonPic.Image = My.Resources.Resources.setting_6_btngreen_off
                Else
                    NewColRedRadioButtonPic.Image = My.Resources.Resources.setting_6_btnred_on
                    NewColBlueRadioButtonPic.Image = My.Resources.Resources.setting_6_btnblue_off
                    NewColGreenRadioButtonPic.Image = My.Resources.Resources.setting_6_btngreen_off
                End If

            ElseIf bRadioButton.Checked Then
                colImageLabel.BackColor = Color.Blue
                settingcolor = Color.Blue
                If bRadioButton.Name.Equals(OldColBlueRadioButton.Name) Then
                    OldColRedRadioButtonPic.Image = My.Resources.Resources.setting_6_btnred_off
                    OldColBlueRadioButtonPic.Image = My.Resources.Resources.setting_6_btnblue_on
                    OldColGreenRadioButtonPic.Image = My.Resources.Resources.setting_6_btngreen_off
                Else
                    NewColRedRadioButtonPic.Image = My.Resources.Resources.setting_6_btnred_off
                    NewColBlueRadioButtonPic.Image = My.Resources.Resources.setting_6_btnblue_on
                    NewColGreenRadioButtonPic.Image = My.Resources.Resources.setting_6_btngreen_off
                End If

            ElseIf gRadioButton.Checked Then
                colImageLabel.BackColor = Color.Green
                settingcolor = Color.Green
                If gRadioButton.Name.Equals(OldColGreenRadioButton.Name) Then
                    OldColRedRadioButtonPic.Image = My.Resources.Resources.setting_6_btnred_off
                    OldColBlueRadioButtonPic.Image = My.Resources.Resources.setting_6_btnblue_off
                    OldColGreenRadioButtonPic.Image = My.Resources.Resources.setting_6_btngreen_on
                Else
                    NewColRedRadioButtonPic.Image = My.Resources.Resources.setting_6_btnred_off
                    NewColBlueRadioButtonPic.Image = My.Resources.Resources.setting_6_btnblue_off
                    NewColGreenRadioButtonPic.Image = My.Resources.Resources.setting_6_btngreen_on
                End If

            Else
                If rRadioButton.Name.Equals(OldColRedRadioButton.Name) Then
                    OldColRedRadioButtonPic.Image = My.Resources.Resources.setting_6_btnred_off
                    OldColBlueRadioButtonPic.Image = My.Resources.Resources.setting_6_btnblue_off
                    OldColGreenRadioButtonPic.Image = My.Resources.Resources.setting_6_btngreen_off
                Else
                    NewColRedRadioButtonPic.Image = My.Resources.Resources.setting_6_btnred_off
                    NewColBlueRadioButtonPic.Image = My.Resources.Resources.setting_6_btnblue_off
                    NewColGreenRadioButtonPic.Image = My.Resources.Resources.setting_6_btngreen_off
                End If
            End If

        End SyncLock
    End Sub

    ''' <summary>
    ''' 旧差分色（赤）ラジオボタン選択時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub OldColRedRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles OldColRedRadioButton.CheckedChanged
        CommonColorRadioButton_Click(OldColColorImageLabel, OldColRedRadioButton, OldColBlueRadioButton, OldColGreenRadioButton, m_setting.oldColColor)
    End Sub

    ''' <summary>
    ''' 旧差分色（青）ラジオボタン選択時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub OldColBlueRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles OldColBlueRadioButton.CheckedChanged
        CommonColorRadioButton_Click(OldColColorImageLabel, OldColRedRadioButton, OldColBlueRadioButton, OldColGreenRadioButton, m_setting.oldColColor)
    End Sub

    ''' <summary>
    ''' 旧差分色（緑）ラジオボタン選択時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub OldColGreenRadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles OldColGreenRadioButton.CheckedChanged
        CommonColorRadioButton_Click(OldColColorImageLabel, OldColRedRadioButton, OldColBlueRadioButton, OldColGreenRadioButton, m_setting.oldColColor)
    End Sub

    ''' <summary>
    ''' 新差分の下線チェックボックス変更時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub NewColUnderlineCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles NewColUnderlineCheckBox.CheckedChanged
        CommonCheckBox_CheckedChanged(NewColUnderlineCheckBox, m_setting.newUnderline)

        If NewColUnderlineCheckBox.Checked = True Then
            NewColUnderlineCheckBoxPic.Image = My.Resources.Resources.setting_6_btn5_on
        Else
            NewColUnderlineCheckBoxPic.Image = My.Resources.Resources.setting_6_btn5_off

        End If
    End Sub

    ''' <summary>
    ''' 旧差分の下線チェックボックス変更時の処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub OldColUnderlineCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles OldColUnderlineCheckBox.CheckedChanged
        CommonCheckBox_CheckedChanged(OldColUnderlineCheckBox, m_setting.oldUnderline)
        If OldColUnderlineCheckBox.Checked = True Then
            OldColUnderlineCheckBoxPic.Image = My.Resources.Resources.setting_6_btn5_on
        Else
            OldColUnderlineCheckBoxPic.Image = My.Resources.Resources.setting_6_btn5_off

        End If
    End Sub

    ''' <summary>
    ''' チェックボックス変更時の共通処理
    ''' メイン画面からの移植
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="cb">変更されたチェックボックス</param>
    ''' <param name="setting">変更する設定情報</param>
    ''' <remarks></remarks>
    Private Sub CommonCheckBox_CheckedChanged(ByRef cb As CheckBox, ByRef setting As Boolean)
        SyncLock (GetType(Application))
            setting = cb.Checked
        End SyncLock
    End Sub

    ''' <summary>
    ''' カスタムパレットのカラー情報
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CustomColor As Integer()
        Get
            Return m_cunstomColor
        End Get
        Set(value As Integer())
            m_cunstomColor = value
        End Set
    End Property


    ''' <summary>
    ''' 旧差分の取消線チェックボックス変更時の処理
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub OldColStrikeCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles OldColStrikeCheckBox.CheckedChanged
        CommonCheckBox_CheckedChanged(OldColStrikeCheckBox, m_setting.oldColStrike)
        If OldColStrikeCheckBox.Checked = True Then
            OldColStrikeCheckBoxPic.Image = My.Resources.Resources.setting_6_btn6_on
        Else
            OldColStrikeCheckBoxPic.Image = My.Resources.Resources.setting_6_btn6_off
        End If
    End Sub

    ''' <summary>
    ''' 更新日の自動チェックチェックボックス変更時の処理
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub CheckUpdatedDateCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckUpdatedDateCheckBox.CheckedChanged
        CommonCheckBox_CheckedChanged(CheckUpdatedDateCheckBox, m_setting.autoCheckUpdatedDate)
        ' 更新日の自動チェックが有効になった場合は、警告表示無効を解除する。
        If m_setting.autoCheckUpdatedDate AndAlso m_initialized Then
            m_setting.autoCheckIgnoreUpdatedDateWarning = False
        End If

        If CheckUpdatedDateCheckBox.Checked = True Then
            CheckUpdatedDateCheckBoxPic.Image = My.Resources.setting_4_btn1_on
        Else
            CheckUpdatedDateCheckBoxPic.Image = My.Resources.setting_4_btn1_off
        End If
    End Sub

    ''' <summary>
    ''' フォルダ表示チェックボックス変更時の処理
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub FolderCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles FolderCheckBox.CheckedChanged
        CommonCheckBox_CheckedChanged(FolderCheckBox, m_setting.outputFolder)
        If FolderCheckBox.Checked = True Then
            FolderCheckBoxPic.Image = My.Resources.setting_5_btn1_on
        Else
            FolderCheckBoxPic.Image = My.Resources.setting_5_btn1_off
        End If
    End Sub

    ''' <summary>
    ''' ファイル表示チェックボックス変更時の処理
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub FileCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles FileCheckBox.CheckedChanged
        CommonCheckBox_CheckedChanged(FileCheckBox, m_setting.outputFile)

        If FileCheckBox.Checked = True Then
            FileCheckBoxPic.Image = My.Resources.setting_5_btn2_on
        Else
            FileCheckBoxPic.Image = My.Resources.setting_5_btn2_off
        End If
    End Sub

    ''' <summary>
    ''' ファイル更新日時表示チェックボックス変更時の処理
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub UpdatedDateCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles UpdatedDateCheckBox.CheckedChanged
        CommonCheckBox_CheckedChanged(UpdatedDateCheckBox, m_setting.outputUpdatedDate)
        If UpdatedDateCheckBox.Checked = True Then
            UpdatedDateCheckBoxPic.Image = My.Resources.setting_5_btn3_on
        Else
            UpdatedDateCheckBoxPic.Image = My.Resources.setting_5_btn3_off
        End If
    End Sub

    ''' <summary>
    ''' OKボタン押下時の処理
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub OKButton_Click(sender As System.Object, e As System.EventArgs) Handles OKButton.Click
        m_setting.save()
        m_changed = True
        Me.Close()
    End Sub

    ''' <summary>
    ''' 適用ボタン押下時の処理
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub SaveButton_Click(sender As System.Object, e As System.EventArgs) Handles SaveButton.Click
        m_setting.save()
        m_changed = True
    End Sub

    ''' <summary>
    ''' キャンセルボタン押下時の処理
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ExitButton_Click(sender As System.Object, e As System.EventArgs) Handles ExitButton.Click
        Me.Close()
    End Sub

    ''' <summary>
    ''' 変更が保存されたかどうか
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <returns>True・・・変更が保存された False・・・変更が保存されていない</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Changed As Boolean
        Get
            Return m_changed
        End Get
    End Property
    ''' <summary>
    ''' キャンセルボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SettingCancleBtn_MouseHover(sender As Object, e As EventArgs) Handles SettingCancleBtn.MouseHover
        SettingCancleBtn.Image = My.Resources.setting_cancel_btn_hover
    End Sub
    ''' <summary>
    ''' キャンセルボタンのマウス離れ
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SettingCancleBtn_MouseLeave(sender As Object, e As EventArgs) Handles SettingCancleBtn.MouseLeave
        SettingCancleBtn.Image = My.Resources.setting_cancel_btn
    End Sub
    ''' <summary>
    ''' 保存ボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SettingSaveBtn_MouseHover(sender As Object, e As EventArgs) Handles SettingSaveBtn.MouseHover
        SettingSaveBtn.Image = My.Resources.setting_save_btn_hover
    End Sub
    ''' <summary>
    ''' 保存ボタンのマウス離れ
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SettingSaveBtn_MouseLeave(sender As Object, e As EventArgs) Handles SettingSaveBtn.MouseLeave
        SettingSaveBtn.Image = My.Resources.setting_save_btn
    End Sub
    ''' <summary>
    ''' キャンセルボタン押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SettingCancleBtn_Click(sender As Object, e As EventArgs) Handles SettingCancleBtn.Click
        Me.Close()
    End Sub

    Private Sub OutputSettingForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' ### 結果表示方法の初期設定 ###
        ' 差分のみ表示checkboxの初期設定
        'If DiffOnlyCheckBox.Checked = False Then
        '    DiffOnlyCheckBoxPic.Image = My.Resources.setting_1_btn1_off
        'Else
        '    DiffOnlyCheckBoxPic.Image = My.Resources.setting_1_btn1_on
        'End If
        '' 追加／削除を表示checkboxの初期設定
        'If AddAndDeleteCheckBox.Checked = False Then
        '    AddAndDeleteCheckBoxPic.Image = My.Resources.setting_1_btn2_off
        'Else
        '    AddAndDeleteCheckBoxPic.Image = My.Resources.setting_1_btn2_on
        'End If
        '' 変更前・後を逆にするcheckboxの初期設定
        'If ReverseColCheckBox.Checked = False Then
        '    ReverseColCheckBoxPic.Image = My.Resources.setting_1_btn3_off
        'Else
        '    ReverseColCheckBoxPic.Image = My.Resources.setting_1_btn3_on
        'End If

        ' ### 備考欄の表示の初期設定 ###
        If NoRemarkRadioButton.Checked = True Then
            NoRemarkRadioButtonPic.Image = My.Resources.setting_2_btn1_on
            Remark1RadioButtonPic.Image = My.Resources.setting_2_btn2_off
            Remark2RadioButtonPic.Image = My.Resources.setting_2_btn3_off
        End If
        If Remark1RadioButton.Checked = True Then
            NoRemarkRadioButtonPic.Image = My.Resources.setting_2_btn1_off
            Remark1RadioButtonPic.Image = My.Resources.setting_2_btn2_on
            Remark2RadioButtonPic.Image = My.Resources.setting_2_btn3_off
        End If
        If Remark2RadioButton.Checked = True Then
            NoRemarkRadioButtonPic.Image = My.Resources.setting_2_btn1_off
            Remark1RadioButtonPic.Image = My.Resources.setting_2_btn2_off
            Remark2RadioButtonPic.Image = My.Resources.setting_2_btn3_on
        End If

        ' ### 出力サイズの初期設定 ###
        If A4SizeRadioButton.Checked = True Then
            A4SizeRadioButtonPic.Image = My.Resources.setting_3_btn1_on
            A3SizeRadioButtonPic.Image = My.Resources.setting_3_btn2_off
        End If
        If A3SizeRadioButton.Checked = True Then
            A4SizeRadioButtonPic.Image = My.Resources.setting_3_btn1_off
            A3SizeRadioButtonPic.Image = My.Resources.setting_3_btn2_on
        End If

        ' ### 自動チェックの初期設定 ###
        'If CheckUpdatedDateCheckBox.Checked = False Then
        '    CheckUpdatedDateCheckBoxPic.Image = My.Resources.setting_4_btn1_off
        'Else
        '    CheckUpdatedDateCheckBoxPic.Image = My.Resources.setting_4_btn1_on
        'End If

        ' ### 結果画面への表示の初期設定 ###
        '' フォルダ名checkboxの初期設定
        'If FolderCheckBox.Checked = False Then
        '    FolderCheckBoxPic.Image = My.Resources.setting_5_btn1_off
        'Else
        '    FolderCheckBoxPic.Image = My.Resources.setting_5_btn1_on
        'End If
        '' ファイル名checkboxの初期設定
        'If FileCheckBox.Checked = False Then
        '    FileCheckBoxPic.Image = My.Resources.setting_5_btn2_off
        'Else
        '    FileCheckBoxPic.Image = My.Resources.setting_5_btn2_on
        'End If
        '' ファイル更新日時checkboxの初期設定
        'If UpdatedDateCheckBox.Checked = False Then
        '    UpdatedDateCheckBoxPic.Image = My.Resources.setting_5_btn3_off
        'Else
        '    UpdatedDateCheckBoxPic.Image = My.Resources.setting_5_btn3_on
        'End If

    End Sub

    Private Sub DiffOnlyCheckBoxPic_Click(sender As Object, e As EventArgs) Handles DiffOnlyCheckBoxPic.Click
        ' 差分のみ表示checkboxの設定
        DiffOnlyCheckBox.Checked = Not DiffOnlyCheckBox.Checked

    End Sub

    Private Sub AddAndDeleteCheckBoxPic_Click(sender As Object, e As EventArgs) Handles AddAndDeleteCheckBoxPic.Click
        ' 追加／削除を表示checkboxの設定
        AddAndDeleteCheckBox.Checked = Not AddAndDeleteCheckBox.Checked
    End Sub

    Private Sub ReverseColCheckBoxPic_Click(sender As Object, e As EventArgs) Handles ReverseColCheckBoxPic.Click
        ' 変更前・後を逆にするcheckboxの設定
        ReverseColCheckBox.Checked = Not ReverseColCheckBox.Checked
    End Sub

    Private Sub NoRemarkRadioButtonPic_Click(sender As Object, e As EventArgs) Handles NoRemarkRadioButtonPic.Click
        ' 備考欄の表示の設定(なし)
        NoRemarkRadioButton.Checked = True
        NoRemarkRadioButtonPic.Image = My.Resources.setting_2_btn1_on
        Remark1RadioButtonPic.Image = My.Resources.setting_2_btn2_off
        Remark2RadioButtonPic.Image = My.Resources.setting_2_btn3_off
    End Sub

    Private Sub Remark1RadioButtonPic_Click(sender As Object, e As EventArgs) Handles Remark1RadioButtonPic.Click
        ' 備考欄の表示の設定(1列)
        Remark1RadioButton.Checked = True
        NoRemarkRadioButtonPic.Image = My.Resources.setting_2_btn1_off
        Remark1RadioButtonPic.Image = My.Resources.setting_2_btn2_on
        Remark2RadioButtonPic.Image = My.Resources.setting_2_btn3_off
    End Sub

    Private Sub Remark2RadioButtonPic_Click(sender As Object, e As EventArgs) Handles Remark2RadioButtonPic.Click
        ' 備考欄の表示の設定(2列)
        Remark2RadioButton.Checked = True
        NoRemarkRadioButtonPic.Image = My.Resources.setting_2_btn1_off
        Remark1RadioButtonPic.Image = My.Resources.setting_2_btn2_off
        Remark2RadioButtonPic.Image = My.Resources.setting_2_btn3_on
    End Sub

    Private Sub A4SizeRadioButtonPic_Click(sender As Object, e As EventArgs) Handles A4SizeRadioButtonPic.Click
        ' 出力サイズの設定(A4)
        A4SizeRadioButton.Checked = True
        A4SizeRadioButtonPic.Image = My.Resources.setting_3_btn1_on
        A3SizeRadioButtonPic.Image = My.Resources.setting_3_btn2_off
    End Sub

    Private Sub A3SizeRadioButtonPic_Click(sender As Object, e As EventArgs) Handles A3SizeRadioButtonPic.Click
        ' 出力サイズの設定(A3)
        A3SizeRadioButton.Checked = True
        A4SizeRadioButtonPic.Image = My.Resources.setting_3_btn1_off
        A3SizeRadioButtonPic.Image = My.Resources.setting_3_btn2_on
    End Sub

    Private Sub CheckUpdatedDateCheckBoxPic_Click(sender As Object, e As EventArgs) Handles CheckUpdatedDateCheckBoxPic.Click
        ' ### 自動チェックの設定 ###
        CheckUpdatedDateCheckBox.Checked = Not CheckUpdatedDateCheckBox.Checked

    End Sub
    ''' <summary>
    ''' 保存して閉じる
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SettingSaveBtn_Click(sender As Object, e As EventArgs) Handles SettingSaveBtn.Click
        OKButton_Click(sender, e)
    End Sub

    Private Sub FolderCheckBoxPic_Click(sender As Object, e As EventArgs) Handles FolderCheckBoxPic.Click
        ' フォルダ名checkboxの初期設定
        FolderCheckBox.Checked = Not FolderCheckBox.Checked
    End Sub

    Private Sub FileCheckBoxPic_Click(sender As Object, e As EventArgs) Handles FileCheckBoxPic.Click
        ' ファイル名checkboxの初期設定
        FileCheckBox.Checked = Not FileCheckBox.Checked
    End Sub

    Private Sub UpdatedDateCheckBoxPic_Click(sender As Object, e As EventArgs) Handles UpdatedDateCheckBoxPic.Click
        ' ファイル更新日時checkboxの初期設定
        UpdatedDateCheckBox.Checked = Not UpdatedDateCheckBox.Checked

    End Sub
    ''' <summary>
    ''' 旧ファイル色選択マウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SelectOldColColorButtonPic_MouseHover(sender As Object, e As EventArgs) Handles SelectOldColColorButtonPic.MouseHover
        SelectOldColColorButtonPic.Image = My.Resources.Resources.setting_6_btn4_hover
    End Sub
    ''' <summary>
    ''' 旧ファイル色選択マウス離れ
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SelectOldColColorButtonPic_MouseLeave(sender As Object, e As EventArgs) Handles SelectOldColColorButtonPic.MouseLeave
        SelectOldColColorButtonPic.Image = My.Resources.Resources.setting_6_btn4
    End Sub
    ''' <summary>
    ''' 旧ファイル色選択押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SelectOldColColorButtonPic_Click(sender As Object, e As EventArgs) Handles SelectOldColColorButtonPic.Click
        SelectOldColColorButton_Click(sender, e)
    End Sub

    Private Sub OldColRedRadioButtonPic_Click(sender As Object, e As EventArgs) Handles OldColRedRadioButtonPic.Click
        OldColRedRadioButton.Checked = True
    End Sub

    Private Sub OldColBlueRadioButtonPic_Click(sender As Object, e As EventArgs) Handles OldColBlueRadioButtonPic.Click
        OldColBlueRadioButton.Checked = True
    End Sub

    Private Sub OldColGreenRadioButtonPic_Click(sender As Object, e As EventArgs) Handles OldColGreenRadioButtonPic.Click
        OldColGreenRadioButton.Checked = True
    End Sub

    Private Sub OldColUnderlineCheckBoxPic_Click(sender As Object, e As EventArgs) Handles OldColUnderlineCheckBoxPic.Click
        OldColUnderlineCheckBox.Checked = Not OldColUnderlineCheckBox.Checked
    End Sub

    Private Sub OldColStrikeCheckBoxPic_Click(sender As Object, e As EventArgs) Handles OldColStrikeCheckBoxPic.Click
        OldColStrikeCheckBox.Checked = Not OldColStrikeCheckBox.Checked
    End Sub

    Private Sub SelectNewColColorButtonPic_Click(sender As Object, e As EventArgs) Handles SelectNewColColorButtonPic.Click
        SelectNewColColorButton_Click(sender, e)
    End Sub

    Private Sub NewColUnderlineCheckBoxPic_Click(sender As Object, e As EventArgs) Handles NewColUnderlineCheckBoxPic.Click
        NewColUnderlineCheckBox.Checked = Not NewColUnderlineCheckBox.Checked
    End Sub

    Private Sub SelectNewColColorButtonPic_MouseHover(sender As Object, e As EventArgs) Handles SelectNewColColorButtonPic.MouseHover
        SelectNewColColorButtonPic.Image = My.Resources.Resources.setting_6_btn4_hover
    End Sub

    Private Sub SelectNewColColorButtonPic_MouseLeave(sender As Object, e As EventArgs) Handles SelectNewColColorButtonPic.MouseLeave
        SelectNewColColorButtonPic.Image = My.Resources.Resources.setting_6_btn4
    End Sub

    Private Sub NewColRedRadioButtonPic_Click(sender As Object, e As EventArgs) Handles NewColRedRadioButtonPic.Click
        NewColRedRadioButton.Checked = True
    End Sub

    Private Sub NewColBlueRadioButtonPic_Click(sender As Object, e As EventArgs) Handles NewColBlueRadioButtonPic.Click
        NewColBlueRadioButton.Checked = True
    End Sub

    Private Sub NewColGreenRadioButtonPic_Click(sender As Object, e As EventArgs) Handles NewColGreenRadioButtonPic.Click
        NewColGreenRadioButton.Checked = True
    End Sub
End Class
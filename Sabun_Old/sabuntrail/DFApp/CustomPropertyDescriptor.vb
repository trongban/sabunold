﻿Imports System.ComponentModel

''' <summary>
''' PropertyGridをカスタマイズするためのCustomPropertyDescriptor
''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
''' </summary>
''' <remarks></remarks>
Public Class CustomPropertyDescriptor
    Inherits PropertyDescriptor

    ''' <summary>
    ''' 従来のPropertyDescriptor
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private oneProperty As PropertyDescriptor

    ''' <summary>
    ''' コンストラクタ
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="desc"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal desc As PropertyDescriptor)
        MyBase.New(desc)
        oneProperty = desc
    End Sub

    ''' <summary>
    ''' 従来のPropertyDescriptorと同機能
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="component"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Function CanResetValue(ByVal component As Object) As Boolean
        Return oneProperty.CanResetValue(component)
    End Function

    ''' <summary>
    ''' 従来のPropertyDescriptorと同機能
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property ComponentType() As Type
        Get
            Return oneProperty.ComponentType
        End Get
    End Property

    ''' <summary>
    ''' 従来のPropertyDescriptorと同機能
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="component"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Function GetValue(ByVal component As Object) As Object
        Return oneProperty.GetValue(component)
    End Function

    ''' <summary>
    ''' 従来のPropertyDescriptorと同機能
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property Description() As String
        Get
            Return oneProperty.Description
        End Get
    End Property

    ''' <summary>
    ''' 従来のPropertyDescriptorと同機能
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property Category() As String
        Get
            Return oneProperty.Category
        End Get
    End Property

    ''' <summary>
    ''' 従来のPropertyDescriptorと同機能
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property IsReadOnly() As Boolean
        Get
            Return oneProperty.IsReadOnly
        End Get
    End Property

    ''' <summary>
    ''' 従来のPropertyDescriptorと同機能
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="component"></param>
    ''' <remarks></remarks>
    Public Overrides Sub ResetValue(ByVal component As Object)
        oneProperty.ResetValue(component)
    End Sub

    ''' <summary>
    ''' 従来のPropertyDescriptorと同機能
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="component"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Function ShouldSerializeValue(ByVal component As Object) As Boolean
        Return oneProperty.ShouldSerializeValue(component)
    End Function

    ''' <summary>
    ''' 従来のPropertyDescriptorと同機能
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="component"></param>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Public Overrides Sub SetValue(ByVal component As Object, ByVal value As Object)
        oneProperty.SetValue(component, value)
    End Sub

    ''' <summary>
    ''' 従来のPropertyDescriptorと同機能
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property PropertyType() As Type
        Get
            Return oneProperty.PropertyType
        End Get
    End Property

    ''' <summary>
    ''' 表示名をカスタマイズする。PropertyNameAttriuteを使用するように変更する。
    ''' PropertyNameAttributeが使用できない場合は、従来のPropertyDescriptorと
    ''' 同じ動作をする。
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property DisplayName() As String
        Get
            Dim attrib As PropertyDisplayNameAttribute = CType(oneProperty.Attributes(GetType(PropertyDisplayNameAttribute)), PropertyDisplayNameAttribute)
            If Not (attrib Is Nothing) Then
                Return attrib.PropertyDisplayName
            End If

            Return oneProperty.DisplayName
        End Get
    End Property
End Class

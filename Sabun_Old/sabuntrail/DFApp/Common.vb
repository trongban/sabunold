﻿Imports System.IO
Imports System.Security.AccessControl

Imports System.Security.Cryptography
Imports System.Text

Imports System.Net.NetworkInformation

''' <summary>
''' 共通処理クラス
''' </summary>
''' <remarks></remarks>
Public Class Common

    ''' <summary>
    ''' お試し期間日数
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MAX_DAY_TRIAL As Integer = 14

    ''' <summary>
    ''' デフォルトの日付フォーマット
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Const DEFAULT_DATE_FORMAT As String = "yyyy/MM/dd HH:mm:ss"

    ''' <summary>
    ''' XMLの禁則文字の一部。（置換前）
    ''' System.Security.EscapeだとOffice側が実体参照の処理ができない。
    ''' よって大文字に置き換える。
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared BeforeString As String() = {"'", "&"}

    ''' <summary>
    ''' XMLの禁則文字の一部。（置換後）
    ''' System.Security.EscapeだとOffice側が実体参照の処理ができない。
    ''' よって大文字に置き換える。
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared AfterString As String() = {"’", "＆"}


    ''' <summary>
    ''' DES暗号化で用いるKey文字列
    ''' 2018/03/07 ADD [オンラインアクティベーション対応] ohta
    ''' </summary>
    ''' <remarks></remarks>
    Private Const DesKey As String = "SabunTrailAuthKy"

    '''<summary>
    ''' DES暗号化で用いるKey文字列
    ''' 2018/03/07 ADD [オンラインアクティベーション対応] ohta
    ''' </summary>
    ''' <remarks></remarks>
    Private Const DesIv As String = "SabunAut"

    '''<summary>
    ''' レジストリキー値文字列
    ''' 2018/03/07 ADD [オンラインアクティベーション対応] ohta
    ''' </summary>
    ''' <remarks></remarks>
    Private Const RegKeyStr As String = "HKEY_LOCAL_MACHINE\SOFTWARE\TOPPANFORMS\SabunTrail"


    ''' <summary>
    ''' ライセンスの認証状態
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum LIC_ENUM
        TRIAL = 0
        OK = 1
        NG = 2
    End Enum

    ''' <summary>
    ''' オンライン認証結果
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum STS_ENUM
        OK = 0
        PARM_ERR = 1
        KEY_LEN_ERR = 2
        KEY_TYPE_ERR = 3
        KEY_DISABLE = 4
        SYS_ERR = 9
    End Enum

    ''' <summary>
    ''' 起動時ライセンスチェック処理
    ''' 2018/03/07 ADD [オンラインアクティベーション対応] ohta レジストリを使用するよう全面改訂
    ''' </summary>
    ''' <returns>ライセンスチェック成否（LIC_ENUM）</returns>
    ''' <remarks></remarks>
    Public Shared Function licCheck() As Integer
        ' レジストリの値を読む
        Dim regHashKey = My.Computer.Registry.GetValue(RegKeyStr, "HashKey", Nothing)
        Dim regLicenseKey = My.Computer.Registry.GetValue(RegKeyStr, "LicKey", Nothing)
        Dim regActivateKey = My.Computer.Registry.GetValue(RegKeyStr, "ActKey", Nothing)

        '        Dim frm As DFForm = DFForm.ActiveForm

        ' アクティベートキーが無く、残りのキーが両方ある場合は再度ネットワーク認証
        If (regActivateKey Is Nothing And (regHashKey IsNot Nothing And regLicenseKey IsNot Nothing)) Then
            ' レジストリのライセンスキーを使用する
            If keyLib.NetWorkKeyChecker.NetWorkKeyCheck(regLicenseKey) = STS_ENUM.OK Then
                Return LIC_ENUM.OK
            End If
        End If

        ' 全てのKeyがレジストリに登録されている場合
        If (regActivateKey IsNot Nothing And regHashKey IsNot Nothing And regLicenseKey IsNot Nothing) Then
            ' ハッシュキーを再生成するためMACアドレスを取得
            Dim macAddress = Nothing
            Dim interfaces = NetworkInterface.GetAllNetworkInterfaces()

            For Each adapter As NetworkInterface In interfaces
                ' 有効なもののみ取得。複数とれる場合は先頭のみ使用する。
                If adapter.OperationalStatus = OperationalStatus.Up Then
                    macAddress = adapter.GetPhysicalAddress().ToString
                End If
                ' MACアドレスと、レジストリから取得したライセンスキーを結合しハッシュ化、レジストリから取得したハッシュキーと前方一致比較する
                ' 複数MACアドレスが存在する環境で、取得されるMACアドレスの順番が不定だった場合の対策
                If (StrComp(keyLib.KeyLib.Encrypt256(regLicenseKey & macAddress).Substring(0, 20), regHashKey) = 0) Then
                    If (StrComp(keyLib.KeyLib.Encrypt256(regLicenseKey & regHashKey).Substring(0, 20), regActivateKey) = 0) Then
                        Return LIC_ENUM.OK
                    End If
                End If
            Next
        End If

        If Date.Compare(licStartDate.AddDays(MAX_DAY_TRIAL), Date.Now) < 0 Then
            Return LIC_ENUM.NG
        Else
            Return LIC_ENUM.TRIAL
        End If
    End Function

    ''' <summary>
    ''' ライセンスチェック処理（ネットワーク認証）
    ''' 2018/03/07 ADD [オンラインアクティベーション対応] ohta 新規
    ''' </summary>
    ''' <param name="licStr">入力されたライセンスキー</param>
    ''' <returns>ライセンスチェック成否（LIC_ENUM）</returns>
    ''' <remarks></remarks>
    Public Shared Function licCheckNet(ByRef licStr As String) As Integer
        Dim sts_cd As Integer

        ' ネットワークライセンスチェック
        If licStr.Length > 0 Then
            sts_cd = keyLib.NetWorkKeyChecker.NetWorkKeyCheck(licStr)
            If sts_cd = STS_ENUM.OK Then
                Return LIC_ENUM.OK
            ElseIf sts_cd = STS_ENUM.SYS_ERR Then
                Return STS_ENUM.SYS_ERR
            End If
        End If

        ' お試し期間チェック
        If chkEndDate() Then
            Return LIC_ENUM.TRIAL
        Else
            Return LIC_ENUM.NG
        End If

    End Function

    ''' <summary>
    ''' お試し期間をチェックする。
    ''' 2018/03/07 ADD [オンラインアクティベーション対応] ohta レジストリを使用するよう全面改訂
    ''' </summary>
    ''' <returns>お試し期間有効可否</returns>
    ''' <remarks></remarks>
    Public Shared Function chkEndDate() As Boolean
        Dim regStartDate = My.Computer.Registry.GetValue(RegKeyStr, "regKey", Nothing)

        ' レジストリに登録されていない場合、お試し期間終了済とする。
        If regStartDate = Nothing Then
            Return False
        End If

        If Date.Compare(licStartDate.AddDays(MAX_DAY_TRIAL), Date.Now) < 0 Then
            Return False
        Else
            Return True
        End If

        Return True
    End Function

    ''' <summary>
    ''' レジストリからお試し期間の起点日（インストール日）を取得し復号して返す。
    ''' 2018/03/07 ADD [オンラインアクティベーション対応] ohta 新規
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function licStartDate() As Date
        Dim regStartDate = My.Computer.Registry.GetValue(RegKeyStr, "regKey", Nothing)

        ' レジストリからキーが削除されている場合、Nothingを返却し、お試し期間終了とする
        If regStartDate Is Nothing Then
            Return Nothing
        End If

        ' DES暗号化オブジェクト
        Dim des As New TripleDESCryptoServiceProvider

        des.Key = Encoding.ASCII.GetBytes(DesKey)
        des.IV = Encoding.ASCII.GetBytes(DesIv)

        Dim strLen As Integer = CInt(regStartDate.Length / 2)
        Dim encrypted(strLen - 1) As Byte

        For i As Integer = 0 To strLen - 1
            Dim tempStr As String = regStartDate.Substring(i * 2, 2)
            encrypted(i) = Byte.Parse(tempStr, Globalization.NumberStyles.AllowHexSpecifier)
        Next i

        Dim startDate
        Dim transformDec As ICryptoTransform = des.CreateDecryptor

        Dim msDec As New MemoryStream
        Dim csDec As New CryptoStream(msDec, transformDec, CryptoStreamMode.Write)

        csDec.Write(encrypted, 0, encrypted.Length)
        csDec.Close()
        startDate = Encoding.Unicode.GetString(msDec.ToArray())
        msDec.Close()

        Return Date.Parse(startDate)

    End Function

    ''' <summary>
    ''' お試し期間の終了日を取得する。
    ''' 2018/03/07 ADD [オンラインアクティベーション対応] ohta 互換性を残すため修正
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function licEndDate() As Date
        Return licStartDate.AddDays(MAX_DAY_TRIAL)
    End Function

    ''' <summary>
    ''' ディレクトリをコピーする
    ''' </summary>
    ''' <param name="sourceDirName">コピーするディレクトリ</param>
    ''' <param name="destDirName">コピー先のディレクトリ</param>
    Public Shared Sub CopyDirectory( _
            ByVal sourceDirName As String, _
            ByVal destDirName As String)
        'コピー先のディレクトリがないときは作る
        If Not System.IO.Directory.Exists(destDirName) Then
            System.IO.Directory.CreateDirectory(destDirName)
            ''属性もコピー
            System.IO.File.SetAttributes(destDirName, _
                System.IO.File.GetAttributes(sourceDirName))
        End If

        'コピー先のディレクトリ名の末尾に"\"をつける
        If destDirName.Chars((destDirName.Length - 1)) <> _
                System.IO.Path.DirectorySeparatorChar Then
            destDirName = destDirName + System.IO.Path.DirectorySeparatorChar
        End If

        'コピー元のディレクトリにあるファイルをコピー
        Dim fs As String() = System.IO.Directory.GetFiles(sourceDirName)
        Dim f As String
        For Each f In fs
            System.IO.File.Copy(f, _
                destDirName + System.IO.Path.GetFileName(f), True)
            ' SetAccessRule(destDirName + System.IO.Path.GetFileName(f))
        Next

        'コピー元のディレクトリにあるディレクトリをコピー
        Dim dirs As String() = System.IO.Directory.GetDirectories(sourceDirName)
        Dim dir As String
        For Each dir In dirs
            CopyDirectory(dir, destDirName + System.IO.Path.GetFileName(dir))
        Next
    End Sub

    ''' <summary>
    '''ファイルにEveryoneにフルコントロールのアクセス権を付与する。
    ''' </summary>
    ''' <param name="path"></param>
    ''' <remarks></remarks>
    Public Shared Sub SetAccessRule(path As String)
        Dim FileInfo As New System.IO.FileInfo(path)
        Dim FileSec As FileSecurity
        FileSec = FileInfo.GetAccessControl()

        Dim AccessRule As New FileSystemAccessRule( _
          "Everyone", _
          FileSystemRights.FullControl, _
          AccessControlType.Allow)

        FileSec.AddAccessRule(AccessRule)
        FileInfo.SetAccessControl(FileSec)
    End Sub

    ''' <summary>
    ''' ファイルを削除します。
    ''' 失敗しても無視
    ''' </summary>
    ''' <param name="path">削除ファイルのパス</param>
    ''' <remarks></remarks>
    Public Shared Sub DeleteFile(path As String)
        Try
            If File.Exists(path) Then
                File.Delete(path)
            End If
        Catch e As Exception
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' 引数のディレクトリが存在しない場合は作成する。
    ''' </summary>
    ''' <param name="dir">パス</param>
    ''' <remarks></remarks>
    Public Shared Sub MakeDirNotFoundDir(ByRef dir As String)
        If (Not IO.Directory.Exists(dir)) Then
            IO.Directory.CreateDirectory(dir)
        End If
    End Sub

    ''' <summary>
    ''' 特殊パス\会社名\製品名のパスを返す。
    ''' </summary>
    ''' <param name="folder">特殊パス（列挙体の値）</param>
    ''' <returns>パス</returns>
    ''' <remarks></remarks>
    Public Shared Function GetFileSystemPath(ByRef folder As Environment.SpecialFolder) As String
        Dim path As String = String.Format("{0}\{1}\{2}", Environment.GetFolderPath(folder), Application.CompanyName, Application.ProductName)
        Return path
    End Function

    ''' <summary>
    ''' 日付型を引数指定のフォーマットの文字列に変換する。
    ''' エラーが発生した場合はDEFAULT_DATE_FORMATでフォーマットした値を返す。
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="d">日付型</param>
    ''' <param name="format">フォーマット</param>
    ''' <returns>フォーマットされた文字列</returns>
    ''' <remarks></remarks>
    Public Shared Function ConvertFormattedDate(ByVal d As DateTime, ByVal format As String) As String
        Dim rtn As String = ""
        Try
            rtn = d.ToString(format)
        Catch ex As Exception
            rtn = d.ToString(DEFAULT_DATE_FORMAT)
        End Try
        Return rtn
    End Function

    ''' <summary>
    ''' XML禁則文字の置き換え
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="value">置き換え前の文字列</param>
    ''' <returns>置き換え後の文字列</returns>
    ''' <remarks></remarks>
    Public Shared Function EscapeXMLInterdictionString(ByVal value As String) As String
        Dim rtn As String = value
        If rtn IsNot Nothing Then
            For i As Integer = 0 To BeforeString.Length - 1
                rtn = rtn.Replace(BeforeString(i), AfterString(i))
            Next
        End If
        Return rtn
    End Function

End Class

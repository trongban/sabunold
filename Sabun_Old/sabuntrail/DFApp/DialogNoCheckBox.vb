﻿Public Class DialogNoCheckBox
    ''' <summary>
    ''' 種別
    '''  2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum MESSAGE_TYPE As Integer
        INFO = 0
        QUE = 1
        WARN = 2
        ERR = 3
    End Enum

    ''' <summary>
    ''' コンストラクタ
    '''  2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="argMsg">表示するメッセージ</param>
    ''' <param name="argTitle">メッセージボックスのタイトル</param>
    ''' <param name="argMesType">メッセージ種別</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal argMsg As String, ByVal argTitle As String, ByVal argYesNoFlg As Boolean, Optional ByVal argMesType As MESSAGE_TYPE = MESSAGE_TYPE.INFO)

        ' この呼び出しはデザイナーで必要です。
        InitializeComponent()

        If argYesNoFlg = True Then
            Me.CloseButton.Visible = False
        Else
            Me.YesButton.Visible = False
            Me.NoButton.Visible = False
        End If

        ' InitializeComponent() 呼び出しの後で初期化を追加します。
        MessageLabel.Text = argMsg

        ' Iconの表示処理
        Dim canvas As New Bitmap(IconPictureBox.Width, IconPictureBox.Height)
        Dim g As Graphics = Graphics.FromImage(canvas)
        Me.Text = argTitle

        If argMesType = MESSAGE_TYPE.INFO Then
            g.DrawIcon(SystemIcons.Information, 0, 0)
        ElseIf argMesType = MESSAGE_TYPE.QUE Then
            g.DrawIcon(SystemIcons.Question, 0, 0)
        ElseIf argMesType = MESSAGE_TYPE.WARN Then
            g.DrawIcon(SystemIcons.Warning, 0, 0)
        ElseIf argMesType = MESSAGE_TYPE.ERR Then
            g.DrawIcon(SystemIcons.Error, 0, 0)
        End If

        g.Dispose()

        IconPictureBox.Image = canvas

    End Sub

    ''' <summary>
    ''' はいボタン押下時の処理
    '''  2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub YesButton_Click(sender As System.Object, e As System.EventArgs) Handles YesButton.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Yes
        Me.Close()
    End Sub

    ''' <summary>
    ''' いいえボタン押下時の処理
    '''  2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub NoButton_Click(sender As System.Object, e As System.EventArgs) Handles NoButton.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.No
        Me.Close()
    End Sub


    ''' <summary>
    ''' 閉じるボタン押下時の処理
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub CloseButton_Click(sender As System.Object, e As System.EventArgs) Handles CloseButton.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub


End Class
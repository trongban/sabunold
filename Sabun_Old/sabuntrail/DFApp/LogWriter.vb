﻿Imports System.IO

''' <summary>
''' ログ出力クラス
''' </summary>
''' <remarks></remarks>
Public Class LogWriter

    ''' <summary>
    ''' トレースログファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const LOG_TRC_FILE As String = "\sabun_trace_{0}.log"

    ''' <summary>
    ''' エラーログファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const LOG_ERR_FILE As String = "\sabun_error_{0}.log"

    ''' <summary>
    ''' ログ出力のテンプレート
    ''' </summary>
    ''' <remarks></remarks>
    Private Const LOG_TEMPLATE As String = "[{0}] [{1}] {2}"

    ''' <summary>
    ''' トレースログ用のヘッダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const SEVERITY_TRACE As String = "TRC"

    ''' <summary>
    ''' エラーログ用のヘッダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const SEVERITY_ERROR As String = "ERR"

    ''' <summary>
    ''' ロック用オブジェクト
    ''' </summary>
    ''' <remarks></remarks>
    Private lock As New Object()

    ''' <summary>
    ''' トレースログフラグ
    ''' true・・・有効
    ''' false・・・無効
    ''' </summary>
    ''' <remarks></remarks>
    Private m_trace_on As Boolean = False

    ''' <summary>
    ''' 出力先ディレクトリ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_outDir As String = Nothing

    ''' <summary>
    ''' 書き込みに失敗した場合のリトライ最大回数
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MAX_RETRY_COUNT As Integer = 5

    ''' <summary>
    ''' 書き込みに失敗した場合のインターバル
    ''' </summary>
    ''' <remarks></remarks>
    Private Const SLEEP_TIME As Integer = 200

    ''' <summary>
    ''' コンストラクタ
    ''' </summary>
    ''' <param name="dir">出力先</param>
    ''' <param name="trace_on">トレースログ有無</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal dir As String, Optional ByVal trace_on As Boolean = False)
        m_trace_on = trace_on
        m_outDir = dir
    End Sub

    ''' <summary>
    ''' ファイル出力処理
    ''' </summary>
    ''' <param name="filename">出力ファイルのフルパス</param>
    ''' <param name="message">メッセージ</param>
    ''' <param name="severity">重要度</param>
    ''' <remarks></remarks>
    Private Sub WriteLog(ByVal filename As String, ByVal message As String, ByVal severity As String)
        For i As Integer = 0 To MAX_RETRY_COUNT - 1
            Try
                Using writer As New StreamWriter(filename, True, System.Text.Encoding.GetEncoding("sjis"))
                    Dim logTimeStamp = DateTime.Now.ToString()
                    writer.WriteLine(String.Format(LOG_TEMPLATE, logTimeStamp, severity, message))
                    writer.Flush()
                End Using
                Exit For
            Catch ex As Exception
                Threading.Thread.Sleep(SLEEP_TIME)
            End Try
        Next
    End Sub

    ''' <summary>
    ''' トレースログ
    ''' </summary>
    ''' <param name="msg">メッセージ</param>
    ''' <remarks></remarks>
    Public Sub WriteTraceLog(ByVal msg As String)
        If m_trace_on Then
            SyncLock lock
                WriteLog(String.Format(m_outDir & LOG_TRC_FILE, DateTime.Now.ToString("yyyyMMdd")), msg, SEVERITY_TRACE)
            End SyncLock
        End If
    End Sub

    ''' <summary>
    ''' エラーログ
    ''' </summary>
    ''' <param name="msg">メッセージ</param>
    ''' <remarks></remarks>
    Public Sub WriteErrorLog(ByVal msg As String)
        SyncLock lock
            WriteLog(String.Format(m_outDir & LOG_ERR_FILE, DateTime.Now.ToString("yyyyMMdd")), msg, SEVERITY_ERROR)
            WriteTraceLog(msg)
        End SyncLock
    End Sub

End Class

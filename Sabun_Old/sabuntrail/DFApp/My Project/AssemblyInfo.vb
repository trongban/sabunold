﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' アセンブリに関する一般情報は以下の属性セットをとおして制御されます。 
' アセンブリに関連付けられている情報を変更するには、
' これらの属性値を変更してください。

' アセンブリ属性の値を確認します。

<Assembly: AssemblyTitle("sabuntrail")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("TOPPANFORMS")> 
<Assembly: AssemblyProduct("sabuntrail")> 
<Assembly: AssemblyCopyright("Copyright © TOPPAN FORMS CO., LTD. All rights reserved")> 
<Assembly: AssemblyTrademark("sabuntrail")> 

<Assembly: ComVisible(False)>

'このプロジェクトが COM に公開される場合、次の GUID がタイプ ライブラリの ID になります。
<Assembly: Guid("faddd56e-415f-46d0-b1d7-6e8d1c074952")>

' アセンブリのバージョン情報は、以下の 4 つの値で構成されています:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' すべての値を指定するか、下のように '*' を使ってビルドおよびリビジョン番号を 
' 既定値にすることができます:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.0.1.0")>
<Assembly: AssemblyFileVersion("2.0.1.0")>

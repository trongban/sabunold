﻿Public Class TextBoxEx
    Inherits TextBox

    Private _placeholder As String = String.Empty

    Public Property Placeholder() As String
        Get
            Return _placeholder
        End Get
        Set(value As String)
            _placeholder = value
            Me.Invalidate()
        End Set
    End Property

    Protected Overrides Sub WndProc(ByRef m As Windows.Forms.Message)
        Const WM_PAINT As Int32 = &HF
        MyBase.WndProc(m)

        If m.Msg = WM_PAINT AndAlso String.IsNullOrEmpty(Me.Text) AndAlso String.IsNullOrEmpty(Placeholder) = False Then
            Using g As Graphics = Graphics.FromHwnd(Me.Handle)
                Dim rect As Rectangle = Me.ClientRectangle
                rect.Offset(1, 1)
                TextRenderer.DrawText(g, Placeholder, Me.Font, rect, SystemColors.ControlDark, TextFormatFlags.Top And TextFormatFlags.Left)
            End Using
        End If
    End Sub
End Class

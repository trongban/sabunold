﻿Imports System.ComponentModel

''' <summary>
''' PropertyGridで使用されているPropertyDescriptor
''' をCustomPropertyDescriptorに置き換えるための型コンバーター
''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
''' </summary>
''' <remarks></remarks>
Public Class CustomPropertyConverter
    Inherits TypeConverter

    ''' <summary>
    ''' 指定したコンテキストと属性を使用して、value パラメーターで指定された配列型のプロパティのコレクションを返します。
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="context">書式指定コンテキストを提供する ITypeDescriptorContext。</param>
    ''' <param name="instance">プロパティを取得する対象となる配列の型を指定する Object。</param>
    ''' <param name="filters">フィルターとして使用される、Attribute 型の配列。</param>
    ''' <returns>指定されたデータ型に対して公開されているプロパティを格納している PropertyDescriptorCollection。</returns>
    ''' <remarks></remarks>
    Public Overloads Overrides Function GetProperties(ByVal context As ITypeDescriptorContext, ByVal instance As Object, ByVal filters() As Attribute) As PropertyDescriptorCollection
        Dim collection As New PropertyDescriptorCollection(Nothing)

        Dim properies As PropertyDescriptorCollection = TypeDescriptor.GetProperties(instance, filters, True)
        Dim desc As PropertyDescriptor
        For Each desc In properies
            collection.Add(New CustomPropertyDescriptor(desc))
        Next desc

        Return collection
    End Function

    ''' <summary>
    ''' 指定したコンテキストを使用して、オブジェクトがプロパティをサポートしているかどうかを示す値を返します。
    ''' 2015/03/02 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <param name="context">書式指定コンテキストを提供する ITypeDescriptorContext。</param>
    ''' <returns>このオブジェクトのプロパティを見つけるために GetProperties を呼び出す必要がある場合は true。それ以外の場合は false。</returns>
    ''' <remarks></remarks>
    Public Overloads Overrides Function GetPropertiesSupported(ByVal context As ITypeDescriptorContext) As Boolean
        Return True
    End Function
End Class

﻿''' <summary>
''' ライセンス認証Form
''' </summary>
Public Class LicenceForm

    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As IntPtr,
                                                                            ByVal wMsg As Integer,
                                                                            ByVal wParam As Integer,
                                                                            ByVal lParam As String) As Integer
    Public Declare Function ReleaseCapture Lib "user32" () As Long
    Private Const WM_NCLBUTTONDOWN = &HA1
    Private Const HTCAPTION = 2

    ''' <summary>
    ''' 定義ファイルフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CONF_DIRECTORY = "\conf"

    ''' <summary>
    ''' スタイルシートフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const STYLE_DIRECTORY = "\style"

    ' ワードフォルダ
    ''' <summary>
    ''' テンプレートフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const WORD_DIRECTORY As String = "\template"


    ''' <summary>
    ''' 一時作業用フォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TMP_DIRECTORY As String = "\tmp"

    ''' <summary>
    ''' ログフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const LOG_DIRECTORY As String = "\log"

    ''' <summary>
    ''' ライセンスフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const LIC_DIRECTORY As String = "\lic"


    ''' <summary>
    ''' マニュアルフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MAN_DIRECTORY As String = "\man"

    ''' <summary>
    ''' 設定ファイル
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CONFIG_FILE As String = "\setting.xml"

    ''' <summary>
    ''' ライセンスファイル
    ''' </summary>
    ''' <remarks></remarks>
    Private Const LIC_FILE As String = "\lic.dat"

    ''' <summary>
    ''' GUI起動時に変換結果を保存する一時ファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TEMP_OUT_FILE_NAME As String = "\tempout_{0}"

    ''' <summary>
    ''' GUI起動時に変換結果を保存する一時ファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TEMP_NEW_FILE_NAME As String = "\tempnew_{0}"

    ''' <summary>
    ''' GUI起動時に変換結果を保存する一時ファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const TEMP_OLD_FILE_NAME As String = "\tempold_{0}"

    ''' <summary>
    ''' GUI起動時に変換結果を保存する一時ファイル名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MANUAL_FILE_NAME As String = "\manual.pdf"

    ''' <summary>
    ''' 定義ファイルフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_conf_dir As String = Nothing

    ''' <summary>
    ''' WORDテンプレートフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_word_dir As String = Nothing

    ''' <summary>
    ''' 一次作業フォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_tmp_dir As String = Nothing

    ''' <summary>
    ''' スタイルシートフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_style_dir As String = Nothing

    ''' <summary>
    ''' ログフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_log_dir As String = Nothing

    ''' <summary>
    ''' ライセンスパス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_lic_dir As String = Nothing

    ''' <summary>
    ''' マニュアルフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private m_man_dir As String = Nothing

    ''' <summary>
    ''' 定義ファイルのファイルパス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_conf_fpath As String = Nothing

    ''' <summary>
    ''' コマンド起動時の戻り値
    ''' </summary>
    ''' <remarks></remarks>
    Private Enum RETURN_CODE
        SUCCESS = 0
        LIC_ERROR = 1
        ARGUMENT_ERROR = 2
        FILE_NOT_INPUT = 3
        FILE_EXT_INVALID = 4
        FILE_NOT_FOUND = 5
        SYSTEM_ERROR = 99
    End Enum

    ''' <summary>
    ''' ログオブジェクト
    ''' </summary>
    ''' <remarks></remarks>
    Private m_log As LogWriter = Nothing

    ''' <summary>
    ''' 差分作成オブジェクト
    ''' </summary>
    ''' <remarks></remarks>
    Private m_dfCreator As DFCreator = Nothing

    ''' <summary>
    ''' 設定情報オブジェクト
    ''' </summary>
    ''' <remarks></remarks>
    Private m_setting As Setting = Nothing

    ''' <summary>
    ''' カスタムカラー
    ''' 色選択画面で作成した色を保持する。
    ''' </summary>
    ''' <remarks></remarks>
    Private m_cunstomColor As Integer() = Nothing

    ''' <summary>
    ''' 出力パス
    ''' </summary>
    ''' <remarks></remarks>
    Private m_outputPath As String = Nothing

    ''' <summary>
    ''' コマンドラインモード
    ''' </summary>
    ''' <remarks></remarks>
    Private m_consoleMode As Boolean = False

    Private Const TRIAL_MESSAGE As String = "試用版(あと{0}日)"
    Private Const TRIAL_END_MESSAGE As String = "試用期限切れ"
    Private Const LIC_ERROR_MESSAGE As String = "入力されたライセンスキーに誤りがある、" & vbCrLf & "または既に認証済みのライセンスキーが入力されています。"
    'Private Const LIC_ERROR_MESSAGE As String = "ライセンスキーが違います。"
    Private Const LENGTH_ERROR_MESSAGE As String = "ライセンスキーの文字数が正しくありません。"
    Private Const LIC_TYPE_ERROR_MESSAGE As String = "ライセンスキーに使用できない文字が含まれています。"
    Private Const LIC_SYS_ERROR_MESSAGE As String = "認証処理でシステムエラーが発生しました。" & vbCrLf & "しばらく時間を置いた後、再度実行してください。"

    Private m_licpath As String = Nothing

    Private m_status As Integer = 0

    Public Sub New(ByRef licpath As String)

        ' この呼び出しは、Windows フォーム デザイナで必要です。
        InitializeComponent()

        m_licpath = licpath

        LicCheck()

    End Sub

    Public Function isStatus() As Integer
        Return m_status
    End Function

    Private Sub LicCheck()

        m_status = Common.licCheck()

        If m_status = Common.LIC_ENUM.OK Then
            'LicLabel.Visible = False
            'LicTextBox.Visible = False
            'LicButton.Visible = False
            'TrialLabel.Text = ""
        ElseIf m_status = Common.LIC_ENUM.TRIAL Then
            'TrialLabel.Text = String.Format(TRIAL_MESSAGE, DateDiff("d", Date.Now, Common.licEndDate(), ) + 1)
            TrialLabel.Text = DateDiff("d", Date.Now, Common.licEndDate(), ) + 1
        ElseIf m_status = Common.LIC_ENUM.NG Then
            'TrialLabel.Text = String.Format(TRIAL_END_MESSAGE)
            TrialLabel.Text = "0"
        Else
            'TrialLabel.Text = String.Format(TRIAL_MESSAGE, DateDiff("d", Date.Now, Common.licEndDate(), ) + 1)
            TrialLabel.Text = DateDiff("d", Date.Now, Common.licEndDate(), ) + 1
        End If

    End Sub
    Private Sub NetWorkLicCheck(ByVal licStr As String)

        m_status = Common.licCheckNet(licStr)

        If m_status = Common.LIC_ENUM.OK Then
            'LicLabel.Visible = False
            'LicTextBox1.Visible = False
            'LicTextBox2.Visible = False
            'LicTextBox3.Visible = False
            'LicTextBox4.Visible = False
            'LicButton.Visible = False
            'SepText1.Visible = False
            'SepText2.Visible = False
            'SepText3.Visible = False
            'TrialLabel.Text = ""

        ElseIf m_status = Common.LIC_ENUM.TRIAL Then
            'TrialLabel.Text = String.Format(TRIAL_MESSAGE, DateDiff("d", Date.Now, Common.licEndDate(), ) + 1)
        Else
            'TrialLabel.Text = String.Format(TRIAL_END_MESSAGE)
        End If
    End Sub


    ''' <summary>
    ''' エラーダイアログ表示メソッド
    ''' </summary>
    ''' <param name="msgId">メッセージ</param>
    ''' <param name="msg">メッセージID</param>
    ''' <remarks></remarks>
    Public Sub ShowErrorDialog(ByVal msg As String, ByVal msgId As String)
        MessageBox.Show(msg, msgId, MessageBoxButtons.OK, MessageBoxIcon.Error)
        Dim frm As New MsgBoxEx(msg, msgId)
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()
        If m_log IsNot Nothing Then
            m_log.WriteErrorLog(msgId & " " & msg)
        End If
    End Sub
    ''' <summary>
    ''' 特殊パス\会社名\製品名のパスを返す。
    ''' </summary>
    ''' <param name="folder">特殊パス（列挙体の値）</param>
    ''' <returns>パス</returns>
    ''' <remarks></remarks>
    Private Function GetFileSystemPath(ByRef folder As Environment.SpecialFolder) As String
        Dim path As String = String.Format("{0}\{1}\{2}", Environment.GetFolderPath(folder), Application.CompanyName, Application.ProductName)
        Return path
    End Function

    ''' <summary>
    ''' タイトルバーを非表示したため、Formのマウス移動を実現
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub LicenceForm_MouseDown(sender As Object, e As MouseEventArgs) Handles MyBase.MouseDown
        ReleaseCapture()
        SendMessage(Handle, WM_NCLBUTTONDOWN, HTCAPTION, 0)
    End Sub
    ''' <summary>
    ''' メンバ変数の初期化
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Initialize()
        SyncLock (GetType(Application))

            ' 非表示にしておく。
            Me.Visible = False

            ' 定義ファイルフォルダ(\なし)
            m_conf_dir = GetFileSystemPath(Environment.SpecialFolder.LocalApplicationData) & CONF_DIRECTORY

            ' ワードテンプレートフォルダ(\なし)
            m_word_dir = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) & WORD_DIRECTORY

            ' 一時フォルダ(\なし)
            m_tmp_dir = IO.Path.GetTempPath & Application.ProductName

            ' スタイルシートフォルダ(\なし)
            ' スタイルシートのコピーはインストーラーがやる
            'Dim pg_style_dir As String = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) & STYLE_DIRECTORY
            m_style_dir = GetFileSystemPath(Environment.SpecialFolder.CommonApplicationData) & STYLE_DIRECTORY

            ' ログフォルダ(\なし)
            m_log_dir = GetFileSystemPath(Environment.SpecialFolder.LocalApplicationData) & LOG_DIRECTORY

            ' ライセンスパス(\なし)
            m_lic_dir = GetFileSystemPath(Environment.SpecialFolder.CommonApplicationData) & LIC_DIRECTORY

            ' マニュアルフォルダ(\なし)
            m_man_dir = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) & MAN_DIRECTORY

            m_conf_fpath = m_conf_dir & CONFIG_FILE

            m_tmp_dir = m_tmp_dir & "\" & DateTime.Now.ToString("yyyyMMddHHmmssfffffff")

            Common.MakeDirNotFoundDir(m_tmp_dir)

            Common.MakeDirNotFoundDir(m_log_dir)

            Common.MakeDirNotFoundDir(m_lic_dir)

            Common.MakeDirNotFoundDir(m_conf_dir)

            If Not IO.File.Exists(m_conf_fpath) Then
                IO.File.Copy(IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) & CONFIG_FILE, m_conf_fpath)
            End If

            Dim cmds As String() = System.Environment.GetCommandLineArgs()
        End SyncLock
    End Sub
    ''' <summary>
    ''' Windows Form初期化
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub LicenceForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Initialize()
        'FormBorderStyle = FormBorderStyle.None
        'Panel2.BackColor = Color.FromArgb(200, Color.White)

        'SetStyle(ControlStyles.SupportsTransparentBackColor, True)
        'Me.BackColor = Color.Transparent

        'ライセンスキー入力エリアの背景設定
        SetBG()



    End Sub
    ''' <summary>
    ''' ヘルプボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub HelpBtn_MouseHover(sender As Object, e As EventArgs) Handles HelpBtn.MouseHover
        HelpBtn.Image = My.Resources.licence_help_btn_hover
    End Sub
    ''' <summary>
    ''' ヘルプボタンのマウス離れ
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub HelpBtn_MouseLeave(sender As Object, e As EventArgs) Handles HelpBtn.MouseLeave
        HelpBtn.Image = My.Resources.licence_help_btn
    End Sub
    ''' <summary>
    ''' ヘルプボタンの押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub HelpBtn_Click(sender As Object, e As EventArgs) Handles HelpBtn.Click
        Try
            System.Diagnostics.Process.Start(m_man_dir & MANUAL_FILE_NAME)
        Catch ex As Exception
            ShowErrorDialog(String.Format(Message.GetInstance.GetMessage(Message.ERR_CODE_9992), ex.Message), Message.ERR_CODE_9992)
            If m_log IsNot Nothing Then
                m_log.WriteErrorLog(ex.ToString)
            End If
        Finally
        End Try
    End Sub
    ''' <summary>
    ''' ライセンス認証ボタンクリック
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AuthenticateLicence_Click(sender As Object, e As EventArgs) Handles AuthenticateLicence.Click

        Dim licStr As String = LicenceTextBox1.Text & LicenceTextBox2.Text & LicenceTextBox3.Text & LicenceTextBox4.Text
        Dim regStr As New System.Text.RegularExpressions.Regex("^[a-zA-Z0-9]+$")

        If licStr.Length <> 20 Then
            'MessageBox.Show(LENGTH_ERROR_MESSAGE, "認証エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Dim frm As New MsgBoxEx(LENGTH_ERROR_MESSAGE, "")
            'frm.StartPosition = FormStartPosition.CenterParent
            'frm.ShowDialog()

            Dim dialog As New DialogNoCheckBox(LENGTH_ERROR_MESSAGE, "認証エラー", False, DialogWithCheckBox.MESSAGE_TYPE.ERR)
            dialog.StartPosition = FormStartPosition.CenterParent
            Dim result As DialogResult = dialog.ShowDialog()

            Exit Sub
        End If

        If regStr.IsMatch(licStr) = False Then
            'MessageBox.Show(LIC_TYPE_ERROR_MESSAGE, "認証エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Dim frm As New MsgBoxEx(LIC_TYPE_ERROR_MESSAGE, "")
            'frm.StartPosition = FormStartPosition.CenterParent
            'frm.ShowDialog()
            Dim dialog As New DialogNoCheckBox(LIC_TYPE_ERROR_MESSAGE, "認証エラー", False, DialogWithCheckBox.MESSAGE_TYPE.ERR)
            dialog.StartPosition = FormStartPosition.CenterParent
            Dim result As DialogResult = dialog.ShowDialog()


            Exit Sub
        End If

        NetWorkLicCheck(licStr)
        If m_status <> Common.LIC_ENUM.OK Then
            If m_status <> Common.STS_ENUM.SYS_ERR Then
                'MessageBox.Show(LIC_ERROR_MESSAGE, "認証エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'Dim frm As New MsgBoxEx(LIC_ERROR_MESSAGE, "")
                'frm.StartPosition = FormStartPosition.CenterParent
                'frm.ShowDialog()
                Dim dialog As New DialogNoCheckBox(LIC_ERROR_MESSAGE, "認証エラー", False, DialogWithCheckBox.MESSAGE_TYPE.ERR)
                dialog.StartPosition = FormStartPosition.CenterParent
                Dim result As DialogResult = dialog.ShowDialog()

            Else
                'MessageBox.Show(LIC_SYS_ERROR_MESSAGE, "認証エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'Dim frm As New MsgBoxEx(LIC_SYS_ERROR_MESSAGE, "")
                'frm.StartPosition = FormStartPosition.CenterParent
                'frm.ShowDialog()
                Dim dialog As New DialogNoCheckBox(LIC_SYS_ERROR_MESSAGE, "認証エラー", False, DialogWithCheckBox.MESSAGE_TYPE.ERR)
                dialog.StartPosition = FormStartPosition.CenterParent
                Dim result As DialogResult = dialog.ShowDialog()

            End If
        Else
            Dim frm As DFForm
            frm = CType(Me.Owner, DFForm)

            'MessageBox.Show("認証に成功しました。")
            'Dim frm2 As New MsgBoxEx("認証に成功しました。", "")
            'frm2.StartPosition = FormStartPosition.CenterParent
            'frm2.ShowDialog()

            Dim dialog As New DialogNoCheckBox("認証に成功しました。", "", False, DialogWithCheckBox.MESSAGE_TYPE.INFO)
            dialog.StartPosition = FormStartPosition.CenterParent
            Dim result As DialogResult = dialog.ShowDialog()


            frm.pic_licence.Visible = False
            Me.Close()
        End If

    End Sub
    ''' <summary>
    ''' ライセンス認証ボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AuthenticateLicence_MouseHover(sender As Object, e As EventArgs) Handles AuthenticateLicence.MouseHover
        AuthenticateLicence.Image = My.Resources.licence_auth_btn_hover
    End Sub
    ''' <summary>
    ''' ライセンス認証ボタンのマウス離れ
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AuthenticateLicence_MouseLeave(sender As Object, e As EventArgs) Handles AuthenticateLicence.MouseLeave
        AuthenticateLicence.Image = My.Resources.licence_auth_btn
    End Sub
    ''' <summary>
    ''' 最小化ボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub HidenBtn_MouseHover(sender As Object, e As EventArgs) Handles HidenBtn.MouseHover
        HidenBtn.Image = My.Resources.licence_small_btn_hover
    End Sub
    ''' <summary>
    ''' 最小化ボタンのマウス離れ
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub HidenBtn_MouseLeave(sender As Object, e As EventArgs) Handles HidenBtn.MouseLeave
        HidenBtn.Image = My.Resources.licence_small_btn
    End Sub
    ''' <summary>
    ''' 最小化ボタン押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub HidenBtn_Click(sender As Object, e As EventArgs) Handles HidenBtn.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub
    ''' <summary>
    ''' 終了ボタンのマウスホバー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub CloseBtn_MouseHover(sender As Object, e As EventArgs) Handles CloseBtn.MouseHover
        CloseBtn.Image = My.Resources.licence_exit_btn_hover
    End Sub
    ''' <summary>
    ''' 終了ボタンのマウス離れ
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub CloseBtn_MouseLeave(sender As Object, e As EventArgs) Handles CloseBtn.MouseLeave
        CloseBtn.Image = My.Resources.licence_exit_btn
    End Sub
    ''' <summary>
    ''' 終了ボタン押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub CloseBtn_Click(sender As Object, e As EventArgs) Handles CloseBtn.Click
        'Application.Exit()
        Me.Close()
    End Sub

    Private Sub LicenceTextBox1_TextChanged(sender As Object, e As EventArgs) Handles LicenceTextBox1.TextChanged
        'ライセンスキー入力エリアの背景設定
        SetBG()
        If LicenceTextBox1.Text.Length = 5 Then
            LicenceTextBox2.Focus()
        End If
    End Sub
    Private Sub LicenceTextBox2_TextChanged(sender As Object, e As EventArgs) Handles LicenceTextBox2.TextChanged
        'ライセンスキー入力エリアの背景設定
        SetBG()
        If LicenceTextBox2.Text.Length = 5 Then
            LicenceTextBox3.Focus()
        End If
    End Sub

    Private Sub LicenceTextBox3_TextChanged(sender As Object, e As EventArgs) Handles LicenceTextBox3.TextChanged
        'ライセンスキー入力エリアの背景設定
        SetBG()
        If LicenceTextBox3.Text.Length = 5 Then
            LicenceTextBox4.Focus()
        End If
    End Sub

    Private Sub LicenceTextBox4_TextChanged(sender As Object, e As EventArgs) Handles LicenceTextBox4.TextChanged
        'ライセンスキー入力エリアの背景設定
        SetBG()
    End Sub
    ''' <summary>
    ''' ライセンスキー入力エリアの背景設定
    ''' </summary>
    Private Sub SetBG()
        If LicenceTextBox1.Text.Length > 0 Then
            LicenceBG1.Image = My.Resources.licence_tf
            LicenceTextBox1.BackColor = Color.FromArgb(243, 249, 250)
        Else
            LicenceBG1.Image = My.Resources.licence_tf_active
            LicenceTextBox1.BackColor = Color.White
        End If

        If LicenceTextBox2.Text.Length > 0 Then
            LicenceBG2.Image = My.Resources.licence_tf
            LicenceTextBox2.BackColor = Color.FromArgb(243, 249, 250)
        Else
            LicenceBG2.Image = My.Resources.licence_tf_active
            LicenceTextBox2.BackColor = Color.White
        End If

        If LicenceTextBox3.Text.Length > 0 Then
            LicenceBG3.Image = My.Resources.licence_tf
            LicenceTextBox3.BackColor = Color.FromArgb(243, 249, 250)
        Else
            LicenceBG3.Image = My.Resources.licence_tf_active
            LicenceTextBox3.BackColor = Color.White
        End If

        If LicenceTextBox4.Text.Length > 0 Then
            LicenceBG4.Image = My.Resources.licence_tf
            LicenceTextBox4.BackColor = Color.FromArgb(243, 249, 250)
        Else
            LicenceBG4.Image = My.Resources.licence_tf_active
            LicenceTextBox4.BackColor = Color.White
        End If
    End Sub

    Private Sub LicenceForm_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        LicenceTextBox1.Focus()
    End Sub
End Class
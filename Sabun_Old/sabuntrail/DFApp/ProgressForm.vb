﻿Imports System.Threading
Imports System.ComponentModel

Public Class ProgressForm

    Private Const INTERVAL As Integer = 200

    Private m_dfCreator As DFCreator = Nothing
    Private m_setting As Setting = Nothing
    Private m_newFilePath As String = Nothing
    Private m_oldFilePath As String = Nothing
    Private m_outFilePath As String = Nothing

    Private m_status As Integer = 0
    Private m_exception As Exception = Nothing

    ''' <summary>
    ''' 出力情報
    ''' 2015/03/03 ADD [sabuntrail Version 1.1対応] Kitajima
    ''' </summary>
    ''' <remarks></remarks>
    Private m_outputItems As OutputItems = Nothing

    Public Enum PROGRESS_STATUS
        SUCCESS = 0
        EXCEPTION = 1
        CANCELED = 2
    End Enum

    Public ReadOnly Property status As Integer
        Get
            Return m_status
        End Get
    End Property

    Public ReadOnly Property Exception As Exception
        Get
            Return m_exception
        End Get
    End Property

    ' 2015/03/03 MOD START [sabuntrail Version 1.1対応] Kitajima
    Public Sub New(creator As DFCreator, newFilePath As String, oldFilePath As String, outFilePath As String, settings As Setting, oItems As OutputItems)
        'Public Sub New(creator As DFCreator, newFilePath As String, oldFilePath As String, outFilePath As String, settings As Setting)
        ' 2015/03/03 MOD END [sabuntrail Version 1.1対応] Kitajima

        ' この呼び出しはデザイナーで必要です。
        InitializeComponent()

        ' InitializeComponent() 呼び出しの後で初期化を追加します。
        m_dfCreator = creator
        m_newFilePath = newFilePath
        m_oldFilePath = oldFilePath
        m_outFilePath = outFilePath
        m_setting = settings

        ' 2015/03/03 ADD START [sabuntrail Version 1.1対応] Kitajima
        m_outputItems = oItems
        ' 2015/03/03 ADD END [sabuntrail Version 1.1対応] Kitajima

        BackgroundConvertWorker.RunWorkerAsync()

    End Sub

    Private Sub Run()
        Try
            'm_dfCreator.Create(m_newFilePath, m_oldFilePath, m_outFilePath, m_setting.sabun_only, m_setting.add_and_delete, m_setting.reverse, True)
            ' 2015/03/03 MOD START [sabuntrail Version 1.1対応] Kitajima
            'm_dfCreator.Create(m_newFilePath, m_oldFilePath, m_outFilePath, m_setting)
            m_dfCreator.Create(m_newFilePath, m_oldFilePath, m_outFilePath, m_setting, m_outputItems)
            ' 2015/03/03 MOD END [sabuntrail Version 1.1対応] Kitajima
        Catch tath As ThreadAbortException
            ' キャンセルされた場合は何もしない
        Catch ex As Exception
            m_status = PROGRESS_STATUS.EXCEPTION
            m_exception = ex
        End Try
    End Sub


    Private Sub CancelButton_Click(sender As System.Object, e As System.EventArgs) Handles ProgressCancelButton.Click
        BackgroundConvertWorker.CancelAsync()
    End Sub

    Private Sub BackgroundConvertWorker_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundConvertWorker.DoWork
        Dim th As Thread = Nothing
        Try
            th = New Thread(New ThreadStart(AddressOf Run))
            th.Start()

            While (th.IsAlive)
                Thread.Sleep(INTERVAL)

                If BackgroundConvertWorker.CancellationPending Then
                    th.Abort()
                    th.Join()
                    e.Cancel = True
                    Return
                End If

                BackgroundConvertWorker.ReportProgress(1)
            End While
        Catch ex As Exception
            Try
                If th IsNot Nothing Then
                    th.Abort()
                    th.Join()
                End If
            Finally
            End Try
            m_status = PROGRESS_STATUS.EXCEPTION
            m_exception = ex
        End Try
    End Sub

    Private Sub BackgroundConvertWorker_ProgressChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs) Handles BackgroundConvertWorker.ProgressChanged

        Dim CurrentMaxStatus As Integer

        If m_dfCreator.status < DFCreator.STATUS_CODE.WORD_DIFF Then
            CurrentMaxStatus = DFCreator.STATUS_CODE.WORD_DIFF
        ElseIf m_dfCreator.status < DFCreator.STATUS_CODE.XML_TRASLATING Then
            CurrentMaxStatus = DFCreator.STATUS_CODE.XML_TRASLATING
        ElseIf m_dfCreator.status < DFCreator.STATUS_CODE.XML_TRANSLATED Then
            CurrentMaxStatus = DFCreator.STATUS_CODE.XML_TRANSLATED
        Else
            ConvertProgressBar.Value = m_dfCreator.status
            Return
        End If

        ProgressUp(CurrentMaxStatus)

    End Sub

    Private Sub ProgressUp(ByRef maxStatusCode As Integer)
        If ConvertProgressBar.Value < maxStatusCode Then
            ConvertProgressBar.Value = ConvertProgressBar.Value + 10
        End If
    End Sub



    Private Sub BackgroundConvertWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles BackgroundConvertWorker.RunWorkerCompleted
        If e.Cancelled Then
            m_status = PROGRESS_STATUS.CANCELED
        End If

        Me.Close()
    End Sub


End Class
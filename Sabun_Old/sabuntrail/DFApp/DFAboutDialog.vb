﻿Public Class DFAboutDialog

    Private Const TRIAL_MESSAGE As String = "試用版(あと{0}日)"
    Private Const TRIAL_END_MESSAGE As String = "試用期限切れ"
    Private Const LIC_ERROR_MESSAGE As String = "入力されたライセンスキーに誤りがある、" & vbCrLf & "または既に認証済みのライセンスキーが入力されています。"
    'Private Const LIC_ERROR_MESSAGE As String = "ライセンスキーが違います。"
    Private Const LENGTH_ERROR_MESSAGE As String = "ライセンスキーの文字数が正しくありません。"
    Private Const LIC_TYPE_ERROR_MESSAGE As String = "ライセンスキーに使用できない文字が含まれています。"
    Private Const LIC_SYS_ERROR_MESSAGE As String = "認証処理で" & vbCrLf & "システムエラーが発生しました。" & vbCrLf & "しばらく時間を置いた後、" & vbCrLf & "再度実行してください。"

    Private m_licpath As String = Nothing

    Private m_status As Integer = 0

    Public Sub New(ByRef licpath As String)

        ' この呼び出しは、Windows フォーム デザイナで必要です。
        InitializeComponent()

        ' InitializeComponent() 呼び出しの後で初期化を追加します。
        VersionLabel.Text = "Version：" & Application.ProductVersion

        m_licpath = licpath

        LicCheck()

    End Sub

    Public Function isStatus() As Integer
        Return m_status
    End Function

    Private Sub LicCheck()
        m_status = Common.licCheck()

        If m_status = Common.LIC_ENUM.OK Then
            LicLabel.Visible = False
            'LicTextBox1.Visible = False
            'LicTextBox2.Visible = False
            'LicTextBox3.Visible = False
            'LicTextBox4.Visible = False
            LicButton.Visible = False
            'SepText1.Visible = False
            'SepText2.Visible = False
            'SepText3.Visible = False
            TrialLabel.Text = ""
        ElseIf m_status = Common.LIC_ENUM.TRIAL Then
            TrialLabel.Text = String.Format(TRIAL_MESSAGE, DateDiff("d", Date.Now, Common.licEndDate(), ) + 1)
        ElseIf m_status = Common.LIC_ENUM.NG Then
            TrialLabel.Text = String.Format(TRIAL_END_MESSAGE)
        Else
            TrialLabel.Text = String.Format(TRIAL_MESSAGE, DateDiff("d", Date.Now, Common.licEndDate(), ) + 1)
        End If

    End Sub


    Private Sub NetWorkLicCheck(ByVal licStr As String)

        m_status = Common.licCheckNet(licStr)

        If m_status = Common.LIC_ENUM.OK Then
            LicLabel.Visible = False
            'LicTextBox1.Visible = False
            'LicTextBox2.Visible = False
            'LicTextBox3.Visible = False
            'LicTextBox4.Visible = False
            LicButton.Visible = False
            'SepText1.Visible = False
            'SepText2.Visible = False
            'SepText3.Visible = False
            TrialLabel.Text = ""

        ElseIf m_status = Common.LIC_ENUM.TRIAL Then
            TrialLabel.Text = String.Format(TRIAL_MESSAGE, DateDiff("d", Date.Now, Common.licEndDate(), ) + 1)
        Else
            TrialLabel.Text = String.Format(TRIAL_END_MESSAGE)
        End If
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub LicButton_Click(sender As System.Object, e As System.EventArgs) Handles LicButton.Click

        Dim licStr As String = LicTextBox.Text 'LicTextBox1.Text & LicTextBox2.Text & LicTextBox3.Text & LicTextBox4.Text
        Dim regStr As New System.Text.RegularExpressions.Regex("^[a-zA-Z0-9]+$")

        If licStr.Length <> 20 Then
            MessageBox.Show(LENGTH_ERROR_MESSAGE, "認証エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If regStr.IsMatch(licStr) = False Then
            MessageBox.Show(LIC_TYPE_ERROR_MESSAGE, "認証エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        NetWorkLicCheck(licStr)
        If m_status <> Common.LIC_ENUM.OK Then
            If m_status <> Common.STS_ENUM.SYS_ERR Then
                MessageBox.Show(LIC_ERROR_MESSAGE, "認証エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show(LIC_SYS_ERROR_MESSAGE, "認証エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End If
    End Sub

End Class

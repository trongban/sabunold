﻿Partial Class SabunTrailRibbon
    Inherits Microsoft.Office.Tools.Ribbon.RibbonBase

    <System.Diagnostics.DebuggerNonUserCode()> _
   Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Windows.Forms クラス作成デザイナーのサポートに必要です。
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New(Globals.Factory.GetRibbonFactory())

        'この呼び出しは、コンポーネント デザイナーで必要です。
        InitializeComponent()

    End Sub

    'Component は、コンポーネント一覧に後処理を実行するために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'コンポーネント デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャはコンポーネント デザイナーで必要です。
    'これはコンポーネント デザイナーを使用して変更できます。
    'コード エディターを使用して変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SabunTrail = Me.Factory.CreateRibbonTab
        Me.CompareGroup = Me.Factory.CreateRibbonGroup
        Me.ExecuteButton = Me.Factory.CreateRibbonButton
        Me.ConfigButton = Me.Factory.CreateRibbonButton
        Me.SabunTrail.SuspendLayout
        Me.CompareGroup.SuspendLayout
        '
        'SabunTrail
        '
        Me.SabunTrail.Groups.Add(Me.CompareGroup)
        Me.SabunTrail.Label = "SabunTrail"
        Me.SabunTrail.Name = "SabunTrail"
        '
        'CompareGroup
        '
        Me.CompareGroup.Items.Add(Me.ExecuteButton)
        Me.CompareGroup.Items.Add(Me.ConfigButton)
        Me.CompareGroup.Label = "差分比較"
        Me.CompareGroup.Name = "CompareGroup"
        '
        'ExecuteButton
        '
        Me.ExecuteButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge
        Me.ExecuteButton.Label = "実行"
        Me.ExecuteButton.Name = "ExecuteButton"
        Me.ExecuteButton.OfficeImageId = "ReviewCompareTwoVersions"
        Me.ExecuteButton.ShowImage = true
        '
        'ConfigButton
        '
        Me.ConfigButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge
        Me.ConfigButton.Label = "設定"
        Me.ConfigButton.Name = "ConfigButton"
        Me.ConfigButton.OfficeImageId = "GroupTools"
        Me.ConfigButton.ShowImage = true
        '
        'SabunTrailRibbon
        '
        Me.Name = "SabunTrailRibbon"
        Me.RibbonType = "Microsoft.Word.Document"
        Me.Tabs.Add(Me.SabunTrail)
        Me.SabunTrail.ResumeLayout(false)
        Me.SabunTrail.PerformLayout
        Me.CompareGroup.ResumeLayout(false)
        Me.CompareGroup.PerformLayout

End Sub

    Friend WithEvents SabunTrail As Microsoft.Office.Tools.Ribbon.RibbonTab
    Friend WithEvents CompareGroup As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents ExecuteButton As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents ConfigButton As Microsoft.Office.Tools.Ribbon.RibbonButton
End Class

Partial Class ThisRibbonCollection

    <System.Diagnostics.DebuggerNonUserCode()> _
    Friend ReadOnly Property SabunTrailRibbon() As SabunTrailRibbon
        Get
            Return Me.GetRibbon(Of SabunTrailRibbon)()
        End Get
    End Property
End Class

﻿Imports Microsoft.Office.Tools.Ribbon
Imports System.Windows.Forms

''' <summary>
''' サブントレイルアドインプログラム
''' </summary>
''' <remarks></remarks>
Public Class SabunTrailRibbon

    ''' <summary>
    ''' メッセージ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MESSAGE_001 As String = "sabuntrailの初期化に失敗しました。"

    ''' <summary>
    ''' メッセージ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MESSAGE_002 As String = "比較するためにはファイルを保存する必要があります。"

    ''' <summary>
    ''' メッセージ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MESSAGE_003 As String = "変更が保存されていません。保存してから実行してください。"

    ''' <summary>
    ''' メッセージ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MESSAGE_004 As String = "比較する旧ファイルを指定してください"

    ''' <summary>
    ''' メッセージ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MESSAGE_005 As String = "ファイルが未指定のため、処理を中断します。"

    ''' <summary>
    ''' メッセージ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MESSAGE_006 As String = "出力先を指定してください。"

    ''' <summary>
    ''' メッセージ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MESSAGE_007 As String = "sabuntrailでエラーが発生しました。詳細={0}"

    ''' <summary>
    ''' メッセージ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const MESSAGE_999 As String = "システムエラーが発生しました。詳細={0}"

    ''' <summary>
    ''' インストール時のレジストリキー
    ''' </summary>
    ''' <remarks></remarks>
    Private REGKEY As String = "Software\TOPPANFORMS\SabunTrailAddIn"

    ''' <summary>
    ''' アプリケーションの実行パスのレジストリ値名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const REGLEY_APPLICATION As String = "Application"

    ''' <summary>
    ''' アプリケーションのプロダクト名のレジストリ値名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const REGLEY_PRODUCT As String = "Product"

    ''' <summary>
    ''' 一時ファイル領域
    ''' </summary>
    ''' <remarks></remarks>
    Private m_tmp_dir As String = Nothing

    ''' <summary>
    ''' サブントレイルの実行ファイルパス（レジストリから取得）
    ''' </summary>
    ''' <remarks></remarks>
    Private m_executeFileName As String = Nothing

    ''' <summary>
    ''' 製品名
    ''' </summary>
    ''' <remarks></remarks>
    Private m_productName As String = Nothing

    ''' <summary>
    ''' ロード時の処理
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub SabunTrailRibbon_Load(ByVal sender As System.Object, ByVal e As RibbonUIEventArgs) Handles MyBase.Load
        Dim key As Microsoft.Win32.RegistryKey = Nothing
        Try
            ' key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(REGKEY, False)
            key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(REGKEY, False)

            ' カレントユーザのレジストリにない場合はマシンで探す。
            'If key Is Nothing Then
            '    Microsoft.Win32.Registry.LocalMachine.OpenSubKey(REGKEY, False)
            'End If

            ' 2018/03/28 64Bit環境でWordが起動された場合、Wow6432Nodeのレジストリを読みに行くためエラーとなってしまう対応
            If key Is Nothing Then
                Dim baseKey As Microsoft.Win32.RegistryKey = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry64)
                key = baseKey.OpenSubKey(REGKEY, False)
            End If

            m_executeFileName = DirectCast(key.GetValue(REGLEY_APPLICATION), String)
            m_productName = DirectCast(key.GetValue(REGLEY_PRODUCT), String)

            ' 一時フォルダ
            m_tmp_dir = IO.Path.GetTempPath & m_productName & "\" & DateTime.Now.ToString("yyyyMMddHHmmssfffffff")

            MakeDirNotFoundDir(m_tmp_dir)
        Catch ex As Exception
            MsgBox(MESSAGE_001, MsgBoxStyle.OkOnly, m_productName)
        Finally
            If key IsNot Nothing Then
                Try
                    key.Close()
                Finally
                End Try
                key = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' 実行ボタン押下時の処理
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ExecuteButton_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles ExecuteButton.Click
        Try

            ExecuteButton.Enabled = False
            ConfigButton.Enabled = False

            Dim app = Globals.ThisAddIn.Application

            If app.ActiveDocument.Path Is Nothing OrElse app.ActiveDocument.Path.Length = 0 Then
                '新規作成で未保存の場合
                MsgBox(MESSAGE_002, MsgBoxStyle.OkOnly, m_productName)
                Return
            ElseIf Not app.ActiveDocument.Saved Then
                '新規ではないか未保存の場合
                '新規作成で未保存の場合
                MsgBox(MESSAGE_003, MsgBoxStyle.OkOnly, m_productName)
                Return
            End If

            Dim oldFile As String = OpenFileDialog(MESSAGE_004, False, "docファイル|*.doc|docxファイル|*.docx")
            If oldFile Is Nothing Then
                MsgBox(MESSAGE_005, MsgBoxStyle.OkOnly, m_productName)
                Return
            End If

            Dim outFile As String = SaveFileDialog(MESSAGE_006, True, "docファイル|*.doc|docxファイル|*.docx")

            If outFile Is Nothing Then
                MsgBox(MESSAGE_005, MsgBoxStyle.OkOnly, m_productName)
                Return
            End If

            Dim opt As String = "/C """ & app.ActiveDocument.FullName & """ """ & oldFile & """ """ & outFile & """"

            Dim proc As System.Diagnostics.Process = System.Diagnostics.Process.Start(m_executeFileName, opt)
            proc.WaitForExit()
            If proc.ExitCode = 0 Then
                app.Documents.Open(outFile)
            Else
                MsgBox(String.Format(MESSAGE_007, proc.ExitCode), MsgBoxStyle.OkCancel, m_productName)
            End If
        Catch ex As Exception
            MsgBox(String.Format(MESSAGE_999, ex.ToString), MsgBoxStyle.OkCancel, m_productName)
        Finally
            ExecuteButton.Enabled = True
            ConfigButton.Enabled = True
        End Try
    End Sub


    ''' <summary>
    ''' ファイルダイアログを開く
    ''' </summary>
    ''' <param name="title">タイトル</param>
    ''' <param name="AllowNotFound">存在しないファイルを許容するか</param>
    ''' <param name="filter">拡張子の制限</param>
    ''' <returns>選択されたファイルパス</returns>
    ''' <remarks>例外を上位で処理</remarks>
    Private Function OpenFileDialog(ByVal title As String, ByVal AllowNotFound As Boolean, ByVal filter As String) As String
        ' OpenFileDialog の新しいインスタンスを生成する (デザイナから追加している場合は必要ない)
        Dim OpenFileDialog1 As New OpenFileDialog()

        Dim rtnFName As String = Nothing

        ' ダイアログのタイトルを設定する
        OpenFileDialog1.Title = title

        ' ファイルのフィルタを設定する
        OpenFileDialog1.Filter = filter

        OpenFileDialog1.CheckFileExists = Not (AllowNotFound)

        ' ダイアログを表示し、戻り値が [OK] の場合は、選択したファイルを戻り値とする。
        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            rtnFName = OpenFileDialog1.FileName
        End If

        OpenFileDialog1.Dispose()

        Return rtnFName

    End Function

    ''' <summary>
    ''' 保存ダイアログを開く
    ''' </summary>
    ''' <param name="title">ダイアログのキャプション</param>
    ''' <param name="AllowNotFound">存在しないファイルを許可するかどうか</param>
    ''' <param name="filter">表示する拡張子を絞り込むフィルタ</param>
    ''' <returns>選択されたパス</returns>
    ''' <remarks></remarks>
    Private Function SaveFileDialog(ByVal title As String, ByVal AllowNotFound As Boolean, ByVal filter As String) As String

        Dim rtn As String = Nothing

        ' OpenFileDialog の新しいインスタンスを生成する (デザイナから追加している場合は必要ない)
        Dim SaveFileDialog1 As New SaveFileDialog()

        ' ダイアログのタイトルを設定する
        SaveFileDialog1.Title = title

        ' ファイルのフィルタを設定する
        SaveFileDialog1.Filter = filter

        SaveFileDialog1.CheckFileExists = Not (AllowNotFound)

        SaveFileDialog1.OverwritePrompt = False

        ' ダイアログを表示し、戻り値が [OK] の場合は、選択したファイルパスを返す
        If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
            rtn = SaveFileDialog1.FileName
        End If

        ' 不要になった時点で破棄する
        SaveFileDialog1.Dispose()

        Return rtn

    End Function

    ''' <summary>
    ''' 引数のディレクトリが存在しない場合は作成する。
    ''' </summary>
    ''' <param name="dir">パス</param>
    ''' <remarks>例外は上位で処理</remarks>
    Private Shared Sub MakeDirNotFoundDir(ByRef dir As String)
        If (Not IO.Directory.Exists(dir)) Then
            IO.Directory.CreateDirectory(dir)
        End If
    End Sub

    ''' <summary>
    ''' 設定ボタン押下時の処理
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ConfigButton_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles ConfigButton.Click
        Try
            ExecuteButton.Enabled = False
            ConfigButton.Enabled = False

            Dim proc As System.Diagnostics.Process = System.Diagnostics.Process.Start(m_executeFileName)
            proc.WaitForExit()
        Catch ex As Exception
            MsgBox(String.Format(MESSAGE_999, ex.ToString), MsgBoxStyle.OkCancel, m_productName)
        Finally
            ExecuteButton.Enabled = True
            ConfigButton.Enabled = True
        End Try
    End Sub
End Class

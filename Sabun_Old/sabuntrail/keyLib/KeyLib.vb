﻿Imports System.Text
Imports System.Security.Cryptography

Public Class KeyLib

    Public Shared ADDKEY As String = "liartnubas"

    Public Shared Function Encrypt(ByRef str As String) As String

        Dim byteValue As Byte() = Encoding.UTF8.GetBytes(str)

        Dim crypto As SHA1 = New SHA1CryptoServiceProvider()
        Dim hashValue As Byte() = crypto.ComputeHash(byteValue)

        Dim hashedText As New StringBuilder()
        For i As Integer = 0 To hashValue.Length - 1
            hashedText.AppendFormat("{0:X2}", hashValue(i))
        Next

        Return hashedText.ToString()

    End Function


    Public Shared Function Encrypt256(ByRef str As String) As String

        Dim byteValue As Byte() = Encoding.UTF8.GetBytes(str)

        Dim crypto As SHA256 = New SHA256CryptoServiceProvider()
        Dim hashValue As Byte() = crypto.ComputeHash(byteValue)

        Dim hashedText As New StringBuilder()
        For i As Integer = 0 To hashValue.Length - 1
            hashedText.AppendFormat("{0:X2}", hashValue(i))
        Next

        Return hashedText.ToString()

    End Function

    Public Shared Function GenKey(ByRef str As String) As String
        Return Encrypt(str & ADDKEY)
    End Function

End Class

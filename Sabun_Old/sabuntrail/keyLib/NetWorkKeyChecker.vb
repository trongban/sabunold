﻿Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.IO


Public Class NetWorkKeyChecker

#If DEBUG Then
    '''<summary>
    ''' APIのURL(テスト環境)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const APIURL As String = "http://localhost:52028/Activate/"
    'Private Const APIURL As String = "http://localhost:3000/TFSoftwareActivate/Activate/"
#Else
    '''<summary>
    ''' APIのURL(本番環境)
    ''' </summary>
    ''' <remarks></remarks>
    Private Const APIURL As String = "https://www2.toppan-f.co.jp/activation/Activate/"
#End If


    '''<summary>
    ''' アプリ名
    ''' </summary>
    ''' <remarks></remarks>
    Private Const APINAME As String = "SabunTrail"

    ''' <summary>
    ''' オンライン認証結果
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum STS_ENUM
        OK = 0
        PARM_ERR = 1
        KEY_LEN_ERR = 2
        KEY_TYPE_ERR = 3
        KEY_DISABLE = 4
        SYS_ERR = 9
    End Enum

    ''' <summary>
    ''' 認証エラー時ループ回数
    ''' </summary>
    ''' <remarks></remarks>
    Public Const MaxLoop As Integer = 3

    ''' <summary>
    ''' オンライン認証タイムアウト秒数（ミリ秒）
    ''' </summary>
    ''' <remarks></remarks>
    Public Const TimeoutSec As Integer = 10000

    '''<summary>
    ''' レジストリキー値文字列フルパス
    ''' </summary>
    ''' <remarks></remarks>
    Private Const RegKeyStr As String = "HKEY_LOCAL_MACHINE\SOFTWARE\TOPPANFORMS\SabunTrail"


    '''<summary>
    ''' レジストリ値文字列
    ''' </summary>
    ''' <remarks></remarks>
    Private Const RegSubKeyStr As String = "SOFTWARE\TOPPANFORMS\SabunTrail"

    Public Shared Function NetWorkKeyCheck(ByRef licKey As String) As Integer
        Dim hashKey As String
        Dim actKey As String
        Dim LocalActKey As String
        Dim StsCode As String
        Dim LastSts As Integer
        Dim TimeoutSts As Boolean

        ' 自己署名証明書回避
        ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf OnRemoteCertificateValidationCallback)

        ' ▼ add 2020/05/12 TLS 1.2 対応
        ' TLS 1.2 対応
        ' .NET 4.0 以前では、SecurityProtocolTYpeの中に TLS 1.2 の列挙値が存在しない
        ' そのため、自前で TLS 1.2 の値(3072)を作成する
        Dim sctTLS12 As SecurityProtocolType = DirectCast(3072, SecurityProtocolType)
        ' セキュリティプロトコルに TLS 1.2 を追加
        ServicePointManager.SecurityProtocol = ServicePointManager.SecurityProtocol Or sctTLS12
        ' ▲ add 2020/05/12 TLS 1.2 対応

        ' 暗号化用にMACアドレスを取得する
        Dim macAddress = Nothing
        Dim interfaces = NetworkInterface.GetAllNetworkInterfaces()

        For Each adapter As NetworkInterface In interfaces
            ' 有効なもののみ取得。複数とれる場合は先頭のみ使用する。
            If adapter.OperationalStatus = OperationalStatus.Up Then
                macAddress = adapter.GetPhysicalAddress().ToString
            End If
            ' 先頭の値が取得できた場合脱出
            If macAddress IsNot Nothing Then
                Exit For
            End If
        Next
        hashKey = KeyLib.Encrypt256(licKey & macAddress).Substring(0, 20)

        ' レジストリ書き込み
        My.Computer.Registry.LocalMachine.CreateSubKey(RegSubKeyStr)
        My.Computer.Registry.SetValue(RegKeyStr, "licKey", licKey)
        My.Computer.Registry.SetValue(RegKeyStr, "hashKey", hashKey)

        ' 削除用レジストリオープン
        Dim delReg As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(RegSubKeyStr, True)

        For LpCnt As Integer = 0 To MaxLoop - 1
            TimeoutSts = False
            Dim request As HttpWebRequest = WebRequest.Create(APIURL & APINAME & "/" & licKey & "/" & hashKey)
            ' タイムアウトをミリ秒指定
            request.Timeout = TimeoutSec
            Try
                Using response As HttpWebResponse = request.GetResponse
                    Using responseStream As Stream = response.GetResponseStream
                        Dim STReader As StreamReader = New StreamReader(responseStream)
                        Dim ResJson As String = STReader.ReadToEnd()

                        ' Json.NETを使用せずにデシリアライズ
                        Dim sw = New System.Runtime.Serialization.Json.DataContractJsonSerializer(GetType(TFActivation))
                        Dim deJsonObj As TFActivation
                        Using memStream = New MemoryStream(System.Text.Encoding.UTF8.GetBytes(ResJson))
                            deJsonObj = sw.ReadObject(memStream)
                        End Using
                        StsCode = deJsonObj.errorCode
                        If StsCode <> STS_ENUM.OK Then
                            ' 使用済、もしくは入力コードが正しくない場合はレジストリから削除
                            delReg.DeleteValue("licKey", False)
                            delReg.DeleteValue("hashKey", False)
                            delReg.Close()
                            Return StsCode
                        End If
                        actKey = deJsonObj.activationKey
                        delReg.Close()

                        LocalActKey = KeyLib.Encrypt256(licKey & hashKey).Substring(0, 20)
                        If (StrComp(LocalActKey, actKey) = 0) Then
                            My.Computer.Registry.SetValue(RegKeyStr, "actKey", actKey)
                            Return STS_ENUM.OK
                        Else
                            Return STS_ENUM.SYS_ERR
                        End If

                    End Using
                End Using
            Catch ex As WebException
                If (ex.Status = WebExceptionStatus.Timeout) Then
                    TimeoutSts = True
                End If
                LastSts = STS_ENUM.SYS_ERR
                request.Abort()
            Catch ex As Exception
                LastSts = STS_ENUM.SYS_ERR
                request.Abort()
            End Try
        Next LpCnt

        ' 既定回数リトライを行って正常終了しなかった場合、書き込んだレジストリ値を削除する
        ' タイムアウトの場合削除しない
        If TimeoutSts = False Then
            delReg.DeleteValue("licKey", False)
            delReg.DeleteValue("hashKey", False)
        End If
        delReg.Close()

        Return LastSts

    End Function


    ' 信頼できないSSL証明書を「問題なし」にするメソッド
    Public Shared Function OnRemoteCertificateValidationCallback(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal sslPolicyErrors As SslPolicyErrors) As Boolean
        Return True  ' 「SSL証明書の使用は問題なし」と示す
    End Function

End Class


<System.Runtime.Serialization.DataContract()>
Public Class TFActivation
    <System.Runtime.Serialization.DataMember()>
    Public Property errorCode As String
    <System.Runtime.Serialization.DataMember()>
    Public Property activationKey As String
    <System.Runtime.Serialization.DataMember()>
    Public Property lisenceLimitDate As String
End Class

﻿Imports System.IO
Imports System.Security.AccessControl
Imports System.Security.Cryptography
Imports System.Text

Public Class SabunTrailInstaller

    ''' <summary>
    ''' スタイルシートフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const STYLE_DIRECTORY = "style"

    ''' <summary>
    ''' ライセンスフォルダ
    ''' </summary>
    ''' <remarks></remarks>
    Private Const LIC_DIRECTORY As String = "lic"


    ''' <summary>
    ''' DES暗号化で用いるKey文字列
    ''' 2018/03/07 ADD [オンラインアクティベーション対応] ohta
    ''' </summary>
    ''' <remarks></remarks>
    Private Const DesKey As String = "SabunTrailAuthKy"

    '''<summary>
    ''' DES暗号化で用いるKey文字列
    ''' 2018/03/07 ADD [オンラインアクティベーション対応] ohta
    ''' </summary>
    ''' <remarks></remarks>
    Private Const DesIv As String = "SabunAut"

    '''<summary>
    ''' レジストリキー値文字列
    ''' 2018/03/07 ADD [オンラインアクティベーション対応] ohta
    ''' </summary>
    ''' <remarks></remarks>
    Private Const RegKeyStr As String = "HKEY_LOCAL_MACHINE\SOFTWARE\TOPPANFORMS\SabunTrail"

    Private Const ERRMES As String = "動作に必要なディレクトリの作成に失敗しました。インストールを中止します。詳細コード={0}"


    Public Sub New()
        MyBase.New()

        'この呼び出しは、コンポーネント デザイナーで必要です。
        InitializeComponent()

        'InitializeComponent への呼び出し後、初期化コードを追加します

    End Sub

    Public Overrides Sub Install(ByVal stateSaver As System.Collections.IDictionary)

        MyBase.Install(stateSaver)

        ' ライセンスフォルダとスタイルシートのコピーはインストーラーがやる
        Dim targetdir As String = Me.Context.Parameters("targetDir")
        Dim companyName As String = Me.Context.Parameters("companyName")
        Dim productName As String = Me.Context.Parameters("productName")

        Dim from_style_dir As String = targetdir & STYLE_DIRECTORY
        Dim to_style_dir As String = GetFileSystemPath(Environment.SpecialFolder.CommonApplicationData, companyName, productName) & STYLE_DIRECTORY

        Dim to_lic_dir As String = GetFileSystemPath(Environment.SpecialFolder.CommonApplicationData, companyName, productName) & LIC_DIRECTORY

        Try
            CopyDirectory(from_style_dir, to_style_dir)

            If Not IO.Directory.Exists(to_lic_dir) Then
                IO.Directory.CreateDirectory(to_lic_dir)
            End If

            ' ライセンスファイル作成を削除
            'Dim licFilePath As String = to_lic_dir & "\lic.dat"
            'If Not IO.File.Exists(licFilePath) Then
            '    IO.File.Create(licFilePath)
            '    SetAccessRule(licFilePath)
            'End If

            ' レジストリ作成開始
            Dim regBase As Microsoft.Win32.RegistryKey = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry64)

            Dim regKey = regBase.OpenSubKey("SOFTWARE\TOPPANFORMS\SabunTrail", True)
            If regKey Is Nothing Then
                regKey = regBase.CreateSubKey("SOFTWARE\TOPPANFORMS\SabunTrail")


                ' DES暗号化オブジェクト
                ' お試し日付を暗号化してレジストリに記録
                Dim des As New TripleDESCryptoServiceProvider

                des.Key = Encoding.ASCII.GetBytes(DesKey)
                des.IV = Encoding.ASCII.GetBytes(DesIv)

                ' 現在の日付をお試し期間の起点とする。
                Dim regStartDate = Date.Now
                Dim dataSrc As Byte() = Encoding.Unicode.GetBytes(regStartDate.ToString)
                Dim dataEnc() As Byte

                Dim transformEnc As ICryptoTransform = des.CreateEncryptor

                Dim msEnc As New MemoryStream
                Dim csEnc As New CryptoStream(msEnc, transformEnc, CryptoStreamMode.Write)

                csEnc.Write(dataSrc, 0, dataSrc.Length)
                csEnc.Close()

                dataEnc = msEnc.ToArray()
                msEnc.Close()

                Dim result As New StringBuilder()
                Dim b As Byte
                For Each b In dataEnc
                    result.Append(b.ToString("x2"))
                Next
                Dim regStartDateStr = result.ToString()
                regKey.SetValue("regKey", regStartDateStr)
            End If
            '' お試し期間起点日付設定ここまで
            regKey.Close()
            regBase.Close()

            ' アクセス権を設定しなおす
            Dim regSec As Microsoft.Win32.RegistryKey = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry64)
            Dim regKeynew = regBase.OpenSubKey("SOFTWARE\TOPPANFORMS\SabunTrail", True)
            Dim regAcc = regKeynew.GetAccessControl()

            ' UsersグループやAdministratorグループが全ての環境にあるとは限らないため、Everyone権限とする
            regAcc.AddAccessRule(New System.Security.AccessControl.RegistryAccessRule("Everyone",
                                                                                      RegistryRights.FullControl,
                                                                                      InheritanceFlags.ContainerInherit,
                                                                                      PropagationFlags.None,
                                                                                      AccessControlType.Allow))

            regKeynew.SetAccessControl(regAcc)
            regSec.Close()

        Catch ex As Exception
            Throw New System.Configuration.Install.InstallException(String.Format(ERRMES, ex.ToString))
        End Try

    End Sub

    ''' <summary>
    ''' ディレクトリをコピーする
    ''' </summary>
    ''' <param name="sourceDirName">コピーするディレクトリ</param>
    ''' <param name="destDirName">コピー先のディレクトリ</param>
    Public Shared Sub CopyDirectory( _
            ByVal sourceDirName As String, _
            ByVal destDirName As String)
        'コピー先のディレクトリがないときは作る
        If Not System.IO.Directory.Exists(destDirName) Then
            System.IO.Directory.CreateDirectory(destDirName)
            ''属性もコピー
            System.IO.File.SetAttributes(destDirName, _
                System.IO.File.GetAttributes(sourceDirName))
        End If

        'コピー先のディレクトリ名の末尾に"\"をつける
        If destDirName.Chars((destDirName.Length - 1)) <> _
                System.IO.Path.DirectorySeparatorChar Then
            destDirName = destDirName + System.IO.Path.DirectorySeparatorChar
        End If

        'コピー元のディレクトリにあるファイルをコピー
        Dim fs As String() = System.IO.Directory.GetFiles(sourceDirName)
        Dim f As String
        For Each f In fs
            System.IO.File.Copy(f, _
                destDirName + System.IO.Path.GetFileName(f), True)
            SetAccessRule(destDirName + System.IO.Path.GetFileName(f))
        Next

        'コピー元のディレクトリにあるディレクトリをコピー
        Dim dirs As String() = System.IO.Directory.GetDirectories(sourceDirName)
        Dim dir As String
        For Each dir In dirs
            CopyDirectory(dir, destDirName + System.IO.Path.GetFileName(dir))
        Next
    End Sub

    ''' <summary>
    '''ファイルにEveryoneにフルコントロールのアクセス権を付与する。
    ''' </summary>
    ''' <param name="path"></param>
    ''' <remarks></remarks>
    Private Shared Sub SetAccessRule(path As String)
        Dim FileInfo As New System.IO.FileInfo(path)
        Dim FileSec As FileSecurity
        FileSec = FileInfo.GetAccessControl()

        Dim AccessRule As New FileSystemAccessRule( _
          "Everyone", _
          FileSystemRights.FullControl, _
          AccessControlType.Allow)

        FileSec.AddAccessRule(AccessRule)
        FileInfo.SetAccessControl(FileSec)
    End Sub

    ''' <summary>
    ''' 特殊パス\会社名\製品名のパスを返す。
    ''' </summary>
    ''' <param name="folder">特殊パス（列挙体の値）</param>
    ''' <returns>パス</returns>
    ''' <remarks></remarks>
    Private Shared Function GetFileSystemPath(ByRef folder As Environment.SpecialFolder, ByRef companyName As String, ByRef appName As String) As String
        Dim path As String = String.Format("{0}\{1}\{2}\", Environment.GetFolderPath(folder), companyName, appName)
        Return path
    End Function

End Class
